<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display reviews
     *
     * @return reviews blade
     */
    public function index()
    {        
    }

    /**
     *  Store the specified user reviews in the database
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'title' => 'required',
            'rating' => 'required',
            'comment' => 'required'
        ]);

        $review = new Review;
        $review->user_id = Auth::user()->id;
        $review->product_id = $request->input('product_id');
        $review->rating = $request->input('rating');
        $review->title = $request->input('title');
        $review->comment = $request->input('comment');
        $review->save();

        echo "Review Saved!";
    }
}

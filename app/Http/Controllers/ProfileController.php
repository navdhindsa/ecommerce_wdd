<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\BillingAddress;
use App\ShippingAddress;
use App\ProvinceTax;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display profile page
     *
     * @return profile blade
     */
    public function index()
    {
        $id = Auth::id();
        $user=User::find($id);
        return view('user_profile.user_profile', compact('user'));
        
    }

     /**
     * Display profile edit page
     *
     * @return profile edit blade
     */
    public function edit($id)
    {
        $user=User::find($id);
        return view('user_profile.user_profile_edit', compact('user'));
    }

    /**
     *  Update the specified user profile in the database
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'email' => 'required|email',
            'phone' => array('required','regex:/^(\(?[0-9]{3}\)?)-?\s?([0-9]{3})-?\s?([0-9]{4})$/')
       ]);
        $user = User::find(request('user_id'));
        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->email = request('email');
        $user->phone = request('phone');
        $user->gender = request('gender');
        $user->dob = request('dob');
        $user->bio = request('bio');
        $user->save();
        return view('user_profile.user_profile', compact('user'));
    }

    /**
     *  send address data
     * @return \Illuminate\Http\Response
     */
    public function address(Request $request)
    {   
        if($request->input('type') == 'billing')
            $address = BillingAddress::find($request->input('billing_address_id'));
        else if($request->input('type') == 'shipping')
            $address = ShippingAddress::find($request->input('shipping_address_id'));

        $provinceTaxes = ProvinceTax::all();
        foreach($provinceTaxes as $pr) {
            if($pr->short_code == $address->province) {
                $address->province_id = $pr->id;
                break;
            }
        }

        if($address->user_id == Auth::id()){
            return $address->toArray();
        }
    }

    
}

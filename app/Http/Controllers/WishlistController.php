<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wishlist;
use App\Product;

class WishlistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display wishlist page
     *
     * @return wishlist blade
     */
    public function index()
    {
        $wishlist = Wishlist::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();
    
        $products = Product::whereIn('id', $wishlist)->get();
    
        if(count($products) == 0) {
            return view('cart.empty-wishlist');
        }
        else {
            return view('cart.wishlist', compact('products'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response redirects to the posts page
     */
    public function store(Request $request) {
        //validation using laravel's validator
        $request->validate([
            'id'=>'required',
            'name'=>'required',
        ]);
        $wishlist = Wishlist::firstOrNew(['user_id' => Auth::user()->id, 'product_id' => $request->input('id')]);
        $wishlist->save();
            
        echo "Added " . $request->input('name') ." to wishlist!";
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id)
    {
        $wishlist = Wishlist::where('product_id', $id)
        ->where('user_id', Auth::user()->id);
        $wishlist->delete();
        return redirect('/wishlist')->with('message', 'Wishlist Item Deleted Successfully!');
    }
}

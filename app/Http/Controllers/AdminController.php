<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\Invoice;
use App\Blog;

class AdminController extends Controller
{
    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$total_products = Product::count();
    	$total_blogs = Blog::count();
    	$total_users = User::count();
    	$total_invoices = Invoice::count();

        return view('admin.home', compact('total_products', 'total_blogs', 'total_users', 'total_invoices'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Product;
use App\Shipping;
use App\ProvinceTax;
use App\BillingAddress;
use App\ShippingAddress;
use App\Invoice;
use App\InvoiceItem;
use App\Transaction;
use Pagerange\Bx\_5bx;
use Validator;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display checkout page
     *
     * @return cart blade
     */
    public function index(Request $request)
    {
        if ($request->session()->exists('cart.items')) {
            $carts = $request->session()->get('cart.items', []);
            $carts = array_filter($carts);

            foreach($carts as $product_id) {
                $cart = Cart::firstOrNew(['user_id' => Auth::user()->id, 'product_id' => $product_id]);
                $cart->quantity += 1;
                $cart->save();
            }
        }

        //reset to empty array
        $request->session()->put('cart.items', []);

        $shipping_addresses = [];
        $billing_addresses = [];

        $carts = Cart::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();

        $shipping_addresses = Auth::user()->shipping_address;
        $billing_addresses = Auth::user()->billing_address;

        $products = Product::whereIn('id', $carts)->get();

        $provinceTaxes = ProvinceTax::all();

        $shippings = Shipping::all();
    
        if(count($products) == 0) {
            redirect("/shop");
        }
        else {
            return view('cart.checkout', compact('products', 'provinceTaxes', 'shippings', 'shipping_addresses', 'billing_addresses'));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response redirects to the posts page
     */
    public function store(Request $request)
    {

        //validation using laravel's validator
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'email' => 'required|email',
            'address_1' => 'required',
            'city' => 'required',
            'province' => 'required|not_in:0',
            'postal_code' => array('required','regex:/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/'),
            'shipping_postal_code' => array('nullable','regex:/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/'),
            'shipping_method' => 'required|not_in:0',
            'card_number' => 'required|max:16|min:16',
            'name_on_card' => 'required',
            'card_type' => 'required|not_in:0',
            'month' => 'required|max:2|min:2',
            'year' => 'required|max:2|min:2',
            'cvv' => 'required|max:3|min:3',
        ]);

        if ($validator->fails()) {
            return redirect('/checkout/')
                        ->withErrors($validator)
                        ->withInput();
        }

        //get billing and shipping address ids if available
        $billing_address_id = $request->input('billing_address_id');
        $shipping_address_id = $request->input('shipping_address_id');

        //Get next invoice id
        $invoice = Invoice::orderBy('created_at', 'desc')->first();
        if(!empty($invoice))
            $invoice_id = $invoice->id + 1;
        else
        $invoice_id = 1;

        //TRANSACTION
        $my_login_id = 5664371;
        $my_5bx_key = '0128d38a50cebeb0736570147071c43c';
        $transaction = new _5bx($my_login_id, $my_5bx_key);
        $transaction->amount($request->input('grandTotal'));
        $transaction->card_num($request->input('card_number'));
        $transaction->exp_date ($request->input('month') . $request->input('year'));
        $transaction->cvv($request->input('cvv'));
        $transaction->ref_num($invoice_id);
        $transaction->card_type($request->input('card_type'));
        
        $response = $transaction->authorize_and_capture();
        
        $transaction = new Transaction;
        $transaction->transaction_id = $response->transaction_response->trans_id;
        $transaction->invoice_id = $invoice_id;
        $transaction->response_code = $response->transaction_response->response_code;
        $transaction->auth_code = $response->transaction_response->auth_code;
        $transaction->result_message = $response->result_message;
        $transaction->result_code = $response->result_code;
        $transaction->save();

        if($transaction->response_code == 4) {
            $validator->after(function ($validator) {
                $validator->errors()->add('card_type', 'Invalid credit card type');
            });
            if ($validator->fails()) {
                return redirect('/checkout/')
                            ->withErrors($validator)
                            ->withInput();
            }
        }
        else if($transaction->response_code > 1) {
            $validator->after(function ($validator) {
                $validator->errors()->add('error', 'Invalid Credit Card Information');
            });
            if ($validator->fails()) {
                return redirect('/checkout/')
                            ->withErrors($validator)
                            ->withInput();
            }
        }

        //Save Billing info
        if($billing_address_id == '') {
            $provinceShortCode = ProvinceTax::find($request->input('province'))->short_code;

            $billing_address = new BillingAddress;
            if(Auth::check()) {
                $billing_address->user_id = Auth::user()->id;
            }
            $billing_address->street_1 = $request->input('address_1');
            if($request->input('address_2')) {
                $billing_address->street_2 = $request->input('address_2');
            }
            $billing_address->city = $request->input('city');
            $billing_address->province = $provinceShortCode;
            $billing_address->country = 'Canada';
            $billing_address->postal_code = $request->input('postal_code');
            $billing_address->save();

            $billing_address_id = $billing_address->id;
        }

        //Save Shipping info
        if($shipping_address_id == '') {
            if(
                $request->input('shipping_address_1') && 
                $request->input('shipping_city') && 
                $request->input('shipping_province') && 
                $request->input('shipping_postal_code')
            ) {
                $shippingProvinceShortCode = ProvinceTax::find($request->input('shipping_province'))->short_code;
                $shipping_address = new ShippingAddress;
    
                if(Auth::check()) {
                    $shipping_address->user_id = Auth::user()->id;
                }
                $shipping_address->street_1 = $request->input('shipping_address_1');
                if($request->input('shipping_address_2')) {
                    $shipping_address->street_2 = $request->input('shipping_address_2');
                }
                $shipping_address->city = $request->input('shipping_city');
                $shipping_address->province = $shippingProvinceShortCode;
                $shipping_address->country = 'Canada';
                $shipping_address->postal_code = $request->input('shipping_postal_code');

                $shipping_address->save();
                $shipping_address_id = $shipping_address->id;
            }
        }

        //Save Invoice
        $invoice = new Invoice;
        if(Auth::check()) {
            $invoice->user_id = Auth::user()->id;
        }
        $invoice->subtotal = $request->input('total');
        $invoice->tax_amount = $request->input('tax');
        $invoice->shipping_amount = $request->input('shipping');
        $invoice->total = $request->input('grandTotal');
        $invoice->card_no = substr($request->input('card_number'), -4);
        $invoice->order_status = 'Ordered';
        $invoice->payment_status = 'Success';
        $invoice->shipping_id = $request->input('shipping_method');
        $invoice->billing_address_id = $billing_address_id;
        if($shipping_address_id)
            $invoice->shipping_address_id = $shipping_address_id;
        else   
            $invoice->shipping_address_id = $billing_address_id;
        $invoice->save();

        if(Auth::check()) {
            $carts = Cart::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();
        }
        else {
            if ($request->session()->exists('cart.items')) {
                $cart = $request->session()->get('cart.items', []);
                $carts = array_filter($cart);
            }
        }
    
        $products = Product::whereIn('id', $carts)->get();

        foreach($products as $product) {
            $carts = $request->session()->get('cart.items', []);
            if($carts) {
            $tmp = array_count_values($carts);
            $product->quantity = $tmp[$product->id];
            }
            else {
            $carts = \App\Cart::where('user_id', Auth::user()->id)->where('product_id', $product->id)->first();
            $product->quantity = $carts->quantity;
            }
            $price = ($product->sale_price != '' && $product->sale_price > 0) ? $product->sale_price : $product->price;
            $item_total = $price * $product->quantity;

            $invoiceItem = new InvoiceItem;
            $invoiceItem->invoice_id = $invoice->id;
            $invoiceItem->product_id = $product->id;
            $invoiceItem->quantity = $product->quantity;
            $invoiceItem->price = $item_total;
            $invoiceItem->save();
        }

        
        $carts = $request->session()->put('cart.items', []);
        if(Auth::check()) {
            $carts = Cart::where('user_id', Auth::user()->id);
            $carts->delete();
        }
      
      //return redirect()->back()->with('message', 'Successfully added!');
      return redirect('/checkout/order-confirm')->with('message', 'Successfully');

    }

    /**
    * show order confirm page
    *
    * @return view
    */
   public function orderConfirm(Request $request) {
        return view('cart.order-confirm');
   }
    
    /**
     * calcualte tax
     *
     * @return calculatedTax
     */
    public function calculateTax(Request $request)
    {
        $provinceTax = ProvinceTax::find($request->input('province'));

        echo $tax = $request->input('amount') * ( $provinceTax->rate / 100);
    }

    
    /**
     * calcualte shipping
     *
     * @return calculated shipping
     */
    public function calculateShipping(Request $request)
    {
        $shipping = Shipping::find($request->input('shipping'));

        echo $shipping->rate;
    }
}

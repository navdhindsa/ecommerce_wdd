<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Manufacturer;
use App\Category;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response the list view from the pages/list.blade.php
     */
    public function index()
    {
        $paginate = ['true'];
        $products = Product::paginate(20);
        $manufacturers = Manufacturer::all();

        $filter_manufactuers = [];
        $min_price = 0;
        $max_price = 5000;
        $search = '';

        return view('pages.list', compact('products','paginate', 'manufacturers', 'filter_manufactuers', 'min_price', 'max_price', 'search'));
    }

    /**
     * Display a single resource.
     *
     * @return \Illuminate\Http\Response the single view from the pages/detail.blade.php
     */
    public function single($id)
    {
        $product = Product::findOrFail($id);
        return view('pages.detail', compact('product'));
    }

    /**
     * Display a products of a single category.
     *
     * @return Products for Category
     */
    public function category($category_id)
    {
        $category = Category::findOrFail($category_id);
        $products = $category->products;
        $manufacturers = Manufacturer::all();

        $filter_manufactuers = [];
        $min_price = 0;
        $max_price = 5000;
        $search = '';

        return view('pages.list', compact('products', 'category_id', 'manufacturers', 'filter_manufactuers', 'min_price', 'max_price', 'search'));
    }

    /**
     * Filter & Display a products.
     *
     * @return Products 
     */
    public function filter(Request $request, $category_id = 0)
    {
        $manufacturers = Manufacturer::all();

        $filter_manufactuers = ($request->input('manufacturer')) ? $request->input('manufacturer') : [];
        $min_price = sprintf("%.2f", $request->input('min_price'));
        $max_price = sprintf("%.2f", $request->input('max_price'));

        $search = $request->input('search');

        if($category_id == 0)
            $products = Product::where('name', 'like', '%' . $search . '%')->get();
        else
            $products = Product::where('category_id', '=', $category_id)->where('name', 'like', '%' . $search . '%')->get();

        if(empty($filter_manufactuers)) {
            $products = $products
                                ->where('price', '>=', $min_price)
                                ->where('price', '<=', $max_price);
        }
        else {
            $products = $products
                                ->where('price', '>=', $min_price)
                                ->where('price', '<=', $max_price)
                                ->whereIn('manufacturer_id', $filter_manufactuers);
        }

        return view('pages.list', compact('products', 'manufacturers', 'filter_manufactuers', 'min_price', 'max_price', 'search'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\InvoiceItems;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recents = Product::orderBy('created_at', 'desc')->limit(6)->get();
        return view('pages.home', compact('recents'));
    }
}

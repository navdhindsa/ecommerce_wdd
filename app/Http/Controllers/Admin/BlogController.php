<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Blog;

use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response the list view from the index.blade.php
     */
    public function index()
    {
        //return view('admin.products.add');
        $blogs = Blog::orderBy('updated_at', 'DESC')->paginate(20);
        return view('admin.blog.list', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response the create posts view
     */
    public function create()
    {
        return view('admin.blog.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response redirects to the posts page
     */
    public function store(Request $request)
    {
        //validation using laravel's validator
        $request->validate([
        'title'=>'required',
        'body'=>'required',
        'excerpt' => 'required',
        'thumbnail_image' => 'required|image|mimes:jpg,jpeg,png,gif',
        'featured_image' => 'required|image|mimes:jpg,jpeg,png,gif',
      ]);

      $thumbnail_filename = null;
      $featured_filename = null;

      //Thumbnail Image
      if ($request->hasFile('thumbnail_image')) {
          $thumbnail_image = $request->file('thumbnail_image');
          $thumbnail_extension = $thumbnail_image->getClientOriginalExtension();
          $thumbnail_filename = $thumbnail_image->getFilename().'.'.$thumbnail_extension;
          Storage::disk('blog')->put($thumbnail_filename,  File::get($thumbnail_image));
      }

      //Featured Image
      if ($request->hasFile('featured_image')) {
          $featured_image = $request->file('featured_image');
          $featured_extension = $featured_image->getClientOriginalExtension();
          $featured_filename = $featured_image->getFilename().'.'.$featured_extension;
          Storage::disk('blog')->put($featured_filename,  File::get($featured_image));
      }

      //create a new post in the database using the post model
      $blog = new Blog();
      
      $blog->title = $request->input('title');
      $blog->body = $request->input('body');
      $blog->excerpt = $request->input('excerpt');

      $blog->feature_image = $thumbnail_filename;

      $blog->thumbnail_image = $featured_filename;
      
      $blog->save();
      
      
      //return redirect()->back()->with('message', 'Successfully added!');
      return redirect('/admin/blog')->with('message', 'Successfully added!');
    }

    /**
     * Edit the specified product in the database
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function edit($id)
    {
      $blog = Blog::findOrFail($id);
      return view('admin.blog.edit', compact('blog'));
    }

    /**
     *  Update the specified product in the database
     * @param  int $id 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([
        'title'=>'required',
        'body'=>'required',
        'excerpt' => 'required',
        'thumbnail_image' => 'image|mimes:jpg,jpeg,png,gif',
        'featured_image' => 'image|mimes:jpg,jpeg,png,gif',
      ]);

      $thumbnail_filename = null;
      $featured_filename = null;

      //Thumbnail Image
      if ($request->hasFile('thumbnail_image')) {
          $thumbnail_image = $request->file('thumbnail_image');
          $thumbnail_extension = $thumbnail_image->getClientOriginalExtension();
          $thumbnail_filename = $thumbnail_image->getFilename().'.'.$thumbnail_extension;
          Storage::disk('blog')->put($thumbnail_filename,  File::get($thumbnail_image));
      }

      //Featured Image
      if ($request->hasFile('featured_image')) {
          $featured_image = $request->file('featured_image');
          $featured_extension = $featured_image->getClientOriginalExtension();
          $featured_filename = $featured_image->getFilename().'.'.$featured_extension;
          Storage::disk('blog')->put($featured_filename,  File::get($featured_image));
      }

      $blog = Blog::find(request('id'));

      $blog->title = request('title');
      $blog->body = request('body');
      $blog->excerpt = request('excerpt');

      if($thumbnail_filename != null)
        $blog->feature_image = $thumbnail_filename;

      if($featured_filename != null)
        $blog->thumbnail_image = $featured_filename;
      $blog->save();


      return redirect('/admin/blog')->with('message', 'Blog Successfully Edited!');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $blog = Blog::findOrFail($id);
      $blog->delete();
      return redirect('/admin/blog')->with('message', 'Blog Deleted Successfully!');
    }

}
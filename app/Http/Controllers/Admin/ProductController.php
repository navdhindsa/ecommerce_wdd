<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Product;
use App\ProductOption;
use Carbon\Carbon;
use App\Category;
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response the list view from the index.blade.php
     */
    public function index()
    {
        //return view('admin.products.add');
        $products = Product::orderBy('updated_at', 'DESC')->paginate(20);
        return view('admin.list', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response the create posts view
     */
    public function create()
    {
        return view('admin.products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response redirects to the posts page
     */
    public function store(Request $request)
    {
        $request->validate([        
        'name'=>'required',
        'category_id'=>'required|not_in:0',
        'manufacturer_id'=>'required|not_in:0',
        'price'=>'required|numeric',
        'overview'=>'required',
        'more_information'=>'required',
        'thumbnail_image' => 'required|image|mimes:jpg,jpeg,png,gif',
        
      ]);

      $thumbnail_filename = null;

      //Thumbnail Image
      if ($request->hasFile('thumbnail_image')) {
          $thumbnail_image = $request->file('thumbnail_image');
          $thumbnail_extension = $thumbnail_image->getClientOriginalExtension();
          $thumbnail_filename = $thumbnail_image->getFilename().'.'.$thumbnail_extension;
          Storage::disk('product')->put($thumbnail_filename,  File::get($thumbnail_image));
      }
      
      //create a new post in the database using the post model
      $product = new Product();
      
      $product->name = $request->input('name');
      $product->manufacturer_id = $request->input('manufacturer_id');
      $product->price = $request->input('price');
      $product->sale_price = $request->input('sale_price');
      $product->overview = $request->input('overview');
      $product->category_id=$request->input('category_id');
      $product->more_information = $request->input('more_information');
      $product->thumbnail= $thumbnail_filename;
      $product->save();
      $product_id=$product->id;
      
      $catOptions = Category::find($request->input('category_id'))->options()->get();
      
      foreach($catOptions as $catOption) {
        if($request->input($catOption->id)==null){
            continue;
        }
      
        ProductOption::create(['category_option_id' => $catOption->id,
          'value'=>$request->input($catOption->id), 'product_id'=>$product_id]);
      }
      
      //return redirect()->back()->with('message', 'Successfully added!');
      return redirect('/admin/products')->with('message', 'Successfully added!');
    }

    /**
     * Edit the specified product in the database
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function edit($id)
    {
      $product = Product::findOrFail($id);
      return view('admin.products.edit', compact('product'));
    }

    /**
     *  Update the specified product in the database
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([        
        'name'=>'required',
        'category_id'=>'required|not_in:0',
        'manufacture_id'=>'required|not_in:0',
        'price'=>'required|numeric',
        'overview'=>'required',
        'more_information'=>'required',
        'thumbnail_image' => 'image|mimes:jpg,jpeg,png,gif',
        
      ]);

      $thumbnail_filename = null;

      //Thumbnail Image
      if ($request->hasFile('thumbnail_image')) {
          $thumbnail_image = $request->file('thumbnail_image');
          $thumbnail_extension = $thumbnail_image->getClientOriginalExtension();
          $thumbnail_filename = $thumbnail_image->getFilename().'.'.$thumbnail_extension;
          Storage::disk('product')->put($thumbnail_filename,  File::get($thumbnail_image));
      }

      $product = Product::find(request('product_id'));
      $product->name = request('name');
      $product->manufacturer_id = request('manufacturer_id');
      $product->price = request('price');
      $product->sale_price = request('sale_price');
      $product->overview = request('overview');
      $product->category_id=request('category_id');
      $product->more_information = request('more_information');
      if($thumbnail_filename != null)
        $product->thumbnail= $thumbnail_filename;
      $product->save();

      //Arshdeep haven't applied the edit categories part.

      return redirect('/admin/products')->with('message', 'Product Successfully Edited!');
    }
    
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $product = Product::findOrFail($id);
      $product->delete();
      return redirect('/admin/products')->with('message', 'Product Deleted Successfully!');
    }

}
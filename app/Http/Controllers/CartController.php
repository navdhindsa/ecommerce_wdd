<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Product;

class CartController extends Controller
{
    /**
     * Display cart page
     *
     * @return cart blade
     */
    public function index(Request $request)
    {
        //If the cart has items display them and if user is logged in, save the cart
        if(Auth::check()) {
            if ($request->session()->exists('cart.items')) {
                $carts = $request->session()->get('cart.items', []);
                $carts = array_filter($carts);

                foreach($carts as $product_id) {
                    $cart = Cart::firstOrNew(['user_id' => Auth::user()->id, 'product_id' => $product_id]);
                    $cart->quantity += 1;
                    $cart->save();
                }
            }

            //reset to empty array
            $request->session()->put('cart.items', []);

            $carts = Cart::where('user_id', Auth::user()->id)->pluck('product_id')->toArray();

        }
        else {
            $carts = $request->session()->get('cart.items', []);
            $carts = array_filter($carts);
        }
    
        $products = Product::whereIn('id', $carts)->get();
    
        if(count($products) == 0) {
            return view('cart.empty-cart');
        }
        else {
            return view('cart.cart', compact('products'));
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response redirects to the posts page
     */
    public function store(Request $request) {
        //validation using laravel's validator
        $request->validate([
            'id'=>'required',
            'name'=>'required',
        ]);
        if(Auth::check()) {
            $cart = Cart::firstOrNew(['user_id' => Auth::user()->id, 'product_id' => $request->input('id')]);
            $cart->quantity += 1;
            $cart->save();
            
            echo "Added " . $request->input('name') ." to cart!";
        }
        else {
            //create session if doesnt exist
            if (!$request->session()->exists('cart.items')) {
                $request->session()->put('cart.items', []);
            }

            $request->session()->push('cart.items', $request->input('id'));
            
            echo "Added " . $request->input('name') ." to cart!";
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(Request $request, $id)
    {
        if(Auth::check()) {
            $cart = Cart::where('product_id', $id)
            ->where('user_id', Auth::user()->id);
            $cart->delete();
        }
        else {
            if ($request->session()->exists('cart.items')) {
                $cart = $request->session()->get('cart.items', []);
                $remove = array($id);
                $result = array_diff($cart, $remove); 
                $request->session()->put('cart.items', $result);
            }
        }
        return redirect('/cart')->with('message', 'Cart Item Deleted Successfully!');
    }

    /**
     *  Update the specified user profile in the database
     * @return \Illuminate\Http\Response
     */
    public function updateQuantity(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'increement'  => 'required',
       ]);

       if(Auth::check()) {
           $cart = Cart::firstOrNew(['user_id' => Auth::user()->id, 'product_id' => $request->input('product_id')]);
           $cart->quantity += $request->input('increement');
           $cart->quantity = ($cart->quantity >= 1 ) ? $cart->quantity : 1;
           $cart->save();
           
           echo "Increased/Decreased quantity to " . $cart->quantity .".";
       }
       else {

           if($request->input('increement') == 1)
                $request->session()->push('cart.items', $request->input('product_id'));
           else {
               $carts = $request->session()->get('cart.items', []);
               $pos = array_search($request->input('product_id'), $carts);
               unset($carts[$pos]);
               $request->session()->put('cart.items', $carts);
           }
           
           echo "Increased/Decreased quantity.";
       }
       
    }
}

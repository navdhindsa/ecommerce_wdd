<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    /**
     * Get the products for the manufacture.
     */
    public function products()
    {
        return $this->hasMany('App\Product');
    }
}

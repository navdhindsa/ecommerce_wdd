<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'category_option_id', 'value', 
    ];

    /**
     * Get the product that owns the options.
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * Get the category that owns the options.
     */
    public function categoryoption()
    {
        return $this->belongsTo('App\CategoryOption');
    }
}

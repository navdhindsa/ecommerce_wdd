<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'street_1', 'street_2', 'city', 'province', 'country', 'postal_code' 
    ];

    /**
     * Get the User that owns the address.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
}

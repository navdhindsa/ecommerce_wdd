<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id', 'manufacturer_id', 'price', 'sale_price', 'overview', 'more_information', 'thumbnail', 
    ];

    /**
     * Get the manufacture that owns the product.
     */
    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer');
    }

    /**
     * Get the category that owns the product.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    
    /**
     * Get the product options for the product.
     */
    public function options()
    {
        return $this->hasMany('App\ProductOption');
    }
    
    /**
     * Get the images for the product.
     */
    public function images()
    {
        return $this->hasMany('App\ProudctImage');
    }
    
    /**
     * Get the reviews for the product.
     */
    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}

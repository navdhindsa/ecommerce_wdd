<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'invoice_id', 'auth_code', 'response_code', 'result_code', 'result_message'
    ];

    /**
     * Get the invoice that owns the transaction.
     */
    public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
}

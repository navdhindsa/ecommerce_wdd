<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $table= 'blog';
    protected $fillable = [
        'title', 'excerpt', 'feature_image', 'thumbnail_image', 'body', 'created_at', 'updated_at' 
    ];
}

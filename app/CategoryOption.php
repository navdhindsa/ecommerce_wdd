<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryOption extends Model
{
    /**
     * Get the category that owns the options.
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get the options for this cat option.
     */
    public function productoptions()
    {
        return $this->belongsTo('App\ProductOption');
    }
}

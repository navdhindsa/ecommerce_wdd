<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'subtotal', 'tax_amount', 'shipping_amount', 'card_no', 'total', 'order_status', 'payment_status', 'billing_address_id', 'billing_address_id', 'shipping_address_id', 'shipping_id'
    ];

    /**
     * Get the items for the invoice.
     */
    public function items()
    {
        return $this->hasMany('App\InvoiceItem');
    }

    /**
     * Get the user for the invoice.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

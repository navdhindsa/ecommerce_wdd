<?php

namespace App; 

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\ShippingAddress;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'phone', 'email', 'password','gender','dob','bio',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the shipping_addresses for the blog post.
     */
    public function shipping_address()
    {
        return $this->hasMany('App\ShippingAddress');
    }
    /**
     * Get the Billing_addresses for the blog post.
     */
    public function billing_address()
    {
        return $this->hasMany('App\BillingAddress');
    }
    
    /**
     * Get the reviews for the user.
     */
    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
    
    /**
     * Get the invoices for the user.
     */
    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }
    
}

<?php

use Illuminate\Http\Request;
use App\Category;
use App\Http\Resources\CategoryOptionResource as CategoryOptionResource;
use App\Product;
use App\Http\Resources\ProductResource as ProductResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/categories/{id}/options',function($id){
    
    $options = Category::find($id)->options;
    return new CategoryOptionResource($options);
});

Route::get('/search/products/{query}',function($query){
    
    $products = Product::limit(5)->where('name', 'like', '%' . $query . '%')->get();
    return new ProductResource($products);
});

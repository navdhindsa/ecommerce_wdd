<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

/* All Profile Releated Routes */
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile/edit/{id}', 'ProfileController@edit')->name('profile_edit');
Route::post('/profile/edit/update', 'ProfileController@update')->name('update_profile');
Route::get('/profile/address', 'ShippingController@index')->name('user_address');
Route::get('/profile/address/add', 'ShippingController@create')->name('address');
Route::post('/profile/address/store', 'ShippingController@store')->name('add_address');
Route::post('/user/address/', 'ProfileController@address');
Route::post('/user/card/', 'ProfileController@card');
Route::get('/user/orders', 'OrderController@index')->name('orders');

/* All products or shop routes */
Route::get('/shop', 'ShopController@index')->name('products.index');
Route::get('/products/category/{id}', 'ShopController@category')->name('products.category');
Route::post('/shop', 'ShopController@filter')->name('products.filter');
Route::post('/products/category/{id}', 'ShopController@filter')->name('products.filter');

Route::get('/products/{id}', 'ShopController@single')->name('products.single');

/* All routes releated to review */
Route::post('/review', 'ReviewController@store')->name('review');

/* All Cart Releated Routes */
Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::get('/cart/remove/{id}', 'CartController@remove')->name('cart.remove');
Route::post('/cart/quantity/', 'CartController@updateQuantity')->name('cart.updateQuantity');

/* All Checkout Releated Routes */
Route::get('/checkout', 'CheckoutController@index')->name('checkout');
Route::post('/checkout/store', 'CheckoutController@store')->name('checkout');
Route::post('/checkout/tax', 'CheckoutController@calculateTax')->name('checkout');
Route::post('/checkout/shipping', 'CheckoutController@calculateShipping')->name('checkout');
Route::get('/checkout/order-confirm', 'CheckoutController@orderConfirm')->name('checkout');

/* All Wishlist Releated Routes */
Route::get('/wishlist', 'WishlistController@index')->name('wishlist');
Route::post('/wishlist', 'WishlistController@store')->name('wishlist.store');
Route::get('/wishlist/remove/{id}', 'WishlistController@remove')->name('wishlist.remove');

//temporary route for one generic page
Route::get('/about', function () {
    return view('pages.about');
});

// Temporary Routes
Route::get('/list', function () {
    return view('pages.list');
});
Route::get('/detail', function () {
    return view('pages.detail');
});
Route::get('/adminlogin', function () {
    return view('admin.login');
});
Route::get('/adminhome', function () {
    return view('admin.home');
});
Route::get('/c', function () {
    return view('cart.cart');
});
Route::get('/w', function () {
    return view('cart.wishlist');
});
Route::get('/ew', function () {
    return view('cart.empty-wishlist');
});
Route::get('/ec', function () {
    return view('cart.empty-cart');
});
Route::get('/oc', function () {
    return view('cart.order-confirm');
});
Route::get('/b', function () {
    return view('pages.a_blog');
});
Route::get('/bd', function () {
    return view('pages.a_blogdetail');
});
Route::get('/modal', function () {
    return view('pages.modal');
});

// End of Temporary Routes

// Admin Routes

Route::get('/admin/home', 'AdminController@index')->name('admin')->middleware('admin');

Route::get('/admin/products', 'Admin\ProductController@index')->name('product')->middleware('admin');

Route::get('/admin/blog', 'Admin\BlogController@index')->name('blog')->middleware('admin');



Route::get('/admin/products/add', 'Admin\ProductController@create')->name('add-products');

Route::get('/admin/products/edit/{id}', 'Admin\ProductController@edit')->name('edit_product');

Route::post('/admin/products/update', 'Admin\ProductController@update')->name('update_product');

Route::get('/admin/products/destroy/{id}', 'Admin\ProductController@destroy')->name('delete_product');


Route::get('/admin/users', 'Admin\UserController@index')->name('user')->middleware('admin');
Route::get('/admin/users/{id}', 'Admin\UserController@show')->name('user_detail')->middleware('admin');
Route::get('/admin/users/destroy/{id}', 'Admin\UserController@destroy')->name('delete_user');

Route::get('/admin/blog/add', 'Admin\BlogController@create')->name('add-blog');

Route::post('/admin/blog/store', 'Admin\BlogController@store')->name('add-blog');

Route::post('/admin/blog/image_store', 'Admin\BlogController@storeimage')->name('add-blog-image');

Route::get('/admin/blog/edit/{id}', 'Admin\BlogController@edit')->name('edit_blog');

Route::post('/admin/blog/update', 'Admin\BlogController@update')->name('update_blog');

Route::get('/admin/blog/destroy/{id}', 'Admin\BlogController@destroy')->name('delete_blog');


Route::get('/admin/invoices', 'Admin\InvoiceController@index')->name('invoices')->middleware('admin');
Route::get('/admin/invoices/{id}', 'Admin\InvoiceController@edit')->name('edit_invoice')->middleware('admin');
Route::post('/admin/invoices/update', 'Admin\InvoiceController@update')->name('update_invoice')->middleware('admin');
//blog routes

//to get the list view of a post
Route::get('/blog', 'BlogController@index');
//to get the detailed view of a post
Route::get('/blog/{blog}', 'BlogController@show');

Route::post('/admin/products/store', 'Admin\ProductController@store');

Route::get('/home', 'HomeController@index')->name('home');

//terms and conditions page
Route::get('/terms', function () {
    return view('pages.terms');
});

//privacy policy page
Route::get('/policy', function () {
    return view('pages.policy');
});
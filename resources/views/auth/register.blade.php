@extends('layouts.app')

@section('content')
<section class="section">
    <div class="container">
        <div class="columns">
            <div class="column is-8 is-offset-2">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Register
                        </p>
                    </header>
                    <div class="card-content">
                        {!!Form::open(['url'=>route('register'), 'method'=>'post'])!!}
                        <div class="field">
                            <label class="label">First Name</label>
                            <p class="control">
                                @if($errors->has('first_name'))
                                    {{Form::text('first_name',null, ['class' => 'input is-danger'])}}
                                @else
                                    {{Form::text('first_name',null, ['class' => 'input'])}}
                                @endif
                            </p>
                            @if ($errors->has('email'))
                                <p class="help is-danger">{{ $errors->first('first_name') }}</p>
                            @endif
                        </div>
                        <div class="field">
                            <label class="label">Last Name</label>
                            <p class="control">
                                @if($errors->has('last_name'))
                                    {{Form::text('last_name',null, ['class' => 'input is-danger'])}}
                                @else
                                    {{Form::text('last_name',null, ['class' => 'input'])}}
                                @endif
                            </p>
                            @if ($errors->has('email'))
                                <p class="help is-danger">{{ $errors->first('last_name') }}</p>
                            @endif
                        </div>
                        <div class="field">
                            <label class="label">E-mail Address</label>
                            <p class="control">
                                @if($errors->has('email'))
                                    {{Form::email('email',null, ['class' => 'input is-danger'])}}
                                @else
                                    {{Form::email('email',null, ['class' => 'input'])}}
                                @endif
                            </p>
                            @if ($errors->has('email'))
                                <p class="help is-danger">{{ $errors->first('email') }}</p>
                            @endif
                        </div>
                        <div class="field">
                            <label class="label">Phone Number</label>
                            <p class="control">
                                @if($errors->has('phone'))
                                    {{Form::text('phone',null, ['class' => 'input is-danger'])}}
                                @else
                                    {{Form::text('phone',null, ['class' => 'input'])}}
                                @endif
                            </p>
                            @if ($errors->has('email'))
                                <p class="help is-danger">{{ $errors->first('phone') }}</p>
                            @endif
                        </div>
                        <div class="field">
                            <label class="label">Password</label>
                            <p class="control">
                                @if($errors->has('email'))
                                    {{Form::password('password', ['class' => 'input is-danger'])}}
                                @else
                                    {{Form::password('password', ['class' => 'input'])}}
                                @endif
                            </p>
                            @if ($errors->has('password'))
                                <p class="help is-danger">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                        <div class="field">
                            <label class="label">Confirm Password</label>
                            <p class="control">
                                @if($errors->has('email'))
                                    {{Form::password('password_confirmation', ['class' => 'input is-danger'])}}
                                @else
                                    {{Form::password('password_confirmation', ['class' => 'input'])}}
                                @endif
                            </p>
                            @if ($errors->has('password'))
                                <p class="help is-danger">{{ $errors->first('password') }}</p>
                            @endif
                        </div>
                        
                        <div class="field">
                            <label class="label">
                            {{Form::checkbox('remember', old('remember') ? 'checked' : '')}}
                            Remember me</label>
                        </div>
                        
                        <div class="field">
                            {{Form::submit('Register', ['class' => 'user-register-button'])}}
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
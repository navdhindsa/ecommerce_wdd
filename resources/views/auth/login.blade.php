@extends('layouts.app')

@section('content')
<section class="section">
    <div class="container">
    <div class="columns">
        <div class="column is-8 is-offset-2">
            
            @if(session()->has('message'))
                <div class="notification is-danger">
                    {{ session()->get('message') }}
                </div>
            @endif


            <div class="card"  id="login-form">
                <header class="card-header">
                    <p class="card-header-title">Login to Electra</p>
                </header>
                <div class="card-content">
                    {!!Form::open(['url'=>route('login'), 'method'=>'post'])!!}
                    <div class="field">
                        <label class="label">E-Mail address</label>
                        <p class="control">
                            @if($errors->has('email'))
                                {{Form::email('email',null, ['class' => 'input is-danger'])}}
                            @else
                                {{Form::email('email',null, ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                    <div class="field">
                        <label class="label">Password</label>
                        <p class="control">
                            @if($errors->has('email'))
                            {{Form::password('password', ['class' => 'input is-danger'])}}
                            @else
                            {{Form::password('password', ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                    
                    <div class="field">
                        <label class="label">
                        {{Form::checkbox('remember', old('remember') ? 'checked' : '')}}
                        Remember me</label>
                    </div>
                    
                    <div class="field">
                        {{Form::submit('Submit', ['class' => 'login-button'])}}
                    </div>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
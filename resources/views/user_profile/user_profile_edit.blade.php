@php($slug = "Edit Profile")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<div class="container">
		<section class="hero">
            <h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">USER EDIT PROFILE</h1>
        </section>
		<div class="columns">
		  	<div class="column is-4" style="border-right: 2px solid #d2d2d2; padding-right: 30px">		    
				@include('layouts.partials.profile_sidebar')		    
		  	</div>
		  	<div class="column is-6">
		    	{!!Form::open(['url'=>route('update_profile'), 'method'=>'post'])!!}
		    		<div class="field">
		    			{{Form::hidden('user_id', $user->id)}}
		    		</div>
                    <div class="field">
                        {{Form::label('first_name', 'First Name', ['class' => 'label'])}}
                        <p class="control">
                            @if($errors->has('first_name'))
                                {{Form::text('first_name',old('first_name', $user->first_name), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('first_name',old('first_name', $user->first_name), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('first_name'))
                            <p class="help is-danger">{{ $errors->first('first_name') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        {{Form::label('last_name', 'Last Name', ['class' => 'label'])}}
                        <p class="control">
                            @if($errors->has('last_name'))
                                {{Form::text('last_name',old('last_name', $user->last_name), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('last_name',old('last_name', $user->last_name), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('last_name'))
                            <p class="help is-danger">{{ $errors->first('last_name') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        {{Form::label('email', 'Email Address', ['class' => 'label'])}}
                        <p class="control">
                            @if($errors->has('email'))
                                {{Form::email('email',old('email', $user->email), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::email('email',old('email', $user->email), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('email'))
                            <p class="help is-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        {{Form::label('phone', 'Phone Number', ['class' => 'label'])}}
                        <p class="control">
                            @if($errors->has('phone'))
                                {{Form::text('phone',old('phone', $user->phone), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('phone',old('phone', $user->phone), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('phone'))
                            <p class="help is-danger">{{ $errors->first('phone') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        {{Form::label('gender', 'Gender', ['class' => 'label'])}}
                        <p class="control">
                        	<label class="radio">
                            	{{Form::radio('gender', 'Male')}}Male
                        	</label>
                        </p>
                        <p class="control">
                            {{Form::radio('gender', 'Female')}}Female                         
                        </p>                        
                    </div>

                    <div class="field">
                        {{Form::label('dob', 'Date of Birth', ['class' => 'label'])}}
                        <p class="control">
                            @if($errors->has('dob'))
                                {{Form::date('dob',old('dob', $user->dob), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::date('dob',old('dob', $user->dob), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('dob'))
                            <p class="help is-danger">{{ $errors->first('dob') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        {{Form::label('bio', 'Bio', ['class' => 'label'])}}
                        <p class="control">
                            @if($errors->has('bio'))
                                {{Form::text('bio',old('bio', $user->bio), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('bio',old('bio', $user->bio), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('bio'))
                            <p class="help is-danger">{{ $errors->first('bio') }}</p>
                        @endif
                    </div>

                                        
                    <div class="field">
                        {{Form::submit('Submit', ['class' => 'button is-primary'])}}
                    </div>
                    {!!Form::close()!!}	    	
		  	</div>
		</div>
	</div>

@include('layouts.partials.footer')
@endsection
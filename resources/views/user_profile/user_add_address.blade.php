@php($slug = "User Add Address")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<div class="container">
		<section class="hero">
            <h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">User Add Address</h1>
        </section>
		<div class="columns">
		  <div class="column is-4" style="border-right: 2px solid #d2d2d2; padding-right: 30px">
		    @include('layouts.partials.profile_sidebar')
		  </div>
		  <div class="column">
		    <div class="columns is-8">
		    	<div class="column is-5" style="margin: 20px;">
		    		
		    		{!!Form::open(['url'=>route('add_address'), 'method'=>'post'])!!}
                    <div class="field">
                        <label class="label">Street Address 1</label>
                        <p class="control">
                            @if($errors->has('street_1'))
                                {{Form::text('street_1',old('street_1'), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('street_1',old('street_1'), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('street_1'))
                            <p class="help is-danger">{{ $errors->first('street_1') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">Street Address 2</label>
                        <p class="control">
                            @if($errors->has('street_2'))
                                {{Form::text('street_2',old('street_2'), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('street_2',old('street_2'), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('street_2'))
                            <p class="help is-danger">{{ $errors->first('street_2') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">City</label>
                        <p class="control">
                            @if($errors->has('city'))
                                {{Form::text('city',old('city'), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('city',old('city'), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('city'))
                            <p class="help is-danger">{{ $errors->first('city') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">Province</label>
                        <p class="control">
                            @if($errors->has('province'))
                                {{Form::text('province',old('province'), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('province',old('province'), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('province'))
                            <p class="help is-danger">{{ $errors->first('province') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">Country</label>
                        <p class="control">
                            @if($errors->has('country'))
                                {{Form::text('country',old('country'), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('country',old('country'), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('country'))
                            <p class="help is-danger">{{ $errors->first('country') }}</p>
                        @endif
                    </div>

                    <div class="field">
                        <label class="label">Postal Code</label>
                        <p class="control">
                            @if($errors->has('postal_code'))
                                {{Form::text('postal_code',old('postal_code'), ['class' => 'input is-danger'])}}
                            @else
                                {{Form::text('postal_code',old('postal_code'), ['class' => 'input'])}}
                            @endif
                        </p>
                        @if ($errors->has('postal_code'))
                            <p class="help is-danger">{{ $errors->first('postal_code') }}</p>
                        @endif
                    </div>

                                        
                    <div class="field">
                        {{Form::submit('Submit', ['class' => 'button is-primary'])}}
                    </div>
                    {!!Form::close()!!}

		    	</div>
		    </div>
		  </div>
		</div>
	</div>

@include('layouts.partials.footer')
@endsection
@php($slug = "User Profile")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<div class="container">
		<section class="hero">
            <h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">USER PROFILE</h1>
        </section>
		<div class="columns">
		  <div class="column is-4" style="border-right: 2px solid #d2d2d2; padding-right: 30px">
		  	@include('layouts.partials.profile_sidebar')	    
		  </div>
		  <div class="column">
		    <table class="table is-fullwidth">
	    	<tbody>
	    		<thead>
	    			<td>Field Name</td>
	    			<td>Value</td>
	    		</thead>
	    		<tr>
	    			<td>First Name</td>
	    			<td>{{$user->first_name}}</td>
	    		</tr>
	    		<tr>
	    			<td>Last Name</td>
	    			<td>{{$user->last_name}}</td>
	    		</tr>
	    		<tr>
	    			<td>Email</td>
	    			<td>{{$user->email}}</td>
	    		</tr>
	    		<tr>
	    			<td>Gender</td>
	    			<td>{{$user->gender}}</td>
	    		</tr>
	    		<tr>
	    			<td>Date of Birth</td>
	    			<td>{{$user->dob}}</td>
	    		</tr>
	    		<tr>
	    			<td>Mobile Number</td>
	    			<td>{{$user->phone}}</td> 
	    		</tr>
	    		<tr>
	    			<td>Bio</td>
	    			<td>{{$user->bio}}</td> 
	    		</tr>
	    	</tbody>
	    </table>
	    <a class="button is-danger is-large" href="/profile/edit/{{$user->id}}">EDIT</a>
		  </div>
		</div>
	</div>

@include('layouts.partials.footer')
@endsection
@php($slug = "User Address")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<div class="container">
		<section class="hero">
            <h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">User Address Info.</h1>
        </section>
		
		<div class="columns">
		  <div class="column is-4" style="border-right: 2px solid #d2d2d2; padding-right: 30px">
		    @include('layouts.partials.profile_sidebar')		    
		  </div>
		  <div class="column">
		  	
		    <div class="columns is-8 is-multiline">
		    	@foreach($addresses as $address)
		    	<div class="column is-5" style="margin: 20px; background-color: #ddd">
		    		<p>Your current address is: </p>
		    		<p>{{$address->street_1}}, {{$address->city}}, {{$address->province}} </p>
		    		<p>{{$address->country}}</p>
		    		<p>{{$address->postal_code}}</p>
		    	</div>
		    	@endforeach
		    	<div class="column is-5" style="margin: 20px; background-color: #ddd">
		    		<p><a href="/profile/address/add">ADD NEW ADDRESS</a></p>
		    	</div>  
		    </div>
		  </div>
		</div>
	</div>

@include('layouts.partials.footer')
@endsection
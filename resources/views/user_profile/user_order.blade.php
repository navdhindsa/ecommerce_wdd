@php($slug = "User Profile")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<div class="container">
		<section class="hero">
            <h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">USER ORDERS</h1>
        </section>
		<div class="columns">
		  <div class="column is-4" style="border-right: 2px solid #d2d2d2; padding-right: 30px">
		  	@include('layouts.partials.profile_sidebar')	    
		  </div>
		  <div class="column is-8">

		  	<div class="columns is-multiline">
		  		@foreach($invoices as $invoice)
		  		<div class="column is-12 box" style="margin: 10px;">
		  			<p class="title is-2">Invoice Id: {{ $invoice->id }}</p>
		  			<p class="title is-3">Name: {{$user->first_name}} {{$user->last_name}}</p>
		  			<p class="subtitle is-5">Total Amount: ${{$invoice->total}}</p>
		  			@php($shipping_address = \App\ShippingAddress::find($invoice->shipping_address_id))
		  			@php($billing_address = \App\BillingAddress::find($invoice->billing_address_id))
		  			
		  			<p class="title is-4" style="margin: 20px 0 0 0">Shipping Address:</p>
		  			<p>{{$shipping_address->street_1}}, {{$shipping_address->street_2}}</p>
		  			<p>{{$shipping_address->city}}, {{$shipping_address->province}}</p>
		  			<p>{{$shipping_address->country}}, {{$shipping_address->postal_code}}</p>
	  				<p class="title is-4" style="margin: 20px 0 0 0" >Billing Address:</p>
		  			<p>{{$billing_address->street_1}}, {{$billing_address->street_2}}</p>
		  			<p>{{$billing_address->city}}, {{$billing_address->province}}</p>
		  			<p>{{$billing_address->country}}, {{$billing_address->postal_code}}</p>
			  		
		  		</div>
		  		@endforeach
		  	</div> 
	    
		  </div>
		</div>
	</div>

@include('layouts.partials.footer')
@endsection
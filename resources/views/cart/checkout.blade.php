@php($slug = "Checkout")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" id="special-div">
	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">CHECKOUT</h1>
	</section>
	<div class="columns" id="checkout" style="margin-top: 10px">
	  <div class="column is-8" style="border-right: 2px solid #d2d2d2; padding-right: 30px">
		@if($errors->has('error'))
		<div class="notification is-danger">
			{{ $errors->first('error') }}
		</div>
		@endif
	    
		<form action="/checkout/store" method="post" novalidate>
			@csrf
			<div class="columns">
				<div class="column is-6">
					<div class="field">
						<label class="label" for="full_name">Full Name</label>
						<div class="control">
							<input class="input {{$errors->has('full_name')?'is-danger':''}}" value="{{old('full_name')}}" type="text" id="full_name" name="full_name" placeholder="John Doe" >
							@if ($errors->has('full_name'))
								<p class="help is-danger">{{ $errors->first('full_name') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="email">Email</label>
						<div class="control">
							<input class="input {{$errors->has('email')?'is-danger':''}}" value="{{old('email')}}" type="email" id="email" name="email" placeholder="john@example.com">
							@if ($errors->has('email'))
								<p class="help is-danger">{{ $errors->first('email') }}</p>
							@endif
						</div>
					</div>

					@if(!empty($billing_addresses))
					<div class="field">
						<label class="label" for="billing_address_id">Your Billing Addresses</label>
						<div class="control">
							<div class="select {{$errors->has('billing_address_id')?'is-danger':''}}">
								<select name="billing_address_id" id="billing_address_id" v-model="billing_address_id" v-init:billing_address_id="{{old('billing_address_id', 0)}}">
									<option value="0">Select one or add new</option>
									@foreach($billing_addresses as $billing_address)
									<option value="{{ $billing_address->id }}">{{ $billing_address->street_1 }}</option>
									@endforeach
								</select>
								@if ($errors->has('billing_address_id'))
								<p class="help is-danger">{{ $errors->first('billing_address_id') }}</p>
								@endif
							</div>
						</div>
					</div>
					@endif

					<div class="field">
						<label class="label" for="address_1">Address 1</label>
						<div class="control">
							<input class="input {{$errors->has('address_1')?'is-danger':''}}" value="{{old('address_1')}}" type="text" id="address_1" name="address_1" placeholder="542 Colony St." v-model="address_1">
							@if ($errors->has('address_1'))
								<p class="help is-danger">{{ $errors->first('address_1') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="address_2">Address 2</label>
						<div class="control">
							<input class="input {{$errors->has('address_2')?'is-danger':''}}" value="{{old('address_2')}}" type="text" id="address_2" name="address_2" placeholder="Apt# 321" v-model="address_2">
							@if ($errors->has('address_2'))
								<p class="help is-danger">{{ $errors->first('address_2') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="city">City</label>
						<div class="control">
							<input class="input {{$errors->has('city')?'is-danger':''}}" value="{{old('city')}}"  type="text" id="city" name="city" placeholder="Winnipeg" v-model="city">
							@if ($errors->has('city'))
								<p class="help is-danger">{{ $errors->first('city') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="province">Province</label>
						<div class="control">
							<div class="select {{$errors->has('province')?'is-danger':''}}">
								<select name="province" id="province" v-model="province" v-init:province="{{old('province', 0)}}">
									<option value="0">Select a province</option>
									@foreach($provinceTaxes as $province)
									<option value="{{ $province->id }}">{{ $province->province_name }}</option>
									@endforeach
								</select>
								@if ($errors->has('province'))
								<p class="help is-danger">{{ $errors->first('province') }}</p>
								@endif
							</div>
						</div>
					</div>

					<div class="field">
						<label class="label" for="postal_code">Postal Code</label>
						<div class="control">
							<input class="input {{$errors->has('postal_code')?'is-danger':''}}" value="{{old('postal_code')}}" type="text" id="postal_code" name="postal_code" placeholder="X2X 2X2"  v-model="postal_code">
							@if ($errors->has('postal_code'))
							<p class="help is-danger">{{ $errors->first('postal_code') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="checkbox">
							<input type="checkbox" @if(old('sameadr') || (old('shipping_address_1') == '')) checked @endif value="yes" v-model="sameadr" id="sameadr" name="sameadr" @change="toggleBillingAsShipping()"> Shipping address same as billing
						</label>
					</div>

					<div class="shipping" v-if="sameadr === false">
						@if(!empty($shipping_addresses))
						<div class="field">
							<label class="label" for="shipping_address_id">Your Shipping Addresses</label>
							<div class="control">
								<div class="select {{$errors->has('shipping_address_id')?'is-danger':''}}">
									<select name="shipping_address_id" id="shipping_address_id" v-model="shipping_address_id" v-init:shipping_address_id="{{old('shipping_address_id', 0)}}">
										<option value="0">Select one or add new</option>
										@foreach($shipping_addresses as $shipping_address)
										<option value="{{ $shipping_address->id }}">{{ $shipping_address->street_1 }}</option>
										@endforeach
									</select>
									@if ($errors->has('shipping_address_id'))
									<p class="help is-danger">{{ $errors->first('shipping_address_id') }}</p>
									@endif
								</div>
							</div>
						</div>
						@endif

						<div class="field">
							<label class="label" for="shipping_address_1">Shipping Address 1</label>
							<div class="control">
								<input class="input {{$errors->has('shipping_address_1')?'is-danger':''}}" value="{{old('shipping_address_1')}}" type="text" id="shipping_address_1" name="shipping_address_1" placeholder="542 Colony St." v-model="shipping_address_1">
								@if ($errors->has('shipping_address_1'))
								<p class="help is-danger">{{ $errors->first('shipping_address_1') }}</p>
								@endif
							</div>
						</div>

						<div class="field">
							<label class="label" for="shipping_address_2">Shipping Address 2</label>
							<div class="control">
								<input class="input {{$errors->has('shipping_address_2')?'is-danger':''}}" value="{{old('shipping_address_2')}}" type="text" id="shipping_address_2" name="shipping_address_2" placeholder="Apt# 321" v-model="shipping_address_2">
								@if ($errors->has('shipping_address_2'))
								<p class="help is-danger">{{ $errors->first('shipping_address_2') }}</p>
								@endif
							</div>
						</div>

						<div class="field">
							<label class="label" for="shipping_city">Shipping City</label>
							<div class="control">
								<input class="input {{$errors->has('shipping_city')?'is-danger':''}}" value="{{old('shipping_city')}}" type="text" id="shipping_city" name="shipping_city" placeholder="Winnipeg" v-model="shipping_city">
								@if ($errors->has('shipping_city'))
								<p class="help is-danger">{{ $errors->first('shipping_city') }}</p>
								@endif
							</div>
						</div>

						<div class="field">
							<label class="label" for="shipping_province">Shipping Province</label>
							<div class="control">
								<div class="select {{$errors->has('shipping_province')?'is-danger':''}}">
									<select name="shipping_province" id="shipping_province" v-model="shipping_province" v-init:shipping_province="{{old('shipping_province', 0)}}">
										<option value="0">Select a province</option>
										@foreach($provinceTaxes as $province)
										<option value="{{ $province->id }}">{{ $province->province_name }}</option>
										@endforeach
									</select>
									@if ($errors->has('shipping_province'))
									<p class="help is-danger">{{ $errors->first('shipping_province') }}</p>
									@endif
								</div>
							</div>
						</div>

						<div class="field">
							<label class="label" for="shipping_postal_code">Shipping Postal Code</label>
							<div class="control">
								<input class="input {{$errors->has('shipping_postal_code')?'is-danger':''}}" value="{{old('shipping_postal_code')}}" type="text" id="shipping_postal_code" name="shipping_postal_code" placeholder="X2X 2X2"  v-model="shipping_postal_code">
								@if ($errors->has('shipping_postal_code'))
								<p class="help is-danger">{{ $errors->first('shipping_postal_code') }}</p>
								@endif
							</div>
						</div>
					</div>

				</div>

				<div class="column">
					<div class="field">
						<label class="label" for="shipping_method">Shipping Method</label>
						<div class="control">
							<div class="select {{$errors->has('shipping_method')?'is-danger':''}}">
								<select name="shipping_method" id="shipping_method" v-model="shipping_method" v-init:shipping_method="{{old('shipping_method', 0)}}">
									<option value="0">Select a shipping method</option>
									@foreach($shippings as $shipping)
									<option value="{{ $shipping->id }}">{{ $shipping->company_name }}</option>
									@endforeach
								</select>
								@if ($errors->has('shipping_method'))
								<p class="help is-danger">{{ $errors->first('shipping_method') }}</p>
								@endif
							</div>
						</div>
					</div>
					<div class="field">
						<label class="label" for="card_type">Card Type</label>
						<div class="control">
							<div class="select {{$errors->has('card_type')?'is-danger':''}}">
								<select name="card_type" id="card_type">
									<option value="">Select card type</option>
									<option value="visa" {{ (old('card_type') == 'visa') ? 'selected' : '' }}>Visa</option>
									<option value="mastercard" {{ (old('card_type') == 'mastercard') ? 'selected' : '' }}>Mastercard</option>
									<option value="amex" {{ (old('card_type') == 'amex') ? 'selected' : '' }}>Amex</option>
								</select>
								@if ($errors->has('card_type'))
								<p class="help is-danger">{{ $errors->first('card_type') }}</p>
								@endif
							</div>
						</div>
					</div>

					<div class="field">
						<label class="label" for="card_number">Card Number</label>
						<div class="control">
							<input class="input {{$errors->has('card_number')?'is-danger':''}}" v-model="card_number"  value="{{old('card_number')}}" type="text" id="card_number" name="card_number" placeholder="1111111111111111">
							@if ($errors->has('card_number'))
							<p class="help is-danger">{{ $errors->first('card_number') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="name_on_card">Name on Card</label>
						<div class="control">
							<input class="input {{$errors->has('name_on_card')?'is-danger':''}}" v-model="name_on_card" value="{{old('name_on_card')}}" type="text" id="name_on_card" name="name_on_card" placeholder="John Doe">
							@if ($errors->has('name_on_card'))
							<p class="help is-danger">{{ $errors->first('name_on_card') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="month">Expiry Month</label>
						<div class="control">
							<input class="input {{$errors->has('month')?'is-danger':''}}" v-model="month" value="{{old('month')}}" type="text" id="month" name="month" placeholder="12">
							@if ($errors->has('month'))
							<p class="help is-danger">{{ $errors->first('month') }}</p>
							@endif
						</div>
					</div>

					<div class="fiel`d">
						<label class="label" for="year">Expiry Year</label>
						<div class="control">
							<input class="input {{$errors->has('year')?'is-danger':''}}" v-model="year" value="{{old('year')}}" type="text" id="year" name="year" placeholder="18">
							@if ($errors->has('year'))
							<p class="help is-danger">{{ $errors->first('year') }}</p>
							@endif
						</div>
					</div>

					<div class="field">
						<label class="label" for="cvv">CVV</label>
						<div class="control">
							<input class="input {{$errors->has('cvv')?'is-danger':''}}" v-model="cvv" value="{{old('cvv')}}" type="text" id="cvv" name="cvv" placeholder="321">
						</div>
						@if ($errors->has('cvv'))
						<p class="help is-danger">{{ $errors->first('cvv') }}</p>
						@endif
					</div>

					<div class="field">
						<button class="button is-danger is-large is-fullwidth" type="submit">Order Now</button>
					</div>	
				</div>
			</div>	 
			<input class="input" type="hidden" id="total" name="total" placeholder="total" v-model="total">
			<input class="input" type="hidden" id="tax" name="tax" placeholder="Tax" v-model="tax">
			<input class="input" type="hidden" id="shipping" name="shipping" placeholder="Shipping" v-model="shipping">
			<input class="input" type="hidden" id="grandTotal" name="grandTotal" placeholder="grandTotal" v-model="grandTotal">
		</form>   
	  </div>
	  	<?php $total = 0; ?>
		@foreach($products as $product)
			<?php
			//Get quantity
			$carts = \App\Cart::where('user_id', Auth::user()->id)->where('product_id', $product->id)->first();
			$product->quantity = $carts->quantity;
			$price = ($product->sale_price != '' && $product->sale_price > 0) ? $product->sale_price : $product->price;
			$item_total = $price * $product->quantity;
			$total += $item_total;
			?>
		@endforeach
	  <div class="column" v-init:total="{{$total}}">
	    <p class="title">Summary</p>
	    <table class="table is-fullwidth">
	    	<tbody>
	    		<tr>
	    			<td>Cart Amount</td>
	    			<td>$@{{ total }}</td>
	    		</tr>
	    		<tr>
	    			<td>Shipping Cost</td>
	    			<td>$@{{ shipping }}</td>
	    		</tr>
	    		<tr>
	    			<td>Estimated Tax</td>
	    			<td>$@{{ tax }}</td>
	    		</tr>
	    		<tfoot>
	    			<td>Total</td>
	    			<td>$@{{ grandTotal }}</td>
	    		</tfoot>
	    	</tbody>
	    </table>
	    		
	  </div>
	</div>
</div>

@include('layouts.partials.footer')
@endsection
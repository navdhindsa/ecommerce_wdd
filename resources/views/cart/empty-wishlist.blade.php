@php($slug = "Empty Wishlist")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" id="special-div" style="text-align: center;"">
	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; margin-bottom:20px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">YOUR WISHLIST</h1>
	</section>
	<img src="images\emptycart.svg" alt="" />
	<h2 class="title is-2">Hey, its quite empty in here...</h2>
	<p>add products in your wishlist</p>
	<a href="/shop" class="button">SHOP</a>
	
</div>

@include('layouts.partials.footer')
@endsection
@php($slug = "Wishlist")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" id="special-div">
	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; margin-bottom:20px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">WISHLIST</h1>
	</section>
	<div class="columns is-multiline" style="text-align: center;"  id="product">
			@foreach($products as $product)
			<div class="column is-one-third">
				<a href="/products/{{ $product->id }}"><img src="images/products/{{$product->thumbnail}}" alt=""/></a>
				<p class="subtitle">{{ $product->name }}</p>
				<p><strong>{{ $product->price }}</strong></p>
				<a class="button" @click="addToCart({{ $product }})">ADD TO CART</a>	
        <p><a href="/wishlist/remove/{{ $product->id }}">Remove</a></p>		      
			</div>
			@endforeach
	</div>

	<flash-message v-bind:type="type" v-bind:message="message" v-on:message="message = $event" v-if="message != ''"></flash-message>
</div>

@include('layouts.partials.footer')
@endsection
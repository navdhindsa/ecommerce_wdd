@php($slug = "Cart")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" id="special-div">
	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">CART</h1>
	</section>
	<div class="columns" id="cart" style="margin-top: 10px">
	  <div class="column is-8" style="border-right: 2px solid #d2d2d2; padding-right: 30px">
	  <?php $total = 0; ?>
	  @foreach($products as $product)
	  	<?php
		  //Get quantity
		  $carts = Session::get('cart.items', []);
		  if($carts) {
			$tmp = array_count_values($carts);
			$product->quantity = $tmp[$product->id];
		  }
		  else {
			$carts = \App\Cart::where('user_id', Auth::user()->id)->where('product_id', $product->id)->first();
			$product->quantity = $carts->quantity;
		  }
		  $price = ($product->sale_price != '' && $product->sale_price > 0) ? $product->sale_price : $product->price;
		  $item_total = $price * $product->quantity;
		  $total += $item_total;
		?>
		<cart-product @totalprice="changeTotal" @changeinprice="changeTotal" v-bind:image="'{{ '/images/products/' . $product->thumbnail }}'" v-bind:csrf="csrf" v-bind:id="'{{ $product->id }}'"  v-bind:name="'{{ $product->name }}'" v-bind:manufacturer="'{{ $product->manufacturer->name }}'" v-bind:category="'{{ $product->category->name }}'" v-bind:quantity="'{{ $product->quantity }}'" v-bind:price="'{{ $product->price }}'"></cart-product>
	  @endforeach
	  </div>
	  <div class="column">
	    <p class="title">Summary</p>
	    <table class="table is-fullwidth">
	    	<tbody>
	    		<tr>
	    			<td>Cart Amount</td>
	    			<td>$@{{ tp }}</td>
	    		</tr>
	    		</tfoot>
	    	</tbody>
	    </table>

	    <a class="button is-large is-fullwidth" href="/checkout">Checkout</a>			
	    		
	  </div>
	</div>
</div>

@include('layouts.partials.footer')
@endsection
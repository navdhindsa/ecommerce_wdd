@php($slug = "Empty Cart")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" id="special-div" style="text-align: center;"">
	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">YOUR CART</h1>
	</section>
	<img src="images\emptycart.svg" alt="" />
	<h2 class="title is-2">Hey, its quite empty in here...</h2>
	<p>add products in your cart</p>
	<a href="/shop" class="button">SHOP</a>
	
</div>

@include('layouts.partials.footer')
@endsection
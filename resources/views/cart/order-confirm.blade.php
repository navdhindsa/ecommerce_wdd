@php($slug = "Confirm Order")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" id="special-div" style="text-align: center;"">
	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">ORDER CONFIRMED</h1>
	</section>
	<img src="/images/orderconfirm.svg" alt="" />
	<h2 class="title is-2">Hey, your product will be shipped to you shortly.</h2>
	<p>You will get tracking number after product is shipped.</p>
	<p></p>
	<a href="/shop" class="button">Keep Shopping</a>
	
</div>

@include('layouts.partials.footer')
@endsection
@php($slug = "Page Not found")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<div class="container" style="text-align: center; min-height: 60vh">
	<h1 class="title is-1" style="text-align: center; margin-top: 20px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">Error 404</h1>
	
	<h2 class="title is-2">The page you have requested is not found.</h2>
	<a href="/" class="button is-primary">HOME</a>
	
</div>

@include('layouts.partials.footer')
@endsection
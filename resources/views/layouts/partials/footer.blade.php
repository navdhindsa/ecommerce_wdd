<footer class="footer">
		<div id="footer-background">
			<nav class="level" id="my-footer">
			  <p class="has-text-centered">
			    <a class="level-item link is-info footer-link" href="/policy">Our Policies</a>
			  </p>
			  <p class="has-text-centered">
			    <a class="level-item link is-info footer-link" href="/terms">Terms & Conditions</a>
				</p>
				<p class="has-text-centered">
			    <a class="level-item link is-info footer-link" href="/blog">Blog</a>
				</p>
			  <p class="has-text-centered">
			    <a class="level-item link is-info footer-link" href="/about">About Electra</a>
			  </p>
			</nav>
		</div>
		<div class="content has-text-centered">
	    <p>
	      &#169; Eltra Tech.
	    </p>
	  </div>
	</footer>

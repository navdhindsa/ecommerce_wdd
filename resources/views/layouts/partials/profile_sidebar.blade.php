<aside class="menu" id="profile-sidebar">
	<p class="menu-label">
  		User Profile Options
	</p>
    <ul class="menu-list">
      	<li><a href="/profile">Profile</a></li>
      	<li><a href="/profile/address">Addresses</a></li>
     	<li><a href="/user/orders">Order History</a></li>
    </ul>
</aside> 
<div id="sticky-nav">
	<div class="container is-fluid">
	  <nav class="level ham">
		  <!-- Left side -->
		  <div class="level-left logo-center">
				<div>
					<p>
						<a href="/"><img src="/images/Group2Copy.svg" alt="" style="height: 30px;" /></a>
					</p>
				</div>
			</div>

			@if(!isset($category_id))
			<?php $category_id = 0; ?>
			@endif

			@foreach($categories as $category)
		  <p class="level-item has-text-centered {{ ($category->id == $category_id) ? 'is-active' : '' }}">
		    <a class="link is-info" href="/products/category/{{ $category->id }}">{{ $category->name }}</a>
			</p>
			@endforeach

		  <!-- Right side -->
		  <div class="level-right" id="topHoverbox">
		    <div :class="[ link === 'search' ? 'level-item has-hoverbox is-active' : 'level-item has-hoverbox']">
					<a @click="setLink('search')"><img src="/images/search.svg" alt="icon" style="margin: 0px 10px" /></a>
					<div class="hoverbox" id="topSearch" v-if="link ==='search'" v-cloak>
						<div class="field">
							<div class="control">
								<input class="input" type="text" placeholder="Search product" @keyup="showSuggessions()" v-model="query">
							</div>
						</div>
						<ul class="suggestions">
							<search-items 
								v-for="(option, i) in options"
									v-bind:key="i"
									v-bind:id="option.id"
									v-bind:name="option.name">
							</search-items>
						</ul>
					</div>
				</div>
				<div :class="[ link === 'profile' ? 'level-item has-hoverbox is-active' : 'level-item has-hoverbox']">
					<a @click="setLink('profile')"><img src="/images/LOGINICON.svg" alt="icon" style="margin: 0px 10px" /></a>
					<div class="hoverbox link" v-if="link ==='profile'" v-cloak>
						@if(Auth::check())
							@if(Auth::user()->is_admin)
							<a href="{{ route('admin') }}">{{ __('Admin') }}</a>
							@endif
							<a href="{{ route('profile') }}">{{ __('Profile') }}</a>
							<a href="{{ route('logout') }}"
										onclick="event.preventDefault();
																	document.getElementById('logout-form').submit();">
										{{ __('Logout') }}
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
							</form>
						@else 
						<a href="{{ route('login') }}">{{ __('Login') }}</a>
						<a href="{{ route('register') }}">{{ __('Register') }}</a>
						@endif
					</div>
				</div>
			@if(Auth::check())
			<div class="level-item"><a href="/wishlist"><img src="/images/heart-icon-with-dot.svg" alt="icon" style="margin: 0px 10px" /></a></div>	
			@endif
		    <div class="level-item"><a href="/cart"><img src="/images/Group4.svg" alt="icon" style="margin: 0px 10px" /></a></div>
		    <div class="level-item" style=" margin-right: 0px;"><a href="/blog"><img src="/images/blog-icon.svg" alt="Blog Icon" style="margin: 0px 10px" /></a></div>
			<div class="level-item"><a><img src="/images/CROSSICON.svg" alt="icon" style="margin: 0px 10px; display: none;" /></a></div>    
		    
		  </div>
		</nav>
	</div>
</div>
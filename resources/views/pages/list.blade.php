@php($slug = "List View")

@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<section class="section">
	<form id="filters" method="post" action="" name="filters">
		@csrf
    	<div class="container"  style="margin-top: 100px;">
		  <div class="columns">
		  	<div class="column is-2">
		  		<div class="content">
		        <p class="title is-6">Filters</p>
		        <div class="content">
			      <p class="filter-back">Price Range</p>
			      <label for="min">Min</label><br />
			      <input type="range" name="min_price" min="0" max="5000" step="10" onchange="updateMinInput(this.value);" value="{{ $min_price }}" >
						<span id="minInput" ></span>
						<br />
			      <label for="max">Max</label><br />
			      <input type="range" name="max_price" min="0" max="5000" step="10" onchange="updateMaxInput(this.value);" value="{{ $max_price }}">
						<span id="maxInput" ></span><br />

			      <p class="filter-back">Brand</p>
			      
						@foreach($manufacturers as $manufacturer)
			      <input type="checkbox" name="manufacturer[]" id="{{ $manufacturer->id }}" value="{{ $manufacturer->id }}" {{ (in_array($manufacturer->id, $filter_manufactuers)) ? 'checked' : '' }}/>
			      <label for="{{ $manufacturer->id }}"> {{ $manufacturer->name }}</label><br />
						@endforeach
			    	<br />     
			      <p>
			        <input type="submit" class="button is-danger" value="Find Now" id="send_form"> &nbsp;&nbsp;
			      </p>
			</form>
		        </div>
		      </div>
		  	</div>

		  	<div class="column is-10"  style="text-align: center;">
					<div class="column is-12">
						<div class="field is-grouped">
							<div class="control is-expanded">
								<input type="text" placeholder="Search Products..." class="input is-medium" name="search" value="{{ $search }}" />
							</div>
							<div class="control"><button type="submit" class="button is-danger is-medium"><span class="icon"><img src="/images/search.svg" /></span> <span class="is-hidden-touch">Search</span></button></div>
							<div class="control is-hidden-desktop" style="display: none"><a class="button is-medium is-danger is-outlined"><span class="icon is-small"><img src="/images/search.svg" /></span></a></div>
							
						</div>
					</div>
					<br />
					@if($search != '')
						<h1 class="title is-1" style="text-align: center; margin-top: 20px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">You are looking for {{ $search }}</h1>
		  			@endif
					<div class="columns is-multiline" id="product">
						@if(count($products) < 1 ) 
					    <div class="column is-full">
					    	<div class="hover-style list-item-size">
								<h2 class="title is-2">Hey, its quite empty in here...</h2>
							</div>
						</div>
						@endif
						@foreach($products as $product)
					    <div class="column is-one-third">
					    	<div class="hover-style list-item-size">
					      		<a href="/products/{{ $product->id }}" ><img class="image-size-list" src="/images/products/{{$product->thumbnail}}" alt="" /></a>
					      		<p class="subtitle">{{ $product->name }}</p>
					      		<p>{{ $product->manufacturer->name }}</p>
					      		<p class="extra-list-margin"><span class="myspan">
									<?php $count = count($product->reviews); ?>
									@if($count > 0)
										<?php $total = 0; ?>
									@foreach($product->reviews as $review)
										<?php $total = $total + $review->rating; ?>
									@endforeach
									<?php 
									$avg = $total / $count; 
									$full_star = floor($avg);
									$half_star = (($avg - $full_star) != 0) ? 1 : 0;
									$total_star = $full_star + $half_star;
									?>
								
									@for ($i = 1; $i <= $full_star; $i++)
										<img src="/images/star-full.svg" alt="start-full" width="16" />
									@endfor
									@if($half_star)
										<img src="/images/star-half.svg" alt="start-half" width="16" />
									@endif
										@for ($i = 5; $i > $total_star; $i--)
											<img src="/images/star-empty.svg" alt="start-empty" width="16" />
										@endfor
									@else
										@for ($i = 5; $i > 0; $i--)
											<img src="/images/star-empty.svg" alt="start-full" width="16" />
										@endfor
									@endif
								</span><span><strong>${{ $product->price }}</strong></span></p>
								<a class="button font-thick" @click="addToCart({{ $product }})">ADD TO CART</a>
								@if(Auth::check()) 
								<a class="wishlist-hover" @click="addToWishlist({{ $product }})"><img src="/images/wishlist.svg" alt="" /></a>	
								@endif		      
					    	</div>
					    </div>
						@endforeach
						<flash-message v-bind:type="type" v-bind:message="message" v-on:message="message = $event" v-if="message != ''"></flash-message>
					</div>	
				@if(isset($paginate) )
			  	{{ $products->links('vendor.pagination.bulma') }}
			  	@endif
		  	</div>
		    
		  </div>
		</div>    
		
		
</section>

	<script>
	function updateMinInput(val) {
		document.getElementById('minInput').innerHTML = val; 
	}
	function updateMaxInput(val) {
		document.getElementById('maxInput').innerHTML = val; 
	}
	</script>

@include('layouts.partials.footer')
@endsection
@php($slug = "Detail View")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

<section class="section">
    <div class="container" style="margin-top:130px">
		  <div class="columns is-variable is-8">
		  	<div class="column is-6" style="text-align: center; border: 1px solid #d2d2d2">
		  		<div class="content">
		        <img src="/images/products/{{$product->thumbnail}}" alt="" />		       
		      </div>
		  	</div>

		  	<div class="column is-6" id="product">
		  		<p class="title"> {{ $product->name }} </p>
				  <p><span>
					<?php $count = count($product->reviews); ?>
				  @if($count > 0)
				  	<?php $total = 0; ?>
					@foreach($product->reviews as $review)
						<?php $total = $total + $review->rating; ?>
					@endforeach
					<?php 
					$avg = $total / $count; 
					$full_star = floor($avg);
					$half_star = (($avg - $full_star) != 0) ? 1 : 0;
					$total_star = $full_star + $half_star;
					?>
					
					@for ($i = 1; $i <= $full_star; $i++)
						<img src="/images/star-full.svg" alt="start-full" width="16" />
					@endfor
					@if($half_star)
						<img src="/images/star-half.svg" alt="start-half" width="16" />
					@endif
					@for ($i = 5; $i > $total_star; $i--)
						<img src="/images/star-empty.svg" alt="start-empty" width="16" />
					@endfor
				@else
					@for ($i = 5; $i > 0; $i--)
						<img src="/images/star-empty.svg" alt="start-full" width="16" />
					@endfor
				@endif
				</span>
				@if(Auth::check()) 
				| <span><a class="has-text-black" @click="toggleModal('modal is-active')">write a review</a> </span>
				@endif	
				</p>
				<p style="margin-top: 10px; font-weight: 600;">{{ $product->manufacturer->name }}'s {{ $product->category->name }}</p>
				<p class="price" style="font-size: 44px; color: #eb3352; margin-top: 10px;">${{ $product->price }}</p>
				<p style="width: 80%; margin-top: 20px;">{{ $product->more_information }} </p>
		  		<br />
		  		<a class="button buybutton" @click="addToCart({{ $product }})">ADD TO CART</a>
				@if(Auth::check()) 
				<a @click="addToWishlist({{ $product }})"><img src="/images/wishlist.svg" alt="" style="transform: scale(1.4); margin-top: 6px;"/></a>	
				@endif	
				
				<flash-message v-bind:type="type" v-bind:message="message" v-on:message="message = $event" v-if="message != ''"></flash-message>
					
				<div :class="modal" style="text-align: center;">
				<div class="modal-background" @click="toggleModal('modal')"></div>
				<div class="modal-card">
					
					<section class="modal-card-body">
					<p class="title is-3">Leave a Review</p>
						
						<div class="field">
							<p class="control">
								<ul class="rating">
									<li>
										<input type="radio" name="rating" value="0" checked v-model="rating" /><span class="hide"></span>
										<input type="radio" name="rating" value="1" v-model="rating" /><span></span>
										<input type="radio" name="rating" value="2" v-model="rating" /><span></span>
										<input type="radio" name="rating" value="3" v-model="rating" /><span></span>
										<input type="radio" name="rating" value="4" v-model="rating" /><span></span>
										<input type="radio" name="rating" value="5" v-model="rating" /><span></span>
									</li>
								</ul>
							</p>
							<p class="help is-danger" v-if="rating == 0 && isSubmitted == true">Sorry, you must provide a rating!</p>
						</div>
						
						<div class="field">
							<p class="control">
								<input type="text" name="title" :class="{'input':true, 'is-danger':(title == '' && isSubmitted == true)}" v-model="title" placeholder="Write the title of your review" />
							</p>
							<p class="help is-danger" v-if="title == '' && isSubmitted == true">Sorry, you must provide a title!</p>
						</div>

						<div class="field">
							<p class="control">
								<textarea name="comment" :class="{'input':true, 'textarea':true, 'is-danger':(title == '' && isSubmitted == true)}"  v-model="comment" placeholder="Your thoughts"></textarea>
							</p>
							<p class="help is-danger" v-if="comment == '' && isSubmitted == true">Sorry, you must provide a comment!</p>
						</div>

						
						<div class="field">
							<button class="button is-dark is-fullwidth" @click="saveReview({{ $product }})">SAVE REVIEW</button>
						</div>
					</section>
				</div>
				</div>

		  	</div>
		    
		  </div>
		</div>    
  </section>

  <section id="tabs" class="section">
  	<div class="container">
  		<div class="tabs">
			<ul>
				<li :class="[ tab === 'overview' ? 'is-active' : '']"><a @click="tab='overview'">Overview</a></li>
				<li :class="[ tab === 'specs' ? 'is-active' : '']"><a @click="tab='specs'">Specs</a></li>
				<li :class="[ tab === 'reviews' ? 'is-active' : '']"><a @click="tab='reviews'">Reviews</a></li>
			</ul>
		</div>  

		<div class="content detail-content-width" v-if="tab ==='overview'">
			{{ $product->overview }}
		</div>

		<div class="content" v-if="tab ==='specs'">
			@if(count($product->options) > 0)
				@foreach($product->options as $option)
				<div class="review">
				<?php $co = \App\CategoryOption::find($option->category_option_id); ?>
					<p><b>{{ $co->name }}</b> - {{ $option->value }}</p>
				</div>
				@endforeach
				@else
				<div class="review" style="border-bottom: 0">
					<p>No Specs for this product.</p>
				</div>
				@endif
		</div>

		<div class="content" v-if="tab ==='reviews'">
			@if(count($product->reviews) > 0)
			@foreach($product->reviews as $review)
			<div class="review">
				<h3>{{ $review->title }}</h3>
				@for ($i = 1; $i <= $review->rating; $i++)
					<img src="/images/star-full.svg" alt="start-full" width="20" />
				@endfor
				@for ($i = 5; $i > $review->rating; $i--)
					<img src="/images/star-empty.svg" alt="start-full" width="20" />
				@endfor
				<p>{{ $review->comment }}</p>
				<small>By {{ $review->user->first_name }} {{ $review->user->last_name }}</small>
			</div>
			@endforeach
			@else
			<div class="review" style="border-bottom: 0">
				<p>No reviews yet!</p>
			</div>
			@endif
		</div>				
  	</div>
  	
  </section>
	
	<section>
		<div class="container fluid">
			<p class="title">Related Products</p>
			<hr />
			<div class="columns" style="text-align: center;">
				@foreach($product->category->products->take(4) as $p)
				<?php $c = 0; 
				if($c > 2) break;
				?>
				@if($p->id != $product->id)
				<div class="column is-one-third">
					<a href="/products/{{$p->id}}"><img src="/images/products/{{$p->thumbnail}}" alt="" /></a>
			      <p>{{ $p->name }}</p>
			      <p class="subtitle">{{ $p->price }}</p>
				</div>
				<?php $c++; ?>
				@endif
				@endforeach
			</div>
		</div>
	</section>


@include('layouts.partials.footer')
@endsection
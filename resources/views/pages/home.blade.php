@php($slug = "Electra Home")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<section class="hero">
	  <div class="hero-body big-margin-top">
	    <div class="container">
	      <h1 class="title" >
	        Tech made Simpler
	      </h1>
	      <a class="button" href="/shop">VISIT SHOP</a>

	      <div class="image is-3by1 mobile-resize" style="margin-top: 70px;">

			  <img src="/images/hero_image.png" alt="Hero image" style="height: auto">

			</div>
	    </div>
			
	  </div>


	</section>

	<div class="container">
		<div class="column is-12" id="cta1" style="background-color: #1f384b;">
			<div class="columns">
			  <div class="column is-8">
			    <div class="tile is-child" style="text-align: center;">
				    <img src="/images/iPhone_XS.png" alt="" />
		        </div>
			  </div>
			  <div class="column">
			    <div class="featured-content">
			    	<p class="subtitle" style="color: #fff; padding-bottom: 20px">Offer of the day</p>
		            <p class="title" style="color: #fff; font-size: 36px; padding-bottom: 10px">iPhone XS 256 GB</p>
		            <p class="subtitle" style="color: #fff;">Brilliant. Every Way.</p>
					<div class="content">

						<p><span style="text-decoration: line-through;font-size: 26px;color: #c2c2c2;font-weight: bold;">$1,999</span><span style="font-size: 26px; color: #ffffff;font-weight: bold;">&nbsp;&nbsp;$1,725</span></p>

					</div>
					<a class="button-special-home" href="/products/15">BUY NOW</a>
	        	</div>
			  </div>
			</div>
		</div>
	</div> <!-- end of blue background CTA -->

	<section class="hero">
	  <div class="hero-body">
	    <div class="container">
	      <h1 class="title" >
	        Easy buying guide
	      </h1>
	    </div>		
	  </div>
	</section>
	
	<!-- Tile row of all category icons -->
	<div class="container cat-icons">
		<div class="tile is-ancestor">
		  <div class="tile is-parent">
		    <div class="tile is-child box mybox" style="text-align: center;">
		      <a href="/products/category/1"><img src="/images/noun_TV_158474.svg" alt="icon" /></a>

		    </div>
		  </div>
		  <div class="tile is-parent">
		    <div class="tile is-child box mybox" style="text-align: center;">
				<a href="/products/category/2"><img src="/images/noun_Laptop_1833506.svg" alt="icon" /></a>

		    </div>
		  </div>
		  <div class="tile is-parent">
		    <div class="tile is-child box mybox" style="text-align: center;">

				<a href="/products/category/3"><img src="/images/noun_Phone_1436324.svg" alt="icon" /></a>

		    </div>
		  </div>
		  <div class="tile is-parent">
		    <div class="tile is-child box mybox" style="text-align: center;">

				<a href="/products/category/4"><img src="/images/noun_Tablet_417529.svg" alt="icon"/></a>

		    </div>
		  </div>
		  <div class="tile is-parent">
		    <div class="tile is-child box mybox" style="text-align: center;">
				<a href="/products/category/5"><img src="/images/noun_Camera_1569869.svg" alt="icon" /></a>

		    </div>
		  </div>
		  <div class="tile is-parent">
		    <div class="tile is-child box mybox" style="text-align: center;">

				<a href="/products/category/6"><img src="/images/noun_Speaker_569708.svg" alt="icon" /></a>

		    </div>
		  </div>
		</div>
	</div>
	<!-- Tiles for all category ends. -->

	<!-- Todays deals coding begins -->
	<section class="hero">
	  <div class="hero-body">
	    <div class="container">
	      <h1 class="title" >
	        Recent Products
	      </h1>
	    </div>		
	  </div>
	</section>
	
	<div class="container">	
		<div class="column is-12"  style="text-align: center;">
	  		<div class="columns is-multiline">
					@foreach($recents as $product)
		  		<div class="product-column is-one-third">
						<div class="column-width-style hover-style">
							<img src="/images/products/{{ $product->thumbnail }}" alt="img">
							<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
							<p class="product-title"> {{ $product->name }}</p>
							<p class="subtitle">${{ $product->price }}</p>
						</div>
					</div>
					@endforeach
	  	</div>
	</div>

	<div class="container">
		<div class="column is-12" style="background-color: #eb3352;margin: 60px 0 60px 0;">
			<div class="columns subscribe-padding">
				<div class="column is-7">
					<div>        
						<div class="field has-addons" style="justify-content: flex-start;">
							<p class="title subscribe-title" style="color: #fff;font-weight: 400;">Visit <img src="/images/intextlogo.svg" alt="" style="margin-left: 5px;margin-right: 5px;"/> Blog for special Offers</p>
					    </div>         
			      	</div>
				</div>
				<div class="column">
					<div>        
						<div class="field has-addons">
					        
					        <p class="control" style="text-align: center; width: 100%;"><a class="button" id="subscribe-button" href="/blog">Go to Blog</a></p>
				      </div>         
      				</div>
				</div>
			</div>
		</div>
	</div>
		
	
	<!--section class="hero">
	  <div class="hero-body">
	    <div class="container">
	      <h1 class="title" >
	        Trending Products
	      </h1>
	    </div>		
	  </div>
	</section>

	<div class="container">	
		<div class="column is-12"  style="text-align: center;">
	  		<div class="columns is-multiline">
		  		<div class="product-column is-one-third">
					<div class="column-width-style hover-style">
						<img src="/images/products/cell6.jpg" alt="img">
						<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
						<p class="product-title">Title of the product</p>
						<p class="subtitle">$550</p>
					</div>
				</div>
			    <div class="product-column is-one-third">
					<div class="column-width-style hover-style">
						<img src="/images/products/cell6.jpg" alt="img">
						<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
						<p class="product-title">Title of the product</p>
						<p class="subtitle">$550</p>
					</div>
				</div>
			    <div class="product-column is-one-third">
					<div class="column-width-style hover-style">
						<img src="/images/products/cell6.jpg" alt="img">
						<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
						<p class="product-title">Title of the product</p>
						<p class="subtitle">$550</p>
					</div>
				</div>
			    <div class="product-column is-one-third">
					<div class="column-width-style hover-style">
						<img src="/images/products/cell6.jpg" alt="img">
						<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
						<p class="product-title">Title of the product</p>
						<p class="subtitle">$550</p>
					</div>
				</div>
			    <div class="product-column is-one-third">
					<div class="column-width-style hover-style">
						<img src="/images/products/cell6.jpg" alt="img">
						<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
						<p class="product-title">Title of the product</p>
						<p class="subtitle">$550</p>
					</div>
				</div>
				<div class="product-column is-one-third">
					<div class="column-width-style hover-style">
						<img src="/images/products/cell6.jpg" alt="img">
						<p class="subtitle" style="color: red;padding-top: 5px;">new</p>
						<p class="product-title">Title of the product</p>
						<p class="subtitle">$550</p>
					</div>
				</div>
		  	</div>
	  	</div>
	</div-->

	@include('layouts.partials.footer')
@endsection
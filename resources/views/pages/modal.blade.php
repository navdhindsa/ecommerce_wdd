@php($slug = "User Profile")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<div class="container">
		<div class="modal is-active" style="text-align: center;">
	  <div class="modal-background"></div>
	  <div class="modal-card">
	    
	    <section class="modal-card-body">
	    <p class="title is-3">Leave a Review</p>

			<div>
				<img src="/images/star-full.svg" alt="star" />
				<img src="/images/star-full.svg" alt="star" />
				<img src="/images/star-half.svg" alt="star" />
				<img src="/images/star-empty.svg" alt="star" />
				<img src="/images/star-empty.svg" alt="star" />

			</div>

	      {!!Form::open(['url'=>route('update_profile'), 'method'=>'post'])!!}
    		
            <div class="field">
                {{Form::label('title', 'Title of the Review', ['class' => 'label'])}}
                <p class="control">
                    @if($errors->has('title'))
                        {{Form::text('title',null, ['class' => 'input is-danger')}}
                    @else
                        {{Form::text('title',null, ['class' => 'input', 'placeholder' => 'Title of the Review'])}}
                    @endif
                </p>
                @if ($errors->has('title'))
                    <p class="help is-danger">{{ $errors->first('title') }}</p>
                @endif
            </div>

            <div class="field">
                {{Form::label('body', 'Explain', ['class' => 'label'])}}
                <p class="control">
                    @if($errors->has('body'))
                        {{Form::textarea('body',null, ['id' => 'body', 'rows' => 8, 'cols' => 4, 'class' => 'input is-danger'])}}
                    @else
                        {{Form::textarea('body',null, ['id' => 'body', 'rows' => 8, 'cols' => 4, 'class' => 'input'])}}
                    @endif
                </p>
                @if ($errors->has('body'))
                    <p class="help is-danger">{{ $errors->first('body') }}</p>
                @endif
            </div>

               
            <div class="field">
                {{Form::submit('DONE', ['class' => 'button is-dark is-fullwidth'])}}
            </div>
            {!!Form::close()!!}
	    </section>
	  </div>
	</div>
	</div>

@include('layouts.partials.footer')
@endsection
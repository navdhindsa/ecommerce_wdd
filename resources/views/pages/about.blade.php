@php($slug = "About us")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')
	<section class="hero"><h1 class="title h1" style="text-align: center; padding: 30px 0 70px 0; border-bottom: 2px solid #d2d2d2; margin-bottom: 40px; margin-top: 101px;">Our Company</h1></section>
	<div class="container">
		

		<div class="columns" style="margin: 20px 0 40px 0;">
		  <div class="column is-12" style="padding: 30px;">
		    <p>Hello and welcome to Electra, the place to find the best Electronics for every taste and occasion. We thoroughly check the quality of our goods, working only with reliable suppliers so that you only receive the best quality product.</p><br />

			<p>We at Electra believe in high quality and exceptional customer service. But most importantly, we believe shopping is a right, not a luxury, so we strive to deliver the best products at the most affordable prices, and ship them to you regardless of where you are located.</p><br />
			
			<p>We are a small but motivated company specializing in TV, Laptops, Cell phones, Camera ans Speakers. We believe passionately in great bargains and excellent service, which is why we commit ourselves to giving you the best of both.</p><br />

			<p>If you’re looking for something new, you’re in the right place. We strive to be industrious and innovative, offering our customers something they want, putting their desires at the top of our priority list.</p><br />
		  </div>
		  <div class="column" style="text-align: center;">
		    <img src="images/demo_image.png" alt="Company CEO">
		  </div>
		</div>

		

		</div>


@include('layouts.partials.footer')
@endsection
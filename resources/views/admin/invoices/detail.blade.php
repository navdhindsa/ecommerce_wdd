@php($slug = "Invoices")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				<nav class="level content-navbar">
				  <!-- Left side -->
				  <div class="level-left">
				    <p class="level-item">All Invoices</p>		
				    <!-- <p class="level-item"><a class="button is-danger" href="/admin/products/add">Add New Item</a></p> -->
				  </div>					    
				</nav>	

				@if(session()->has('message'))
					<div class="notification is-success">
						{{ session()->get('message') }}
					</div>
				@endif
					
				<div class="columns">
				  	<div class="column is-full">					
					  	<div class="columns">
					  		<!-- <div class="column is-2">
					  			<img src="images/adminList.png" alt="demotext" />
					  		</div> -->
					  		<div class="column is-5 admin-list">
					  			<table class="table is-fullwidth">
							    	<tbody>
							    		<tr>
							    			<td>Invoice ID:</td>
							    			<td>{{$invoice->id}}</td>
							    		</tr>
							    		<tr>
							    			<td>User Name:</td>
							    			<td>{{$user->first_name}} {{$user->last_name}}</td>
							    		</tr>
							    		<tr>
							    			<td>Email:</td>
							    			<td>{{$user->email}}</td>
							    		</tr>
							    		<tr>
							    			<td>Mobile Number:</td>
							    			<td>{{$user->phone}}</td> 
							    		</tr>
							    		<tr>
							    			<td>Status</td>
							    			<td>
							    				<form method="post" action="/admin/invoices/update" style="display: inline;">
				            						{{ csrf_field() }}
													<div class="field">
													  <div class="control">
													  	<input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
													    <div class="select is-fullwidth {{$errors->has('status')?'is-danger':''}}">
													      <select name="status" id="status">
													        <option value="0">Select a status</option>
													        <option value="Confirmed">Confirmed</option>
													        <option value="Shipped">Shipped</option>
													        <option value="Delivered">Delivered</option>
													      </select>
													    </div>
													    @if ($errors->has('status'))
								                            <p class="help is-danger">{{ $errors->first('status') }}</p>
								                        @endif
													  </div>
													  <button class="button admin-list-button" type="submit">Update</button>
													</div>								    	
											    </form>

							    			</td> 
							    		</tr>
							    		<tr></tr>
							    	</tbody>
							    </table>
					  		</div>
					  	</div>
					</div>
				</div>

				
										
			</section>

			
		</div>

	</div>

@endsection
@php($slug = "Invoices")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				<nav class="level content-navbar">
				  <!-- Left side -->
				  <div class="level-left">
				    <p class="level-item">All Invoices</p>		
				    <!-- <p class="level-item"><a class="button is-danger" href="/admin/products/add">Add New Item</a></p> -->
				  </div>					    
				</nav>	

				@if(session()->has('message'))
					<div class="notification is-success">
						{{ session()->get('message') }}
					</div>
				@endif
					
				<div class="columns">
				  	<div class="column is-full">	
					@foreach($invoices as $invoice)
				  	<div class="columns">
				  		<!-- <div class="column is-2">
				  			<img src="images/adminList.png" alt="demotext" />
				  		</div> -->
				  		<div class="column admin-list">
				  			<nav class="level">
								  <div class="level-left">
								    <p class="level-item"><strong>{{ $invoice->id }}</strong></p>
								    
								  </div>
								  <div class="level-right">
								  	<div class="level-item">
								    	<a class="button admin-list-button" href="/admin/invoices/{{ $invoice->id }}">Update</a>
								    </div>									    		
								  </div>    
								</nav>	
							<div>
				  		</div>
				  	</div>
				  	@endforeach	

				  </div>
				</div>

				{{ $invoices->links('vendor.pagination.bulma') }}
										
			</section>

			
		</div>

	</div>

@endsection
@php($slug = "List View")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				<nav class="level content-navbar">
				  <!-- Left side -->
				  <div class="level-left">
				    <p class="level-item">All Blog Posts</p>		
				    <p class="level-item"><a class="button is-danger" href="/admin/blog/add">Add New Post</a></p>			    
				  </div>

				  					    
				</nav>	

				@if(session()->has('message'))
					<div class="notification is-success">
						{{ session()->get('message') }}
					</div>
				@endif
					
				<div class="columns">
				  	<div class="column is-full">	
					@foreach($blogs as $blog)
				  	<div class="columns">
				  		<div class="column is-2">
				  			<img src="\images\blog\{{$blog->thumbnail_image}}" alt="demotext" />
				  		</div>
				  		<div class="column admin-list">
				  			<nav class="level">
								  <div class="level-left">
								    <p class="level-item"><strong>{{ $blog->title }}</strong></p>					    
								  </div>
								  <div class="level-right">
								    <p class="level-item"><a class="button admin-list-button" href="/admin/blog/edit/{{ $blog->id }}">EDIT</a></p>		
								    <form method="get" action="/admin/blog/destroy/{{ $blog->id}}" style="display: inline;" onsubmit="return confirm('Are you sure to delete this blog?');">
            					{{ csrf_field() }}
            					<input type="hidden" name="_method" value="DELETE">
								    	<p class="level-item"><button class="button admin-list-button" type="submit">DELETE</button></p>
								    </form>				
								  </div>    
								</nav>	
				  		</div>
				  	</div>
				  	@endforeach	

				  </div>
				</div>

				{{ $blogs->links('vendor.pagination.bulma') }}
										
			</section>

			
		</div>

	</div>

@endsection
@php($slug = "Add Page")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				@if(session()->has('message'))
				<div class="alert alert-success">
						{{ session()->get('message') }}
				</div>
				@endif
				<h2 class="title is-2">Add a new Blog Post</h2>

				
				<form method="post" action="/admin/blog/store"  id="addBlogForm"  enctype="multipart/form-data">
					@csrf
					
				  	<div class="field">
					  <label class="label" for="title">Title</label>
					  <div class="control">
					    <input class="input {{$errors->has('title')?'is-danger':''}}" name="title" id="title" type="text" placeholder="Title of the post" value="{{old('title')}}">
					  </div>
					  @if ($errors->has('title'))
                        <p class="help is-danger">{{ $errors->first('title') }}</p>
                      @endif
					</div>

					<div class="field">
					  <label class="label" for="excerpt">Excerpt of the post</label>
					  <div class="control">
					    <textarea class="textarea {{$errors->has('excerpt')?'is-danger':''}}" id="excerpt" name="excerpt" rows="2" placeholder="excerpt of the post">{{old('excerpt')}}</textarea>
					  </div>
					  @if ($errors->has('excerpt'))
                        <p class="help is-danger">{{ $errors->first('excerpt') }}</p>
                      @endif
					</div>
					  
					<div class="field">
					  <label class="label" for="body">Body of the post</label>
					  <div class="control">
					    <textarea class="textarea {{$errors->has('body')?'is-danger':''}}" id="body" name="body" rows="5" placeholder="Body of the post">{{old('body')}}</textarea>
					  </div>
					  @if ($errors->has('body'))
                        <p class="help is-danger">{{ $errors->first('body') }}</p>
                      @endif
					</div>

					<div class="field">
							<label class="label" for="thumbnail_image">Thumbnail</label>
							<input type="file" class="form-control-file" id="thumbnail_image" name="thumbnail_image" value="">
							@if ($errors->has('thumbnail_image'))
									<p class="help is-danger">{{ $errors->first('thumbnail_image') }}</p>
							@endif
					</div>

					<div class="field">
							<label class="label" for="featured_image">Featured</label>
							<input type="file" class="form-control-file" id="featured_image" name="featured_image" value="">
							@if ($errors->has('featured_image'))
									<p class="help is-danger">{{ $errors->first('featured_image') }}</p>
							@endif
					</div>

					<div class="buttons is-centered">
					  <button class="button is-medium is-fullwidth is-success is-hovered" type="submit">{{ __('Add Blog') }}</button>
					</div>

				</form>
	
			</section>

		</div><!-- end div column is-10 -->
		
	</div> <!-- end div columns is-gapless -->

@endsection
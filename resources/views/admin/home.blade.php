@php($slug = "Admin Home")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">
    
    @include('admin.partials.admin_sidebar')

    <div class="column is-10">
      @include('admin.partials.admin_navbar')

          <section>            
              <h1>dashboard</h1>
              <hr />
              <div class="columns is-multiline" style="text-align: center;">
                <div class="column is-3 has-background-grey-lighter" style="margin: 10px">
                  <h2 class="title is-2">Total Products</h2>
                  <h3 class="title is-3">{{$total_products}}</h3>
                </div>
                <div class="column is-3 has-background-grey-lighter" style="margin: 10px">
                  <h2 class="title is-2">Total Blogs</h2>
                  <h3 class="title is-3">{{$total_blogs}}</h3>
                </div>
                <div class="column is-3 has-background-grey-lighter" style="margin: 10px">
                  <h2 class="title is-2">Total Users</h2>
                  <h3 class="title is-3">{{$total_users}}</h3>
                </div>
                <div class="column is-3 has-background-grey-lighter" style="margin: 10px">
                  <h2 class="title is-2">Total Invoices</h2>
                  <h3 class="title is-3">{{$total_invoices}}</h3>
                </div>
              </div>
              
            
          </section>
          <!--
            <section>
              
               <h1>Other chart's may go here</h1>
                
            </section>
          -->
        </div>
        
      </div>

@endsection
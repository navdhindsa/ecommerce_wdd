@php($slug = "User Detail")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				<nav class="level content-navbar">
				  <!-- Left side -->
				  <div class="level-left">
				    <p class="level-item">All Users</p>		
				    <!-- <p class="level-item"><a class="button is-danger" href="/admin/products/add">Add New Item</a></p> -->
				  </div>					    
				</nav>	
	
				<div class="columns">
				  	<div class="column is-6">	
						<table class="table is-fullwidth">
					    	<tbody>
					    		<tr>
					    			<td>First Name:</td>
					    			<td>{{$user->first_name}}</td>
					    		</tr>
					    		<tr>
					    			<td>Last Name:</td>
					    			<td>{{$user->last_name}}</td>
					    		</tr>
					    		<tr>
					    			<td>Email:</td>
					    			<td>{{$user->email}}</td>
					    		</tr>
					    		<tr>
					    			<td>Gender</td>
					    			<td>{{$user->gender}}</td>
					    		</tr>
					    		<tr>
					    			<td>Date of Birth:</td>
					    			<td>{{$user->dob}}</td>
					    		</tr>
					    		<tr>
					    			<td>Mobile Number:</td>
					    			<td>{{$user->phone}}</td> 
					    		</tr>
					    		<tr>
					    			<td>Bio</td>
					    			<td>{{$user->bio}}</td> 
					    		</tr>
					    		<tr></tr>
					    	</tbody>
					    </table>
					    <div class="has-text-centered">
						    <a href="/admin/users" class="button is-primary">BACK</a>
						    <form method="get" action="/admin/users/destroy/{{ $user->id}}" style="display: inline;" onsubmit="return confirm('Are you sure to delete this user?');">
    							{{ csrf_field() }}
    							<input type="hidden" name="_method" value="DELETE">
						    	<button class="button admin-list-button" type="submit">DELETE</button>
						    </form>	
						</div>

				    </div>
				</div>		
			</section>

			
		</div>

	</div>

@endsection
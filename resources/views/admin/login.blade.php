@php($slug = "Admin Login")
@extends('layouts.admin')

@section('content')

<section class="hero is-fullheight">
    <div class="hero-body">
        <div class="container">
            <div class="column is-4 is-offset-4">
              <img src="images/admin2.png" alt="Logo" />
              <form>
                  <div class="field">
                      <div class="control">
                      	<label for="email">Email:</label>
                        <input class="input" name="email" id="email" type="email" placeholder="Your Email Address" autofocus="">
                      </div>
                  </div>

                  <div class="field">
                      <div class="control">
                      	<label for="pass">Password:</label>
                        <input class="input" name="pass" id="pass" type="password" placeholder="Your Password">
                      </div>
                  </div>
                 
                  <button class="button is-block is-fullwidth">LOGIN</button>
              </form>
            </div>    
        </div>
    </div>
</section>


@endsection
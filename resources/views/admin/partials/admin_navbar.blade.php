<nav class="level admin-navbar">
        <!-- Left side -->
        <div class="level-left">
          <p class="level-item admin-navbar-item">Welcome ADMIN!</p>              
        </div>

        <!-- Right side -->
        <div class="level-right">
          <!-- <p class="level-item admin-navbar-item">Logout</p> -->
          <p class="level-item"><a class="button is-success" href="/">Live Site</a></p>
          <div class="navbar-item has-dropdown is-hoverable">
                  <a class="navbar-link" href="#">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>

                  <div class="navbar-dropdown">
                      <a class="navbar-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                          Logout
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST"
                            style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </div>
              </div>
        </div>
      </nav>
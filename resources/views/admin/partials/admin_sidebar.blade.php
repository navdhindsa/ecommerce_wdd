<div class="column is-2 admin-sidebar">
      <p class="title"><img src="/images/admin2.png" alt="demotext" /></p>
      <aside class="menu">
        <p class="menu-label">
          Main
        </p>
        <ul class="menu-list">
          <li><a class="has-text-white" href="/admin/home">Dashboard</a></li>
          <li><a class="has-text-white" href="/admin/products">Products</a></li>
          <li><a class="has-text-white" href="/admin/blog">Blog Posts</a></li>
          <li><a class="has-text-white" href="/admin/invoices">Invoices</a></li>
          <li><a class="has-text-white" href="/admin/transaction">Transactions</a></li>
          <li><a class="has-text-white" href="/admin/users">Users</a></li>
        </ul>
      </aside>
    </div>
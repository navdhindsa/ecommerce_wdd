@php($slug = "Edit Page")
@extends('layouts.admin')

@section('content')


<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				<form method="post" action="/admin/products/update" id="addProductForm" enctype="multipart/form-data">
					@csrf
					<h2 class="title is-2">Edit This Product</h2>
				  	<div class="field">
					  <label class="label" for="name">Name</label>
					  <div class="control">
					  	<input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}">
					    <input class="input {{$errors->has('name')?'is-danger':''}}" name="name" id="name" type="text" placeholder="Name of the product" value="{{ old('name', $product->name) }}">
					  </div>
					    @if ($errors->has('name'))
                            <p class="help is-danger">{{ $errors->first('name') }}</p>
                        @endif
					</div>

					<div class="field">
							<label class="label" for="thumbnail_image">Thumbnail</label>
							<input type="file" class="form-control-file" id="thumbnail_image" name="thumbnail_image" value="">
							@if ($errors->has('thumbnail_image'))
									<p class="help is-danger">{{ $errors->first('thumbnail_image') }}</p>
							@endif
					</div>

					<div class="field">
					  <label class="label" for="manufacture_id">Manufacturer</label>
					  <div class="control">
					    <div class="select is-fullwidth {{$errors->has('manufacture_id')?'is-danger':''}}">
					      <select name="manufacturer_id" id="manufacture_id">
					        <option value="0">Select a manufacturer</option>
					        <option {{ ($product->manufacturer_id == "1")? 'selected="selected"' :'' }} value="1">Google</option>
					        <option {{ ($product->manufacturer_id == "2")? 'selected="selected"' :'' }} value="2">OnePlus</option>
					        <option {{ ($product->manufacturer_id == "4")? 'selected="selected"' :'' }} value="4">Huawei</option>
					        <option {{ ($product->manufacturer_id == "5")? 'selected="selected"' :'' }} value="5">Samsung</option>
					        <option {{ ($product->manufacturer_id == "6")? 'selected="selected"' :'' }} value="6">Xiaomi</option>
					        <option {{ ($product->manufacturer_id == "7")? 'selected="selected"' :'' }} value="7">Dell</option>
					        <option {{ ($product->manufacturer_id == "8")? 'selected="selected"' :'' }} value="8">Apple</option>
					        <option {{ ($product->manufacturer_id == "9")? 'selected="selected"' :'' }} value="9">Lenovo</option>
					        <option {{ ($product->manufacturer_id == "10")? 'selected="selected"' :'' }} value="10">Acer</option>
					        <option {{ ($product->manufacturer_id == "11")? 'selected="selected"' :'' }} value="11">Blackberry</option>
					        <option {{ ($product->manufacturer_id == "12")? 'selected="selected"' :'' }} value="12">Vivo</option>
					        <option {{ ($product->manufacturer_id == "13")? 'selected="selected"' :'' }} value="13">HTC</option>
					        <option {{ ($product->manufacturer_id == "14")? 'selected="selected"' :'' }} value="14">Microsoft</option>
					        <option {{ ($product->manufacturer_id == "15")? 'selected="selected"' :'' }} value="15">Asus</option>
					        <option {{ ($product->manufacturer_id == "16")? 'selected="selected"' :'' }} value="16">HP</option>
					        <option {{ ($product->manufacturer_id == "17")? 'selected="selected"' :'' }} value="17">Pixel</option>
					        <option {{ ($product->manufacturer_id == "18")? 'selected="selected"' :'' }} value="18">Canon</option>
					        <option {{ ($product->manufacturer_id == "19")? 'selected="selected"' :'' }} value="19">Sony</option>
					        <option {{ ($product->manufacturer_id == "20")? 'selected="selected"' :'' }} value="20">Nikon</option>
					        <option {{ ($product->manufacturer_id == "21")? 'selected="selected"' :'' }} value="21">Olympus</option>
					        <option {{ ($product->manufacturer_id == "22")? 'selected="selected"' :'' }} value="22">FujiFilm</option>
					        <option {{ ($product->manufacturer_id == "23")? 'selected="selected"' :'' }} value="23">Hasselblad</option>
					        <option {{ ($product->manufacturer_id == "24")? 'selected="selected"' :'' }} value="24">Ultimate ears</option>
					        <option {{ ($product->manufacturer_id == "25")? 'selected="selected"' :'' }} value="25">JBL</option>
					        <option {{ ($product->manufacturer_id == "26")? 'selected="selected"' :'' }} value="26">Klipsch</option>
					        <option {{ ($product->manufacturer_id == "27")? 'selected="selected"' :'' }} value="27">Sonos</option>
					        <option {{ ($product->manufacturer_id == "28")? 'selected="selected"' :'' }} value="28">Marshal</option>
					        <option {{ ($product->manufacturer_id == "29")? 'selected="selected"' :'' }} value="29">House of Marle</option>
					        <option {{ ($product->manufacturer_id == "30")? 'selected="selected"' :'' }} value="30">Harman</option>
					        <option {{ ($product->manufacturer_id == "31")? 'selected="selected"' :'' }} value="31">Bang & Olufsen</option>
					        <option {{ ($product->manufacturer_id == "32")? 'selected="selected"' :'' }} value="32">Bowers</option>
					        <option {{ ($product->manufacturer_id == "33")? 'selected="selected"' :'' }} value="33">Amazon</option>
					        <option {{ ($product->manufacturer_id == "34")? 'selected="selected"' :'' }} value="34">LG</option>
					        <option {{ ($product->manufacturer_id == "35")? 'selected="selected"' :'' }} value="35">Vizio</option>
					        <option {{ ($product->manufacturer_id == "36")? 'selected="selected"' :'' }} value="36">Toshiba</option>
					        <option {{ ($product->manufacturer_id == "37")? 'selected="selected"' :'' }} value="37">Sharp</option>
					        <option {{ ($product->manufacturer_id == "38")? 'selected="selected"' :'' }} value="38">Other</option>
					      </select>
					    </div>
					    @if ($errors->has('manufacture_id'))
                            <p class="help is-danger">{{ $errors->first('manufacture_id') }}</p>
                        @endif
					  </div>
					</div>
					
					<div class="field">
					  <label class="label" for="price">Price</label>
					  <div class="control">
					    <input class="input {{$errors->has('price')?'is-danger':''}}" name="price" id="price" type="text" placeholder="Price of the product" value="{{ old('price', $product->price) }}">
					  </div>
					  @if ($errors->has('price'))
                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                        @endif
					</div>

					<div class="field">
					  <label class="label" for="sale_price">Sale Price</label>
					  <div class="control">
					    <input class="input {{$errors->has('sale_price')?'is-danger':''}}" name="sale_price" id="sale_price" type="text" placeholder="Sale Price of the product" value="{{ old('sale_price', $product->sale_price) }}">
					  </div>
					  @if ($errors->has('sale_price'))
                            <p class="help is-danger">{{ $errors->first('sale_price') }}</p>
                        @endif
					</div>

					<div class="field">
						<label class="label" for="overview">Overview</label>
						<div class="control">
							<textarea class="textarea {{$errors->has('overview')?'is-danger':''}}" id="overview" name="overview" rows="5" placeholder="Write an overview">{{ old('overview', $product->overview) }}</textarea>
						</div>
						@if ($errors->has('overview'))
                            <p class="help is-danger">{{ $errors->first('overview') }}</p>
                        @endif
					</div>


					<div class="field">
					  <label class="label" for="more_information">More Information</label>
					  <div class="control">
					    <textarea class="textarea {{$errors->has('more_information')?'is-danger':''}}" id="more_information" name="more_information" rows="5" placeholder="Write additional information">{{ old('more_information', $product->more_information) }}</textarea>
					  </div>
					  @if ($errors->has('more_information'))
                            <p class="help is-danger">{{ $errors->first('more_information') }}</p>
                        @endif
					</div>

					<div class="field">
					  <label class="label" for="category_id">Category</label>
					  <div class="control">
					    <div class="select is-fullwidth {{$errors->has('category_id')?'is-danger':''}}">
					    	<!-- to make this work we need to remove the v-model="selected" directive -->
					      <select name="category_id" id="category_id" v-model="selected"  v-init:selected="{{ $product->category_id }}">
					        <option value="0">Select a category</option>
					    	<option {{ ($product->category_id == "1")? 'selected="selected"' :'' }} value="1">TV</option>
					    	<option {{ ($product->category_id == "2")? 'selected="selected"' :'' }} value="2">Laptop</option>
					    	<option {{ ($product->category_id == "3")? 'selected="selected"' :'' }} value="3">Cell Phone</option>
					    	<option {{ ($product->category_id == "4")? 'selected="selected"' :'' }} value="4">Tablet</option>
					    	<option {{ ($product->category_id == "5")? 'selected="selected"' :'' }} value="5">Camera</option>
					    	<option {{ ($product->category_id == "6")? 'selected="selected"' :'' }} value="6">Speaker</option>
					      </select>
					    </div>
					    @if ($errors->has('category_id'))
                            <p class="help is-danger">{{ $errors->first('category_id') }}</p>
                        @endif
					  </div>
					</div> 
					
					<spec-form 
						v-for="option in options"
			  			v-bind:key="option.id"
			  			v-bind:id="option.id"
							v-bind:name="option.name">
					</spec-form>

					

					<div class="buttons is-centered">
					  <button class="button is-medium is-fullwidth is-success is-hovered" type="submit">{{ __('Edit Product') }}</button>
					</div>

				</form>
			</section>
		</div><!-- end div column is-10 -->
	</div><!-- end div columns is-gapless -->

@endsection
@php($slug = "Add Page")
@extends('layouts.admin')

@section('content')

<div class="columns is-gapless">

	@include('admin.partials.admin_sidebar')

		<div class="column is-10">
			@include('admin.partials.admin_navbar')
			
			<section>
				@if(session()->has('message'))
				<div class="alert alert-success">
					{{ session()->get('message') }}
				</div>
				@endif
				<form method="post" action="/admin/products/store" id="addProductForm" enctype="multipart/form-data">
					@csrf
					<h2 class="title is-2">Add a new Product</h2>
				  	<div class="field">
					  <label class="label" for="name">Name</label>
					  <div class="control">
					    <input class="input {{$errors->has('name')?'is-danger':''}}" name="name" id="name" type="text" placeholder="Name of the product" value="{{old('name')}}">
					  </div>
					   @if ($errors->has('name'))
								<p class="help is-danger">{{ $errors->first('name') }}</p>
						@endif
					</div>

					<div class="field">
							<label class="label" for="thumbnail_image">Thumbnail</label>
							<input type="file" class="form-control-file" id="thumbnail_image" name="thumbnail_image" value="">
							@if ($errors->has('thumbnail_image'))
									<p class="help is-danger">{{ $errors->first('thumbnail_image') }}</p>
							@endif
					</div>

					<div class="field">
					  <label class="label" for="manufacture_id">Manufacturer</label>
					  <div class="control">
					    <div class="select is-fullwidth {{$errors->has('manufacture_id')?'is-danger':''}}">
					      <select name="manufacturer_id" id="manufacture_id">
					        <option value="0">Select a manufacturer</option>
					        <option value="1">Google</option>
					        <option value="2">OnePlus</option>
					        <option value="3">Xplore</option>
					        <option value="4">Huawei</option>
					        <option value="5">Samsung</option>
					        <option value="6">Xiaomi</option>
					        <option value="7">Dell</option>
					        <option value="8">Apple</option>
					        <option value="9">Lenovo</option>
					        <option value="10">Acer</option>
					        <option value="11">Blackberry</option>
					        <option value="12">Vivo</option>
					        <option value="13">HTC</option>
					        <option value="14">Microsoft</option>
					        <option value="15">Asus</option>
					        <option value="16">HP</option>
					        <option value="17">Pixel</option>
					        <option value="18">Canon</option>
					        <option value="19">Sony</option>
					        <option value="20">Nikon</option>
					        <option value="21">Olympus</option>
					        <option value="22">FujiFilm</option>
					        <option value="23">Hasselblad</option>
					        <option value="24">Ultimate ears</option>
					        <option value="25">JBL</option>
					        <option value="26">Klipsch</option>
					        <option value="27">Sonos</option>
					        <option value="28">Marshal</option>
					        <option value="29">House of Marle</option>
					        <option value="30">Harman</option>
					        <option value="31">Bang & Olufsen</option>
					        <option value="32">Bowers</option>
					        <option value="33">Amazon</option>
					        <option value="34">LG</option>
					        <option value="35">Vizio</option>
					        <option value="36">Toshiba</option>
					        <option value="37">Sharp</option>
					        <option value="38">Other</option>
					      </select>
					    </div>
					    @if ($errors->has('manufacture_id'))
									<p class="help is-danger">{{ $errors->first('manufacture_id') }}</p>
							@endif
					  </div>
					</div>
					
					<div class="field">
					  <label class="label" for="price">Price</label>
					  <div class="control">
					    <input class="input {{$errors->has('price')?'is-danger':''}}" name="price" id="price" type="text" placeholder="Price of the product" value="{{old('price')}}">
					  </div>
					    @if ($errors->has('price'))
                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                        @endif
					</div>

					<div class="field">
					  <label class="label" for="sale_price">Sale Price</label>
					  <div class="control">
					    <input class="input {{$errors->has('sale_price')?'is-danger':''}}" name="sale_price" id="sale_price" type="text" placeholder="Sale Price of the product" value="{{old('sale_price')}}">
					  </div>
					    @if ($errors->has('sale_price'))
                            <p class="help is-danger">{{ $errors->first('sale_price') }}</p>
                        @endif
					</div>

					<div class="field">
					  <label class="label" for="overview">Overview</label>
					  <div class="control">
					    <textarea class="textarea {{$errors->has('overview')?'is-danger':''}}" id="overview" name="overview" rows="5" placeholder="Write an overview">{{old('overview')}}</textarea>
					  </div>
					    @if ($errors->has('overview'))
                            <p class="help is-danger">{{ $errors->first('overview') }}</p>
                        @endif
					</div>

					<div class="field">
					  <label class="label" for="more_information">More Information</label>
					  <div class="control">
					    <textarea class="textarea {{$errors->has('more_information')?'is-danger':''}}" id="more_information" name="more_information" rows="5" placeholder="Write additional information">{{old('more_information')}}</textarea>
					  </div>
					 	 @if ($errors->has('more_information'))
                            <p class="help is-danger">{{ $errors->first('more_information') }}</p>
                        @endif
					</div>

					<div class="field">
					  <label class="label" for="category_id">Category</label>
					  <div class="control">
					    <div class="select is-fullwidth {{$errors->has('category_id')?'is-danger':''}}">
					      <select name="category_id" id="category_id" v-model="selected">
					        <option value="0">Select a category</option>
					    	<option value="1">TV</option>
					    	<option value="2">Laptop</option>
					    	<option value="3">Cell phone</option>
					    	<option value="4">Tablet</option>
					    	<option value="5">Camera</option>
					    	<option value="6">Speaker</option>
					      </select>
					    </div>
					    @if ($errors->has('category_id'))
                            <p class="help is-danger">{{ $errors->first('category_id') }}</p>
                        @endif
					  </div>
					</div> 

					<spec-form 
						v-for="option in options"
			  			v-bind:key="option.id"
			  			v-bind:id="option.id"
			  			v-bind:name="option.name">
					</spec-form>

					

					<div class="buttons is-centered">
					  <button class="button is-medium is-fullwidth is-success is-hovered" type="submit">{{ __('Add Product') }}</button>
					</div>

				</form>
	
			</section>

		</div><!-- end div column is-10 -->
		
	</div> <!-- end div columns is-gapless -->

@endsection
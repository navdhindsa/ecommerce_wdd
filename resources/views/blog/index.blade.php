@php($slug = "Blog")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">OUR BLOG</h1>
	</section>
	<div class="container">
		
		<div class="columns is-multiline">

		  @foreach( $posts as $post)	
		  <div class="column is-4" style="margin: 30px 0;">
		  	<a href="/blog/{{ $post->id }}" ><img src="/images/blog/{{$post->thumbnail_image}}" alt="blog image"></a>
		  	<p class="title">{{ $post->title }}</p>
		    <p class="subtitle">{{$post->excerpt}}</p> 
		    <p class="title is-4">Electra Tech</p>
			<p class="subtitle is-6"> {{ $post->created_at }}</p>
		  </div>
		  @endforeach	  

		  
		</div>
	</div>

@include('layouts.partials.footer')
@endsection
@php($slug = "Blog Detail")
@extends('layouts.master')

@section('content')

@include('layouts.partials.header')

	<section class="hero">
		<h1 class="title is-1" style="text-align: center; margin-top: 120px; padding-bottom: 40px; border-bottom: 2px solid #d2d2d2">BLOG POST</h1>
	</section>

	<div class="container" style="margin-top: 30px;">
		<div style="text-align: center;">
			<img src="\images\blog\{{$blog->feature_image}}" alt="image" />
		</div>
		<h1 class="title is-1" style="padding:30px 0 20px 0">{{$blog->title}}</h1>
		<h2 class="subtitle" style="padding: 10px 0 20px 0; border-bottom: 2px solid #ddd">Electra Tech | {{$blog->created_at}}</h2>

		{{$blog->body}}

	</div>

@include('layouts.partials.footer')
@endsection
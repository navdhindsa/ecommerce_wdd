
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
window.Vue.prototype.$http = window.axios;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('spec-form', require('./components/SpecForm.vue'));
Vue.component('search-items', require('./components/SearchItems.vue'));
Vue.component('flash-message', require('./components/FlashMessage.vue'));
Vue.component('cart-product', require('./components/CartProduct.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))
/* Directives */
Vue.directive('init', {
    bind: function(el, binding, vnode) {
      vnode.context[binding.arg] = binding.value;
    }
  });
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 //Top Header
 if(document.getElementById('topHoverbox')) {
    const topHoverbox = new Vue( {
       el: '#topHoverbox',
       data: {
           link: '',
           query: '',
           options: '',
       },
       methods: {
           setLink: function ( l ) {
               if(this.link == '')
                    this.link = l;
               else 
                this.link = '';
           },
           showSuggessions: function () {
               if(this.query){
                    // GET /someUrl
                    this.$http.get('/api/search/products/' + this.query).then(response => {
                        // get body data
                        this.options = response.data.data;
                        console.log(this.options);
                    }, response => {
                        // error callback
                    });
               }
               else {
                   this.options = '';
               }
            }
        }
    });
 }

 //Tabs
 if(document.getElementById('tabs')) {
    const tabs = new Vue( {
       el: '#tabs',
       data: {
           tab: 'overview',
       }
    });
 }

 //admin product form
 if(document.getElementById('addProductForm')) {
    const productForm = new Vue({
        el: '#addProductForm',
        // define methods under the `methods` object
        data: {
            selected: 0,
            options: [],
        },
        watch: {
            selected: function (val) {
                // GET /someUrl
                this.$http.get('/api/categories/' + this.selected + '/options').then(response => {
                    // get body data
                    this.options = response.data.data;
                    console.log(this.options);
                }, response => {
                    // error callback
                });
            }
        },
    });
}

//Product methods
if(document.getElementById('product')) {
   const product = new Vue({
       el: '#product',
       // define methods under the `methods` object
       data: {
           result: '',
           csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
           message: '',
           type: '',
           modal: 'modal',
           rating: 0,
           title: '',
           comment: '',
           isSubmitted: false,
       },
       methods: {
            addToCart: function (product) {
                console.log("Clicked id: " + product.id);
               // GET /someUrl
               this.$http.post('/cart/', {id: product.id, name: product.name, _token: this.csrf}).then(response => {
                   // get body data
                   this.message = response.data;
                   this.type = 'success';
               }, response => {
                   // error callback
               });
           },
           addToWishlist: function (product) {
              // GET /someUrl
              this.$http.post('/wishlist/', {id: product.id, name: product.name, _token: this.csrf}).then(response => {
                  // get body data
                  this.message = response.data;
                  this.type = 'success';
              }, response => {
                  // error callback
              });
          },
          toggleModal: function(classNames) {
            this.modal = classNames;
          },
          saveReview: function(product) {
            this.isSubmitted = true;
            if(this.rating != 0 && this.title != '' && this.comment != '') {
                // Post /someUrl
                this.$http.post('/review/', {product_id: product.id, rating: this.rating, title: this.title, comment: this.comment, _token: this.csrf}).then(response => {
                    // get body data
                    this.message = response.data;
                    this.type = 'success';
                    this.modal = 'modal';
                    this.title = '';
                    this.rating = 0;
                    this.comment = '';
                    this.isSubmitted = false;
                    location.reload();
                }, response => {
                    // error callback
                });
            }
          }
       }
   });
}

//Cart
if(document.getElementById('cart')) {
   const cart = new Vue({
       el: '#cart',
       // define methods under the `methods` object
       data: {
            csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
            total: 0,
            tp: 0
       },
       methods: {
            changeTotal: function(value) {
                this.total = this.total + value;
                this.tp = parseFloat(this.total).toFixed(2);
                console.log(this.total);
            }
       }
    });
}

//Checkout
if(document.getElementById('checkout')) {
    const checkout = new Vue({
        el: '#checkout',
        // define methods under the `methods` object
        data: {
             csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
             sameadr: document.getElementById('sameadr').checked,
             shipping_method: document.getElementById('shipping_method').value,
             address_1: document.getElementById('address_1').value,
             address_2: document.getElementById('address_2').value,
             province: document.getElementById('province').value,
             city: document.getElementById('city').value,
             postal_code: document.getElementById('postal_code').value,
             shipping_address_1: document.getElementById('shipping_address_1').value,
             shipping_address_2: document.getElementById('shipping_address_2').value,
             shipping_province: document.getElementById('shipping_province').value,
             shipping_city: document.getElementById('shipping_city').value,
             shipping_postal_code: document.getElementById('shipping_postal_code').value,
             billing_address_id: (document.getElementById('billing_address_id')) ? document.getElementById('billing_address_id').value : 0,
             shipping_address_id: (document.getElementById('shipping_address_id')) ? document.getElementById('shipping_address_id').value : 0,
             card_number: document.getElementById('card_number').value,
             name_on_card: document.getElementById('name_on_card').value,
             month: document.getElementById('month').value,
             year: document.getElementById('year').value,
             cvv: document.getElementById('cvv').value,
             total: 0,
             tax: 0,
             shipping: 0,
             grandTotal: 0,
        },
        watch: {
            address_1: function(val) {
                if(this.sameadr === true)
                    this.shipping_address_1 = this.address_1;
            },
            address_2: function(val) {
                if(this.sameadr === true)
                    this.shipping_address_2 = this.address_2;
            },
            province: function(val) {
                if(this.sameadr === true)
                this.shipping_province = this.province;
            },
            city: function(val) {
                if(this.sameadr === true)
                    this.shipping_city = this.city;
            },
            postal_code: function(val) {
                if(this.sameadr === true)
                    this.shipping_postal_code = this.postal_code;
            },
            shipping_province: function(val) {
                if(this.shipping_province != 0) {
                    // Post /someUrl
                    this.$http.post('/checkout/tax/', {amount: (this.total + this.shipping), province: this.shipping_province, _token: this.csrf}).then(response => {
                        // get body data
                        this.tax = response.data;
                    }, response => {
                        // error callback
                    });
                }
                console.log(this.shipping_province);
            },
            shipping_method: function(val) {
                if(this.shipping_method != 0) {
                    // Post /someUrl
                    this.$http.post('/checkout/shipping/', {shipping: this.shipping_method, _token: this.csrf}).then(response => {
                        // get body data
                        this.shipping = response.data;
                        //Get tax again
                        // Post /someUrl
                        if(this.shipping_province != 0) {
                            this.$http.post('/checkout/tax/', {amount: (this.total + this.shipping), province: this.shipping_province, _token: this.csrf}).then(response => {
                                // get body data
                                this.tax = response.data;
                            }, response => {
                                // error callback
                            });
                        }
                    }, response => {
                        // error callback
                    });
                }
            },
            billing_address_id: function() {
                if(this.billing_address_id != 0) {
                    // Post /someUrl
                    this.$http.post('/user/address/', {type: 'billing', billing_address_id: this.billing_address_id, _token: this.csrf}).then(response => {
                        // get body data
                        this.address_1 = response.data.street_1;
                        this.address_2 = response.data.street_2;
                        this.province = response.data.province_id;
                        this.city = response.data.city;
                        this.postal_code = response.data.postal_code;
                        console.log(response.data);
                    }, response => {
                        // error callback
                    });
                }
            },
            shipping_address_id: function() {
                if(this.shipping_address_id != 0) {
                    // Post /someUrl
                    this.$http.post('/user/address/', {type: 'shipping', shipping_address_id: this.shipping_address_id, _token: this.csrf}).then(response => {
                        // get body data
                        this.shipping_address_1 = response.data.street_1;
                        this.shipping_address_2 = response.data.street_2;
                        this.shipping_province = response.data.province_id;
                        this.shipping_city = response.data.city;
                        this.shipping_postal_code = response.data.postal_code;
                        console.log(response.data);
                    }, response => {
                        // error callback
                    });
                }
            },
        },
        methods: {
            toggleBillingAsShipping: function() {
                if(this.sameadr != '') {
                    this.sameadr = true;
                    this.shipping_address_1 = this.address_1;
                    this.shipping_address_2 = this.address_2;
                    this.shipping_province = this.province;
                    this.shipping_city = this.city;
                    this.shipping_postal_code = this.postal_code;
                }
                else {
                    this.sameadr = false;
                    this.shipping_address_1 = '';
                    this.shipping_address_2 = '';
                    this.shipping_province = 0;
                    this.shipping_city = '';
                    this.shipping_postal_code = '';
                }
            }
        },
        mounted: function() {
            this.grandTotal = this.total + this.tax + this.shipping;
            if(this.shipping_province != 0) {
                // Post /someUrl
                this.$http.post('/checkout/tax/', {amount: (this.total + this.shipping), province: this.shipping_province, _token: this.csrf}).then(response => {
                    // get body data
                    this.tax = response.data;
                }, response => {
                    // error callback
                });
            }

            if(this.shipping_method != 0) {
                // Post /someUrl
                this.$http.post('/checkout/shipping/', {shipping: this.shipping_method, _token: this.csrf}).then(response => {
                    // get body data
                    this.shipping = response.data;
                    //Get tax again
                    // Post /someUrl
                    if(this.shipping_province != 0) {
                        this.$http.post('/checkout/tax/', {amount: (this.total + this.shipping), province: this.shipping_province, _token: this.csrf}).then(response => {
                            // get body data
                            this.tax = response.data;
                        }, response => {
                            // error callback
                        });
                    }
                }, response => {
                    // error callback
                });
            }
        },
        updated: function() {
            this.grandTotal = this.total + this.tax + this.shipping;
        }
     });
 }

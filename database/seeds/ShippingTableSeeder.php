<?php

use Illuminate\Database\Seeder;

class ShippingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shippings')->insert(
            [
                'company_name' => 'Purolator',
                'rate' => 20.50
            ]);
            
        DB::table('shippings')->insert(
            [
                'company_name' => 'Canada Post',
                'rate' => 10.00
            ]);
            
        DB::table('shippings')->insert(
            [
                'company_name' => 'Canada Post Express',
                'rate' => 25.00
            ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
        	[
        		'name'=>'TV',
        		'icon'=>'tv.jpg',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'Laptop',
                'icon'=>'laptop.jpg',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
                'name'=>'Phone',
                'icon'=>'cell_phone.jpg',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'Tablet',
        		'icon'=>'tablet.jpg',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'Camera',
        		'icon'=>'camera.jpg',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'Speaker',
        		'icon'=>'speaker.jpg',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
    }
}

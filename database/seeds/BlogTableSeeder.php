<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//5 manual post entries
    	DB::table('blog')->insert([
        'title' => 'Google is adding more parental controls to Chromebooks',
        'excerpt' => 'Google has had an app called Family Link for a little over a year now, designed to give parents more control over their kids use of their Android phones — and even turn them off at dinner time. Now its expanding the app to support Chromebooks, adding many of the same features to that platform.',
        'body' => 'Google has had an app called Family Link for a little over a year now, designed to give parents more control over their kids use of their Android phones — and even turn them off at dinner time. Now its expanding the app to support Chromebooks, adding many of the same features to that platform.

            Parents will be able to whitelist websites that theyre okay having their children visit, set screen time limits, establish a bedtime, remotely lock devices, and monitor usage. And for the first time, Chromebooks set up with supervised accounts will gain access to Android apps from the Google Play store.

            Android apps can be whitelisted for use or hidden on the device, which could be useful for shared devices. Parents will also be able to manage in-app purchases on supervised accounts.
            If you have been following the saga of Android on Chrome OS, you have probably mostly thought of it as simple access to Google Play apps. But in fact, many parts of Chrome OS itself have taken on elements of Android — including the new quick settings menu and even the software keyboard. It seems like that deeper integration may be helping to enable some of these new parental control features.

            One interesting idea here is that Google will let you set up supervised accounts for teenagers, but the teens will have the ability to turn off those controls. You’ll get an alert when that happens and then probably have a very exciting conversation about your teenagers Big Grown Up Moment of taking responsibility for their computer use.',
        'feature_image' => 'blog-1.png',
        'thumbnail_image' => 'blog-1.png',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

    	DB::table('blog')->insert([
        'title' => 'YouTube brings its trending charts to the YouTube Music app',
        'excerpt' => 'YouTube has announced that its YouTube Music Charts will now be integrated within the YouTube Music app on users home screens, and they will be available through search. Previously, these charts were only parked and viewable at a separate domain.',
        'body' => 'YouTube has announced that its YouTube Music Charts will now be integrated within the YouTube Music app on users home screens, and they will be available through search. Previously, these charts were only parked and viewable at a separate domain.

            Charts should be available in the YouTube Music app starting today. All 29 markets YouTube Music is available in will get five charts playlists three specific to their country and two global lists. Users can then add charts they like to their library so they can stay up to date on trending material.

            The charts, updated weekly, have been around for some time and were last revamped in May of this year. They show whats trending both locally (by country) and overall across the platform in categories like Top Songs and Top Music Videos. The charts not only show the current position of an artist, song, or video, but whether its moved up or down in position since the previous chart. Each chart is also packaged into playlists.
            The Trending chart is particularly interesting; back in May, YouTube said it was the companys first dedicated external signal of the most viewed new music on the platform. Stephen Bryan, YouTubes head of label relations claimed in an interview with Rolling Stone that YouTube charts are more accurate than Billboards charts, with greater representation from Latin and hip-hop acts.

            Earlier this year, Billboard changed how streams are weighted on its charts, devaluing YouTubes ad-supported music streams in comparison to streams from paid services like Apple Music and Spotify. YouTube launched its charts just 10 days after Billboards announcement, but it insisted it wasnt because Billboard gave it a back seat. YouTube is already the biggest music streaming platform on the internet, and now the company is looking to use these charts to bring weight to YouTube as an accurate marker for whats relevant in the moment.',
        'feature_image' => 'blog-2.png',
        'thumbnail_image' => 'blog-2.png',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

    	DB::table('blog')->insert([
        'title' => 'HP’s Omen 15t gaming laptop is fast',
        'excerpt' => 'If youre looking to buy a fast gaming laptop thats also a stellar value, thats usually a tough feat to pull off. However, at just under 1,200, HPs Omen 15t definitely qualifies. ',
        'body' => 'If youre looking to buy a fast gaming laptop thats also a stellar value, thats usually a tough feat to pull off. However, at just under 1,200, HPs Omen 15t definitely qualifies. This 15-inch laptop is stocked with an 8th Gen Intel Core i7 processor, a 144Hz IPS display, and, most impressively for the price, Nvidias GeForce GTX 1070.

            As noted on deal aggregation site Slickdeals, you will have to customize the Omen 15ts specifications over at HPs site to get this configuration, then add the promo code 20GAMERHOL at checkout to see the price fall below 1,200. Shipping is currently free, though the deal is set to end on Saturday, December 15th.',
        'feature_image' => 'blog-3.png',
        'thumbnail_image' => 'blog-3.png',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

    	DB::table('blog')->insert([
        'title' => 'Intel unveils Foveros 3D chip stacking and new 10nm chiplets',
        'excerpt' => 'At an Architecture Day event hosted this week, Intel articulated an unusually lucid strategy for its development of future processors, most of which will revolve around fragmenting the various elements of a modern CPU into individual, stackable chiplets.',
        'body' => 'At an Architecture Day event hosted this week, Intel articulated an unusually lucid strategy for its development of future processors, most of which will revolve around fragmenting the various elements of a modern CPU into individual, stackable chiplets. Intels big goal for late 2019 is to offer products built on what it calls Foveros 3D stacking an industry-first implementation of stacked processing components inside a chip. Weve already seen stacked memory; now, Intel is doing something similar with the CPU, allowing its designers to essentially drop in extra processing muscle atop an already assembled chip die. So your on-die memory, power regulation, graphics, and AI processing can all constitute separate chiplets, some of which can be stacked atop one another. The benefits of greater computational density and flexibility are obvious, but this modular approach also helps Intel skirt one of its biggest challenges: building full chips at 10nm scale.

            Intels previous 10nm road maps have consistently and repeatedly slipped, and theres good reason to believe that the company faces insurmountable engineering challenges on that project. An October report from SemiAccurate even suggested that Intel has canceled its 10nm plans altogether, though the grand old chipmaker denied the rumor and said it was making good progress on 10nm. The two may, in fact, both be true, judging from Intels new disclosures. On the way to Foveros, Intel suggests it will do something it calls 2D stacking, which is a separation of the various processor components into smaller chiplets, each of which can be manufactured using a different production node. Thus, Intel could deliver nominally 10nm CPUs, which will nonetheless have various 14nm and 22nm chiplet modules within them',
        'feature_image' => 'blog-4.png',
        'thumbnail_image' => 'blog-4.png',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

       

    
    }
}

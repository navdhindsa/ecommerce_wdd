<?php

use Illuminate\Database\Seeder;

class BillingAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('billing_addresses')->insert(
            [
                'user_id' => '1',
                'street_1' => '250 Colony St.',
                'street_2' => 'Apt# 510',
                'city' => 'Winnipeg',
                'province' => 'MB',
                'country' => 'Canada',
                'postal_code' => 'R3C3L8',
            ]);
        DB::table('billing_addresses')->insert(
            [
                'user_id' => '2',
                'street_1' => '250 Colony St.',
                'street_2' => 'Apt# 510',
                'city' => 'Winnipeg',
                'province' => 'MB',
                'country' => 'Canada',
                'postal_code' => 'R3C3L8',
            ]);
    }
}

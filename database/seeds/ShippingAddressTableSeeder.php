<?php

use Illuminate\Database\Seeder;

class ShippingAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping_addresses')->insert(
            [
                'user_id' => '1',
                'street_1' => '251 Colony St.',
                'street_2' => 'Apt# 511',
                'city' => 'Winnipeg',
                'province' => 'MB',
                'country' => 'Canada',
                'postal_code' => 'R3C 3L8',
            ]);
        DB::table('shipping_addresses')->insert(
            [
                'user_id' => '2',
                'street_1' => '251 Colony St.',
                'street_2' => 'Apt# 511',
                'city' => 'Winnipeg',
                'province' => 'MB',
                'country' => 'Canada',
                'postal_code' => 'R3C 3L8',
            ]);
    }
}

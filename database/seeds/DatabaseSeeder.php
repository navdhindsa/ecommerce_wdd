<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CategoryOptionsTableSeeder::class);
        $this->call(BlogTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ManufacturersTableSeeder::class);
        $this->call(ShippingTableSeeder::class);
        $this->call(TaxTableSeeder::class);
        $this->call(BillingAddressTableSeeder::class);
        $this->call(ShippingAddressTableSeeder::class);
        $this->call(ProductOptionsTableSeeder::class);
    }
}

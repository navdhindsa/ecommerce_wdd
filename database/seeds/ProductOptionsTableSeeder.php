<?php

use Illuminate\Database\Seeder;
use App\ProductOption;

class ProductOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductOption::create( [
        'id'=>1,
        'product_id'=>1,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:40',
        'updated_at'=>'2018-12-08 22:49:40'
        ] );
                    
        ProductOption::create( [
        'id'=>2,
        'product_id'=>1,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>3,
        'product_id'=>1,
        'category_option_id'=>54,
        'value'=>'3 GB',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>4,
        'product_id'=>1,
        'category_option_id'=>55,
        'value'=>'Android 5.1',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>5,
        'product_id'=>1,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>6,
        'product_id'=>1,
        'category_option_id'=>57,
        'value'=>'Li-Ion 2550',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>7,
        'product_id'=>1,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>8,
        'product_id'=>1,
        'category_option_id'=>59,
        'value'=>'EDGE, EV-DO, GPRS, HSDPA, HSPA+, HSUPA, LTE',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>9,
        'product_id'=>1,
        'category_option_id'=>60,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>10,
        'product_id'=>1,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>11,
        'product_id'=>1,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>12,
        'product_id'=>1,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>13,
        'product_id'=>1,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>14,
        'product_id'=>1,
        'category_option_id'=>65,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>15,
        'product_id'=>1,
        'category_option_id'=>66,
        'value'=>'16 MP',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>16,
        'product_id'=>1,
        'category_option_id'=>67,
        'value'=>'5 MP',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>17,
        'product_id'=>1,
        'category_option_id'=>68,
        'value'=>'na',
        'created_at'=>'2018-12-08 22:49:41',
        'updated_at'=>'2018-12-08 22:49:41'
        ] );
                    
        ProductOption::create( [
        'id'=>18,
        'product_id'=>1,
        'category_option_id'=>69,
        'value'=>'na',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>19,
        'product_id'=>1,
        'category_option_id'=>70,
        'value'=>'1440 x 2560 pixels ',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>20,
        'product_id'=>1,
        'category_option_id'=>71,
        'value'=>'5.1 in',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>21,
        'product_id'=>1,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>22,
        'product_id'=>1,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>23,
        'product_id'=>1,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>24,
        'product_id'=>1,
        'category_option_id'=>75,
        'value'=>'black',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>25,
        'product_id'=>1,
        'category_option_id'=>76,
        'value'=>'5.65 (W) x 2.78 (H) x 0.27 (D) in',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>26,
        'product_id'=>1,
        'category_option_id'=>77,
        'value'=>'14.34 (W) x 7.05 (H) x 0.68 (D) cm',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>27,
        'product_id'=>1,
        'category_option_id'=>78,
        'value'=>'0.138 kg',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>28,
        'product_id'=>1,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>29,
        'product_id'=>1,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-08 22:49:42',
        'updated_at'=>'2018-12-08 22:49:42'
        ] );
                    
        ProductOption::create( [
        'id'=>30,
        'product_id'=>2,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:55',
        'updated_at'=>'2018-12-08 23:55:55'
        ] );
                    
        ProductOption::create( [
        'id'=>31,
        'product_id'=>2,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:55',
        'updated_at'=>'2018-12-08 23:55:55'
        ] );
                    
        ProductOption::create( [
        'id'=>32,
        'product_id'=>2,
        'category_option_id'=>54,
        'value'=>'4 GB',
        'created_at'=>'2018-12-08 23:55:55',
        'updated_at'=>'2018-12-08 23:55:55'
        ] );
                    
        ProductOption::create( [
        'id'=>33,
        'product_id'=>2,
        'category_option_id'=>55,
        'value'=>'Android 8.0',
        'created_at'=>'2018-12-08 23:55:55',
        'updated_at'=>'2018-12-08 23:55:55'
        ] );
                    
        ProductOption::create( [
        'id'=>34,
        'product_id'=>2,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>35,
        'product_id'=>2,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3520 mAh battery',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>36,
        'product_id'=>2,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>37,
        'product_id'=>2,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>38,
        'product_id'=>2,
        'category_option_id'=>60,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>39,
        'product_id'=>2,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>40,
        'product_id'=>2,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>41,
        'product_id'=>2,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>42,
        'product_id'=>2,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>43,
        'product_id'=>2,
        'category_option_id'=>65,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>44,
        'product_id'=>2,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>45,
        'product_id'=>2,
        'category_option_id'=>67,
        'value'=>'8 MP',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>46,
        'product_id'=>2,
        'category_option_id'=>68,
        'value'=>'2160p@30fps (gyro-EIS)',
        'created_at'=>'2018-12-08 23:55:56',
        'updated_at'=>'2018-12-08 23:55:56'
        ] );
                    
        ProductOption::create( [
        'id'=>47,
        'product_id'=>2,
        'category_option_id'=>69,
        'value'=>'2160p@30fps (gyro-EIS)',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>48,
        'product_id'=>2,
        'category_option_id'=>70,
        'value'=>'1440 x 2880 pixels, 18:9 ratio (~538 ppi density)',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>49,
        'product_id'=>2,
        'category_option_id'=>71,
        'value'=>'6.0 inches, 92.6 cm2 (~76.4% screen-to-body ratio)',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>50,
        'product_id'=>2,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>51,
        'product_id'=>2,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>52,
        'product_id'=>2,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>53,
        'product_id'=>2,
        'category_option_id'=>75,
        'value'=>'Just Black, Black & White',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>54,
        'product_id'=>2,
        'category_option_id'=>76,
        'value'=>'(6.22 x 3.02 x 0.31 in)',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>55,
        'product_id'=>2,
        'category_option_id'=>77,
        'value'=>'15.9 x 7.6. x 0.8 mm',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>56,
        'product_id'=>2,
        'category_option_id'=>78,
        'value'=>'175 g (6.17 oz)',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>57,
        'product_id'=>2,
        'category_option_id'=>79,
        'value'=>'Yes',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>58,
        'product_id'=>2,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-08 23:55:57',
        'updated_at'=>'2018-12-08 23:55:57'
        ] );
                    
        ProductOption::create( [
        'id'=>59,
        'product_id'=>3,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:39',
        'updated_at'=>'2018-12-09 02:07:39'
        ] );
                    
        ProductOption::create( [
        'id'=>60,
        'product_id'=>3,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>61,
        'product_id'=>3,
        'category_option_id'=>54,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>62,
        'product_id'=>3,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>63,
        'product_id'=>3,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>64,
        'product_id'=>3,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Po 3700 mAh battery',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>65,
        'product_id'=>3,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>66,
        'product_id'=>3,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / LTE',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>67,
        'product_id'=>3,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>68,
        'product_id'=>3,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>69,
        'product_id'=>3,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>70,
        'product_id'=>3,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>71,
        'product_id'=>3,
        'category_option_id'=>64,
        'value'=>'2.0, Type-C 1.0 reversible connector, USB On-The-Go',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>72,
        'product_id'=>3,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>73,
        'product_id'=>3,
        'category_option_id'=>66,
        'value'=>'16 MP, f/1.7, 25mm (wide), 1/2.6\", 1.22µm, OIS, PDAF',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>74,
        'product_id'=>3,
        'category_option_id'=>67,
        'value'=>'16 MP, f/2.0, 25mm (wide), 1/3.1\", 1.0µm',
        'created_at'=>'2018-12-09 02:07:40',
        'updated_at'=>'2018-12-09 02:07:40'
        ] );
                    
        ProductOption::create( [
        'id'=>75,
        'product_id'=>3,
        'category_option_id'=>68,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>76,
        'product_id'=>3,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>77,
        'product_id'=>3,
        'category_option_id'=>70,
        'value'=>'1080 x 2340 pixels, 19.5:9 ratio (~402 ppi density)',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>78,
        'product_id'=>3,
        'category_option_id'=>71,
        'value'=>'6.41 inches, 100.9 cm2 (~85.6% screen-to-body ratio)',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>79,
        'product_id'=>3,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>80,
        'product_id'=>3,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>81,
        'product_id'=>3,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>82,
        'product_id'=>3,
        'category_option_id'=>75,
        'value'=>'Thunder Purple, Mirror Black, Midnight Black',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>83,
        'product_id'=>3,
        'category_option_id'=>76,
        'value'=>'6.20 x 2.94 x 0.32 in',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>84,
        'product_id'=>3,
        'category_option_id'=>77,
        'value'=>'157.5 x 74.8 x 8.2 mm',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>85,
        'product_id'=>3,
        'category_option_id'=>78,
        'value'=>'185 g (6.53 oz)',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>86,
        'product_id'=>3,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>87,
        'product_id'=>3,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:07:41',
        'updated_at'=>'2018-12-09 02:07:41'
        ] );
                    
        ProductOption::create( [
        'id'=>88,
        'product_id'=>4,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>89,
        'product_id'=>4,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>90,
        'product_id'=>4,
        'category_option_id'=>54,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>91,
        'product_id'=>4,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>92,
        'product_id'=>4,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>93,
        'product_id'=>4,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3300 mAh battery',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>94,
        'product_id'=>4,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>95,
        'product_id'=>4,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>96,
        'product_id'=>4,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
                    
        ProductOption::create( [
        'id'=>97,
        'product_id'=>4,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>98,
        'product_id'=>4,
        'category_option_id'=>62,
        'value'=>'y',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>99,
        'product_id'=>4,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>100,
        'product_id'=>4,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>101,
        'product_id'=>4,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>102,
        'product_id'=>4,
        'category_option_id'=>66,
        'value'=>'16 MP, f/1.7, 25mm (wide), 1/2.6\", 1.22µm, OIS, PDAF',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>103,
        'product_id'=>4,
        'category_option_id'=>67,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>104,
        'product_id'=>4,
        'category_option_id'=>68,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>105,
        'product_id'=>4,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>106,
        'product_id'=>4,
        'category_option_id'=>70,
        'value'=>'1440 x 2960 pixels, 18.5:9 ratio (~523 ppi density)',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>107,
        'product_id'=>4,
        'category_option_id'=>71,
        'value'=>'6.3 inches, 100.3 cm2 (~82.8% screen-to-body ratio)',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>108,
        'product_id'=>4,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>109,
        'product_id'=>4,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>110,
        'product_id'=>4,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>111,
        'product_id'=>4,
        'category_option_id'=>75,
        'value'=>'Black, White, Blue, Pink, Red',
        'created_at'=>'2018-12-09 02:24:55',
        'updated_at'=>'2018-12-09 02:24:55'
        ] );
                    
        ProductOption::create( [
        'id'=>112,
        'product_id'=>4,
        'category_option_id'=>76,
        'value'=>'6.22 x 3.02 x 0.31 in',
        'created_at'=>'2018-12-09 02:24:56',
        'updated_at'=>'2018-12-09 02:24:56'
        ] );
                    
        ProductOption::create( [
        'id'=>113,
        'product_id'=>4,
        'category_option_id'=>77,
        'value'=>'158 x 76.7 x 7.9 mm',
        'created_at'=>'2018-12-09 02:24:56',
        'updated_at'=>'2018-12-09 02:24:56'
        ] );
                    
        ProductOption::create( [
        'id'=>114,
        'product_id'=>4,
        'category_option_id'=>78,
        'value'=>'184 g',
        'created_at'=>'2018-12-09 02:24:56',
        'updated_at'=>'2018-12-09 02:24:56'
        ] );
                    
        ProductOption::create( [
        'id'=>115,
        'product_id'=>4,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:56',
        'updated_at'=>'2018-12-09 02:24:56'
        ] );
                    
        ProductOption::create( [
        'id'=>116,
        'product_id'=>4,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:24:56',
        'updated_at'=>'2018-12-09 02:24:56'
        ] );
                    
        ProductOption::create( [
        'id'=>117,
        'product_id'=>5,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:32',
        'updated_at'=>'2018-12-09 02:45:32'
        ] );
                    
        ProductOption::create( [
        'id'=>118,
        'product_id'=>5,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>119,
        'product_id'=>5,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>120,
        'product_id'=>5,
        'category_option_id'=>55,
        'value'=>'Android 8.1',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>121,
        'product_id'=>5,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>122,
        'product_id'=>5,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3300 mAh battery',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>123,
        'product_id'=>5,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>124,
        'product_id'=>5,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>125,
        'product_id'=>5,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>126,
        'product_id'=>5,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>127,
        'product_id'=>5,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>128,
        'product_id'=>5,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>129,
        'product_id'=>5,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>130,
        'product_id'=>5,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>131,
        'product_id'=>5,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>132,
        'product_id'=>5,
        'category_option_id'=>67,
        'value'=>'8 MP',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>133,
        'product_id'=>5,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>134,
        'product_id'=>5,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 02:45:33',
        'updated_at'=>'2018-12-09 02:45:33'
        ] );
                    
        ProductOption::create( [
        'id'=>135,
        'product_id'=>5,
        'category_option_id'=>70,
        'value'=>'1080 x 2246 pixels, 18.7:9 ratio (~402 ppi density)',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>136,
        'product_id'=>5,
        'category_option_id'=>71,
        'value'=>'6.2 inches, 96.9 cm2 (~83.6% screen-to-body ratio)',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>137,
        'product_id'=>5,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>138,
        'product_id'=>5,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>139,
        'product_id'=>5,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>140,
        'product_id'=>5,
        'category_option_id'=>75,
        'value'=>'Black, White, Blue, Pink, Red',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>141,
        'product_id'=>5,
        'category_option_id'=>76,
        'value'=>'6.02 x 2.98 x 0.31 in',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>142,
        'product_id'=>5,
        'category_option_id'=>77,
        'value'=>'153 x 75.7 x 7.9 mm',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>143,
        'product_id'=>5,
        'category_option_id'=>78,
        'value'=>'165 g (5.82 oz)',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>144,
        'product_id'=>5,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>145,
        'product_id'=>5,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:45:34',
        'updated_at'=>'2018-12-09 02:45:34'
        ] );
                    
        ProductOption::create( [
        'id'=>146,
        'product_id'=>6,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>147,
        'product_id'=>6,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>148,
        'product_id'=>6,
        'category_option_id'=>54,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>149,
        'product_id'=>6,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>150,
        'product_id'=>6,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>151,
        'product_id'=>6,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3300 mAh battery',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>152,
        'product_id'=>6,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>153,
        'product_id'=>6,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>154,
        'product_id'=>6,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>155,
        'product_id'=>6,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>156,
        'product_id'=>6,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>157,
        'product_id'=>6,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>158,
        'product_id'=>6,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>159,
        'product_id'=>6,
        'category_option_id'=>65,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>160,
        'product_id'=>6,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>161,
        'product_id'=>6,
        'category_option_id'=>67,
        'value'=>'8 MP',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>162,
        'product_id'=>6,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>163,
        'product_id'=>6,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>164,
        'product_id'=>6,
        'category_option_id'=>70,
        'value'=>'1080 x 2220 pixels',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>165,
        'product_id'=>6,
        'category_option_id'=>71,
        'value'=>'6.0\" (15.24 cm)',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>166,
        'product_id'=>6,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>167,
        'product_id'=>6,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>168,
        'product_id'=>6,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>169,
        'product_id'=>6,
        'category_option_id'=>75,
        'value'=>'Black, White',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>170,
        'product_id'=>6,
        'category_option_id'=>76,
        'value'=>'5.92 x 2.83 x 0.33 in',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>171,
        'product_id'=>6,
        'category_option_id'=>77,
        'value'=>'150.3 x 71.8 x 8.4 mm',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>172,
        'product_id'=>6,
        'category_option_id'=>78,
        'value'=>'156 g (5.50 oz)',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>173,
        'product_id'=>6,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>174,
        'product_id'=>6,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 02:54:08',
        'updated_at'=>'2018-12-09 02:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>175,
        'product_id'=>7,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>176,
        'product_id'=>7,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>177,
        'product_id'=>7,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>178,
        'product_id'=>7,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>179,
        'product_id'=>7,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>180,
        'product_id'=>7,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3300 mAh battery',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>181,
        'product_id'=>7,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>182,
        'product_id'=>7,
        'category_option_id'=>59,
        'value'=>'GSM / HSPA / LTE',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
                    
        ProductOption::create( [
        'id'=>183,
        'product_id'=>7,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>184,
        'product_id'=>7,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>185,
        'product_id'=>7,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>186,
        'product_id'=>7,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>187,
        'product_id'=>7,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>188,
        'product_id'=>7,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>189,
        'product_id'=>7,
        'category_option_id'=>66,
        'value'=>'40 MP',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>190,
        'product_id'=>7,
        'category_option_id'=>67,
        'value'=>'24 MP',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>191,
        'product_id'=>7,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>192,
        'product_id'=>7,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>193,
        'product_id'=>7,
        'category_option_id'=>70,
        'value'=>'1080 x 2244 pixels, 18.7:9 ratio (~346 ppi density)',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>194,
        'product_id'=>7,
        'category_option_id'=>71,
        'value'=>'7.2 inches, 130.7 cm2 (~87.6% screen-to-body ratio)',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>195,
        'product_id'=>7,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>196,
        'product_id'=>7,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>197,
        'product_id'=>7,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>198,
        'product_id'=>7,
        'category_option_id'=>75,
        'value'=>'Black, White, Blue, Pink, Red',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>199,
        'product_id'=>7,
        'category_option_id'=>76,
        'value'=>'6.87 x 3.36 x 0.32 in',
        'created_at'=>'2018-12-09 03:01:26',
        'updated_at'=>'2018-12-09 03:01:26'
        ] );
                    
        ProductOption::create( [
        'id'=>200,
        'product_id'=>7,
        'category_option_id'=>77,
        'value'=>'174.6 x 85.4 x 8.2 mm',
        'created_at'=>'2018-12-09 03:01:27',
        'updated_at'=>'2018-12-09 03:01:27'
        ] );
                    
        ProductOption::create( [
        'id'=>201,
        'product_id'=>7,
        'category_option_id'=>78,
        'value'=>'232 g (8.18 oz)',
        'created_at'=>'2018-12-09 03:01:27',
        'updated_at'=>'2018-12-09 03:01:27'
        ] );
                    
        ProductOption::create( [
        'id'=>202,
        'product_id'=>7,
        'category_option_id'=>79,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 03:01:27',
        'updated_at'=>'2018-12-09 03:01:27'
        ] );
                    
        ProductOption::create( [
        'id'=>203,
        'product_id'=>7,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:01:27',
        'updated_at'=>'2018-12-09 03:01:27'
        ] );
                    
        ProductOption::create( [
        'id'=>204,
        'product_id'=>8,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>205,
        'product_id'=>8,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>206,
        'product_id'=>8,
        'category_option_id'=>54,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>207,
        'product_id'=>8,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>208,
        'product_id'=>8,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>209,
        'product_id'=>8,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3300 mAh battery',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>210,
        'product_id'=>8,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
                    
        ProductOption::create( [
        'id'=>211,
        'product_id'=>8,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>212,
        'product_id'=>8,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>213,
        'product_id'=>8,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>214,
        'product_id'=>8,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>215,
        'product_id'=>8,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>216,
        'product_id'=>8,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>217,
        'product_id'=>8,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>218,
        'product_id'=>8,
        'category_option_id'=>66,
        'value'=>'16 MP, f/1.7, 25mm (wide), 1/2.6\", 1.22µm, OIS, PDAF',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>219,
        'product_id'=>8,
        'category_option_id'=>67,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>220,
        'product_id'=>8,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>221,
        'product_id'=>8,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>222,
        'product_id'=>8,
        'category_option_id'=>70,
        'value'=>'1080 x 2340 pixels, 19.5:9 ratio (~401 ppi density)',
        'created_at'=>'2018-12-09 03:11:13',
        'updated_at'=>'2018-12-09 03:11:13'
        ] );
                    
        ProductOption::create( [
        'id'=>223,
        'product_id'=>8,
        'category_option_id'=>71,
        'value'=>'6.0\" (15.24 cm)',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>224,
        'product_id'=>8,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>225,
        'product_id'=>8,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>226,
        'product_id'=>8,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>227,
        'product_id'=>8,
        'category_option_id'=>75,
        'value'=>'Black, White',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>228,
        'product_id'=>8,
        'category_option_id'=>76,
        'value'=>'6.15 x 3.01 x 0.33 in',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>229,
        'product_id'=>8,
        'category_option_id'=>77,
        'value'=>'157.5 x 74.8 x 8.2 mm',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>230,
        'product_id'=>8,
        'category_option_id'=>78,
        'value'=>'184 g',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>231,
        'product_id'=>8,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>232,
        'product_id'=>8,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 03:11:14',
        'updated_at'=>'2018-12-09 03:11:14'
        ] );
                    
        ProductOption::create( [
        'id'=>233,
        'product_id'=>9,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:25',
        'updated_at'=>'2018-12-09 04:26:25'
        ] );
                    
        ProductOption::create( [
        'id'=>234,
        'product_id'=>9,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>235,
        'product_id'=>9,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>236,
        'product_id'=>9,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>237,
        'product_id'=>9,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>238,
        'product_id'=>9,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Po 3500 mAh battery',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>239,
        'product_id'=>9,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>240,
        'product_id'=>9,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / LTE',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>241,
        'product_id'=>9,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>242,
        'product_id'=>9,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>243,
        'product_id'=>9,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>244,
        'product_id'=>9,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>245,
        'product_id'=>9,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>246,
        'product_id'=>9,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>247,
        'product_id'=>9,
        'category_option_id'=>66,
        'value'=>'16 MP, f/1.7, 25mm (wide), 1/2.6\", 1.22µm, OIS, PDAF',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>248,
        'product_id'=>9,
        'category_option_id'=>67,
        'value'=>'16 MP, f/2.0, 25mm (wide), 1/3.1\", 1.0µm',
        'created_at'=>'2018-12-09 04:26:26',
        'updated_at'=>'2018-12-09 04:26:26'
        ] );
                    
        ProductOption::create( [
        'id'=>249,
        'product_id'=>9,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>250,
        'product_id'=>9,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>251,
        'product_id'=>9,
        'category_option_id'=>70,
        'value'=>'1080 x 2340 pixels, 19.5:9 ratio (~403 ppi density)',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>252,
        'product_id'=>9,
        'category_option_id'=>71,
        'value'=>'6.39 inches, 100.2 cm2 (~84.8% screen-to-body ratio)',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>253,
        'product_id'=>9,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>254,
        'product_id'=>9,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>255,
        'product_id'=>9,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>256,
        'product_id'=>9,
        'category_option_id'=>75,
        'value'=>'Blue, Purple',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>257,
        'product_id'=>9,
        'category_option_id'=>76,
        'value'=>'6.19 x 2.96 x 0.33 in',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>258,
        'product_id'=>9,
        'category_option_id'=>77,
        'value'=>'157.3 x 75.1 x 8.3 mm',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>259,
        'product_id'=>9,
        'category_option_id'=>78,
        'value'=>'206 g (7.27 oz)',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>260,
        'product_id'=>9,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>261,
        'product_id'=>9,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:26:27',
        'updated_at'=>'2018-12-09 04:26:27'
        ] );
                    
        ProductOption::create( [
        'id'=>262,
        'product_id'=>10,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>263,
        'product_id'=>10,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>264,
        'product_id'=>10,
        'category_option_id'=>54,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>265,
        'product_id'=>10,
        'category_option_id'=>55,
        'value'=>'Android 7.11',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>266,
        'product_id'=>10,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>267,
        'product_id'=>10,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Po 3300 mAh battery',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>268,
        'product_id'=>10,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>269,
        'product_id'=>10,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>270,
        'product_id'=>10,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>271,
        'product_id'=>10,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>272,
        'product_id'=>10,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>273,
        'product_id'=>10,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>274,
        'product_id'=>10,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
                    
        ProductOption::create( [
        'id'=>275,
        'product_id'=>10,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>276,
        'product_id'=>10,
        'category_option_id'=>66,
        'value'=>'16 MP',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>277,
        'product_id'=>10,
        'category_option_id'=>67,
        'value'=>'16 MP',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>278,
        'product_id'=>10,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>279,
        'product_id'=>10,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>280,
        'product_id'=>10,
        'category_option_id'=>70,
        'value'=>'1080 x 2160 pixels, 18:9 ratio (~401 ppi density)',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>281,
        'product_id'=>10,
        'category_option_id'=>71,
        'value'=>'6.0\" (15.24 cm)',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>282,
        'product_id'=>10,
        'category_option_id'=>72,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>283,
        'product_id'=>10,
        'category_option_id'=>73,
        'value'=>'No',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>284,
        'product_id'=>10,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>285,
        'product_id'=>10,
        'category_option_id'=>75,
        'value'=>'Just Black, Black & White',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>286,
        'product_id'=>10,
        'category_option_id'=>76,
        'value'=>'6.15 x 2.95 x 0.29 in',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>287,
        'product_id'=>10,
        'category_option_id'=>77,
        'value'=>'156.1 x 75 x 7.3 mm',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>288,
        'product_id'=>10,
        'category_option_id'=>78,
        'value'=>'165 g (5.82 oz)',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>289,
        'product_id'=>10,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>290,
        'product_id'=>10,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:31:52',
        'updated_at'=>'2018-12-09 04:31:52'
        ] );
                    
        ProductOption::create( [
        'id'=>291,
        'product_id'=>11,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:27',
        'updated_at'=>'2018-12-09 04:37:27'
        ] );
                    
        ProductOption::create( [
        'id'=>292,
        'product_id'=>11,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>293,
        'product_id'=>11,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>294,
        'product_id'=>11,
        'category_option_id'=>55,
        'value'=>'Android 9.0',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>295,
        'product_id'=>11,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>296,
        'product_id'=>11,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Po 3300 mAh battery',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>297,
        'product_id'=>11,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>298,
        'product_id'=>11,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>299,
        'product_id'=>11,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>300,
        'product_id'=>11,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>301,
        'product_id'=>11,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>302,
        'product_id'=>11,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>303,
        'product_id'=>11,
        'category_option_id'=>64,
        'value'=>'2.0, Type-C 1.0 reversible connector, USB On-The-Go',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>304,
        'product_id'=>11,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>305,
        'product_id'=>11,
        'category_option_id'=>66,
        'value'=>'24 MP',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>306,
        'product_id'=>11,
        'category_option_id'=>67,
        'value'=>'8 MP',
        'created_at'=>'2018-12-09 04:37:28',
        'updated_at'=>'2018-12-09 04:37:28'
        ] );
                    
        ProductOption::create( [
        'id'=>307,
        'product_id'=>11,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>308,
        'product_id'=>11,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>309,
        'product_id'=>11,
        'category_option_id'=>70,
        'value'=>'1080 x 2220 pixels, 18.5:9 ratio (~392 ppi density)',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>310,
        'product_id'=>11,
        'category_option_id'=>71,
        'value'=>'6.3 inches, 100.3 cm2 (~82.8% screen-to-body ratio)',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>311,
        'product_id'=>11,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>312,
        'product_id'=>11,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>313,
        'product_id'=>11,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>314,
        'product_id'=>11,
        'category_option_id'=>75,
        'value'=>'Caviar Black, Lemonade Blue, Bubblegum Pink',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>315,
        'product_id'=>11,
        'category_option_id'=>76,
        'value'=>'6.40 x 3.03 x 0.31 in',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>316,
        'product_id'=>11,
        'category_option_id'=>77,
        'value'=>'162.5 x 77 x 7.8 mm',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>317,
        'product_id'=>11,
        'category_option_id'=>78,
        'value'=>'183 g (6.46 oz)',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>318,
        'product_id'=>11,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>319,
        'product_id'=>11,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:37:29',
        'updated_at'=>'2018-12-09 04:37:29'
        ] );
                    
        ProductOption::create( [
        'id'=>320,
        'product_id'=>12,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:07',
        'updated_at'=>'2018-12-09 04:48:07'
        ] );
                    
        ProductOption::create( [
        'id'=>321,
        'product_id'=>12,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:07',
        'updated_at'=>'2018-12-09 04:48:07'
        ] );
                    
        ProductOption::create( [
        'id'=>322,
        'product_id'=>12,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 04:48:07',
        'updated_at'=>'2018-12-09 04:48:07'
        ] );
                    
        ProductOption::create( [
        'id'=>323,
        'product_id'=>12,
        'category_option_id'=>55,
        'value'=>'Android 8.0',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>324,
        'product_id'=>12,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>325,
        'product_id'=>12,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3520 mAh battery',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>326,
        'product_id'=>12,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>327,
        'product_id'=>12,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>328,
        'product_id'=>12,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>329,
        'product_id'=>12,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>330,
        'product_id'=>12,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>331,
        'product_id'=>12,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>332,
        'product_id'=>12,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>333,
        'product_id'=>12,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>334,
        'product_id'=>12,
        'category_option_id'=>66,
        'value'=>'12',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>335,
        'product_id'=>12,
        'category_option_id'=>67,
        'value'=>'5 MP',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>336,
        'product_id'=>12,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>337,
        'product_id'=>12,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>338,
        'product_id'=>12,
        'category_option_id'=>70,
        'value'=>'1080 x 2160 pixels, 18:9 ratio (~401 ppi density)',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>339,
        'product_id'=>12,
        'category_option_id'=>71,
        'value'=>'6.0\" (15.24 cm)',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>340,
        'product_id'=>12,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>341,
        'product_id'=>12,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>342,
        'product_id'=>12,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>343,
        'product_id'=>12,
        'category_option_id'=>75,
        'value'=>'Deep Indigo, Onyx Black (128/6GB only)',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>344,
        'product_id'=>12,
        'category_option_id'=>76,
        'value'=>'6.16 x 3.01 x 0.27 in',
        'created_at'=>'2018-12-09 04:48:08',
        'updated_at'=>'2018-12-09 04:48:08'
        ] );
                    
        ProductOption::create( [
        'id'=>345,
        'product_id'=>12,
        'category_option_id'=>77,
        'value'=>'156.5 x 76.5 x 6.8 mm',
        'created_at'=>'2018-12-09 04:48:09',
        'updated_at'=>'2018-12-09 04:48:09'
        ] );
                    
        ProductOption::create( [
        'id'=>346,
        'product_id'=>12,
        'category_option_id'=>78,
        'value'=>'184 g',
        'created_at'=>'2018-12-09 04:48:09',
        'updated_at'=>'2018-12-09 04:48:09'
        ] );
                    
        ProductOption::create( [
        'id'=>347,
        'product_id'=>12,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:09',
        'updated_at'=>'2018-12-09 04:48:09'
        ] );
                    
        ProductOption::create( [
        'id'=>348,
        'product_id'=>12,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:48:09',
        'updated_at'=>'2018-12-09 04:48:09'
        ] );
                    
        ProductOption::create( [
        'id'=>349,
        'product_id'=>13,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:06',
        'updated_at'=>'2018-12-09 04:54:06'
        ] );
                    
        ProductOption::create( [
        'id'=>350,
        'product_id'=>13,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:06',
        'updated_at'=>'2018-12-09 04:54:06'
        ] );
                    
        ProductOption::create( [
        'id'=>351,
        'product_id'=>13,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 04:54:06',
        'updated_at'=>'2018-12-09 04:54:06'
        ] );
                    
        ProductOption::create( [
        'id'=>352,
        'product_id'=>13,
        'category_option_id'=>55,
        'value'=>'Android 8.0',
        'created_at'=>'2018-12-09 04:54:06',
        'updated_at'=>'2018-12-09 04:54:06'
        ] );
                    
        ProductOption::create( [
        'id'=>353,
        'product_id'=>13,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>354,
        'product_id'=>13,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3520 mAh battery',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>355,
        'product_id'=>13,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>356,
        'product_id'=>13,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>357,
        'product_id'=>13,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>358,
        'product_id'=>13,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>359,
        'product_id'=>13,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>360,
        'product_id'=>13,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>361,
        'product_id'=>13,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>362,
        'product_id'=>13,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>363,
        'product_id'=>13,
        'category_option_id'=>66,
        'value'=>'16 MP',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>364,
        'product_id'=>13,
        'category_option_id'=>67,
        'value'=>'5 MP',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>365,
        'product_id'=>13,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>366,
        'product_id'=>13,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>367,
        'product_id'=>13,
        'category_option_id'=>70,
        'value'=>'1080 x 2160 pixels, 18:9 ratio (~401 ppi density)',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>368,
        'product_id'=>13,
        'category_option_id'=>71,
        'value'=>'6.0\" (15.24 cm)',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>369,
        'product_id'=>13,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>370,
        'product_id'=>13,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>371,
        'product_id'=>13,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>372,
        'product_id'=>13,
        'category_option_id'=>75,
        'value'=>'Black, White, Blue, Pink, Red',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>373,
        'product_id'=>13,
        'category_option_id'=>76,
        'value'=>'6.24 x 2.97 x 0.33 in',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>374,
        'product_id'=>13,
        'category_option_id'=>77,
        'value'=>'158.5 x 75.4 x 8.3 mm',
        'created_at'=>'2018-12-09 04:54:07',
        'updated_at'=>'2018-12-09 04:54:07'
        ] );
                    
        ProductOption::create( [
        'id'=>375,
        'product_id'=>13,
        'category_option_id'=>78,
        'value'=>'175 g (6.17 oz)',
        'created_at'=>'2018-12-09 04:54:08',
        'updated_at'=>'2018-12-09 04:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>376,
        'product_id'=>13,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:08',
        'updated_at'=>'2018-12-09 04:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>377,
        'product_id'=>13,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 04:54:08',
        'updated_at'=>'2018-12-09 04:54:08'
        ] );
                    
        ProductOption::create( [
        'id'=>378,
        'product_id'=>14,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:11',
        'updated_at'=>'2018-12-09 05:00:11'
        ] );
                    
        ProductOption::create( [
        'id'=>379,
        'product_id'=>14,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:11',
        'updated_at'=>'2018-12-09 05:00:11'
        ] );
                    
        ProductOption::create( [
        'id'=>380,
        'product_id'=>14,
        'category_option_id'=>54,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 05:00:11',
        'updated_at'=>'2018-12-09 05:00:11'
        ] );
                    
        ProductOption::create( [
        'id'=>381,
        'product_id'=>14,
        'category_option_id'=>55,
        'value'=>'Android 8.1',
        'created_at'=>'2018-12-09 05:00:11',
        'updated_at'=>'2018-12-09 05:00:11'
        ] );
                    
        ProductOption::create( [
        'id'=>382,
        'product_id'=>14,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>383,
        'product_id'=>14,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 3300 mAh battery',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>384,
        'product_id'=>14,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>385,
        'product_id'=>14,
        'category_option_id'=>59,
        'value'=>'GSM / HSPA / LTE',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>386,
        'product_id'=>14,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>387,
        'product_id'=>14,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>388,
        'product_id'=>14,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>389,
        'product_id'=>14,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>390,
        'product_id'=>14,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>391,
        'product_id'=>14,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>392,
        'product_id'=>14,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>393,
        'product_id'=>14,
        'category_option_id'=>67,
        'value'=>'5 MP',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>394,
        'product_id'=>14,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>395,
        'product_id'=>14,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>396,
        'product_id'=>14,
        'category_option_id'=>70,
        'value'=>'1080 x 2160 pixels, 18:9 ratio (~401 ppi density)',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>397,
        'product_id'=>14,
        'category_option_id'=>71,
        'value'=>'6.0\" (15.24 cm)',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>398,
        'product_id'=>14,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>399,
        'product_id'=>14,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>400,
        'product_id'=>14,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>401,
        'product_id'=>14,
        'category_option_id'=>75,
        'value'=>'Graphite Black, Steel Blue, Rosso Red, Armored Edition with Kevlar',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>402,
        'product_id'=>14,
        'category_option_id'=>76,
        'value'=>'6.12 x 2.96 x 0.35 in',
        'created_at'=>'2018-12-09 05:00:12',
        'updated_at'=>'2018-12-09 05:00:12'
        ] );
                    
        ProductOption::create( [
        'id'=>403,
        'product_id'=>14,
        'category_option_id'=>77,
        'value'=>'155.5 x 75.3 x 8.8 mm',
        'created_at'=>'2018-12-09 05:00:13',
        'updated_at'=>'2018-12-09 05:00:13'
        ] );
                    
        ProductOption::create( [
        'id'=>404,
        'product_id'=>14,
        'category_option_id'=>78,
        'value'=>'182 g',
        'created_at'=>'2018-12-09 05:00:13',
        'updated_at'=>'2018-12-09 05:00:13'
        ] );
                    
        ProductOption::create( [
        'id'=>405,
        'product_id'=>14,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:13',
        'updated_at'=>'2018-12-09 05:00:13'
        ] );
                    
        ProductOption::create( [
        'id'=>406,
        'product_id'=>14,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:00:13',
        'updated_at'=>'2018-12-09 05:00:13'
        ] );
                    
        ProductOption::create( [
        'id'=>407,
        'product_id'=>15,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>408,
        'product_id'=>15,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>409,
        'product_id'=>15,
        'category_option_id'=>54,
        'value'=>'6 GB RAM',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>410,
        'product_id'=>15,
        'category_option_id'=>55,
        'value'=>'Android 8.1 (Oreo)',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>411,
        'product_id'=>15,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>412,
        'product_id'=>15,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Po 4000 mAh battery',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>413,
        'product_id'=>15,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>414,
        'product_id'=>15,
        'category_option_id'=>59,
        'value'=>'GSM / HSPA / LTE',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>415,
        'product_id'=>15,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>416,
        'product_id'=>15,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>417,
        'product_id'=>15,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>418,
        'product_id'=>15,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>419,
        'product_id'=>15,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>420,
        'product_id'=>15,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>421,
        'product_id'=>15,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>422,
        'product_id'=>15,
        'category_option_id'=>67,
        'value'=>'7 MP',
        'created_at'=>'2018-12-09 05:07:30',
        'updated_at'=>'2018-12-09 05:07:30'
        ] );
                    
        ProductOption::create( [
        'id'=>423,
        'product_id'=>15,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>424,
        'product_id'=>15,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>425,
        'product_id'=>15,
        'category_option_id'=>70,
        'value'=>'1125 x 2436 pixels, 19.5:9 ratio (~458 ppi density)',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>426,
        'product_id'=>15,
        'category_option_id'=>71,
        'value'=>'5.8 inches, 84.4 cm2 (~82.9% screen-to-body ratio)',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>427,
        'product_id'=>15,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>428,
        'product_id'=>15,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>429,
        'product_id'=>15,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>430,
        'product_id'=>15,
        'category_option_id'=>75,
        'value'=>'Space Gray, Silver, Gold',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>431,
        'product_id'=>15,
        'category_option_id'=>76,
        'value'=>'5.65 x 2.79 x 0.30 in',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>432,
        'product_id'=>15,
        'category_option_id'=>77,
        'value'=>'143.6 x 70.9 x 7.7 mm',
        'created_at'=>'2018-12-09 05:07:31',
        'updated_at'=>'2018-12-09 05:07:31'
        ] );
                    
        ProductOption::create( [
        'id'=>433,
        'product_id'=>15,
        'category_option_id'=>78,
        'value'=>'173 g',
        'created_at'=>'2018-12-09 05:07:32',
        'updated_at'=>'2018-12-09 05:07:32'
        ] );
                    
        ProductOption::create( [
        'id'=>434,
        'product_id'=>15,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:32',
        'updated_at'=>'2018-12-09 05:07:32'
        ] );
                    
        ProductOption::create( [
        'id'=>435,
        'product_id'=>15,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:07:32',
        'updated_at'=>'2018-12-09 05:07:32'
        ] );
                    
        ProductOption::create( [
        'id'=>436,
        'product_id'=>16,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>437,
        'product_id'=>16,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>438,
        'product_id'=>16,
        'category_option_id'=>54,
        'value'=>'3 GB',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>439,
        'product_id'=>16,
        'category_option_id'=>55,
        'value'=>'Android 8.1 (Oreo)',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>440,
        'product_id'=>16,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>441,
        'product_id'=>16,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Po 4000 mAh battery',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>442,
        'product_id'=>16,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>443,
        'product_id'=>16,
        'category_option_id'=>59,
        'value'=>'GSM / HSPA / LTE',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
                    
        ProductOption::create( [
        'id'=>444,
        'product_id'=>16,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>445,
        'product_id'=>16,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>446,
        'product_id'=>16,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>447,
        'product_id'=>16,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>448,
        'product_id'=>16,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>449,
        'product_id'=>16,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>450,
        'product_id'=>16,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>451,
        'product_id'=>16,
        'category_option_id'=>67,
        'value'=>'7 MP',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>452,
        'product_id'=>16,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>453,
        'product_id'=>16,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>454,
        'product_id'=>16,
        'category_option_id'=>70,
        'value'=>'828 x 1792 pixels, 19.5:9 ratio (~326 ppi density)',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>455,
        'product_id'=>16,
        'category_option_id'=>71,
        'value'=>'150.9 x 75.7 x 8.3 mm (5.94 x 2.98 x 0.33 in)',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>456,
        'product_id'=>16,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>457,
        'product_id'=>16,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>458,
        'product_id'=>16,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:53',
        'updated_at'=>'2018-12-09 05:13:53'
        ] );
                    
        ProductOption::create( [
        'id'=>459,
        'product_id'=>16,
        'category_option_id'=>75,
        'value'=>'Black, Red, Yellow, Blue, Coral, White',
        'created_at'=>'2018-12-09 05:13:54',
        'updated_at'=>'2018-12-09 05:13:54'
        ] );
                    
        ProductOption::create( [
        'id'=>460,
        'product_id'=>16,
        'category_option_id'=>76,
        'value'=>'5.94 x 2.98 x 0.33 in',
        'created_at'=>'2018-12-09 05:13:54',
        'updated_at'=>'2018-12-09 05:13:54'
        ] );
                    
        ProductOption::create( [
        'id'=>461,
        'product_id'=>16,
        'category_option_id'=>77,
        'value'=>'150.9 x 75.7 x 8.3 mm',
        'created_at'=>'2018-12-09 05:13:54',
        'updated_at'=>'2018-12-09 05:13:54'
        ] );
                    
        ProductOption::create( [
        'id'=>462,
        'product_id'=>16,
        'category_option_id'=>78,
        'value'=>'194 g',
        'created_at'=>'2018-12-09 05:13:54',
        'updated_at'=>'2018-12-09 05:13:54'
        ] );
                    
        ProductOption::create( [
        'id'=>463,
        'product_id'=>16,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:54',
        'updated_at'=>'2018-12-09 05:13:54'
        ] );
                    
        ProductOption::create( [
        'id'=>464,
        'product_id'=>16,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:13:54',
        'updated_at'=>'2018-12-09 05:13:54'
        ] );
                    
        ProductOption::create( [
        'id'=>465,
        'product_id'=>17,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>466,
        'product_id'=>17,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>467,
        'product_id'=>17,
        'category_option_id'=>54,
        'value'=>'3 GB',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>468,
        'product_id'=>17,
        'category_option_id'=>55,
        'value'=>'iOS 11.1.1, upgradable to iOS 12.1',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>469,
        'product_id'=>17,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>470,
        'product_id'=>17,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 2716 mAh battery (10.35 Wh)',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>471,
        'product_id'=>17,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>472,
        'product_id'=>17,
        'category_option_id'=>59,
        'value'=>'GSM / HSPA / LTE',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>473,
        'product_id'=>17,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>474,
        'product_id'=>17,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>475,
        'product_id'=>17,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>476,
        'product_id'=>17,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>477,
        'product_id'=>17,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>478,
        'product_id'=>17,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>479,
        'product_id'=>17,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>480,
        'product_id'=>17,
        'category_option_id'=>67,
        'value'=>'7 MP',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
                    
        ProductOption::create( [
        'id'=>481,
        'product_id'=>17,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>482,
        'product_id'=>17,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>483,
        'product_id'=>17,
        'category_option_id'=>70,
        'value'=>'1125 x 2436 pixels, 19.5:9 ratio (~458 ppi density)',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>484,
        'product_id'=>17,
        'category_option_id'=>71,
        'value'=>'5.8 inches, 84.4 cm2 (~82.9% screen-to-body ratio)',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>485,
        'product_id'=>17,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>486,
        'product_id'=>17,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>487,
        'product_id'=>17,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>488,
        'product_id'=>17,
        'category_option_id'=>75,
        'value'=>'Caviar Black, Lemonade Blue, Bubblegum Pink',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>489,
        'product_id'=>17,
        'category_option_id'=>76,
        'value'=>'5.65 x 2.79 x 0.30 in',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>490,
        'product_id'=>17,
        'category_option_id'=>77,
        'value'=>'143.6 x 70.9 x 7.7 mm',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>491,
        'product_id'=>17,
        'category_option_id'=>78,
        'value'=>'174 g',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>492,
        'product_id'=>17,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>493,
        'product_id'=>17,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 05:25:04',
        'updated_at'=>'2018-12-09 05:25:04'
        ] );
                    
        ProductOption::create( [
        'id'=>494,
        'product_id'=>18,
        'category_option_id'=>52,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
                    
        ProductOption::create( [
        'id'=>495,
        'product_id'=>18,
        'category_option_id'=>53,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
                    
        ProductOption::create( [
        'id'=>496,
        'product_id'=>18,
        'category_option_id'=>54,
        'value'=>'3 GB',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
                    
        ProductOption::create( [
        'id'=>497,
        'product_id'=>18,
        'category_option_id'=>55,
        'value'=>'iOS 11, upgradable to iOS 12.1',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
                    
        ProductOption::create( [
        'id'=>498,
        'product_id'=>18,
        'category_option_id'=>56,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
                    
        ProductOption::create( [
        'id'=>499,
        'product_id'=>18,
        'category_option_id'=>57,
        'value'=>'Non-removable Li-Ion 2691 mAh battery (10.28 Wh)',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
                    
        ProductOption::create( [
        'id'=>500,
        'product_id'=>18,
        'category_option_id'=>58,
        'value'=>'na',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );

        ProductOption::create( [
        'id'=>501,
        'product_id'=>18,
        'category_option_id'=>59,
        'value'=>'GSM / CDMA / HSPA / EVDO / LTE',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>502,
        'product_id'=>18,
        'category_option_id'=>60,
        'value'=>'Wi-Fi 802.11 a/b/g/n/ac, dual-band, Wi-Fi Direct, hotspot',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>503,
        'product_id'=>18,
        'category_option_id'=>61,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>504,
        'product_id'=>18,
        'category_option_id'=>62,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>505,
        'product_id'=>18,
        'category_option_id'=>63,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>506,
        'product_id'=>18,
        'category_option_id'=>64,
        'value'=>'USB 2.0, Type-C 1.0 reversible connector',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>507,
        'product_id'=>18,
        'category_option_id'=>65,
        'value'=>'no',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>508,
        'product_id'=>18,
        'category_option_id'=>66,
        'value'=>'12 MP',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>509,
        'product_id'=>18,
        'category_option_id'=>67,
        'value'=>'7 MP',
        'created_at'=>'2018-12-09 09:58:58',
        'updated_at'=>'2018-12-09 09:58:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>510,
        'product_id'=>18,
        'category_option_id'=>68,
        'value'=>'2160p@30fps',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>511,
        'product_id'=>18,
        'category_option_id'=>69,
        'value'=>'1080p@30fps',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>512,
        'product_id'=>18,
        'category_option_id'=>70,
        'value'=>'1080 x 1920 pixels, 16:9 ratio (~401 ppi density)',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>513,
        'product_id'=>18,
        'category_option_id'=>71,
        'value'=>'5.5 inches, 83.4 cm2 (~67.4% screen-to-body ratio)',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>514,
        'product_id'=>18,
        'category_option_id'=>72,
        'value'=>'Yes',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>515,
        'product_id'=>18,
        'category_option_id'=>73,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>516,
        'product_id'=>18,
        'category_option_id'=>74,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>517,
        'product_id'=>18,
        'category_option_id'=>75,
        'value'=>'Gold, Space Gray, Silver, Red',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>518,
        'product_id'=>18,
        'category_option_id'=>76,
        'value'=>'6.24 x 3.07 x 0.30 in',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>519,
        'product_id'=>18,
        'category_option_id'=>77,
        'value'=>'158.4 x 78.1 x 7.5 mm',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>520,
        'product_id'=>18,
        'category_option_id'=>78,
        'value'=>'202 g (7.13 oz)',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>521,
        'product_id'=>18,
        'category_option_id'=>79,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>522,
        'product_id'=>18,
        'category_option_id'=>80,
        'value'=>'yes',
        'created_at'=>'2018-12-09 09:58:59',
        'updated_at'=>'2018-12-09 09:58:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>523,
        'product_id'=>19,
        'category_option_id'=>23,
        'value'=>'13 inch',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>524,
        'product_id'=>19,
        'category_option_id'=>24,
        'value'=>'3200x1800',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>525,
        'product_id'=>19,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>526,
        'product_id'=>19,
        'category_option_id'=>26,
        'value'=>'Intel Core i7-8550U',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>527,
        'product_id'=>19,
        'category_option_id'=>27,
        'value'=>'no',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>528,
        'product_id'=>19,
        'category_option_id'=>28,
        'value'=>'CPU @ 1.80GHz',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>529,
        'product_id'=>19,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>530,
        'product_id'=>19,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>531,
        'product_id'=>19,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>532,
        'product_id'=>19,
        'category_option_id'=>32,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>533,
        'product_id'=>19,
        'category_option_id'=>33,
        'value'=>'Intel UHD Graphics 620',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>534,
        'product_id'=>19,
        'category_option_id'=>34,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>535,
        'product_id'=>19,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>536,
        'product_id'=>19,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>537,
        'product_id'=>19,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>538,
        'product_id'=>19,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>539,
        'product_id'=>19,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>540,
        'product_id'=>19,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>541,
        'product_id'=>19,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>542,
        'product_id'=>19,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>543,
        'product_id'=>19,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>544,
        'product_id'=>19,
        'category_option_id'=>44,
        'value'=>'english',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>545,
        'product_id'=>19,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>546,
        'product_id'=>19,
        'category_option_id'=>46,
        'value'=>'black, blue',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>547,
        'product_id'=>19,
        'category_option_id'=>47,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>548,
        'product_id'=>19,
        'category_option_id'=>48,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>549,
        'product_id'=>19,
        'category_option_id'=>49,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>550,
        'product_id'=>19,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>551,
        'product_id'=>19,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:16:41',
        'updated_at'=>'2018-12-09 10:16:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>552,
        'product_id'=>20,
        'category_option_id'=>23,
        'value'=>'13.3',
        'created_at'=>'2018-12-09 10:32:59',
        'updated_at'=>'2018-12-09 10:32:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>553,
        'product_id'=>20,
        'category_option_id'=>24,
        'value'=>'2560-by-1600 native resolution at 227 pixels per inch',
        'created_at'=>'2018-12-09 10:32:59',
        'updated_at'=>'2018-12-09 10:32:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>554,
        'product_id'=>20,
        'category_option_id'=>25,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>555,
        'product_id'=>20,
        'category_option_id'=>26,
        'value'=>'1.6GHz dual-core Intel Core i5, Turbo Boost up to 3.6GHz, with 4MB L3 cache',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>556,
        'product_id'=>20,
        'category_option_id'=>27,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>557,
        'product_id'=>20,
        'category_option_id'=>28,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>558,
        'product_id'=>20,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>559,
        'product_id'=>20,
        'category_option_id'=>30,
        'value'=>'128GB PCIe-based SSD',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>560,
        'product_id'=>20,
        'category_option_id'=>31,
        'value'=>'8GB of 2133MHz',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>561,
        'product_id'=>20,
        'category_option_id'=>32,
        'value'=>'LPDDR3',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>562,
        'product_id'=>20,
        'category_option_id'=>33,
        'value'=>'Intel UHD Graphics 617',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>563,
        'product_id'=>20,
        'category_option_id'=>34,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>564,
        'product_id'=>20,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>565,
        'product_id'=>20,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>566,
        'product_id'=>20,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>567,
        'product_id'=>20,
        'category_option_id'=>38,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>568,
        'product_id'=>20,
        'category_option_id'=>39,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>569,
        'product_id'=>20,
        'category_option_id'=>40,
        'value'=>'Up to 12 hours wireless web',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>570,
        'product_id'=>20,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:00',
        'updated_at'=>'2018-12-09 10:33:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>571,
        'product_id'=>20,
        'category_option_id'=>42,
        'value'=>'macOS',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>572,
        'product_id'=>20,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>573,
        'product_id'=>20,
        'category_option_id'=>44,
        'value'=>'english',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>574,
        'product_id'=>20,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>575,
        'product_id'=>20,
        'category_option_id'=>46,
        'value'=>'white,silver',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>576,
        'product_id'=>20,
        'category_option_id'=>47,
        'value'=>'0.16x 11.97x8.36 inches',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>577,
        'product_id'=>20,
        'category_option_id'=>48,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>578,
        'product_id'=>20,
        'category_option_id'=>49,
        'value'=>'2.75 pounds',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>579,
        'product_id'=>20,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>580,
        'product_id'=>20,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:33:01',
        'updated_at'=>'2018-12-09 10:33:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>581,
        'product_id'=>21,
        'category_option_id'=>23,
        'value'=>'13.3',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>582,
        'product_id'=>21,
        'category_option_id'=>24,
        'value'=>'3200x1800',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>583,
        'product_id'=>21,
        'category_option_id'=>25,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>584,
        'product_id'=>21,
        'category_option_id'=>26,
        'value'=>'1.6GHz dual-core Intel Core i5, Turbo Boost up to 3.6GHz, with 4MB L3 cache',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>585,
        'product_id'=>21,
        'category_option_id'=>27,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>586,
        'product_id'=>21,
        'category_option_id'=>28,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>587,
        'product_id'=>21,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>588,
        'product_id'=>21,
        'category_option_id'=>30,
        'value'=>'128GB PCIe-based SSD',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>589,
        'product_id'=>21,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>590,
        'product_id'=>21,
        'category_option_id'=>32,
        'value'=>'LPDDR3',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>591,
        'product_id'=>21,
        'category_option_id'=>33,
        'value'=>'Intel UHD Graphics 617',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>592,
        'product_id'=>21,
        'category_option_id'=>34,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>593,
        'product_id'=>21,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>594,
        'product_id'=>21,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>595,
        'product_id'=>21,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>596,
        'product_id'=>21,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>597,
        'product_id'=>21,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>598,
        'product_id'=>21,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>599,
        'product_id'=>21,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>600,
        'product_id'=>21,
        'category_option_id'=>42,
        'value'=>'macOS',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>601,
        'product_id'=>21,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>602,
        'product_id'=>21,
        'category_option_id'=>44,
        'value'=>'english',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>603,
        'product_id'=>21,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>604,
        'product_id'=>21,
        'category_option_id'=>46,
        'value'=>'black, blue',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>605,
        'product_id'=>21,
        'category_option_id'=>47,
        'value'=>'0.16x 11.97x8.36 inches',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>606,
        'product_id'=>21,
        'category_option_id'=>48,
        'value'=>'na',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>607,
        'product_id'=>21,
        'category_option_id'=>49,
        'value'=>'2.75 pounds',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>608,
        'product_id'=>21,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:06',
        'updated_at'=>'2018-12-09 10:52:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>609,
        'product_id'=>21,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 10:52:07',
        'updated_at'=>'2018-12-09 10:52:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>610,
        'product_id'=>22,
        'category_option_id'=>23,
        'value'=>'15.6',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>611,
        'product_id'=>22,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>612,
        'product_id'=>22,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>613,
        'product_id'=>22,
        'category_option_id'=>26,
        'value'=>'Intel Core i7-8750H',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>614,
        'product_id'=>22,
        'category_option_id'=>27,
        'value'=>'6',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>615,
        'product_id'=>22,
        'category_option_id'=>28,
        'value'=>'2.2GHz',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>616,
        'product_id'=>22,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>617,
        'product_id'=>22,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>618,
        'product_id'=>22,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>619,
        'product_id'=>22,
        'category_option_id'=>32,
        'value'=>'LPDDR3',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>620,
        'product_id'=>22,
        'category_option_id'=>33,
        'value'=>'NVIDIA GTX1070',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>621,
        'product_id'=>22,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>622,
        'product_id'=>22,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>623,
        'product_id'=>22,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:26',
        'updated_at'=>'2018-12-09 11:06:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>624,
        'product_id'=>22,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>625,
        'product_id'=>22,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>626,
        'product_id'=>22,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>627,
        'product_id'=>22,
        'category_option_id'=>40,
        'value'=>'na',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>628,
        'product_id'=>22,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>629,
        'product_id'=>22,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>630,
        'product_id'=>22,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>631,
        'product_id'=>22,
        'category_option_id'=>44,
        'value'=>'english',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>632,
        'product_id'=>22,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>633,
        'product_id'=>22,
        'category_option_id'=>46,
        'value'=>'ROG Scar II Edition; Gunmetal',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>634,
        'product_id'=>22,
        'category_option_id'=>47,
        'value'=>'14.2 (W) x 1 (H) x 10.3 (D) in',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>635,
        'product_id'=>22,
        'category_option_id'=>48,
        'value'=>'36.07 (W) x 2.54 (H) x 26.16 (D) cm',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>636,
        'product_id'=>22,
        'category_option_id'=>49,
        'value'=>'5.29 lb',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>637,
        'product_id'=>22,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>638,
        'product_id'=>22,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:06:27',
        'updated_at'=>'2018-12-09 11:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>639,
        'product_id'=>23,
        'category_option_id'=>23,
        'value'=>'17.3',
        'created_at'=>'2018-12-09 11:23:18',
        'updated_at'=>'2018-12-09 11:23:18'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>640,
        'product_id'=>23,
        'category_option_id'=>24,
        'value'=>'1600x900',
        'created_at'=>'2018-12-09 11:23:18',
        'updated_at'=>'2018-12-09 11:23:18'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>641,
        'product_id'=>23,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 11:23:18',
        'updated_at'=>'2018-12-09 11:23:18'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>642,
        'product_id'=>23,
        'category_option_id'=>26,
        'value'=>'Intel Core i5-8250U',
        'created_at'=>'2018-12-09 11:23:18',
        'updated_at'=>'2018-12-09 11:23:18'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>643,
        'product_id'=>23,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>644,
        'product_id'=>23,
        'category_option_id'=>28,
        'value'=>'1.6 GHz',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>645,
        'product_id'=>23,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>646,
        'product_id'=>23,
        'category_option_id'=>30,
        'value'=>'1 TB',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>647,
        'product_id'=>23,
        'category_option_id'=>31,
        'value'=>'12 GB',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>648,
        'product_id'=>23,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>649,
        'product_id'=>23,
        'category_option_id'=>33,
        'value'=>'NVIDIA GeForce MX150',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>650,
        'product_id'=>23,
        'category_option_id'=>34,
        'value'=>'SD/SDXC Card Reader',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>651,
        'product_id'=>23,
        'category_option_id'=>35,
        'value'=>'2',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>652,
        'product_id'=>23,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>653,
        'product_id'=>23,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>654,
        'product_id'=>23,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>655,
        'product_id'=>23,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>656,
        'product_id'=>23,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>657,
        'product_id'=>23,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>658,
        'product_id'=>23,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>659,
        'product_id'=>23,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>660,
        'product_id'=>23,
        'category_option_id'=>44,
        'value'=>'english',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>661,
        'product_id'=>23,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:19',
        'updated_at'=>'2018-12-09 11:23:19'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>662,
        'product_id'=>23,
        'category_option_id'=>46,
        'value'=>'black, blue',
        'created_at'=>'2018-12-09 11:23:20',
        'updated_at'=>'2018-12-09 11:23:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>663,
        'product_id'=>23,
        'category_option_id'=>47,
        'value'=>'16.67 (W) x 1.11 (H) x 11.41 (D) in',
        'created_at'=>'2018-12-09 11:23:20',
        'updated_at'=>'2018-12-09 11:23:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>664,
        'product_id'=>23,
        'category_option_id'=>48,
        'value'=>'42.33 (W) x 2.82 (H) x 28.99 (D) cm',
        'created_at'=>'2018-12-09 11:23:20',
        'updated_at'=>'2018-12-09 11:23:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>665,
        'product_id'=>23,
        'category_option_id'=>49,
        'value'=>'6.61 lb',
        'created_at'=>'2018-12-09 11:23:20',
        'updated_at'=>'2018-12-09 11:23:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>666,
        'product_id'=>23,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:20',
        'updated_at'=>'2018-12-09 11:23:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>667,
        'product_id'=>23,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 11:23:20',
        'updated_at'=>'2018-12-09 11:23:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>668,
        'product_id'=>24,
        'category_option_id'=>23,
        'value'=>'14 inch',
        'created_at'=>'2018-12-09 12:28:14',
        'updated_at'=>'2018-12-09 12:28:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>669,
        'product_id'=>24,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>670,
        'product_id'=>24,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>671,
        'product_id'=>24,
        'category_option_id'=>26,
        'value'=>'8th Generation Intel Core i5-8250U Processor',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>672,
        'product_id'=>24,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>673,
        'product_id'=>24,
        'category_option_id'=>28,
        'value'=>'1.6 GHz',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>674,
        'product_id'=>24,
        'category_option_id'=>29,
        'value'=>'6 GB',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>675,
        'product_id'=>24,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>676,
        'product_id'=>24,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>677,
        'product_id'=>24,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>678,
        'product_id'=>24,
        'category_option_id'=>33,
        'value'=>'NVIDIA GTX1070',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>679,
        'product_id'=>24,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>680,
        'product_id'=>24,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>681,
        'product_id'=>24,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>682,
        'product_id'=>24,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>683,
        'product_id'=>24,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>684,
        'product_id'=>24,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>685,
        'product_id'=>24,
        'category_option_id'=>40,
        'value'=>'Up to 12 hours wireless web',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>686,
        'product_id'=>24,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:15',
        'updated_at'=>'2018-12-09 12:28:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>687,
        'product_id'=>24,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>688,
        'product_id'=>24,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>689,
        'product_id'=>24,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>690,
        'product_id'=>24,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>691,
        'product_id'=>24,
        'category_option_id'=>46,
        'value'=>'Natural Silver; Ash Silver',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>692,
        'product_id'=>24,
        'category_option_id'=>47,
        'value'=>'12.76 (W) x 0.78 (H) x 8.89 (D) in',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>693,
        'product_id'=>24,
        'category_option_id'=>48,
        'value'=>'32.41 (W) x 1.98 (H) x 22.58 (D) cm',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>694,
        'product_id'=>24,
        'category_option_id'=>49,
        'value'=>'3.24 lb',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>695,
        'product_id'=>24,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>696,
        'product_id'=>24,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:28:16',
        'updated_at'=>'2018-12-09 12:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>697,
        'product_id'=>25,
        'category_option_id'=>23,
        'value'=>'17.3 in',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>698,
        'product_id'=>25,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>699,
        'product_id'=>25,
        'category_option_id'=>25,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>700,
        'product_id'=>25,
        'category_option_id'=>26,
        'value'=>'8th Generation Intel Core i7-8550U',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>701,
        'product_id'=>25,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>702,
        'product_id'=>25,
        'category_option_id'=>28,
        'value'=>'1.8 GHz',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>703,
        'product_id'=>25,
        'category_option_id'=>29,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>704,
        'product_id'=>25,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>705,
        'product_id'=>25,
        'category_option_id'=>31,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>706,
        'product_id'=>25,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>707,
        'product_id'=>25,
        'category_option_id'=>33,
        'value'=>'NVIDIA GeForce MX150',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>708,
        'product_id'=>25,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>709,
        'product_id'=>25,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 12:41:20',
        'updated_at'=>'2018-12-09 12:41:20'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>710,
        'product_id'=>25,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>711,
        'product_id'=>25,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>712,
        'product_id'=>25,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>713,
        'product_id'=>25,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>714,
        'product_id'=>25,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>715,
        'product_id'=>25,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>716,
        'product_id'=>25,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>717,
        'product_id'=>25,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>718,
        'product_id'=>25,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>719,
        'product_id'=>25,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>720,
        'product_id'=>25,
        'category_option_id'=>46,
        'value'=>'Natural Silver; Ash Silver',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>721,
        'product_id'=>25,
        'category_option_id'=>47,
        'value'=>'16.38 (W) x 0.91 (H) x 10.94 (D) in',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>722,
        'product_id'=>25,
        'category_option_id'=>48,
        'value'=>'41.6 (W) x 2.31 (H) x 27.78 (D) cm',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>723,
        'product_id'=>25,
        'category_option_id'=>49,
        'value'=>'6.39 lb',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>724,
        'product_id'=>25,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>725,
        'product_id'=>25,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:41:21',
        'updated_at'=>'2018-12-09 12:41:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>726,
        'product_id'=>26,
        'category_option_id'=>23,
        'value'=>'17.3 in',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>727,
        'product_id'=>26,
        'category_option_id'=>24,
        'value'=>'1920 x 1080',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>728,
        'product_id'=>26,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>729,
        'product_id'=>26,
        'category_option_id'=>26,
        'value'=>'Intel Core i7-8550U',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>730,
        'product_id'=>26,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>731,
        'product_id'=>26,
        'category_option_id'=>28,
        'value'=>'1.80 GHz',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>732,
        'product_id'=>26,
        'category_option_id'=>29,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>733,
        'product_id'=>26,
        'category_option_id'=>30,
        'value'=>'2 TB',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>734,
        'product_id'=>26,
        'category_option_id'=>31,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>735,
        'product_id'=>26,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>736,
        'product_id'=>26,
        'category_option_id'=>33,
        'value'=>'AMD Radeon 530; Graphics with 4G GDDR5 graphics memory',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>737,
        'product_id'=>26,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 12:59:48',
        'updated_at'=>'2018-12-09 12:59:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>738,
        'product_id'=>26,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>739,
        'product_id'=>26,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>740,
        'product_id'=>26,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>741,
        'product_id'=>26,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>742,
        'product_id'=>26,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>743,
        'product_id'=>26,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>744,
        'product_id'=>26,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>745,
        'product_id'=>26,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>746,
        'product_id'=>26,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>747,
        'product_id'=>26,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 12:59:49',
        'updated_at'=>'2018-12-09 12:59:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>748,
        'product_id'=>26,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>749,
        'product_id'=>26,
        'category_option_id'=>46,
        'value'=>'Natural Silver; Ash Silver',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>750,
        'product_id'=>26,
        'category_option_id'=>47,
        'value'=>'0.98 (W) x 16.35 (H) x 10.99 (D) in',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>751,
        'product_id'=>26,
        'category_option_id'=>48,
        'value'=>'2.49 (W) x 41.53 (H) x 27.91 (D) cm',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>752,
        'product_id'=>26,
        'category_option_id'=>49,
        'value'=>'6.15 lb',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>753,
        'product_id'=>26,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>754,
        'product_id'=>26,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 12:59:50',
        'updated_at'=>'2018-12-09 12:59:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>755,
        'product_id'=>27,
        'category_option_id'=>23,
        'value'=>'17 in',
        'created_at'=>'2018-12-09 13:04:25',
        'updated_at'=>'2018-12-09 13:04:25'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>756,
        'product_id'=>27,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 13:04:25',
        'updated_at'=>'2018-12-09 13:04:25'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>757,
        'product_id'=>27,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 13:04:25',
        'updated_at'=>'2018-12-09 13:04:25'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>758,
        'product_id'=>27,
        'category_option_id'=>26,
        'value'=>'Intel Core i7-8550U',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>759,
        'product_id'=>27,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>760,
        'product_id'=>27,
        'category_option_id'=>28,
        'value'=>'2.2GHz',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>761,
        'product_id'=>27,
        'category_option_id'=>29,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>762,
        'product_id'=>27,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>763,
        'product_id'=>27,
        'category_option_id'=>31,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>764,
        'product_id'=>27,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>765,
        'product_id'=>27,
        'category_option_id'=>33,
        'value'=>'NVIDIA GTX1070',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>766,
        'product_id'=>27,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>767,
        'product_id'=>27,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>768,
        'product_id'=>27,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>769,
        'product_id'=>27,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>770,
        'product_id'=>27,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>771,
        'product_id'=>27,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>772,
        'product_id'=>27,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>773,
        'product_id'=>27,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>774,
        'product_id'=>27,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>775,
        'product_id'=>27,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:26',
        'updated_at'=>'2018-12-09 13:04:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>776,
        'product_id'=>27,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>777,
        'product_id'=>27,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>778,
        'product_id'=>27,
        'category_option_id'=>46,
        'value'=>'black, blue',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>779,
        'product_id'=>27,
        'category_option_id'=>47,
        'value'=>'0.98 (W) x 16.35 (H) x 10.99 (D) in',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>780,
        'product_id'=>27,
        'category_option_id'=>48,
        'value'=>'2.49 (W) x 41.53 (H) x 27.91 (D) cm',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>781,
        'product_id'=>27,
        'category_option_id'=>49,
        'value'=>'6.15 lb',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>782,
        'product_id'=>27,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>783,
        'product_id'=>27,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:04:27',
        'updated_at'=>'2018-12-09 13:04:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>784,
        'product_id'=>28,
        'category_option_id'=>23,
        'value'=>'15.6\"',
        'created_at'=>'2018-12-09 13:12:43',
        'updated_at'=>'2018-12-09 13:12:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>785,
        'product_id'=>28,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 13:12:43',
        'updated_at'=>'2018-12-09 13:12:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>786,
        'product_id'=>28,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 13:12:43',
        'updated_at'=>'2018-12-09 13:12:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>787,
        'product_id'=>28,
        'category_option_id'=>26,
        'value'=>'Intel Core i7-8550U',
        'created_at'=>'2018-12-09 13:12:43',
        'updated_at'=>'2018-12-09 13:12:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>788,
        'product_id'=>28,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 13:12:44',
        'updated_at'=>'2018-12-09 13:12:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>789,
        'product_id'=>28,
        'category_option_id'=>28,
        'value'=>'1.8 GHz',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>790,
        'product_id'=>28,
        'category_option_id'=>29,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>791,
        'product_id'=>28,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>792,
        'product_id'=>28,
        'category_option_id'=>31,
        'value'=>'32 GB',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>793,
        'product_id'=>28,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>794,
        'product_id'=>28,
        'category_option_id'=>33,
        'value'=>'NVIDIA GeForce MX150',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>795,
        'product_id'=>28,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>796,
        'product_id'=>28,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>797,
        'product_id'=>28,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>798,
        'product_id'=>28,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>799,
        'product_id'=>28,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>800,
        'product_id'=>28,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>801,
        'product_id'=>28,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>802,
        'product_id'=>28,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>803,
        'product_id'=>28,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>804,
        'product_id'=>28,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>805,
        'product_id'=>28,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>806,
        'product_id'=>28,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>807,
        'product_id'=>28,
        'category_option_id'=>46,
        'value'=>'black',
        'created_at'=>'2018-12-09 13:12:45',
        'updated_at'=>'2018-12-09 13:12:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>808,
        'product_id'=>28,
        'category_option_id'=>47,
        'value'=>'14.2 (W) x 1 (H) x 10.3 (D) in',
        'created_at'=>'2018-12-09 13:12:46',
        'updated_at'=>'2018-12-09 13:12:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>809,
        'product_id'=>28,
        'category_option_id'=>48,
        'value'=>'36.07 (W) x 2.54 (H) x 26.16 (D) cm',
        'created_at'=>'2018-12-09 13:12:46',
        'updated_at'=>'2018-12-09 13:12:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>810,
        'product_id'=>28,
        'category_option_id'=>49,
        'value'=>'3.6 lbs',
        'created_at'=>'2018-12-09 13:12:46',
        'updated_at'=>'2018-12-09 13:12:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>811,
        'product_id'=>28,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:46',
        'updated_at'=>'2018-12-09 13:12:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>812,
        'product_id'=>28,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:12:46',
        'updated_at'=>'2018-12-09 13:12:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>813,
        'product_id'=>29,
        'category_option_id'=>23,
        'value'=>'15.6 in',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>814,
        'product_id'=>29,
        'category_option_id'=>24,
        'value'=>'1366 x 768',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>815,
        'product_id'=>29,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>816,
        'product_id'=>29,
        'category_option_id'=>26,
        'value'=>'AMD A12-9720P',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>817,
        'product_id'=>29,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>818,
        'product_id'=>29,
        'category_option_id'=>28,
        'value'=>'2.7 GHz',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>819,
        'product_id'=>29,
        'category_option_id'=>29,
        'value'=>'2 MB',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>820,
        'product_id'=>29,
        'category_option_id'=>30,
        'value'=>'1 TB',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>821,
        'product_id'=>29,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>822,
        'product_id'=>29,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>823,
        'product_id'=>29,
        'category_option_id'=>33,
        'value'=>'AMD Radeon R7',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>824,
        'product_id'=>29,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>825,
        'product_id'=>29,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>826,
        'product_id'=>29,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>827,
        'product_id'=>29,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>828,
        'product_id'=>29,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>829,
        'product_id'=>29,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>830,
        'product_id'=>29,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>831,
        'product_id'=>29,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>832,
        'product_id'=>29,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>833,
        'product_id'=>29,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>834,
        'product_id'=>29,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>835,
        'product_id'=>29,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>836,
        'product_id'=>29,
        'category_option_id'=>46,
        'value'=>'Platinum Grey',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>837,
        'product_id'=>29,
        'category_option_id'=>47,
        'value'=>'14.88 (W) x 0.91 (H) x 10.24 (D) in',
        'created_at'=>'2018-12-09 13:21:37',
        'updated_at'=>'2018-12-09 13:21:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>838,
        'product_id'=>29,
        'category_option_id'=>48,
        'value'=>'37.8 (W) x 2.3 (H) x 26 (D) cm',
        'created_at'=>'2018-12-09 13:21:38',
        'updated_at'=>'2018-12-09 13:21:38'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>839,
        'product_id'=>29,
        'category_option_id'=>49,
        'value'=>'4.85 lb',
        'created_at'=>'2018-12-09 13:21:38',
        'updated_at'=>'2018-12-09 13:21:38'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>840,
        'product_id'=>29,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:38',
        'updated_at'=>'2018-12-09 13:21:38'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>841,
        'product_id'=>29,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:21:38',
        'updated_at'=>'2018-12-09 13:21:38'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>842,
        'product_id'=>30,
        'category_option_id'=>23,
        'value'=>'17.3 in',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>843,
        'product_id'=>30,
        'category_option_id'=>24,
        'value'=>'1600 x 900',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>844,
        'product_id'=>30,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>845,
        'product_id'=>30,
        'category_option_id'=>26,
        'value'=>'Intel Pentium N4200',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>846,
        'product_id'=>30,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>847,
        'product_id'=>30,
        'category_option_id'=>28,
        'value'=>'1.10 GHz',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>848,
        'product_id'=>30,
        'category_option_id'=>29,
        'value'=>'2 MB',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>849,
        'product_id'=>30,
        'category_option_id'=>30,
        'value'=>'500 GB',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>850,
        'product_id'=>30,
        'category_option_id'=>31,
        'value'=>'4 GB',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>851,
        'product_id'=>30,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>852,
        'product_id'=>30,
        'category_option_id'=>33,
        'value'=>'Intel HD Graphics 505',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>853,
        'product_id'=>30,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>854,
        'product_id'=>30,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>855,
        'product_id'=>30,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>856,
        'product_id'=>30,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>857,
        'product_id'=>30,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>858,
        'product_id'=>30,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>859,
        'product_id'=>30,
        'category_option_id'=>40,
        'value'=>'Up to 12 hours wireless web',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>860,
        'product_id'=>30,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>861,
        'product_id'=>30,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>862,
        'product_id'=>30,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>863,
        'product_id'=>30,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>864,
        'product_id'=>30,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>865,
        'product_id'=>30,
        'category_option_id'=>46,
        'value'=>'black',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>866,
        'product_id'=>30,
        'category_option_id'=>47,
        'value'=>'16.7 (W) x 1.1 (H) x 11.4 (D) in',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>867,
        'product_id'=>30,
        'category_option_id'=>48,
        'value'=>'42.42 (W) x 2.79 (H) x 28.96 (D) cm',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>868,
        'product_id'=>30,
        'category_option_id'=>49,
        'value'=>'6.17 lb',
        'created_at'=>'2018-12-09 13:28:17',
        'updated_at'=>'2018-12-09 13:28:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>869,
        'product_id'=>31,
        'category_option_id'=>23,
        'value'=>'17 in',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>870,
        'product_id'=>31,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>871,
        'product_id'=>31,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>872,
        'product_id'=>31,
        'category_option_id'=>26,
        'value'=>'Intel i7-7700HQ CPU',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>873,
        'product_id'=>31,
        'category_option_id'=>27,
        'value'=>'6',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>874,
        'product_id'=>31,
        'category_option_id'=>28,
        'value'=>'2.2GHz',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>875,
        'product_id'=>31,
        'category_option_id'=>29,
        'value'=>'2 MB',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>876,
        'product_id'=>31,
        'category_option_id'=>30,
        'value'=>'2 TB',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>877,
        'product_id'=>31,
        'category_option_id'=>31,
        'value'=>'16 GB',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>878,
        'product_id'=>31,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>879,
        'product_id'=>31,
        'category_option_id'=>33,
        'value'=>'GeForce GTX',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>880,
        'product_id'=>31,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>881,
        'product_id'=>31,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>882,
        'product_id'=>31,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>883,
        'product_id'=>31,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>884,
        'product_id'=>31,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>885,
        'product_id'=>31,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>886,
        'product_id'=>31,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>887,
        'product_id'=>31,
        'category_option_id'=>41,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>888,
        'product_id'=>31,
        'category_option_id'=>42,
        'value'=>'Windows 10 home',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>889,
        'product_id'=>31,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>890,
        'product_id'=>31,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>891,
        'product_id'=>31,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>892,
        'product_id'=>31,
        'category_option_id'=>46,
        'value'=>'black',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>893,
        'product_id'=>31,
        'category_option_id'=>47,
        'value'=>'14.2 (W) x 1 (H) x 10.3 (D) in',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>894,
        'product_id'=>31,
        'category_option_id'=>48,
        'value'=>'36.07 (W) x 2.54 (H) x 26.16 (D) cm',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>895,
        'product_id'=>31,
        'category_option_id'=>49,
        'value'=>'4.85 lb',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>896,
        'product_id'=>31,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>897,
        'product_id'=>31,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:36:59',
        'updated_at'=>'2018-12-09 13:36:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>898,
        'product_id'=>32,
        'category_option_id'=>23,
        'value'=>'13.3 in',
        'created_at'=>'2018-12-09 13:49:12',
        'updated_at'=>'2018-12-09 13:49:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>899,
        'product_id'=>32,
        'category_option_id'=>24,
        'value'=>'2560 x 1600',
        'created_at'=>'2018-12-09 13:49:12',
        'updated_at'=>'2018-12-09 13:49:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>900,
        'product_id'=>32,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>901,
        'product_id'=>32,
        'category_option_id'=>26,
        'value'=>'Intel Core i5',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>902,
        'product_id'=>32,
        'category_option_id'=>27,
        'value'=>'2',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>903,
        'product_id'=>32,
        'category_option_id'=>28,
        'value'=>'2.3 GHz',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>904,
        'product_id'=>32,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>905,
        'product_id'=>32,
        'category_option_id'=>30,
        'value'=>'128 GB',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>906,
        'product_id'=>32,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>907,
        'product_id'=>32,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>908,
        'product_id'=>32,
        'category_option_id'=>33,
        'value'=>'Intel Iris Plus Graphics 640',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>909,
        'product_id'=>32,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>910,
        'product_id'=>32,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>911,
        'product_id'=>32,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>912,
        'product_id'=>32,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>913,
        'product_id'=>32,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>914,
        'product_id'=>32,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>915,
        'product_id'=>32,
        'category_option_id'=>40,
        'value'=>'Up to 12 hours wireless web',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>916,
        'product_id'=>32,
        'category_option_id'=>41,
        'value'=>'macOS High Sierra',
        'created_at'=>'2018-12-09 13:49:13',
        'updated_at'=>'2018-12-09 13:49:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>917,
        'product_id'=>32,
        'category_option_id'=>42,
        'value'=>'english',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>918,
        'product_id'=>32,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>919,
        'product_id'=>32,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>920,
        'product_id'=>32,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>921,
        'product_id'=>32,
        'category_option_id'=>46,
        'value'=>'white,silver',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>922,
        'product_id'=>32,
        'category_option_id'=>47,
        'value'=>'11.97 (W) x 0.59 (H) x 8.36 (D) in',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>923,
        'product_id'=>32,
        'category_option_id'=>48,
        'value'=>'30.41 (W) x 1.49 (H) x 21.24 (D) cm',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>924,
        'product_id'=>32,
        'category_option_id'=>49,
        'value'=>'3.02 lb',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>925,
        'product_id'=>32,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>926,
        'product_id'=>32,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 13:49:14',
        'updated_at'=>'2018-12-09 13:49:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>927,
        'product_id'=>33,
        'category_option_id'=>23,
        'value'=>'12.3 in',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>928,
        'product_id'=>33,
        'category_option_id'=>24,
        'value'=>'2400 x 1600',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>929,
        'product_id'=>33,
        'category_option_id'=>25,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>930,
        'product_id'=>33,
        'category_option_id'=>26,
        'value'=>'Intel Kaby Lake-Y i5',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>931,
        'product_id'=>33,
        'category_option_id'=>27,
        'value'=>'5',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>932,
        'product_id'=>33,
        'category_option_id'=>28,
        'value'=>'1.2 GHz',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>933,
        'product_id'=>33,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>934,
        'product_id'=>33,
        'category_option_id'=>30,
        'value'=>'ns',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>935,
        'product_id'=>33,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>936,
        'product_id'=>33,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>937,
        'product_id'=>33,
        'category_option_id'=>33,
        'value'=>'Integrated',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>938,
        'product_id'=>33,
        'category_option_id'=>34,
        'value'=>'na',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>939,
        'product_id'=>33,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>940,
        'product_id'=>33,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>941,
        'product_id'=>33,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>942,
        'product_id'=>33,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>943,
        'product_id'=>33,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>944,
        'product_id'=>33,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>945,
        'product_id'=>33,
        'category_option_id'=>41,
        'value'=>'Chrome OS',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>946,
        'product_id'=>33,
        'category_option_id'=>42,
        'value'=>'english',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>947,
        'product_id'=>33,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>948,
        'product_id'=>33,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>949,
        'product_id'=>33,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>950,
        'product_id'=>33,
        'category_option_id'=>46,
        'value'=>'sliver',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>951,
        'product_id'=>33,
        'category_option_id'=>47,
        'value'=>'8.69 (W) x 0.44 (H) x 11.43 (D) in',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>952,
        'product_id'=>33,
        'category_option_id'=>48,
        'value'=>'22.08 (W) x 1.12 (H) x 29.04 (D) cm',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>953,
        'product_id'=>33,
        'category_option_id'=>49,
        'value'=>'2.43 lb',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>954,
        'product_id'=>33,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:13',
        'updated_at'=>'2018-12-09 23:31:13'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>955,
        'product_id'=>33,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:31:14',
        'updated_at'=>'2018-12-09 23:31:14'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>956,
        'product_id'=>34,
        'category_option_id'=>23,
        'value'=>'15.6 in',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>957,
        'product_id'=>34,
        'category_option_id'=>24,
        'value'=>'1920x1080',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>958,
        'product_id'=>34,
        'category_option_id'=>25,
        'value'=>'no',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>959,
        'product_id'=>34,
        'category_option_id'=>26,
        'value'=>'Intel Core i7-8750H',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>960,
        'product_id'=>34,
        'category_option_id'=>27,
        'value'=>'6',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>961,
        'product_id'=>34,
        'category_option_id'=>28,
        'value'=>'2.2 GHz',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>962,
        'product_id'=>34,
        'category_option_id'=>29,
        'value'=>'na',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>963,
        'product_id'=>34,
        'category_option_id'=>30,
        'value'=>'256 GB',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>964,
        'product_id'=>34,
        'category_option_id'=>31,
        'value'=>'16 GB',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>965,
        'product_id'=>34,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>966,
        'product_id'=>34,
        'category_option_id'=>33,
        'value'=>'NVIDIA GTX 1070',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>967,
        'product_id'=>34,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>968,
        'product_id'=>34,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>969,
        'product_id'=>34,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>970,
        'product_id'=>34,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>971,
        'product_id'=>34,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>972,
        'product_id'=>34,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 23:38:26',
        'updated_at'=>'2018-12-09 23:38:26'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>973,
        'product_id'=>34,
        'category_option_id'=>40,
        'value'=>'Up to 12 hours wireless web',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>974,
        'product_id'=>34,
        'category_option_id'=>41,
        'value'=>'Windows 10 Professional 64-Bit',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>975,
        'product_id'=>34,
        'category_option_id'=>42,
        'value'=>'english',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>976,
        'product_id'=>34,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>977,
        'product_id'=>34,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>978,
        'product_id'=>34,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>979,
        'product_id'=>34,
        'category_option_id'=>46,
        'value'=>'Metallic Black',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>980,
        'product_id'=>34,
        'category_option_id'=>47,
        'value'=>'15.1 (W) x 0.8 (H) x 10.3 (D) in',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>981,
        'product_id'=>34,
        'category_option_id'=>48,
        'value'=>'38.35 (W) x 2.03 (H) x 26.16 (D) cm',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>982,
        'product_id'=>34,
        'category_option_id'=>49,
        'value'=>'5.4 lbs',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>983,
        'product_id'=>34,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>984,
        'product_id'=>34,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:38:27',
        'updated_at'=>'2018-12-09 23:38:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>985,
        'product_id'=>35,
        'category_option_id'=>23,
        'value'=>'13.5 in',
        'created_at'=>'2018-12-09 23:47:05',
        'updated_at'=>'2018-12-09 23:47:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>986,
        'product_id'=>35,
        'category_option_id'=>24,
        'value'=>'2256 x 1504',
        'created_at'=>'2018-12-09 23:47:05',
        'updated_at'=>'2018-12-09 23:47:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>987,
        'product_id'=>35,
        'category_option_id'=>25,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:05',
        'updated_at'=>'2018-12-09 23:47:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>988,
        'product_id'=>35,
        'category_option_id'=>26,
        'value'=>'8th Gen Intel Core i5',
        'created_at'=>'2018-12-09 23:47:05',
        'updated_at'=>'2018-12-09 23:47:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>989,
        'product_id'=>35,
        'category_option_id'=>27,
        'value'=>'4',
        'created_at'=>'2018-12-09 23:47:05',
        'updated_at'=>'2018-12-09 23:47:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>990,
        'product_id'=>35,
        'category_option_id'=>28,
        'value'=>'1.6 GHz',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>991,
        'product_id'=>35,
        'category_option_id'=>29,
        'value'=>'6 MB',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>992,
        'product_id'=>35,
        'category_option_id'=>30,
        'value'=>'128 GB',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>993,
        'product_id'=>35,
        'category_option_id'=>31,
        'value'=>'8 GB',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>994,
        'product_id'=>35,
        'category_option_id'=>32,
        'value'=>'DDR4',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>995,
        'product_id'=>35,
        'category_option_id'=>33,
        'value'=>'Intel UHD Graphics 620',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>996,
        'product_id'=>35,
        'category_option_id'=>34,
        'value'=>'SD',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>997,
        'product_id'=>35,
        'category_option_id'=>35,
        'value'=>'3',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>998,
        'product_id'=>35,
        'category_option_id'=>36,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>999,
        'product_id'=>35,
        'category_option_id'=>37,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1000,
        'product_id'=>35,
        'category_option_id'=>38,
        'value'=>'Li-ion',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1001,
        'product_id'=>35,
        'category_option_id'=>39,
        'value'=>'1',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1002,
        'product_id'=>35,
        'category_option_id'=>40,
        'value'=>'6 hours',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1003,
        'product_id'=>35,
        'category_option_id'=>41,
        'value'=>'Windows 10 Home',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1004,
        'product_id'=>35,
        'category_option_id'=>42,
        'value'=>'english',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1005,
        'product_id'=>35,
        'category_option_id'=>43,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1006,
        'product_id'=>35,
        'category_option_id'=>44,
        'value'=>'bilingual',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1007,
        'product_id'=>35,
        'category_option_id'=>45,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:06',
        'updated_at'=>'2018-12-09 23:47:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1008,
        'product_id'=>35,
        'category_option_id'=>46,
        'value'=>'Platinum',
        'created_at'=>'2018-12-09 23:47:07',
        'updated_at'=>'2018-12-09 23:47:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1009,
        'product_id'=>35,
        'category_option_id'=>47,
        'value'=>'9.95 (W) x 13.39 (H) x 2.06 (D) in',
        'created_at'=>'2018-12-09 23:47:07',
        'updated_at'=>'2018-12-09 23:47:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1010,
        'product_id'=>35,
        'category_option_id'=>48,
        'value'=>'25.27 (W) x 34.01 (H) x 5.23 (D) cm',
        'created_at'=>'2018-12-09 23:47:07',
        'updated_at'=>'2018-12-09 23:47:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1011,
        'product_id'=>35,
        'category_option_id'=>49,
        'value'=>'3.2 lb',
        'created_at'=>'2018-12-09 23:47:07',
        'updated_at'=>'2018-12-09 23:47:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1012,
        'product_id'=>35,
        'category_option_id'=>50,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:07',
        'updated_at'=>'2018-12-09 23:47:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1013,
        'product_id'=>35,
        'category_option_id'=>51,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:47:07',
        'updated_at'=>'2018-12-09 23:47:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1014,
        'product_id'=>36,
        'category_option_id'=>81,
        'value'=>'Multi-Touch Retina',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-09 23:58:35'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1015,
        'product_id'=>36,
        'category_option_id'=>82,
        'value'=>'9.7 in',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-09 23:58:35'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1016,
        'product_id'=>36,
        'category_option_id'=>83,
        'value'=>'2048 x 1536',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-09 23:58:35'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1017,
        'product_id'=>36,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-09 23:58:35'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1018,
        'product_id'=>36,
        'category_option_id'=>85,
        'value'=>'128 GB',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-09 23:58:35'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1019,
        'product_id'=>36,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-09 23:58:35'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1020,
        'product_id'=>36,
        'category_option_id'=>87,
        'value'=>'2 MP',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1021,
        'product_id'=>36,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1022,
        'product_id'=>36,
        'category_option_id'=>89,
        'value'=>'720p HD',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1023,
        'product_id'=>36,
        'category_option_id'=>90,
        'value'=>'1080p HD at 120 fps',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1024,
        'product_id'=>36,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1025,
        'product_id'=>36,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1026,
        'product_id'=>36,
        'category_option_id'=>93,
        'value'=>'A10 Fusion Chip',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1027,
        'product_id'=>36,
        'category_option_id'=>94,
        'value'=>'iOS 11',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1028,
        'product_id'=>36,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1029,
        'product_id'=>36,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1030,
        'product_id'=>36,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1031,
        'product_id'=>36,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1032,
        'product_id'=>36,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1033,
        'product_id'=>36,
        'category_option_id'=>100,
        'value'=>'32.4 Wh',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1034,
        'product_id'=>36,
        'category_option_id'=>101,
        'value'=>'Space Grey',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1035,
        'product_id'=>36,
        'category_option_id'=>102,
        'value'=>'24 cm',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1036,
        'product_id'=>36,
        'category_option_id'=>103,
        'value'=>'0.75 cm',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1037,
        'product_id'=>36,
        'category_option_id'=>104,
        'value'=>'16.95 cm',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1038,
        'product_id'=>36,
        'category_option_id'=>105,
        'value'=>'1.2 lbs',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1039,
        'product_id'=>36,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1040,
        'product_id'=>36,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-09 23:58:36',
        'updated_at'=>'2018-12-09 23:58:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1041,
        'product_id'=>37,
        'category_option_id'=>81,
        'value'=>'LCD',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1042,
        'product_id'=>37,
        'category_option_id'=>82,
        'value'=>'11 in',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1043,
        'product_id'=>37,
        'category_option_id'=>83,
        'value'=>'2388 x 1668',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1044,
        'product_id'=>37,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1045,
        'product_id'=>37,
        'category_option_id'=>85,
        'value'=>'64 GB',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1046,
        'product_id'=>37,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1047,
        'product_id'=>37,
        'category_option_id'=>87,
        'value'=>'7 MP',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1048,
        'product_id'=>37,
        'category_option_id'=>88,
        'value'=>'12 MP',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1049,
        'product_id'=>37,
        'category_option_id'=>89,
        'value'=>'720p HD',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1050,
        'product_id'=>37,
        'category_option_id'=>90,
        'value'=>'1080p HD at 120 fps',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1051,
        'product_id'=>37,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1052,
        'product_id'=>37,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1053,
        'product_id'=>37,
        'category_option_id'=>93,
        'value'=>'A12X Bionic',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1054,
        'product_id'=>37,
        'category_option_id'=>94,
        'value'=>'iOS 12',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-10 00:05:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1055,
        'product_id'=>37,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1056,
        'product_id'=>37,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1057,
        'product_id'=>37,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1058,
        'product_id'=>37,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1059,
        'product_id'=>37,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1060,
        'product_id'=>37,
        'category_option_id'=>100,
        'value'=>'32.4 Wh',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1061,
        'product_id'=>37,
        'category_option_id'=>101,
        'value'=>'space Grey',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1062,
        'product_id'=>37,
        'category_option_id'=>102,
        'value'=>'0.59 cm',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1063,
        'product_id'=>37,
        'category_option_id'=>103,
        'value'=>'24.76 cm',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1064,
        'product_id'=>37,
        'category_option_id'=>104,
        'value'=>'17.85 cm',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1065,
        'product_id'=>37,
        'category_option_id'=>105,
        'value'=>'2 lbs',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1066,
        'product_id'=>37,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1067,
        'product_id'=>37,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:05:50',
        'updated_at'=>'2018-12-10 00:05:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1068,
        'product_id'=>38,
        'category_option_id'=>81,
        'value'=>'Retina Display',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1069,
        'product_id'=>38,
        'category_option_id'=>82,
        'value'=>'7.9 in',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1070,
        'product_id'=>38,
        'category_option_id'=>83,
        'value'=>'2066 x 1536',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1071,
        'product_id'=>38,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1072,
        'product_id'=>38,
        'category_option_id'=>85,
        'value'=>'128 GB',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1073,
        'product_id'=>38,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1074,
        'product_id'=>38,
        'category_option_id'=>87,
        'value'=>'1.2 MP',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1075,
        'product_id'=>38,
        'category_option_id'=>88,
        'value'=>'12 MP',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1076,
        'product_id'=>38,
        'category_option_id'=>89,
        'value'=>'720p HD',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1077,
        'product_id'=>38,
        'category_option_id'=>90,
        'value'=>'1080p HD at 120 fps',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1078,
        'product_id'=>38,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1079,
        'product_id'=>38,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1080,
        'product_id'=>38,
        'category_option_id'=>93,
        'value'=>'A8 Second-Generation Chip With 64-Bit Architecture',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1081,
        'product_id'=>38,
        'category_option_id'=>94,
        'value'=>'iOS 9',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1082,
        'product_id'=>38,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1083,
        'product_id'=>38,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1084,
        'product_id'=>38,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-10 00:16:23'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1085,
        'product_id'=>38,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1086,
        'product_id'=>38,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1087,
        'product_id'=>38,
        'category_option_id'=>100,
        'value'=>'19.1 Wh',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1088,
        'product_id'=>38,
        'category_option_id'=>101,
        'value'=>'Space Grey',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1089,
        'product_id'=>38,
        'category_option_id'=>102,
        'value'=>'20.32 cm',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1090,
        'product_id'=>38,
        'category_option_id'=>103,
        'value'=>'0.61 cm',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1091,
        'product_id'=>38,
        'category_option_id'=>104,
        'value'=>'13.48 cm',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1092,
        'product_id'=>38,
        'category_option_id'=>105,
        'value'=>'1.2 lbs',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1093,
        'product_id'=>38,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1094,
        'product_id'=>38,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:16:24',
        'updated_at'=>'2018-12-10 00:16:24'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1095,
        'product_id'=>39,
        'category_option_id'=>81,
        'value'=>'TFT',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1096,
        'product_id'=>39,
        'category_option_id'=>82,
        'value'=>'8 in',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1097,
        'product_id'=>39,
        'category_option_id'=>83,
        'value'=>'1280 x 800 (WXGA)',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1098,
        'product_id'=>39,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1099,
        'product_id'=>39,
        'category_option_id'=>85,
        'value'=>'32 GB',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1100,
        'product_id'=>39,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1101,
        'product_id'=>39,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-10 00:25:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1102,
        'product_id'=>39,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1103,
        'product_id'=>39,
        'category_option_id'=>89,
        'value'=>'FHD (1080p) at 30fps',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1104,
        'product_id'=>39,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1105,
        'product_id'=>39,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1106,
        'product_id'=>39,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1107,
        'product_id'=>39,
        'category_option_id'=>93,
        'value'=>'Qualcomm APQ8017',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1108,
        'product_id'=>39,
        'category_option_id'=>94,
        'value'=>'Android N',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1109,
        'product_id'=>39,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1110,
        'product_id'=>39,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1111,
        'product_id'=>39,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1112,
        'product_id'=>39,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1113,
        'product_id'=>39,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1114,
        'product_id'=>39,
        'category_option_id'=>100,
        'value'=>'19.1 Wh',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1115,
        'product_id'=>39,
        'category_option_id'=>101,
        'value'=>'black',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1116,
        'product_id'=>39,
        'category_option_id'=>102,
        'value'=>'12.41 cm',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1117,
        'product_id'=>39,
        'category_option_id'=>103,
        'value'=>'0.89 cm',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1118,
        'product_id'=>39,
        'category_option_id'=>104,
        'value'=>'21.21 cm',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1119,
        'product_id'=>39,
        'category_option_id'=>105,
        'value'=>'1.7 lbs',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1120,
        'product_id'=>39,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1121,
        'product_id'=>39,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:25:17',
        'updated_at'=>'2018-12-10 00:25:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1122,
        'product_id'=>40,
        'category_option_id'=>81,
        'value'=>'QXGA Super AMOLED',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-10 00:37:08'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1123,
        'product_id'=>40,
        'category_option_id'=>82,
        'value'=>'9.7 in',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-10 00:37:08'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1124,
        'product_id'=>40,
        'category_option_id'=>83,
        'value'=>'2048 x 1536 (QXGA)',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-10 00:37:08'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1125,
        'product_id'=>40,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-10 00:37:08'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1126,
        'product_id'=>40,
        'category_option_id'=>85,
        'value'=>'32 GB',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-10 00:37:08'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1127,
        'product_id'=>40,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-10 00:37:08'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1128,
        'product_id'=>40,
        'category_option_id'=>87,
        'value'=>'2 MP',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1129,
        'product_id'=>40,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1130,
        'product_id'=>40,
        'category_option_id'=>89,
        'value'=>'2560 x 1440 @30 fps (QHD)',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1131,
        'product_id'=>40,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1132,
        'product_id'=>40,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1133,
        'product_id'=>40,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1134,
        'product_id'=>40,
        'category_option_id'=>93,
        'value'=>'Helsinki Prime [Exynos 5433]',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1135,
        'product_id'=>40,
        'category_option_id'=>94,
        'value'=>'Android 6.0 (Marshmallow)',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1136,
        'product_id'=>40,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1137,
        'product_id'=>40,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1138,
        'product_id'=>40,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1139,
        'product_id'=>40,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1140,
        'product_id'=>40,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1141,
        'product_id'=>40,
        'category_option_id'=>100,
        'value'=>'5870 mAh',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1142,
        'product_id'=>40,
        'category_option_id'=>101,
        'value'=>'gold',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1143,
        'product_id'=>40,
        'category_option_id'=>102,
        'value'=>'23.73 cm',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1144,
        'product_id'=>40,
        'category_option_id'=>103,
        'value'=>'0.56 cm',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1145,
        'product_id'=>40,
        'category_option_id'=>104,
        'value'=>'16.9 cm',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1146,
        'product_id'=>40,
        'category_option_id'=>105,
        'value'=>'1.7 lbs',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1147,
        'product_id'=>40,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1148,
        'product_id'=>40,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:37:09',
        'updated_at'=>'2018-12-10 00:37:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1149,
        'product_id'=>41,
        'category_option_id'=>81,
        'value'=>'QXGA Super AMOLED',
        'created_at'=>'2018-12-10 00:49:05',
        'updated_at'=>'2018-12-10 00:49:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1150,
        'product_id'=>41,
        'category_option_id'=>82,
        'value'=>'10.5 in',
        'created_at'=>'2018-12-10 00:49:05',
        'updated_at'=>'2018-12-10 00:49:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1151,
        'product_id'=>41,
        'category_option_id'=>83,
        'value'=>'2560 x 1600',
        'created_at'=>'2018-12-10 00:49:05',
        'updated_at'=>'2018-12-10 00:49:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1152,
        'product_id'=>41,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1153,
        'product_id'=>41,
        'category_option_id'=>85,
        'value'=>'256 GB',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1154,
        'product_id'=>41,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1155,
        'product_id'=>41,
        'category_option_id'=>87,
        'value'=>'na',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1156,
        'product_id'=>41,
        'category_option_id'=>88,
        'value'=>'na',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1157,
        'product_id'=>41,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1158,
        'product_id'=>41,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1159,
        'product_id'=>41,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1160,
        'product_id'=>41,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1161,
        'product_id'=>41,
        'category_option_id'=>93,
        'value'=>'Qualcomm MSM 8998 (Octa-Core)',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1162,
        'product_id'=>41,
        'category_option_id'=>94,
        'value'=>'Android O',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1163,
        'product_id'=>41,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1164,
        'product_id'=>41,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1165,
        'product_id'=>41,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1166,
        'product_id'=>41,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 00:49:06',
        'updated_at'=>'2018-12-10 00:49:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1167,
        'product_id'=>41,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1168,
        'product_id'=>41,
        'category_option_id'=>100,
        'value'=>'7300 mAh',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1169,
        'product_id'=>41,
        'category_option_id'=>101,
        'value'=>'black',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1170,
        'product_id'=>41,
        'category_option_id'=>102,
        'value'=>'16.43 cm',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1171,
        'product_id'=>41,
        'category_option_id'=>103,
        'value'=>'0.71 cm',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1172,
        'product_id'=>41,
        'category_option_id'=>104,
        'value'=>'24.93 cm',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1173,
        'product_id'=>41,
        'category_option_id'=>105,
        'value'=>'1.2 lbs',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1174,
        'product_id'=>41,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1175,
        'product_id'=>41,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 00:49:07',
        'updated_at'=>'2018-12-10 00:49:07'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1176,
        'product_id'=>42,
        'category_option_id'=>81,
        'value'=>'WUXGA',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1177,
        'product_id'=>42,
        'category_option_id'=>82,
        'value'=>'10.1 in',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1178,
        'product_id'=>42,
        'category_option_id'=>83,
        'value'=>'1920 X 1200',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1179,
        'product_id'=>42,
        'category_option_id'=>84,
        'value'=>'Yes',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1180,
        'product_id'=>42,
        'category_option_id'=>85,
        'value'=>'16 GB',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1181,
        'product_id'=>42,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1182,
        'product_id'=>42,
        'category_option_id'=>87,
        'value'=>'2 MP',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1183,
        'product_id'=>42,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1184,
        'product_id'=>42,
        'category_option_id'=>89,
        'value'=>'1080p @ 30fps',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1185,
        'product_id'=>42,
        'category_option_id'=>90,
        'value'=>'1080p HD at 120 fps',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1186,
        'product_id'=>42,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1187,
        'product_id'=>42,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1188,
        'product_id'=>42,
        'category_option_id'=>93,
        'value'=>'Exynos 7870',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1189,
        'product_id'=>42,
        'category_option_id'=>94,
        'value'=>'Android 6.0',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1190,
        'product_id'=>42,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1191,
        'product_id'=>42,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1192,
        'product_id'=>42,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1193,
        'product_id'=>42,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1194,
        'product_id'=>42,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1195,
        'product_id'=>42,
        'category_option_id'=>100,
        'value'=>'32.4 Wh',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1196,
        'product_id'=>42,
        'category_option_id'=>101,
        'value'=>'white',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1197,
        'product_id'=>42,
        'category_option_id'=>102,
        'value'=>'25.40 cm',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1198,
        'product_id'=>42,
        'category_option_id'=>103,
        'value'=>'82 cm',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1199,
        'product_id'=>42,
        'category_option_id'=>104,
        'value'=>'15.53 cm',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1200,
        'product_id'=>42,
        'category_option_id'=>105,
        'value'=>'1.1 lbs',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1201,
        'product_id'=>42,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1202,
        'product_id'=>42,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:04:06',
        'updated_at'=>'2018-12-10 01:04:06'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1203,
        'product_id'=>43,
        'category_option_id'=>81,
        'value'=>'LCD',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1204,
        'product_id'=>43,
        'category_option_id'=>82,
        'value'=>'12.9 in',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1205,
        'product_id'=>43,
        'category_option_id'=>83,
        'value'=>'2732 x 2048',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1206,
        'product_id'=>43,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1207,
        'product_id'=>43,
        'category_option_id'=>85,
        'value'=>'256 GB',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1208,
        'product_id'=>43,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1209,
        'product_id'=>43,
        'category_option_id'=>87,
        'value'=>'7 MP',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1210,
        'product_id'=>43,
        'category_option_id'=>88,
        'value'=>'12 MP',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1211,
        'product_id'=>43,
        'category_option_id'=>89,
        'value'=>'1080p @ 30fps',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1212,
        'product_id'=>43,
        'category_option_id'=>90,
        'value'=>'4K video recording at 30 fps or 60 fps',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1213,
        'product_id'=>43,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1214,
        'product_id'=>43,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1215,
        'product_id'=>43,
        'category_option_id'=>93,
        'value'=>'A12X Bionic',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1216,
        'product_id'=>43,
        'category_option_id'=>94,
        'value'=>'iOS 12',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1217,
        'product_id'=>43,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1218,
        'product_id'=>43,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1219,
        'product_id'=>43,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1220,
        'product_id'=>43,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1221,
        'product_id'=>43,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1222,
        'product_id'=>43,
        'category_option_id'=>100,
        'value'=>'36.59 mAh',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1223,
        'product_id'=>43,
        'category_option_id'=>101,
        'value'=>'space grey',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1224,
        'product_id'=>43,
        'category_option_id'=>102,
        'value'=>'0.59 cm',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1225,
        'product_id'=>43,
        'category_option_id'=>103,
        'value'=>'28.06 cm',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1226,
        'product_id'=>43,
        'category_option_id'=>104,
        'value'=>'21.49 cm',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1227,
        'product_id'=>43,
        'category_option_id'=>105,
        'value'=>'1.38 lbs',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1228,
        'product_id'=>43,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1229,
        'product_id'=>43,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 01:12:49',
        'updated_at'=>'2018-12-10 01:12:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1230,
        'product_id'=>44,
        'category_option_id'=>81,
        'value'=>'LCD',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1231,
        'product_id'=>44,
        'category_option_id'=>82,
        'value'=>'9.7 in',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1232,
        'product_id'=>44,
        'category_option_id'=>83,
        'value'=>'2048 x 1536',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1233,
        'product_id'=>44,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1234,
        'product_id'=>44,
        'category_option_id'=>85,
        'value'=>'64 GB',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1235,
        'product_id'=>44,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1236,
        'product_id'=>44,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 02:12:59',
        'updated_at'=>'2018-12-10 02:12:59'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1237,
        'product_id'=>44,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1238,
        'product_id'=>44,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1239,
        'product_id'=>44,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1240,
        'product_id'=>44,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1241,
        'product_id'=>44,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1242,
        'product_id'=>44,
        'category_option_id'=>93,
        'value'=>'Mediatek 8176 Turbo Dual CA72 + Quad CA53',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1243,
        'product_id'=>44,
        'category_option_id'=>94,
        'value'=>'Android 6.0 (Marshmallow)',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1244,
        'product_id'=>44,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1245,
        'product_id'=>44,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1246,
        'product_id'=>44,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1247,
        'product_id'=>44,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1248,
        'product_id'=>44,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1249,
        'product_id'=>44,
        'category_option_id'=>100,
        'value'=>'5870 mAh',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1250,
        'product_id'=>44,
        'category_option_id'=>101,
        'value'=>'Glacier Silver',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1251,
        'product_id'=>44,
        'category_option_id'=>102,
        'value'=>'0.7 cm',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1252,
        'product_id'=>44,
        'category_option_id'=>103,
        'value'=>'24.1 cm',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1253,
        'product_id'=>44,
        'category_option_id'=>104,
        'value'=>'16.4 cm',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1254,
        'product_id'=>44,
        'category_option_id'=>105,
        'value'=>'1.7 lbs',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1255,
        'product_id'=>44,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1256,
        'product_id'=>44,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:13:00',
        'updated_at'=>'2018-12-10 02:13:00'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1257,
        'product_id'=>45,
        'category_option_id'=>81,
        'value'=>'LCD',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1258,
        'product_id'=>45,
        'category_option_id'=>82,
        'value'=>'12.3 in',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1259,
        'product_id'=>45,
        'category_option_id'=>83,
        'value'=>'2736 x 1824',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1260,
        'product_id'=>45,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1261,
        'product_id'=>45,
        'category_option_id'=>85,
        'value'=>'8 GB',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1262,
        'product_id'=>45,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1263,
        'product_id'=>45,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1264,
        'product_id'=>45,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1265,
        'product_id'=>45,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1266,
        'product_id'=>45,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1267,
        'product_id'=>45,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1268,
        'product_id'=>45,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1269,
        'product_id'=>45,
        'category_option_id'=>93,
        'value'=>'Intel Core i5-8250U',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1270,
        'product_id'=>45,
        'category_option_id'=>94,
        'value'=>'Windows 10 Home',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1271,
        'product_id'=>45,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1272,
        'product_id'=>45,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1273,
        'product_id'=>45,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1274,
        'product_id'=>45,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1275,
        'product_id'=>45,
        'category_option_id'=>99,
        'value'=>'4',
        'created_at'=>'2018-12-10 02:19:36',
        'updated_at'=>'2018-12-10 02:19:36'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1276,
        'product_id'=>45,
        'category_option_id'=>100,
        'value'=>'46.5 Wh',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1277,
        'product_id'=>45,
        'category_option_id'=>101,
        'value'=>'silver',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1278,
        'product_id'=>45,
        'category_option_id'=>102,
        'value'=>'32.30 cm',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1279,
        'product_id'=>45,
        'category_option_id'=>103,
        'value'=>'4.85 cm',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1280,
        'product_id'=>45,
        'category_option_id'=>104,
        'value'=>'23.03 cm',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1281,
        'product_id'=>45,
        'category_option_id'=>105,
        'value'=>'6 lbs',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1282,
        'product_id'=>45,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1283,
        'product_id'=>45,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:19:37',
        'updated_at'=>'2018-12-10 02:19:37'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1284,
        'product_id'=>46,
        'category_option_id'=>81,
        'value'=>'LED',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1285,
        'product_id'=>46,
        'category_option_id'=>82,
        'value'=>'9.7 in',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1286,
        'product_id'=>46,
        'category_option_id'=>83,
        'value'=>'2048 x 1536',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1287,
        'product_id'=>46,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1288,
        'product_id'=>46,
        'category_option_id'=>85,
        'value'=>'32 GB',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1289,
        'product_id'=>46,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1290,
        'product_id'=>46,
        'category_option_id'=>87,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1291,
        'product_id'=>46,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1292,
        'product_id'=>46,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1293,
        'product_id'=>46,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1294,
        'product_id'=>46,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1295,
        'product_id'=>46,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1296,
        'product_id'=>46,
        'category_option_id'=>93,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1297,
        'product_id'=>46,
        'category_option_id'=>94,
        'value'=>'iOS 11',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1298,
        'product_id'=>46,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1299,
        'product_id'=>46,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1300,
        'product_id'=>46,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1301,
        'product_id'=>46,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1302,
        'product_id'=>46,
        'category_option_id'=>99,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1303,
        'product_id'=>46,
        'category_option_id'=>100,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1304,
        'product_id'=>46,
        'category_option_id'=>101,
        'value'=>'gold',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1305,
        'product_id'=>46,
        'category_option_id'=>102,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1306,
        'product_id'=>46,
        'category_option_id'=>103,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1307,
        'product_id'=>46,
        'category_option_id'=>104,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1308,
        'product_id'=>46,
        'category_option_id'=>105,
        'value'=>'1.6',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1309,
        'product_id'=>46,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1310,
        'product_id'=>46,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:41:11',
        'updated_at'=>'2018-12-10 02:41:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1311,
        'product_id'=>47,
        'category_option_id'=>81,
        'value'=>'12.3\" 3000 x 2000 LCD touch screen display with Google Pixelbook Pen support',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1312,
        'product_id'=>47,
        'category_option_id'=>82,
        'value'=>'12.3 in',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1313,
        'product_id'=>47,
        'category_option_id'=>83,
        'value'=>'3000x2000',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1314,
        'product_id'=>47,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1315,
        'product_id'=>47,
        'category_option_id'=>85,
        'value'=>'128 GB',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1316,
        'product_id'=>47,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1317,
        'product_id'=>47,
        'category_option_id'=>87,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1318,
        'product_id'=>47,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1319,
        'product_id'=>47,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1320,
        'product_id'=>47,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1321,
        'product_id'=>47,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1322,
        'product_id'=>47,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1323,
        'product_id'=>47,
        'category_option_id'=>93,
        'value'=>'8th Gen Intel Core i5 Processor',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1324,
        'product_id'=>47,
        'category_option_id'=>94,
        'value'=>'Chrome OS',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1325,
        'product_id'=>47,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1326,
        'product_id'=>47,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1327,
        'product_id'=>47,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1328,
        'product_id'=>47,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1329,
        'product_id'=>47,
        'category_option_id'=>99,
        'value'=>'4',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1330,
        'product_id'=>47,
        'category_option_id'=>100,
        'value'=>'48 Wh',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1331,
        'product_id'=>47,
        'category_option_id'=>101,
        'value'=>'Midnight Blue',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1332,
        'product_id'=>47,
        'category_option_id'=>102,
        'value'=>'0.7 cm',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1333,
        'product_id'=>47,
        'category_option_id'=>103,
        'value'=>'29.09 cm',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1334,
        'product_id'=>47,
        'category_option_id'=>104,
        'value'=>'20.2 cm',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1335,
        'product_id'=>47,
        'category_option_id'=>105,
        'value'=>'1.38 lbs',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1336,
        'product_id'=>47,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:44',
        'updated_at'=>'2018-12-10 02:59:44'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1337,
        'product_id'=>47,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 02:59:45',
        'updated_at'=>'2018-12-10 02:59:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1338,
        'product_id'=>48,
        'category_option_id'=>81,
        'value'=>'LED',
        'created_at'=>'2018-12-10 03:13:49',
        'updated_at'=>'2018-12-10 03:13:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1339,
        'product_id'=>48,
        'category_option_id'=>82,
        'value'=>'10.1 in',
        'created_at'=>'2018-12-10 03:13:49',
        'updated_at'=>'2018-12-10 03:13:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1340,
        'product_id'=>48,
        'category_option_id'=>83,
        'value'=>'1920 x 1200',
        'created_at'=>'2018-12-10 03:13:49',
        'updated_at'=>'2018-12-10 03:13:49'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1341,
        'product_id'=>48,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1342,
        'product_id'=>48,
        'category_option_id'=>85,
        'value'=>'16 GB',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1343,
        'product_id'=>48,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1344,
        'product_id'=>48,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1345,
        'product_id'=>48,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1346,
        'product_id'=>48,
        'category_option_id'=>89,
        'value'=>'1080p @ 30fps',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1347,
        'product_id'=>48,
        'category_option_id'=>90,
        'value'=>'1080p HD at 30 fps',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1348,
        'product_id'=>48,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1349,
        'product_id'=>48,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1350,
        'product_id'=>48,
        'category_option_id'=>93,
        'value'=>'Qualcomm Snapdragon 625',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1351,
        'product_id'=>48,
        'category_option_id'=>94,
        'value'=>'Android 7.1',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1352,
        'product_id'=>48,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1353,
        'product_id'=>48,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1354,
        'product_id'=>48,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1355,
        'product_id'=>48,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1356,
        'product_id'=>48,
        'category_option_id'=>99,
        'value'=>'4',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1357,
        'product_id'=>48,
        'category_option_id'=>100,
        'value'=>'5870 mAh',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1358,
        'product_id'=>48,
        'category_option_id'=>101,
        'value'=>'slate black',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );

        ProductOption::create( [
        'id'=>1359,
        'product_id'=>48,
        'category_option_id'=>102,
        'value'=>'17.3 cm',
        'created_at'=>'2018-12-10 03:13:50',
        'updated_at'=>'2018-12-10 03:13:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1360,
        'product_id'=>48,
        'category_option_id'=>103,
        'value'=>'0.75 cm',
        'created_at'=>'2018-12-10 03:13:51',
        'updated_at'=>'2018-12-10 03:13:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1361,
        'product_id'=>48,
        'category_option_id'=>104,
        'value'=>'24.7 cm',
        'created_at'=>'2018-12-10 03:13:51',
        'updated_at'=>'2018-12-10 03:13:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1362,
        'product_id'=>48,
        'category_option_id'=>105,
        'value'=>'2 lbs',
        'created_at'=>'2018-12-10 03:13:51',
        'updated_at'=>'2018-12-10 03:13:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1363,
        'product_id'=>48,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:51',
        'updated_at'=>'2018-12-10 03:13:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1364,
        'product_id'=>48,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:13:51',
        'updated_at'=>'2018-12-10 03:13:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1365,
        'product_id'=>49,
        'category_option_id'=>81,
        'value'=>'ClearType Full HD Plus',
        'created_at'=>'2018-12-10 03:39:29',
        'updated_at'=>'2018-12-10 03:39:29'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1366,
        'product_id'=>49,
        'category_option_id'=>82,
        'value'=>'12 in',
        'created_at'=>'2018-12-10 03:39:29',
        'updated_at'=>'2018-12-10 03:39:29'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1367,
        'product_id'=>49,
        'category_option_id'=>83,
        'value'=>'2160 x 1440',
        'created_at'=>'2018-12-10 03:39:29',
        'updated_at'=>'2018-12-10 03:39:29'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1368,
        'product_id'=>49,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:29',
        'updated_at'=>'2018-12-10 03:39:29'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1369,
        'product_id'=>49,
        'category_option_id'=>85,
        'value'=>'8 GB',
        'created_at'=>'2018-12-10 03:39:29',
        'updated_at'=>'2018-12-10 03:39:29'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1370,
        'product_id'=>49,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1371,
        'product_id'=>49,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1372,
        'product_id'=>49,
        'category_option_id'=>88,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1373,
        'product_id'=>49,
        'category_option_id'=>89,
        'value'=>'1080p @ 30fps',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1374,
        'product_id'=>49,
        'category_option_id'=>90,
        'value'=>'1080p HD at 30 fps',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1375,
        'product_id'=>49,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1376,
        'product_id'=>49,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1377,
        'product_id'=>49,
        'category_option_id'=>93,
        'value'=>'Intel Core i5-4300U',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1378,
        'product_id'=>49,
        'category_option_id'=>94,
        'value'=>'Windows 10 Professional 64bit',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1379,
        'product_id'=>49,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1380,
        'product_id'=>49,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1381,
        'product_id'=>49,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1382,
        'product_id'=>49,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1383,
        'product_id'=>49,
        'category_option_id'=>99,
        'value'=>'4',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1384,
        'product_id'=>49,
        'category_option_id'=>100,
        'value'=>'19.1 Wh',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1385,
        'product_id'=>49,
        'category_option_id'=>101,
        'value'=>'silver',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1386,
        'product_id'=>49,
        'category_option_id'=>102,
        'value'=>'20.13 cm',
        'created_at'=>'2018-12-10 03:39:30',
        'updated_at'=>'2018-12-10 03:39:30'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1387,
        'product_id'=>49,
        'category_option_id'=>103,
        'value'=>'0.91 cm',
        'created_at'=>'2018-12-10 03:39:31',
        'updated_at'=>'2018-12-10 03:39:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1388,
        'product_id'=>49,
        'category_option_id'=>104,
        'value'=>'29.2 cm',
        'created_at'=>'2018-12-10 03:39:31',
        'updated_at'=>'2018-12-10 03:39:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1389,
        'product_id'=>49,
        'category_option_id'=>105,
        'value'=>'1.8 lbs',
        'created_at'=>'2018-12-10 03:39:31',
        'updated_at'=>'2018-12-10 03:39:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1390,
        'product_id'=>49,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:31',
        'updated_at'=>'2018-12-10 03:39:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1391,
        'product_id'=>49,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:39:31',
        'updated_at'=>'2018-12-10 03:39:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1392,
        'product_id'=>50,
        'category_option_id'=>81,
        'value'=>'12.3\" 3000 x 2000 LCD touch screen display with Google Pixelbook Pen support',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1393,
        'product_id'=>50,
        'category_option_id'=>82,
        'value'=>'12.3 in',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1394,
        'product_id'=>50,
        'category_option_id'=>83,
        'value'=>'3000 x 2000',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1395,
        'product_id'=>50,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1396,
        'product_id'=>50,
        'category_option_id'=>85,
        'value'=>'128 GB',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1397,
        'product_id'=>50,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1398,
        'product_id'=>50,
        'category_option_id'=>87,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1399,
        'product_id'=>50,
        'category_option_id'=>88,
        'value'=>'8 MP',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1400,
        'product_id'=>50,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1401,
        'product_id'=>50,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1402,
        'product_id'=>50,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1403,
        'product_id'=>50,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1404,
        'product_id'=>50,
        'category_option_id'=>93,
        'value'=>'8th Gen Intel Core i5 Processor',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1405,
        'product_id'=>50,
        'category_option_id'=>94,
        'value'=>'Chrome OS',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1406,
        'product_id'=>50,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1407,
        'product_id'=>50,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1408,
        'product_id'=>50,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1409,
        'product_id'=>50,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1410,
        'product_id'=>50,
        'category_option_id'=>99,
        'value'=>'4',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1411,
        'product_id'=>50,
        'category_option_id'=>100,
        'value'=>'19.1 Wh',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1412,
        'product_id'=>50,
        'category_option_id'=>101,
        'value'=>'Midnight Blue',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1413,
        'product_id'=>50,
        'category_option_id'=>102,
        'value'=>'0.7 cm',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1414,
        'product_id'=>50,
        'category_option_id'=>103,
        'value'=>'29.09 cm',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1415,
        'product_id'=>50,
        'category_option_id'=>104,
        'value'=>'20.2 cm',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1416,
        'product_id'=>50,
        'category_option_id'=>105,
        'value'=>'1.4 lbs',
        'created_at'=>'2018-12-10 03:46:51',
        'updated_at'=>'2018-12-10 03:46:51'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1417,
        'product_id'=>50,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:52',
        'updated_at'=>'2018-12-10 03:46:52'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1418,
        'product_id'=>50,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 03:46:52',
        'updated_at'=>'2018-12-10 03:46:52'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1419,
        'product_id'=>51,
        'category_option_id'=>81,
        'value'=>'LED',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1420,
        'product_id'=>51,
        'category_option_id'=>82,
        'value'=>'15.6 in',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1421,
        'product_id'=>51,
        'category_option_id'=>83,
        'value'=>'3840 x 2160',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1422,
        'product_id'=>51,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1423,
        'product_id'=>51,
        'category_option_id'=>85,
        'value'=>'512 GB',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1424,
        'product_id'=>51,
        'category_option_id'=>86,
        'value'=>'SSD',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1425,
        'product_id'=>51,
        'category_option_id'=>87,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1426,
        'product_id'=>51,
        'category_option_id'=>88,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1427,
        'product_id'=>51,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1428,
        'product_id'=>51,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1429,
        'product_id'=>51,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1430,
        'product_id'=>51,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1431,
        'product_id'=>51,
        'category_option_id'=>93,
        'value'=>'8th Generation Intel Core i7-8550U Processor',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1432,
        'product_id'=>51,
        'category_option_id'=>94,
        'value'=>'Windows 10 Home',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1433,
        'product_id'=>51,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1434,
        'product_id'=>51,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1435,
        'product_id'=>51,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1436,
        'product_id'=>51,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1437,
        'product_id'=>51,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1438,
        'product_id'=>51,
        'category_option_id'=>100,
        'value'=>'84 Wh',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1439,
        'product_id'=>51,
        'category_option_id'=>101,
        'value'=>'Dark Ash Silver',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1440,
        'product_id'=>51,
        'category_option_id'=>102,
        'value'=>'1.93 cm',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1441,
        'product_id'=>51,
        'category_option_id'=>103,
        'value'=>'35.89 cm',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1442,
        'product_id'=>51,
        'category_option_id'=>104,
        'value'=>'24.99  cm',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1443,
        'product_id'=>51,
        'category_option_id'=>105,
        'value'=>'4.6 lbs',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1444,
        'product_id'=>51,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1445,
        'product_id'=>51,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:30:47',
        'updated_at'=>'2018-12-10 05:30:47'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1446,
        'product_id'=>52,
        'category_option_id'=>81,
        'value'=>'LCD In-plane Switching (IPS) Technology',
        'created_at'=>'2018-12-10 05:54:03',
        'updated_at'=>'2018-12-10 05:54:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1447,
        'product_id'=>52,
        'category_option_id'=>82,
        'value'=>'12 in',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1448,
        'product_id'=>52,
        'category_option_id'=>83,
        'value'=>'12 in Full HD IPS touchscreen (2160 x 1440), 10-finger multi-touch support',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1449,
        'product_id'=>52,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1450,
        'product_id'=>52,
        'category_option_id'=>85,
        'value'=>'4 GB',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1451,
        'product_id'=>52,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1452,
        'product_id'=>52,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1453,
        'product_id'=>52,
        'category_option_id'=>88,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1454,
        'product_id'=>52,
        'category_option_id'=>89,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1455,
        'product_id'=>52,
        'category_option_id'=>90,
        'value'=>'na',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1456,
        'product_id'=>52,
        'category_option_id'=>91,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1457,
        'product_id'=>52,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1458,
        'product_id'=>52,
        'category_option_id'=>93,
        'value'=>'Intel Core M5-6Y54',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1459,
        'product_id'=>52,
        'category_option_id'=>94,
        'value'=>'Windows 10 Home, 64-bit',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1460,
        'product_id'=>52,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1461,
        'product_id'=>52,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:04',
        'updated_at'=>'2018-12-10 05:54:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1462,
        'product_id'=>52,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1463,
        'product_id'=>52,
        'category_option_id'=>98,
        'value'=>'Up to 9 hours battery life',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1464,
        'product_id'=>52,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1465,
        'product_id'=>52,
        'category_option_id'=>100,
        'value'=>'4300mAh',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1466,
        'product_id'=>52,
        'category_option_id'=>101,
        'value'=>'grey',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1467,
        'product_id'=>52,
        'category_option_id'=>102,
        'value'=>'7.64 cm',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1468,
        'product_id'=>52,
        'category_option_id'=>103,
        'value'=>'2.0 cm',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1469,
        'product_id'=>52,
        'category_option_id'=>104,
        'value'=>'20 cm',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1470,
        'product_id'=>52,
        'category_option_id'=>105,
        'value'=>'2 lbs',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1471,
        'product_id'=>52,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1472,
        'product_id'=>52,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 05:54:05',
        'updated_at'=>'2018-12-10 05:54:05'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1473,
        'product_id'=>53,
        'category_option_id'=>81,
        'value'=>'Super AMOLED',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1474,
        'product_id'=>53,
        'category_option_id'=>82,
        'value'=>'12.0 in',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1475,
        'product_id'=>53,
        'category_option_id'=>83,
        'value'=>'2160 x 1440',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1476,
        'product_id'=>53,
        'category_option_id'=>84,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1477,
        'product_id'=>53,
        'category_option_id'=>85,
        'value'=>'256 GB',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1478,
        'product_id'=>53,
        'category_option_id'=>86,
        'value'=>'Flash Memory',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1479,
        'product_id'=>53,
        'category_option_id'=>87,
        'value'=>'5 MP',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1480,
        'product_id'=>53,
        'category_option_id'=>88,
        'value'=>'13 MP',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1481,
        'product_id'=>53,
        'category_option_id'=>89,
        'value'=>'FHD (1920 x 1080) @ 30fps',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1482,
        'product_id'=>53,
        'category_option_id'=>90,
        'value'=>'UHD 4K (3840 x 2160) @ 30fps',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1483,
        'product_id'=>53,
        'category_option_id'=>92,
        'value'=>'no',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1484,
        'product_id'=>53,
        'category_option_id'=>93,
        'value'=>'Intel Core i5 (7th Gen. Kaby Lake)',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1485,
        'product_id'=>53,
        'category_option_id'=>94,
        'value'=>'Windows 10 Pro',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1486,
        'product_id'=>53,
        'category_option_id'=>95,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1487,
        'product_id'=>53,
        'category_option_id'=>96,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1488,
        'product_id'=>53,
        'category_option_id'=>97,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1489,
        'product_id'=>53,
        'category_option_id'=>98,
        'value'=>'10 Hours',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1490,
        'product_id'=>53,
        'category_option_id'=>99,
        'value'=>'1',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1491,
        'product_id'=>53,
        'category_option_id'=>100,
        'value'=>'Up to 11 Hours',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1492,
        'product_id'=>53,
        'category_option_id'=>101,
        'value'=>'silver',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1493,
        'product_id'=>53,
        'category_option_id'=>102,
        'value'=>'19.95 cm',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1494,
        'product_id'=>53,
        'category_option_id'=>103,
        'value'=>'29.09 cm',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1495,
        'product_id'=>53,
        'category_option_id'=>104,
        'value'=>'0.74 cm',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1496,
        'product_id'=>53,
        'category_option_id'=>105,
        'value'=>'1.4 lbs',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1497,
        'product_id'=>53,
        'category_option_id'=>106,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1498,
        'product_id'=>53,
        'category_option_id'=>107,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:06:28',
        'updated_at'=>'2018-12-10 06:06:28'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1499,
        'product_id'=>54,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:21:01',
        'updated_at'=>'2018-12-10 06:21:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1500,
        'product_id'=>54,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:21:01',
        'updated_at'=>'2018-12-10 06:21:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1501,
        'product_id'=>54,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 06:21:01',
        'updated_at'=>'2018-12-10 06:21:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1502,
        'product_id'=>54,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:21:01',
        'updated_at'=>'2018-12-10 06:21:01'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1503,
        'product_id'=>54,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1504,
        'product_id'=>54,
        'category_option_id'=>113,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1505,
        'product_id'=>54,
        'category_option_id'=>114,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1506,
        'product_id'=>54,
        'category_option_id'=>115,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1507,
        'product_id'=>54,
        'category_option_id'=>116,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1508,
        'product_id'=>54,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1509,
        'product_id'=>54,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1510,
        'product_id'=>54,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1511,
        'product_id'=>54,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1512,
        'product_id'=>54,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1513,
        'product_id'=>54,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1514,
        'product_id'=>54,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1515,
        'product_id'=>54,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1516,
        'product_id'=>54,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1517,
        'product_id'=>54,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1518,
        'product_id'=>54,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1519,
        'product_id'=>54,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1520,
        'product_id'=>54,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1521,
        'product_id'=>54,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1522,
        'product_id'=>54,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1523,
        'product_id'=>54,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1524,
        'product_id'=>54,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:02',
        'updated_at'=>'2018-12-10 06:21:02'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1525,
        'product_id'=>54,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1526,
        'product_id'=>54,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1527,
        'product_id'=>54,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1528,
        'product_id'=>54,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1529,
        'product_id'=>54,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1530,
        'product_id'=>54,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1531,
        'product_id'=>54,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1532,
        'product_id'=>54,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:21:03',
        'updated_at'=>'2018-12-10 06:21:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1533,
        'product_id'=>55,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1534,
        'product_id'=>55,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1535,
        'product_id'=>55,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1536,
        'product_id'=>55,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1537,
        'product_id'=>55,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1538,
        'product_id'=>55,
        'category_option_id'=>113,
        'value'=>'Black',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1539,
        'product_id'=>55,
        'category_option_id'=>114,
        'value'=>'15-45mm',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1540,
        'product_id'=>55,
        'category_option_id'=>115,
        'value'=>'0572C002',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1541,
        'product_id'=>55,
        'category_option_id'=>116,
        'value'=>'49mm',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1542,
        'product_id'=>55,
        'category_option_id'=>117,
        'value'=>'130g',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1543,
        'product_id'=>55,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1544,
        'product_id'=>55,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1545,
        'product_id'=>55,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1546,
        'product_id'=>55,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1547,
        'product_id'=>55,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1548,
        'product_id'=>55,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-10 06:29:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1549,
        'product_id'=>55,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1550,
        'product_id'=>55,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1551,
        'product_id'=>55,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1552,
        'product_id'=>55,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1553,
        'product_id'=>55,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1554,
        'product_id'=>55,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1555,
        'product_id'=>55,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1556,
        'product_id'=>55,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1557,
        'product_id'=>55,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1558,
        'product_id'=>55,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1559,
        'product_id'=>55,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1560,
        'product_id'=>55,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1561,
        'product_id'=>55,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1562,
        'product_id'=>55,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1563,
        'product_id'=>55,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1564,
        'product_id'=>55,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:29:56',
        'updated_at'=>'2018-12-10 06:29:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1565,
        'product_id'=>55,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:29:57',
        'updated_at'=>'2018-12-10 06:29:57'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1566,
        'product_id'=>55,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:29:57',
        'updated_at'=>'2018-12-10 06:29:57'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1567,
        'product_id'=>56,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1568,
        'product_id'=>56,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1569,
        'product_id'=>56,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1570,
        'product_id'=>56,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1571,
        'product_id'=>56,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1572,
        'product_id'=>56,
        'category_option_id'=>113,
        'value'=>'Black',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1573,
        'product_id'=>56,
        'category_option_id'=>114,
        'value'=>'FE 28–70 mm F3.5–5.6 OSS Standard Zoom',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1574,
        'product_id'=>56,
        'category_option_id'=>115,
        'value'=>'SEL2870',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1575,
        'product_id'=>56,
        'category_option_id'=>116,
        'value'=>'49mm',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1576,
        'product_id'=>56,
        'category_option_id'=>117,
        'value'=>'130g',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1577,
        'product_id'=>56,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1578,
        'product_id'=>56,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1579,
        'product_id'=>56,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1580,
        'product_id'=>56,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1581,
        'product_id'=>56,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1582,
        'product_id'=>56,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1583,
        'product_id'=>56,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1584,
        'product_id'=>56,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1585,
        'product_id'=>56,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1586,
        'product_id'=>56,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1587,
        'product_id'=>56,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1588,
        'product_id'=>56,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1589,
        'product_id'=>56,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1590,
        'product_id'=>56,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1591,
        'product_id'=>56,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1592,
        'product_id'=>56,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1593,
        'product_id'=>56,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1594,
        'product_id'=>56,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1595,
        'product_id'=>56,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1596,
        'product_id'=>56,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:10',
        'updated_at'=>'2018-12-10 06:36:10'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1597,
        'product_id'=>56,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:11',
        'updated_at'=>'2018-12-10 06:36:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1598,
        'product_id'=>56,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:36:11',
        'updated_at'=>'2018-12-10 06:36:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1599,
        'product_id'=>56,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:36:11',
        'updated_at'=>'2018-12-10 06:36:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1600,
        'product_id'=>56,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:36:11',
        'updated_at'=>'2018-12-10 06:36:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1601,
        'product_id'=>57,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1602,
        'product_id'=>57,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1603,
        'product_id'=>57,
        'category_option_id'=>110,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1604,
        'product_id'=>57,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1605,
        'product_id'=>57,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1606,
        'product_id'=>57,
        'category_option_id'=>113,
        'value'=>'Black',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1607,
        'product_id'=>57,
        'category_option_id'=>114,
        'value'=>'16–50 mm F3.5–5.6 Power Zoom',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1608,
        'product_id'=>57,
        'category_option_id'=>115,
        'value'=>'Sony SELP1650',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1609,
        'product_id'=>57,
        'category_option_id'=>116,
        'value'=>'40.5 mm',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1610,
        'product_id'=>57,
        'category_option_id'=>117,
        'value'=>'116 grams',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1611,
        'product_id'=>57,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1612,
        'product_id'=>57,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1613,
        'product_id'=>57,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1614,
        'product_id'=>57,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1615,
        'product_id'=>57,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1616,
        'product_id'=>57,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:21',
        'updated_at'=>'2018-12-10 06:47:21'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1617,
        'product_id'=>57,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1618,
        'product_id'=>57,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1619,
        'product_id'=>57,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1620,
        'product_id'=>57,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1621,
        'product_id'=>57,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1622,
        'product_id'=>57,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1623,
        'product_id'=>57,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1624,
        'product_id'=>57,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1625,
        'product_id'=>57,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1626,
        'product_id'=>57,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1627,
        'product_id'=>57,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1628,
        'product_id'=>57,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1629,
        'product_id'=>57,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1630,
        'product_id'=>57,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1631,
        'product_id'=>57,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1632,
        'product_id'=>57,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1633,
        'product_id'=>57,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1634,
        'product_id'=>57,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:47:22',
        'updated_at'=>'2018-12-10 06:47:22'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1635,
        'product_id'=>58,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1636,
        'product_id'=>58,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1637,
        'product_id'=>58,
        'category_option_id'=>110,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1638,
        'product_id'=>58,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1639,
        'product_id'=>58,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1640,
        'product_id'=>58,
        'category_option_id'=>113,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1641,
        'product_id'=>58,
        'category_option_id'=>114,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1642,
        'product_id'=>58,
        'category_option_id'=>115,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1643,
        'product_id'=>58,
        'category_option_id'=>116,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1644,
        'product_id'=>58,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1645,
        'product_id'=>58,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1646,
        'product_id'=>58,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1647,
        'product_id'=>58,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1648,
        'product_id'=>58,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1649,
        'product_id'=>58,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1650,
        'product_id'=>58,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1651,
        'product_id'=>58,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1652,
        'product_id'=>58,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1653,
        'product_id'=>58,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1654,
        'product_id'=>58,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1655,
        'product_id'=>58,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1656,
        'product_id'=>58,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1657,
        'product_id'=>58,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1658,
        'product_id'=>58,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1659,
        'product_id'=>58,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1660,
        'product_id'=>58,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1661,
        'product_id'=>58,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1662,
        'product_id'=>58,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1663,
        'product_id'=>58,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1664,
        'product_id'=>58,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1665,
        'product_id'=>58,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1666,
        'product_id'=>58,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1667,
        'product_id'=>58,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 06:50:46',
        'updated_at'=>'2018-12-10 06:50:46'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1668,
        'product_id'=>59,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1669,
        'product_id'=>59,
        'category_option_id'=>109,
        'value'=>'no',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1670,
        'product_id'=>59,
        'category_option_id'=>110,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1671,
        'product_id'=>59,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1672,
        'product_id'=>59,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1673,
        'product_id'=>59,
        'category_option_id'=>113,
        'value'=>'Black',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1674,
        'product_id'=>59,
        'category_option_id'=>114,
        'value'=>'18-55mm',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1675,
        'product_id'=>59,
        'category_option_id'=>115,
        'value'=>'AF-P DX NIKKOR 18-55mm f/3.5-5.6G VR',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1676,
        'product_id'=>59,
        'category_option_id'=>116,
        'value'=>'55mm',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1677,
        'product_id'=>59,
        'category_option_id'=>117,
        'value'=>'205g',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1678,
        'product_id'=>59,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1679,
        'product_id'=>59,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1680,
        'product_id'=>59,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1681,
        'product_id'=>59,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1682,
        'product_id'=>59,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1683,
        'product_id'=>59,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1684,
        'product_id'=>59,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1685,
        'product_id'=>59,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1686,
        'product_id'=>59,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1687,
        'product_id'=>59,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1688,
        'product_id'=>59,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1689,
        'product_id'=>59,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1690,
        'product_id'=>59,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1691,
        'product_id'=>59,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1692,
        'product_id'=>59,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1693,
        'product_id'=>59,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1694,
        'product_id'=>59,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1695,
        'product_id'=>59,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1696,
        'product_id'=>59,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1697,
        'product_id'=>59,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1698,
        'product_id'=>59,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1699,
        'product_id'=>59,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1700,
        'product_id'=>59,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1701,
        'product_id'=>59,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:02:12',
        'updated_at'=>'2018-12-10 07:02:12'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1702,
        'product_id'=>60,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:39',
        'updated_at'=>'2018-12-10 07:12:39'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1703,
        'product_id'=>60,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:39',
        'updated_at'=>'2018-12-10 07:12:39'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1704,
        'product_id'=>60,
        'category_option_id'=>110,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:39',
        'updated_at'=>'2018-12-10 07:12:39'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1705,
        'product_id'=>60,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1706,
        'product_id'=>60,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1707,
        'product_id'=>60,
        'category_option_id'=>113,
        'value'=>'silver',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1708,
        'product_id'=>60,
        'category_option_id'=>114,
        'value'=>'E PZ 16-50mm F3.5-5.6 OSS lens',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1709,
        'product_id'=>60,
        'category_option_id'=>115,
        'value'=>'Sony SELP1650',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1710,
        'product_id'=>60,
        'category_option_id'=>116,
        'value'=>'40.5 mm',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1711,
        'product_id'=>60,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1712,
        'product_id'=>60,
        'category_option_id'=>118,
        'value'=>'silver',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1713,
        'product_id'=>60,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1714,
        'product_id'=>60,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1715,
        'product_id'=>60,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1716,
        'product_id'=>60,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1717,
        'product_id'=>60,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1718,
        'product_id'=>60,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1719,
        'product_id'=>60,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1720,
        'product_id'=>60,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1721,
        'product_id'=>60,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1722,
        'product_id'=>60,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:40',
        'updated_at'=>'2018-12-10 07:12:40'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1723,
        'product_id'=>60,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1724,
        'product_id'=>60,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1725,
        'product_id'=>60,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1726,
        'product_id'=>60,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1727,
        'product_id'=>60,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1728,
        'product_id'=>60,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1729,
        'product_id'=>60,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1730,
        'product_id'=>60,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1731,
        'product_id'=>60,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1732,
        'product_id'=>60,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1733,
        'product_id'=>60,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1734,
        'product_id'=>60,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1735,
        'product_id'=>60,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 07:12:41',
        'updated_at'=>'2018-12-10 07:12:41'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1736,
        'product_id'=>61,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1737,
        'product_id'=>61,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1738,
        'product_id'=>61,
        'category_option_id'=>110,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1739,
        'product_id'=>61,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1740,
        'product_id'=>61,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1741,
        'product_id'=>61,
        'category_option_id'=>113,
        'value'=>'black',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1742,
        'product_id'=>61,
        'category_option_id'=>114,
        'value'=>'18-55mm',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1743,
        'product_id'=>61,
        'category_option_id'=>115,
        'value'=>'0572C002',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1744,
        'product_id'=>61,
        'category_option_id'=>116,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1745,
        'product_id'=>61,
        'category_option_id'=>117,
        'value'=>'130g',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1746,
        'product_id'=>61,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1747,
        'product_id'=>61,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1748,
        'product_id'=>61,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1749,
        'product_id'=>61,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1750,
        'product_id'=>61,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1751,
        'product_id'=>61,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1752,
        'product_id'=>61,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1753,
        'product_id'=>61,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1754,
        'product_id'=>61,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1755,
        'product_id'=>61,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1756,
        'product_id'=>61,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1757,
        'product_id'=>61,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1758,
        'product_id'=>61,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1759,
        'product_id'=>61,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1760,
        'product_id'=>61,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1761,
        'product_id'=>61,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1762,
        'product_id'=>61,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:16',
        'updated_at'=>'2018-12-10 08:54:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1763,
        'product_id'=>61,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1764,
        'product_id'=>61,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1765,
        'product_id'=>61,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1766,
        'product_id'=>61,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1767,
        'product_id'=>61,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1768,
        'product_id'=>61,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1769,
        'product_id'=>61,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 08:54:17',
        'updated_at'=>'2018-12-10 08:54:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1770,
        'product_id'=>62,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1771,
        'product_id'=>62,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1772,
        'product_id'=>62,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1773,
        'product_id'=>62,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1774,
        'product_id'=>62,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1775,
        'product_id'=>62,
        'category_option_id'=>113,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1776,
        'product_id'=>62,
        'category_option_id'=>114,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1777,
        'product_id'=>62,
        'category_option_id'=>115,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1778,
        'product_id'=>62,
        'category_option_id'=>116,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1779,
        'product_id'=>62,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1780,
        'product_id'=>62,
        'category_option_id'=>118,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1781,
        'product_id'=>62,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1782,
        'product_id'=>62,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1783,
        'product_id'=>62,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1784,
        'product_id'=>62,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1785,
        'product_id'=>62,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1786,
        'product_id'=>62,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1787,
        'product_id'=>62,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1788,
        'product_id'=>62,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1789,
        'product_id'=>62,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1790,
        'product_id'=>62,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1791,
        'product_id'=>62,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1792,
        'product_id'=>62,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1793,
        'product_id'=>62,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:55',
        'updated_at'=>'2018-12-10 10:04:55'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1794,
        'product_id'=>62,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1795,
        'product_id'=>62,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1796,
        'product_id'=>62,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1797,
        'product_id'=>62,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1798,
        'product_id'=>62,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1799,
        'product_id'=>62,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1800,
        'product_id'=>62,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1801,
        'product_id'=>62,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1802,
        'product_id'=>62,
        'category_option_id'=>140,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:04:56',
        'updated_at'=>'2018-12-10 10:04:56'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1803,
        'product_id'=>63,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:05:15',
        'updated_at'=>'2018-12-10 10:05:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1804,
        'product_id'=>63,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:05:15',
        'updated_at'=>'2018-12-10 10:05:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1805,
        'product_id'=>63,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 10:05:15',
        'updated_at'=>'2018-12-10 10:05:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1806,
        'product_id'=>63,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:05:15',
        'updated_at'=>'2018-12-10 10:05:15'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1807,
        'product_id'=>63,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1808,
        'product_id'=>63,
        'category_option_id'=>113,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1809,
        'product_id'=>63,
        'category_option_id'=>114,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1810,
        'product_id'=>63,
        'category_option_id'=>115,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1811,
        'product_id'=>63,
        'category_option_id'=>116,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1812,
        'product_id'=>63,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1813,
        'product_id'=>63,
        'category_option_id'=>118,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1814,
        'product_id'=>63,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1815,
        'product_id'=>63,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1816,
        'product_id'=>63,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1817,
        'product_id'=>63,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1818,
        'product_id'=>63,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1819,
        'product_id'=>63,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:16',
        'updated_at'=>'2018-12-10 10:05:16'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1820,
        'product_id'=>63,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1821,
        'product_id'=>63,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1822,
        'product_id'=>63,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1823,
        'product_id'=>63,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1824,
        'product_id'=>63,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1825,
        'product_id'=>63,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1826,
        'product_id'=>63,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1827,
        'product_id'=>63,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1828,
        'product_id'=>63,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1829,
        'product_id'=>63,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1830,
        'product_id'=>63,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1831,
        'product_id'=>63,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1832,
        'product_id'=>63,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1833,
        'product_id'=>63,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1834,
        'product_id'=>63,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1835,
        'product_id'=>63,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1836,
        'product_id'=>63,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:05:17',
        'updated_at'=>'2018-12-10 10:05:17'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1837,
        'product_id'=>64,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1838,
        'product_id'=>64,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1839,
        'product_id'=>64,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1840,
        'product_id'=>64,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1841,
        'product_id'=>64,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1842,
        'product_id'=>64,
        'category_option_id'=>113,
        'value'=>'Black',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1843,
        'product_id'=>64,
        'category_option_id'=>114,
        'value'=>'7.8x optical',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1844,
        'product_id'=>64,
        'category_option_id'=>115,
        'value'=>'2213',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1845,
        'product_id'=>64,
        'category_option_id'=>116,
        'value'=>'67 mm',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1846,
        'product_id'=>64,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1847,
        'product_id'=>64,
        'category_option_id'=>118,
        'value'=>'black',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1848,
        'product_id'=>64,
        'category_option_id'=>119,
        'value'=>'1 x Lithium-ion',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1849,
        'product_id'=>64,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1850,
        'product_id'=>64,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1851,
        'product_id'=>64,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1852,
        'product_id'=>64,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1853,
        'product_id'=>64,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1854,
        'product_id'=>64,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:03',
        'updated_at'=>'2018-12-10 10:10:03'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1855,
        'product_id'=>64,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1856,
        'product_id'=>64,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1857,
        'product_id'=>64,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1858,
        'product_id'=>64,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1859,
        'product_id'=>64,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1860,
        'product_id'=>64,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1861,
        'product_id'=>64,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1862,
        'product_id'=>64,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1863,
        'product_id'=>64,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1864,
        'product_id'=>64,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1865,
        'product_id'=>64,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1866,
        'product_id'=>64,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1867,
        'product_id'=>64,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1868,
        'product_id'=>64,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1869,
        'product_id'=>64,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1870,
        'product_id'=>64,
        'category_option_id'=>141,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:10:04',
        'updated_at'=>'2018-12-10 10:10:04'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1871,
        'product_id'=>65,
        'category_option_id'=>108,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1872,
        'product_id'=>65,
        'category_option_id'=>109,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1873,
        'product_id'=>65,
        'category_option_id'=>110,
        'value'=>'no',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1874,
        'product_id'=>65,
        'category_option_id'=>111,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1875,
        'product_id'=>65,
        'category_option_id'=>112,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1876,
        'product_id'=>65,
        'category_option_id'=>113,
        'value'=>'silver',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1877,
        'product_id'=>65,
        'category_option_id'=>114,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1878,
        'product_id'=>65,
        'category_option_id'=>115,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1879,
        'product_id'=>65,
        'category_option_id'=>116,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1880,
        'product_id'=>65,
        'category_option_id'=>117,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1881,
        'product_id'=>65,
        'category_option_id'=>118,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1882,
        'product_id'=>65,
        'category_option_id'=>119,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:31',
        'updated_at'=>'2018-12-10 10:13:31'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1883,
        'product_id'=>65,
        'category_option_id'=>120,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1884,
        'product_id'=>65,
        'category_option_id'=>121,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1885,
        'product_id'=>65,
        'category_option_id'=>122,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1886,
        'product_id'=>65,
        'category_option_id'=>123,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1887,
        'product_id'=>65,
        'category_option_id'=>124,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1888,
        'product_id'=>65,
        'category_option_id'=>125,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1889,
        'product_id'=>65,
        'category_option_id'=>126,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1890,
        'product_id'=>65,
        'category_option_id'=>127,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1891,
        'product_id'=>65,
        'category_option_id'=>128,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1892,
        'product_id'=>65,
        'category_option_id'=>129,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1893,
        'product_id'=>65,
        'category_option_id'=>130,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1894,
        'product_id'=>65,
        'category_option_id'=>131,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1895,
        'product_id'=>65,
        'category_option_id'=>132,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1896,
        'product_id'=>65,
        'category_option_id'=>133,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1897,
        'product_id'=>65,
        'category_option_id'=>134,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1898,
        'product_id'=>65,
        'category_option_id'=>135,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1899,
        'product_id'=>65,
        'category_option_id'=>136,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1900,
        'product_id'=>65,
        'category_option_id'=>137,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        ProductOption::create( [
        'id'=>1901,
        'product_id'=>65,
        'category_option_id'=>138,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:32',
        'updated_at'=>'2018-12-10 10:13:32'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1902,
        'product_id'=>65,
        'category_option_id'=>139,
        'value'=>'na',
        'created_at'=>'2018-12-10 10:13:33',
        'updated_at'=>'2018-12-10 10:13:33'
        ] );
        
        
                    
        ProductOption::create( [
        'id'=>1903,
        'product_id'=>65,
        'category_option_id'=>140,
        'value'=>'yes',
        'created_at'=>'2018-12-10 10:13:33',
        'updated_at'=>'2018-12-10 10:13:33'
        ] );
        
        
                    
        ProductOption::create( [
            'id'=>1904,
            'product_id'=>65,
            'category_option_id'=>141,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:13:33',
            'updated_at'=>'2018-12-10 10:13:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1905,
            'product_id'=>66,
            'category_option_id'=>108,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1906,
            'product_id'=>66,
            'category_option_id'=>109,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1907,
            'product_id'=>66,
            'category_option_id'=>110,
            'value'=>'no',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1908,
            'product_id'=>66,
            'category_option_id'=>111,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1909,
            'product_id'=>66,
            'category_option_id'=>112,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1910,
            'product_id'=>66,
            'category_option_id'=>113,
            'value'=>'Black',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1911,
            'product_id'=>66,
            'category_option_id'=>114,
            'value'=>'16–50 mm F3.5–5.6 Power Zoom',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1912,
            'product_id'=>66,
            'category_option_id'=>115,
            'value'=>'2213',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1913,
            'product_id'=>66,
            'category_option_id'=>116,
            'value'=>'55mm',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1914,
            'product_id'=>66,
            'category_option_id'=>117,
            'value'=>'130g',
            'created_at'=>'2018-12-10 10:35:16',
            'updated_at'=>'2018-12-10 10:35:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1915,
            'product_id'=>66,
            'category_option_id'=>118,
            'value'=>'black',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1916,
            'product_id'=>66,
            'category_option_id'=>119,
            'value'=>'1 x Lithium-ion',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1917,
            'product_id'=>66,
            'category_option_id'=>120,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1918,
            'product_id'=>66,
            'category_option_id'=>121,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1919,
            'product_id'=>66,
            'category_option_id'=>122,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1920,
            'product_id'=>66,
            'category_option_id'=>123,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1921,
            'product_id'=>66,
            'category_option_id'=>124,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1922,
            'product_id'=>66,
            'category_option_id'=>125,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1923,
            'product_id'=>66,
            'category_option_id'=>126,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1924,
            'product_id'=>66,
            'category_option_id'=>127,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1925,
            'product_id'=>66,
            'category_option_id'=>128,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1926,
            'product_id'=>66,
            'category_option_id'=>129,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1927,
            'product_id'=>66,
            'category_option_id'=>130,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1928,
            'product_id'=>66,
            'category_option_id'=>131,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:17',
            'updated_at'=>'2018-12-10 10:35:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1929,
            'product_id'=>66,
            'category_option_id'=>132,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1930,
            'product_id'=>66,
            'category_option_id'=>133,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1931,
            'product_id'=>66,
            'category_option_id'=>134,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1932,
            'product_id'=>66,
            'category_option_id'=>135,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1933,
            'product_id'=>66,
            'category_option_id'=>136,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1934,
            'product_id'=>66,
            'category_option_id'=>137,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1935,
            'product_id'=>66,
            'category_option_id'=>138,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1936,
            'product_id'=>66,
            'category_option_id'=>139,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1937,
            'product_id'=>66,
            'category_option_id'=>140,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1938,
            'product_id'=>66,
            'category_option_id'=>141,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:35:18',
            'updated_at'=>'2018-12-10 10:35:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1939,
            'product_id'=>67,
            'category_option_id'=>108,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1940,
            'product_id'=>67,
            'category_option_id'=>109,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1941,
            'product_id'=>67,
            'category_option_id'=>110,
            'value'=>'no',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1942,
            'product_id'=>67,
            'category_option_id'=>111,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1943,
            'product_id'=>67,
            'category_option_id'=>112,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1944,
            'product_id'=>67,
            'category_option_id'=>113,
            'value'=>'black',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1945,
            'product_id'=>67,
            'category_option_id'=>114,
            'value'=>'14-42mm',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1946,
            'product_id'=>67,
            'category_option_id'=>115,
            'value'=>'14-42MM F3.5-5.6 II R',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1947,
            'product_id'=>67,
            'category_option_id'=>116,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1948,
            'product_id'=>67,
            'category_option_id'=>117,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:05',
            'updated_at'=>'2018-12-10 10:40:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1949,
            'product_id'=>67,
            'category_option_id'=>118,
            'value'=>'black',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1950,
            'product_id'=>67,
            'category_option_id'=>119,
            'value'=>'1 x Lithium-ion',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1951,
            'product_id'=>67,
            'category_option_id'=>120,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1952,
            'product_id'=>67,
            'category_option_id'=>121,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1953,
            'product_id'=>67,
            'category_option_id'=>122,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1954,
            'product_id'=>67,
            'category_option_id'=>123,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1955,
            'product_id'=>67,
            'category_option_id'=>124,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1956,
            'product_id'=>67,
            'category_option_id'=>125,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1957,
            'product_id'=>67,
            'category_option_id'=>126,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1958,
            'product_id'=>67,
            'category_option_id'=>127,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1959,
            'product_id'=>67,
            'category_option_id'=>128,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1960,
            'product_id'=>67,
            'category_option_id'=>129,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1961,
            'product_id'=>67,
            'category_option_id'=>130,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1962,
            'product_id'=>67,
            'category_option_id'=>131,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1963,
            'product_id'=>67,
            'category_option_id'=>132,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1964,
            'product_id'=>67,
            'category_option_id'=>133,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1965,
            'product_id'=>67,
            'category_option_id'=>134,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1966,
            'product_id'=>67,
            'category_option_id'=>135,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1967,
            'product_id'=>67,
            'category_option_id'=>136,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1968,
            'product_id'=>67,
            'category_option_id'=>137,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:06',
            'updated_at'=>'2018-12-10 10:40:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1969,
            'product_id'=>67,
            'category_option_id'=>138,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:07',
            'updated_at'=>'2018-12-10 10:40:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1970,
            'product_id'=>67,
            'category_option_id'=>139,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:40:07',
            'updated_at'=>'2018-12-10 10:40:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1971,
            'product_id'=>67,
            'category_option_id'=>140,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:40:07',
            'updated_at'=>'2018-12-10 10:40:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1972,
            'product_id'=>67,
            'category_option_id'=>141,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:40:07',
            'updated_at'=>'2018-12-10 10:40:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1973,
            'product_id'=>68,
            'category_option_id'=>108,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1974,
            'product_id'=>68,
            'category_option_id'=>109,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1975,
            'product_id'=>68,
            'category_option_id'=>110,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1976,
            'product_id'=>68,
            'category_option_id'=>111,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1977,
            'product_id'=>68,
            'category_option_id'=>112,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1978,
            'product_id'=>68,
            'category_option_id'=>113,
            'value'=>'Black',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1979,
            'product_id'=>68,
            'category_option_id'=>114,
            'value'=>'FE 28-70mm F3.5-5.6 OSS',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1980,
            'product_id'=>68,
            'category_option_id'=>115,
            'value'=>'SEL2870',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1981,
            'product_id'=>68,
            'category_option_id'=>116,
            'value'=>'55 mm',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1982,
            'product_id'=>68,
            'category_option_id'=>117,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1983,
            'product_id'=>68,
            'category_option_id'=>118,
            'value'=>'black',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1984,
            'product_id'=>68,
            'category_option_id'=>119,
            'value'=>'1 x Lithium-ion',
            'created_at'=>'2018-12-10 10:45:21',
            'updated_at'=>'2018-12-10 10:45:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1985,
            'product_id'=>68,
            'category_option_id'=>120,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1986,
            'product_id'=>68,
            'category_option_id'=>121,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1987,
            'product_id'=>68,
            'category_option_id'=>122,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1988,
            'product_id'=>68,
            'category_option_id'=>123,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1989,
            'product_id'=>68,
            'category_option_id'=>124,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1990,
            'product_id'=>68,
            'category_option_id'=>125,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1991,
            'product_id'=>68,
            'category_option_id'=>126,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1992,
            'product_id'=>68,
            'category_option_id'=>127,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1993,
            'product_id'=>68,
            'category_option_id'=>128,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1994,
            'product_id'=>68,
            'category_option_id'=>129,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1995,
            'product_id'=>68,
            'category_option_id'=>130,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1996,
            'product_id'=>68,
            'category_option_id'=>131,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1997,
            'product_id'=>68,
            'category_option_id'=>132,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1998,
            'product_id'=>68,
            'category_option_id'=>133,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>1999,
            'product_id'=>68,
            'category_option_id'=>134,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2000,
            'product_id'=>68,
            'category_option_id'=>135,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2001,
            'product_id'=>68,
            'category_option_id'=>136,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2002,
            'product_id'=>68,
            'category_option_id'=>137,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:22',
            'updated_at'=>'2018-12-10 10:45:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2003,
            'product_id'=>68,
            'category_option_id'=>138,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:23',
            'updated_at'=>'2018-12-10 10:45:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2004,
            'product_id'=>68,
            'category_option_id'=>139,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:45:23',
            'updated_at'=>'2018-12-10 10:45:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2005,
            'product_id'=>68,
            'category_option_id'=>140,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:23',
            'updated_at'=>'2018-12-10 10:45:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2006,
            'product_id'=>68,
            'category_option_id'=>141,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:45:23',
            'updated_at'=>'2018-12-10 10:45:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2007,
            'product_id'=>69,
            'category_option_id'=>108,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:52:37',
            'updated_at'=>'2018-12-10 10:52:37'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2008,
            'product_id'=>69,
            'category_option_id'=>109,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2009,
            'product_id'=>69,
            'category_option_id'=>110,
            'value'=>'no',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2010,
            'product_id'=>69,
            'category_option_id'=>111,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2011,
            'product_id'=>69,
            'category_option_id'=>112,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2012,
            'product_id'=>69,
            'category_option_id'=>113,
            'value'=>'black',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2013,
            'product_id'=>69,
            'category_option_id'=>114,
            'value'=>'18-55mm',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2014,
            'product_id'=>69,
            'category_option_id'=>115,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2015,
            'product_id'=>69,
            'category_option_id'=>116,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2016,
            'product_id'=>69,
            'category_option_id'=>117,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2017,
            'product_id'=>69,
            'category_option_id'=>118,
            'value'=>'black',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2018,
            'product_id'=>69,
            'category_option_id'=>119,
            'value'=>'1 x Lithium-ion',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2019,
            'product_id'=>69,
            'category_option_id'=>120,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2020,
            'product_id'=>69,
            'category_option_id'=>121,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2021,
            'product_id'=>69,
            'category_option_id'=>122,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2022,
            'product_id'=>69,
            'category_option_id'=>123,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2023,
            'product_id'=>69,
            'category_option_id'=>124,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2024,
            'product_id'=>69,
            'category_option_id'=>125,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:38',
            'updated_at'=>'2018-12-10 10:52:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2025,
            'product_id'=>69,
            'category_option_id'=>126,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2026,
            'product_id'=>69,
            'category_option_id'=>127,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2027,
            'product_id'=>69,
            'category_option_id'=>128,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2028,
            'product_id'=>69,
            'category_option_id'=>129,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2029,
            'product_id'=>69,
            'category_option_id'=>130,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2030,
            'product_id'=>69,
            'category_option_id'=>131,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2031,
            'product_id'=>69,
            'category_option_id'=>132,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2032,
            'product_id'=>69,
            'category_option_id'=>133,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2033,
            'product_id'=>69,
            'category_option_id'=>134,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2034,
            'product_id'=>69,
            'category_option_id'=>135,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2035,
            'product_id'=>69,
            'category_option_id'=>136,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2036,
            'product_id'=>69,
            'category_option_id'=>137,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2037,
            'product_id'=>69,
            'category_option_id'=>138,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2038,
            'product_id'=>69,
            'category_option_id'=>139,
            'value'=>'na',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2039,
            'product_id'=>69,
            'category_option_id'=>140,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2040,
            'product_id'=>69,
            'category_option_id'=>141,
            'value'=>'yes',
            'created_at'=>'2018-12-10 10:52:39',
            'updated_at'=>'2018-12-10 10:52:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2041,
            'product_id'=>70,
            'category_option_id'=>1,
            'value'=>'42.5 in',
            'created_at'=>'2018-12-10 11:19:27',
            'updated_at'=>'2018-12-10 11:19:27'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2042,
            'product_id'=>70,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 11:19:27',
            'updated_at'=>'2018-12-10 11:19:27'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2043,
            'product_id'=>70,
            'category_option_id'=>3,
            'value'=>'LED',
            'created_at'=>'2018-12-10 11:19:27',
            'updated_at'=>'2018-12-10 11:19:27'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2044,
            'product_id'=>70,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:27',
            'updated_at'=>'2018-12-10 11:19:27'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2045,
            'product_id'=>70,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:27',
            'updated_at'=>'2018-12-10 11:19:27'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2046,
            'product_id'=>70,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 11:19:27',
            'updated_at'=>'2018-12-10 11:19:27'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2047,
            'product_id'=>70,
            'category_option_id'=>7,
            'value'=>'DTS Studio Sound',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2048,
            'product_id'=>70,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2049,
            'product_id'=>70,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2050,
            'product_id'=>70,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2051,
            'product_id'=>70,
            'category_option_id'=>11,
            'value'=>'1',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2052,
            'product_id'=>70,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2053,
            'product_id'=>70,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2054,
            'product_id'=>70,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2055,
            'product_id'=>70,
            'category_option_id'=>15,
            'value'=>'Roku TV',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2056,
            'product_id'=>70,
            'category_option_id'=>16,
            'value'=>'Black Glossy With Hairline',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2057,
            'product_id'=>70,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2058,
            'product_id'=>70,
            'category_option_id'=>18,
            'value'=>'97.0 cm',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2059,
            'product_id'=>70,
            'category_option_id'=>19,
            'value'=>'60.7 cm',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2060,
            'product_id'=>70,
            'category_option_id'=>20,
            'value'=>'10.1 kg',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2061,
            'product_id'=>70,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2062,
            'product_id'=>70,
            'category_option_id'=>22,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:19:28',
            'updated_at'=>'2018-12-10 11:19:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2063,
            'product_id'=>71,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2064,
            'product_id'=>71,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2065,
            'product_id'=>71,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2066,
            'product_id'=>71,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2067,
            'product_id'=>71,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2068,
            'product_id'=>71,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2069,
            'product_id'=>71,
            'category_option_id'=>7,
            'value'=>'DTS Studio Sound',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2070,
            'product_id'=>71,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2071,
            'product_id'=>71,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2072,
            'product_id'=>71,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2073,
            'product_id'=>71,
            'category_option_id'=>11,
            'value'=>'1',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2074,
            'product_id'=>71,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2075,
            'product_id'=>71,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2076,
            'product_id'=>71,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2077,
            'product_id'=>71,
            'category_option_id'=>15,
            'value'=>'na',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2078,
            'product_id'=>71,
            'category_option_id'=>16,
            'value'=>'na',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2079,
            'product_id'=>71,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:24:34',
            'updated_at'=>'2018-12-10 11:24:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2080,
            'product_id'=>71,
            'category_option_id'=>18,
            'value'=>'144.97 cm',
            'created_at'=>'2018-12-10 11:24:35',
            'updated_at'=>'2018-12-10 11:24:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2081,
            'product_id'=>71,
            'category_option_id'=>19,
            'value'=>'82.93 cm',
            'created_at'=>'2018-12-10 11:24:35',
            'updated_at'=>'2018-12-10 11:24:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2082,
            'product_id'=>71,
            'category_option_id'=>20,
            'value'=>'25 kg',
            'created_at'=>'2018-12-10 11:24:35',
            'updated_at'=>'2018-12-10 11:24:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2083,
            'product_id'=>71,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 11:24:35',
            'updated_at'=>'2018-12-10 11:24:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2084,
            'product_id'=>71,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 11:24:35',
            'updated_at'=>'2018-12-10 11:24:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2085,
            'product_id'=>72,
            'category_option_id'=>1,
            'value'=>'43 in',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2086,
            'product_id'=>72,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2087,
            'product_id'=>72,
            'category_option_id'=>3,
            'value'=>'LED',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2088,
            'product_id'=>72,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2089,
            'product_id'=>72,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2090,
            'product_id'=>72,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2091,
            'product_id'=>72,
            'category_option_id'=>7,
            'value'=>'Dolby Digital Plus',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2092,
            'product_id'=>72,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2093,
            'product_id'=>72,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2094,
            'product_id'=>72,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2095,
            'product_id'=>72,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 12:00:59',
            'updated_at'=>'2018-12-10 12:00:59'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2096,
            'product_id'=>72,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2097,
            'product_id'=>72,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2098,
            'product_id'=>72,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2099,
            'product_id'=>72,
            'category_option_id'=>15,
            'value'=>'Tizen OS',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2100,
            'product_id'=>72,
            'category_option_id'=>16,
            'value'=>'Charcoal Black',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2101,
            'product_id'=>72,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2102,
            'product_id'=>72,
            'category_option_id'=>18,
            'value'=>'97.02 cm',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2103,
            'product_id'=>72,
            'category_option_id'=>19,
            'value'=>'63.63 cm',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2104,
            'product_id'=>72,
            'category_option_id'=>20,
            'value'=>'9.8 kg',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2105,
            'product_id'=>72,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2106,
            'product_id'=>72,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 12:01:00',
            'updated_at'=>'2018-12-10 12:01:00'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2107,
            'product_id'=>73,
            'category_option_id'=>1,
            'value'=>'55 in',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2108,
            'product_id'=>73,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2109,
            'product_id'=>73,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2110,
            'product_id'=>73,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2111,
            'product_id'=>73,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2112,
            'product_id'=>73,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2113,
            'product_id'=>73,
            'category_option_id'=>7,
            'value'=>'Dolby Digital Plus',
            'created_at'=>'2018-12-10 12:10:13',
            'updated_at'=>'2018-12-10 12:10:13'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2114,
            'product_id'=>73,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2115,
            'product_id'=>73,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2116,
            'product_id'=>73,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2117,
            'product_id'=>73,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2118,
            'product_id'=>73,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2119,
            'product_id'=>73,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2120,
            'product_id'=>73,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2121,
            'product_id'=>73,
            'category_option_id'=>15,
            'value'=>'Tizen OS',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2122,
            'product_id'=>73,
            'category_option_id'=>16,
            'value'=>'Charcoal Black',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2123,
            'product_id'=>73,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2124,
            'product_id'=>73,
            'category_option_id'=>18,
            'value'=>'123.86 cm',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2125,
            'product_id'=>73,
            'category_option_id'=>19,
            'value'=>'79.28 cm',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2126,
            'product_id'=>73,
            'category_option_id'=>20,
            'value'=>'17.3 kg',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2127,
            'product_id'=>73,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2128,
            'product_id'=>73,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 12:10:14',
            'updated_at'=>'2018-12-10 12:10:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2129,
            'product_id'=>74,
            'category_option_id'=>1,
            'value'=>'55 in',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2130,
            'product_id'=>74,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2131,
            'product_id'=>74,
            'category_option_id'=>3,
            'value'=>'LED',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2132,
            'product_id'=>74,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2133,
            'product_id'=>74,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2134,
            'product_id'=>74,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2135,
            'product_id'=>74,
            'category_option_id'=>7,
            'value'=>'Dolby Digital Plus',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2136,
            'product_id'=>74,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2137,
            'product_id'=>74,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2138,
            'product_id'=>74,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2139,
            'product_id'=>74,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2140,
            'product_id'=>74,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2141,
            'product_id'=>74,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2142,
            'product_id'=>74,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2143,
            'product_id'=>74,
            'category_option_id'=>15,
            'value'=>'Fire TV',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2144,
            'product_id'=>74,
            'category_option_id'=>16,
            'value'=>'Charcoal Black',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2145,
            'product_id'=>74,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2146,
            'product_id'=>74,
            'category_option_id'=>18,
            'value'=>'124.7 cm',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2147,
            'product_id'=>74,
            'category_option_id'=>19,
            'value'=>'76.7 cm',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2148,
            'product_id'=>74,
            'category_option_id'=>20,
            'value'=>'15 kg',
            'created_at'=>'2018-12-10 12:20:14',
            'updated_at'=>'2018-12-10 12:20:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2149,
            'product_id'=>74,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 12:20:15',
            'updated_at'=>'2018-12-10 12:20:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2150,
            'product_id'=>74,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 12:20:15',
            'updated_at'=>'2018-12-10 12:20:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2151,
            'product_id'=>75,
            'category_option_id'=>1,
            'value'=>'55 in',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2152,
            'product_id'=>75,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2153,
            'product_id'=>75,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2154,
            'product_id'=>75,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2155,
            'product_id'=>75,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2156,
            'product_id'=>75,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2157,
            'product_id'=>75,
            'category_option_id'=>7,
            'value'=>'Dolby Digital Plus',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2158,
            'product_id'=>75,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 21:27:14',
            'updated_at'=>'2018-12-10 21:27:14'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2159,
            'product_id'=>75,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2160,
            'product_id'=>75,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2161,
            'product_id'=>75,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2162,
            'product_id'=>75,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2163,
            'product_id'=>75,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2164,
            'product_id'=>75,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2165,
            'product_id'=>75,
            'category_option_id'=>15,
            'value'=>'Tizen OS',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2166,
            'product_id'=>75,
            'category_option_id'=>16,
            'value'=>'Charcoal Black',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2167,
            'product_id'=>75,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2168,
            'product_id'=>75,
            'category_option_id'=>18,
            'value'=>'123.86 cm',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2169,
            'product_id'=>75,
            'category_option_id'=>19,
            'value'=>'76.7 cm',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2170,
            'product_id'=>75,
            'category_option_id'=>20,
            'value'=>'25 kg',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2171,
            'product_id'=>75,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2172,
            'product_id'=>75,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 21:27:15',
            'updated_at'=>'2018-12-10 21:27:15'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2173,
            'product_id'=>76,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 21:36:16',
            'updated_at'=>'2018-12-10 21:36:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2174,
            'product_id'=>76,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 21:36:16',
            'updated_at'=>'2018-12-10 21:36:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2175,
            'product_id'=>76,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 21:36:16',
            'updated_at'=>'2018-12-10 21:36:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2176,
            'product_id'=>76,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:36:16',
            'updated_at'=>'2018-12-10 21:36:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2177,
            'product_id'=>76,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2178,
            'product_id'=>76,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2179,
            'product_id'=>76,
            'category_option_id'=>7,
            'value'=>'Acoustic Surface+',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2180,
            'product_id'=>76,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2181,
            'product_id'=>76,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2182,
            'product_id'=>76,
            'category_option_id'=>10,
            'value'=>'4',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2183,
            'product_id'=>76,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2184,
            'product_id'=>76,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2185,
            'product_id'=>76,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2186,
            'product_id'=>76,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2187,
            'product_id'=>76,
            'category_option_id'=>15,
            'value'=>'Android OS',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2188,
            'product_id'=>76,
            'category_option_id'=>16,
            'value'=>'Charcoal Black',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2189,
            'product_id'=>76,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2190,
            'product_id'=>76,
            'category_option_id'=>18,
            'value'=>'144.9 cm',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2191,
            'product_id'=>76,
            'category_option_id'=>19,
            'value'=>'83.2 cm',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2192,
            'product_id'=>76,
            'category_option_id'=>20,
            'value'=>'35.6 kg',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2193,
            'product_id'=>76,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2194,
            'product_id'=>76,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 21:36:17',
            'updated_at'=>'2018-12-10 21:36:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2195,
            'product_id'=>77,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2196,
            'product_id'=>77,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2197,
            'product_id'=>77,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2198,
            'product_id'=>77,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2199,
            'product_id'=>77,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2200,
            'product_id'=>77,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2201,
            'product_id'=>77,
            'category_option_id'=>7,
            'value'=>'DOLBY ATMOS, Dolby Surround, Magic Sound Tuning, Clear Voice III, Hi-Fi Audio',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2202,
            'product_id'=>77,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2203,
            'product_id'=>77,
            'category_option_id'=>9,
            'value'=>'60 W',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2204,
            'product_id'=>77,
            'category_option_id'=>10,
            'value'=>'4',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2205,
            'product_id'=>77,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2206,
            'product_id'=>77,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 21:43:23',
            'updated_at'=>'2018-12-10 21:43:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2207,
            'product_id'=>77,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2208,
            'product_id'=>77,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2209,
            'product_id'=>77,
            'category_option_id'=>15,
            'value'=>'webOS',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2210,
            'product_id'=>77,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2211,
            'product_id'=>77,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2212,
            'product_id'=>77,
            'category_option_id'=>18,
            'value'=>'163.3 cm',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2213,
            'product_id'=>77,
            'category_option_id'=>19,
            'value'=>'95.4 cm',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2214,
            'product_id'=>77,
            'category_option_id'=>20,
            'value'=>'28.8 cm',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2215,
            'product_id'=>77,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2216,
            'product_id'=>77,
            'category_option_id'=>22,
            'value'=>'na',
            'created_at'=>'2018-12-10 21:43:24',
            'updated_at'=>'2018-12-10 21:43:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2217,
            'product_id'=>78,
            'category_option_id'=>1,
            'value'=>'75 in',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2218,
            'product_id'=>78,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2219,
            'product_id'=>78,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2220,
            'product_id'=>78,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2221,
            'product_id'=>78,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2222,
            'product_id'=>78,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2223,
            'product_id'=>78,
            'category_option_id'=>7,
            'value'=>'DTS Studio Sound',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2224,
            'product_id'=>78,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2225,
            'product_id'=>78,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2226,
            'product_id'=>78,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2227,
            'product_id'=>78,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2228,
            'product_id'=>78,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2229,
            'product_id'=>78,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2230,
            'product_id'=>78,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2231,
            'product_id'=>78,
            'category_option_id'=>15,
            'value'=>'Android',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2232,
            'product_id'=>78,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2233,
            'product_id'=>78,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2234,
            'product_id'=>78,
            'category_option_id'=>18,
            'value'=>'167.64 cm',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2235,
            'product_id'=>78,
            'category_option_id'=>19,
            'value'=>'167.64 cm',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2236,
            'product_id'=>78,
            'category_option_id'=>20,
            'value'=>'36.7 kg',
            'created_at'=>'2018-12-10 22:00:20',
            'updated_at'=>'2018-12-10 22:00:20'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2237,
            'product_id'=>78,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:00:21',
            'updated_at'=>'2018-12-10 22:00:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2238,
            'product_id'=>78,
            'category_option_id'=>22,
            'value'=>'XBR75X900F',
            'created_at'=>'2018-12-10 22:00:21',
            'updated_at'=>'2018-12-10 22:00:21'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2239,
            'product_id'=>79,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2240,
            'product_id'=>79,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2241,
            'product_id'=>79,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2242,
            'product_id'=>79,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2243,
            'product_id'=>79,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2244,
            'product_id'=>79,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2245,
            'product_id'=>79,
            'category_option_id'=>7,
            'value'=>'Dolby Digital Plus',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2246,
            'product_id'=>79,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2247,
            'product_id'=>79,
            'category_option_id'=>10,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2248,
            'product_id'=>79,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2249,
            'product_id'=>79,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:14:05',
            'updated_at'=>'2018-12-10 22:14:05'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2250,
            'product_id'=>79,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2251,
            'product_id'=>79,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2252,
            'product_id'=>79,
            'category_option_id'=>15,
            'value'=>'Tizen Smart',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2253,
            'product_id'=>79,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2254,
            'product_id'=>79,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2255,
            'product_id'=>79,
            'category_option_id'=>18,
            'value'=>'145.75 cm',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2256,
            'product_id'=>79,
            'category_option_id'=>19,
            'value'=>'87.4 cm',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2257,
            'product_id'=>79,
            'category_option_id'=>20,
            'value'=>'31.73 cm',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2258,
            'product_id'=>79,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2259,
            'product_id'=>79,
            'category_option_id'=>22,
            'value'=>'UN65NU6900FXZC',
            'created_at'=>'2018-12-10 22:14:06',
            'updated_at'=>'2018-12-10 22:14:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2260,
            'product_id'=>80,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2261,
            'product_id'=>80,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2262,
            'product_id'=>80,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2263,
            'product_id'=>80,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2264,
            'product_id'=>80,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2265,
            'product_id'=>80,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2266,
            'product_id'=>80,
            'category_option_id'=>7,
            'value'=>'na',
            'created_at'=>'2018-12-10 22:19:57',
            'updated_at'=>'2018-12-10 22:19:57'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2267,
            'product_id'=>80,
            'category_option_id'=>8,
            'value'=>'na',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2268,
            'product_id'=>80,
            'category_option_id'=>9,
            'value'=>'8W',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2269,
            'product_id'=>80,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2270,
            'product_id'=>80,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2271,
            'product_id'=>80,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2272,
            'product_id'=>80,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2273,
            'product_id'=>80,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2274,
            'product_id'=>80,
            'category_option_id'=>15,
            'value'=>'Android',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2275,
            'product_id'=>80,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2276,
            'product_id'=>80,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2277,
            'product_id'=>80,
            'category_option_id'=>18,
            'value'=>'123.86 cm',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2278,
            'product_id'=>80,
            'category_option_id'=>19,
            'value'=>'76.7 cm',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2279,
            'product_id'=>80,
            'category_option_id'=>20,
            'value'=>'10.1 kg',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2280,
            'product_id'=>80,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2281,
            'product_id'=>80,
            'category_option_id'=>22,
            'value'=>'P65-E1',
            'created_at'=>'2018-12-10 22:19:58',
            'updated_at'=>'2018-12-10 22:19:58'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2282,
            'product_id'=>81,
            'category_option_id'=>1,
            'value'=>'48.5 in',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2283,
            'product_id'=>81,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2284,
            'product_id'=>81,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2285,
            'product_id'=>81,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2286,
            'product_id'=>81,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2287,
            'product_id'=>81,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2288,
            'product_id'=>81,
            'category_option_id'=>7,
            'value'=>'Dolby Digital Plus',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2289,
            'product_id'=>81,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2290,
            'product_id'=>81,
            'category_option_id'=>9,
            'value'=>'40 W',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2291,
            'product_id'=>81,
            'category_option_id'=>10,
            'value'=>'4',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2292,
            'product_id'=>81,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2293,
            'product_id'=>81,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2294,
            'product_id'=>81,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:24:18',
            'updated_at'=>'2018-12-10 22:24:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2295,
            'product_id'=>81,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2296,
            'product_id'=>81,
            'category_option_id'=>15,
            'value'=>'Tizen OS',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2297,
            'product_id'=>81,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2298,
            'product_id'=>81,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2299,
            'product_id'=>81,
            'category_option_id'=>18,
            'value'=>'109.16 cm',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2300,
            'product_id'=>81,
            'category_option_id'=>19,
            'value'=>'63.46 cm',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2301,
            'product_id'=>81,
            'category_option_id'=>20,
            'value'=>'13.4 kg',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2302,
            'product_id'=>81,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2303,
            'product_id'=>81,
            'category_option_id'=>22,
            'value'=>'QN49Q6FNAFXZC',
            'created_at'=>'2018-12-10 22:24:19',
            'updated_at'=>'2018-12-10 22:24:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2304,
            'product_id'=>82,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 22:29:06',
            'updated_at'=>'2018-12-10 22:29:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2305,
            'product_id'=>82,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:29:06',
            'updated_at'=>'2018-12-10 22:29:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2306,
            'product_id'=>82,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2307,
            'product_id'=>82,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2308,
            'product_id'=>82,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2309,
            'product_id'=>82,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2310,
            'product_id'=>82,
            'category_option_id'=>7,
            'value'=>'Down Firing with Bass Reflex',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2311,
            'product_id'=>82,
            'category_option_id'=>8,
            'value'=>'2.2CH (Down Firing with Bass Reflex)',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2312,
            'product_id'=>82,
            'category_option_id'=>9,
            'value'=>'20 W',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2313,
            'product_id'=>82,
            'category_option_id'=>10,
            'value'=>'3',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2314,
            'product_id'=>82,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2315,
            'product_id'=>82,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2316,
            'product_id'=>82,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2317,
            'product_id'=>82,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2318,
            'product_id'=>82,
            'category_option_id'=>15,
            'value'=>'Tizen OS',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2319,
            'product_id'=>82,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2320,
            'product_id'=>82,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2321,
            'product_id'=>82,
            'category_option_id'=>18,
            'value'=>'145.39 cm',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2322,
            'product_id'=>82,
            'category_option_id'=>19,
            'value'=>'87.75 cm',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2323,
            'product_id'=>82,
            'category_option_id'=>20,
            'value'=>'29.43 cm',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2324,
            'product_id'=>82,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:29:07',
            'updated_at'=>'2018-12-10 22:29:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2325,
            'product_id'=>82,
            'category_option_id'=>22,
            'value'=>'UN65LS03NAFXZC',
            'created_at'=>'2018-12-10 22:29:08',
            'updated_at'=>'2018-12-10 22:29:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2326,
            'product_id'=>83,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 22:34:22',
            'updated_at'=>'2018-12-10 22:34:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2327,
            'product_id'=>83,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2328,
            'product_id'=>83,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2329,
            'product_id'=>83,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2330,
            'product_id'=>83,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2331,
            'product_id'=>83,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2332,
            'product_id'=>83,
            'category_option_id'=>7,
            'value'=>'Ultra Surround; Clear Voice III; Smart Sound Mode; Audio Upscaler',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2333,
            'product_id'=>83,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2334,
            'product_id'=>83,
            'category_option_id'=>9,
            'value'=>'40 W',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2335,
            'product_id'=>83,
            'category_option_id'=>10,
            'value'=>'4',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2336,
            'product_id'=>83,
            'category_option_id'=>11,
            'value'=>'3',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2337,
            'product_id'=>83,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2338,
            'product_id'=>83,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2339,
            'product_id'=>83,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2340,
            'product_id'=>83,
            'category_option_id'=>15,
            'value'=>'webOS',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2341,
            'product_id'=>83,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2342,
            'product_id'=>83,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2343,
            'product_id'=>83,
            'category_option_id'=>18,
            'value'=>'145.6 cm',
            'created_at'=>'2018-12-10 22:34:23',
            'updated_at'=>'2018-12-10 22:34:23'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2344,
            'product_id'=>83,
            'category_option_id'=>19,
            'value'=>'90.6 cm',
            'created_at'=>'2018-12-10 22:34:24',
            'updated_at'=>'2018-12-10 22:34:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2345,
            'product_id'=>83,
            'category_option_id'=>20,
            'value'=>'26.9 cm',
            'created_at'=>'2018-12-10 22:34:24',
            'updated_at'=>'2018-12-10 22:34:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2346,
            'product_id'=>83,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:34:24',
            'updated_at'=>'2018-12-10 22:34:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2347,
            'product_id'=>83,
            'category_option_id'=>22,
            'value'=>'65UK6500',
            'created_at'=>'2018-12-10 22:34:24',
            'updated_at'=>'2018-12-10 22:34:24'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2348,
            'product_id'=>84,
            'category_option_id'=>1,
            'value'=>'55 in',
            'created_at'=>'2018-12-10 22:41:28',
            'updated_at'=>'2018-12-10 22:41:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2349,
            'product_id'=>84,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:41:28',
            'updated_at'=>'2018-12-10 22:41:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2350,
            'product_id'=>84,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:41:28',
            'updated_at'=>'2018-12-10 22:41:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2351,
            'product_id'=>84,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:41:28',
            'updated_at'=>'2018-12-10 22:41:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2352,
            'product_id'=>84,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:41:28',
            'updated_at'=>'2018-12-10 22:41:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2353,
            'product_id'=>84,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:41:28',
            'updated_at'=>'2018-12-10 22:41:28'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2354,
            'product_id'=>84,
            'category_option_id'=>7,
            'value'=>'DOLBY ATMOS, Dolby Surround, Magic Sound Tuning, Clear Voice III, Hi-Fi Audio',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2355,
            'product_id'=>84,
            'category_option_id'=>8,
            'value'=>'Bottom',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2356,
            'product_id'=>84,
            'category_option_id'=>9,
            'value'=>'40 W',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2357,
            'product_id'=>84,
            'category_option_id'=>10,
            'value'=>'4',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2358,
            'product_id'=>84,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2359,
            'product_id'=>84,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2360,
            'product_id'=>84,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2361,
            'product_id'=>84,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2362,
            'product_id'=>84,
            'category_option_id'=>15,
            'value'=>'webOS',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2363,
            'product_id'=>84,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2364,
            'product_id'=>84,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2365,
            'product_id'=>84,
            'category_option_id'=>18,
            'value'=>'122.8 cm',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2366,
            'product_id'=>84,
            'category_option_id'=>19,
            'value'=>'70.7 cm',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2367,
            'product_id'=>84,
            'category_option_id'=>20,
            'value'=>'16.3 kg',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2368,
            'product_id'=>84,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2369,
            'product_id'=>84,
            'category_option_id'=>22,
            'value'=>'OLED55B8',
            'created_at'=>'2018-12-10 22:41:29',
            'updated_at'=>'2018-12-10 22:41:29'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2370,
            'product_id'=>85,
            'category_option_id'=>1,
            'value'=>'65 in',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2371,
            'product_id'=>85,
            'category_option_id'=>2,
            'value'=>'4K Ultra HD',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2372,
            'product_id'=>85,
            'category_option_id'=>3,
            'value'=>'QLED',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2373,
            'product_id'=>85,
            'category_option_id'=>4,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2374,
            'product_id'=>85,
            'category_option_id'=>5,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2375,
            'product_id'=>85,
            'category_option_id'=>6,
            'value'=>'Flat',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2376,
            'product_id'=>85,
            'category_option_id'=>7,
            'value'=>'DOLBY ATMOS, Dolby Surround, Magic Sound Tuning, Clear Voice III, Hi-Fi Audio',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2377,
            'product_id'=>85,
            'category_option_id'=>8,
            'value'=>'Accoustic Panel',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2378,
            'product_id'=>85,
            'category_option_id'=>9,
            'value'=>'40 W',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2379,
            'product_id'=>85,
            'category_option_id'=>10,
            'value'=>'4',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2380,
            'product_id'=>85,
            'category_option_id'=>11,
            'value'=>'2',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2381,
            'product_id'=>85,
            'category_option_id'=>12,
            'value'=>'no',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2382,
            'product_id'=>85,
            'category_option_id'=>13,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2383,
            'product_id'=>85,
            'category_option_id'=>14,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2384,
            'product_id'=>85,
            'category_option_id'=>15,
            'value'=>'Android OS',
            'created_at'=>'2018-12-10 22:47:16',
            'updated_at'=>'2018-12-10 22:47:16'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2385,
            'product_id'=>85,
            'category_option_id'=>16,
            'value'=>'black',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2386,
            'product_id'=>85,
            'category_option_id'=>17,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2387,
            'product_id'=>85,
            'category_option_id'=>18,
            'value'=>'145.1 cm',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2388,
            'product_id'=>85,
            'category_option_id'=>19,
            'value'=>'83.2 cm',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2389,
            'product_id'=>85,
            'category_option_id'=>20,
            'value'=>'28.8 kg',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2390,
            'product_id'=>85,
            'category_option_id'=>21,
            'value'=>'yes',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2391,
            'product_id'=>85,
            'category_option_id'=>22,
            'value'=>'XBR65A1E',
            'created_at'=>'2018-12-10 22:47:17',
            'updated_at'=>'2018-12-10 22:47:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2392,
            'product_id'=>86,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2393,
            'product_id'=>86,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2394,
            'product_id'=>86,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2395,
            'product_id'=>86,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2396,
            'product_id'=>86,
            'category_option_id'=>146,
            'value'=>'18 cm',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2397,
            'product_id'=>86,
            'category_option_id'=>147,
            'value'=>'6.7 cm',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2398,
            'product_id'=>86,
            'category_option_id'=>148,
            'value'=>'6.7 cm',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2399,
            'product_id'=>86,
            'category_option_id'=>149,
            'value'=>'0.55 kg',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2400,
            'product_id'=>86,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:05:38',
            'updated_at'=>'2018-12-10 23:05:38'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2401,
            'product_id'=>86,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:05:39',
            'updated_at'=>'2018-12-10 23:05:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2402,
            'product_id'=>86,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:05:39',
            'updated_at'=>'2018-12-10 23:05:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2403,
            'product_id'=>87,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:08:44',
            'updated_at'=>'2018-12-10 23:08:44'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2404,
            'product_id'=>87,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:08:44',
            'updated_at'=>'2018-12-10 23:08:44'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2405,
            'product_id'=>87,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:08:44',
            'updated_at'=>'2018-12-10 23:08:44'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2406,
            'product_id'=>87,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:08:45',
            'updated_at'=>'2018-12-10 23:08:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2407,
            'product_id'=>87,
            'category_option_id'=>146,
            'value'=>'12.6 cm',
            'created_at'=>'2018-12-10 23:08:45',
            'updated_at'=>'2018-12-10 23:08:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2408,
            'product_id'=>87,
            'category_option_id'=>147,
            'value'=>'28.3 cm',
            'created_at'=>'2018-12-10 23:08:45',
            'updated_at'=>'2018-12-10 23:08:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2409,
            'product_id'=>87,
            'category_option_id'=>148,
            'value'=>'12.2 cm',
            'created_at'=>'2018-12-10 23:08:45',
            'updated_at'=>'2018-12-10 23:08:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2410,
            'product_id'=>87,
            'category_option_id'=>149,
            'value'=>'2.11 kg',
            'created_at'=>'2018-12-10 23:08:46',
            'updated_at'=>'2018-12-10 23:08:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2411,
            'product_id'=>87,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:08:46',
            'updated_at'=>'2018-12-10 23:08:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2412,
            'product_id'=>87,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:08:46',
            'updated_at'=>'2018-12-10 23:08:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2413,
            'product_id'=>87,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:08:46',
            'updated_at'=>'2018-12-10 23:08:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2414,
            'product_id'=>88,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2415,
            'product_id'=>88,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2416,
            'product_id'=>88,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2417,
            'product_id'=>88,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2418,
            'product_id'=>88,
            'category_option_id'=>146,
            'value'=>'3.9 in',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2419,
            'product_id'=>88,
            'category_option_id'=>147,
            'value'=>'25.6 in',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2420,
            'product_id'=>88,
            'category_option_id'=>148,
            'value'=>'2.7 in',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2421,
            'product_id'=>88,
            'category_option_id'=>149,
            'value'=>'2.88 kg',
            'created_at'=>'2018-12-10 23:14:18',
            'updated_at'=>'2018-12-10 23:14:18'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2422,
            'product_id'=>88,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:14:19',
            'updated_at'=>'2018-12-10 23:14:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2423,
            'product_id'=>88,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:14:19',
            'updated_at'=>'2018-12-10 23:14:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2424,
            'product_id'=>88,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:14:19',
            'updated_at'=>'2018-12-10 23:14:19'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2425,
            'product_id'=>89,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2426,
            'product_id'=>89,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2427,
            'product_id'=>89,
            'category_option_id'=>144,
            'value'=>'na',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2428,
            'product_id'=>89,
            'category_option_id'=>145,
            'value'=>'na',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2429,
            'product_id'=>89,
            'category_option_id'=>146,
            'value'=>'2.75 in',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2430,
            'product_id'=>89,
            'category_option_id'=>147,
            'value'=>'21.6 in',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2431,
            'product_id'=>89,
            'category_option_id'=>148,
            'value'=>'3.39 in',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2432,
            'product_id'=>89,
            'category_option_id'=>149,
            'value'=>'1.69 kg',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2433,
            'product_id'=>89,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2434,
            'product_id'=>89,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2435,
            'product_id'=>89,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:19:22',
            'updated_at'=>'2018-12-10 23:19:22'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2436,
            'product_id'=>90,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2437,
            'product_id'=>90,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2438,
            'product_id'=>90,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2439,
            'product_id'=>90,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2440,
            'product_id'=>90,
            'category_option_id'=>146,
            'value'=>'15 cm',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2441,
            'product_id'=>90,
            'category_option_id'=>147,
            'value'=>'24 cm',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2442,
            'product_id'=>90,
            'category_option_id'=>148,
            'value'=>'14 cm',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2443,
            'product_id'=>90,
            'category_option_id'=>149,
            'value'=>'3 kg',
            'created_at'=>'2018-12-10 23:27:06',
            'updated_at'=>'2018-12-10 23:27:06'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2444,
            'product_id'=>90,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:27:07',
            'updated_at'=>'2018-12-10 23:27:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2445,
            'product_id'=>90,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:27:07',
            'updated_at'=>'2018-12-10 23:27:07'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2446,
            'product_id'=>90,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:27:08',
            'updated_at'=>'2018-12-10 23:27:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2447,
            'product_id'=>91,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:39:39',
            'updated_at'=>'2018-12-10 23:39:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2448,
            'product_id'=>91,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:39:39',
            'updated_at'=>'2018-12-10 23:39:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2449,
            'product_id'=>91,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:39:39',
            'updated_at'=>'2018-12-10 23:39:39'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2450,
            'product_id'=>91,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2451,
            'product_id'=>91,
            'category_option_id'=>146,
            'value'=>'8.2 cm',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2452,
            'product_id'=>91,
            'category_option_id'=>147,
            'value'=>'15.2 cm',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2453,
            'product_id'=>91,
            'category_option_id'=>148,
            'value'=>'8.2 cm',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2454,
            'product_id'=>91,
            'category_option_id'=>149,
            'value'=>'660 g',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2455,
            'product_id'=>91,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2456,
            'product_id'=>91,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2457,
            'product_id'=>91,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:39:40',
            'updated_at'=>'2018-12-10 23:39:40'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2458,
            'product_id'=>92,
            'category_option_id'=>142,
            'value'=>'2',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2459,
            'product_id'=>92,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2460,
            'product_id'=>92,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2461,
            'product_id'=>92,
            'category_option_id'=>145,
            'value'=>'no',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2462,
            'product_id'=>92,
            'category_option_id'=>146,
            'value'=>'13.2 cm',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2463,
            'product_id'=>92,
            'category_option_id'=>147,
            'value'=>'5 cm',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2464,
            'product_id'=>92,
            'category_option_id'=>148,
            'value'=>'20.13 cm',
            'created_at'=>'2018-12-10 23:43:34',
            'updated_at'=>'2018-12-10 23:43:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2465,
            'product_id'=>92,
            'category_option_id'=>149,
            'value'=>'1.2 kg',
            'created_at'=>'2018-12-10 23:43:35',
            'updated_at'=>'2018-12-10 23:43:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2466,
            'product_id'=>92,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:43:35',
            'updated_at'=>'2018-12-10 23:43:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2467,
            'product_id'=>92,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:43:35',
            'updated_at'=>'2018-12-10 23:43:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2468,
            'product_id'=>92,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:43:35',
            'updated_at'=>'2018-12-10 23:43:35'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2469,
            'product_id'=>93,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-10 23:50:53',
            'updated_at'=>'2018-12-10 23:50:53'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2470,
            'product_id'=>93,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-10 23:50:53',
            'updated_at'=>'2018-12-10 23:50:53'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2471,
            'product_id'=>93,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2472,
            'product_id'=>93,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2473,
            'product_id'=>93,
            'category_option_id'=>146,
            'value'=>'17.2 cm',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2474,
            'product_id'=>93,
            'category_option_id'=>147,
            'value'=>'14.2 cm',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2475,
            'product_id'=>93,
            'category_option_id'=>148,
            'value'=>'14.2 cm',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2476,
            'product_id'=>93,
            'category_option_id'=>149,
            'value'=>'2500 g',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2477,
            'product_id'=>93,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2478,
            'product_id'=>93,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2479,
            'product_id'=>93,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:50:54',
            'updated_at'=>'2018-12-10 23:50:54'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2480,
            'product_id'=>94,
            'category_option_id'=>142,
            'value'=>'2',
            'created_at'=>'2018-12-10 23:56:42',
            'updated_at'=>'2018-12-10 23:56:42'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2481,
            'product_id'=>94,
            'category_option_id'=>143,
            'value'=>'light cedar',
            'created_at'=>'2018-12-10 23:56:42',
            'updated_at'=>'2018-12-10 23:56:42'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2482,
            'product_id'=>94,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:56:42',
            'updated_at'=>'2018-12-10 23:56:42'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2483,
            'product_id'=>94,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2484,
            'product_id'=>94,
            'category_option_id'=>146,
            'value'=>'10 cm',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2485,
            'product_id'=>94,
            'category_option_id'=>147,
            'value'=>'40 cm',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2486,
            'product_id'=>94,
            'category_option_id'=>148,
            'value'=>'10 cm',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2487,
            'product_id'=>94,
            'category_option_id'=>149,
            'value'=>'2.72 Kg',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2488,
            'product_id'=>94,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2489,
            'product_id'=>94,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2490,
            'product_id'=>94,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-10 23:56:43',
            'updated_at'=>'2018-12-10 23:56:43'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2491,
            'product_id'=>95,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2492,
            'product_id'=>95,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2493,
            'product_id'=>95,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2494,
            'product_id'=>95,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2495,
            'product_id'=>95,
            'category_option_id'=>146,
            'value'=>'29.21 cm',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2496,
            'product_id'=>95,
            'category_option_id'=>147,
            'value'=>'28.58 cm',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2497,
            'product_id'=>95,
            'category_option_id'=>148,
            'value'=>'12.7 cm',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2498,
            'product_id'=>95,
            'category_option_id'=>149,
            'value'=>'2.98 kg',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2499,
            'product_id'=>95,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2500,
            'product_id'=>95,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2501,
            'product_id'=>95,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 00:01:01',
            'updated_at'=>'2018-12-11 00:01:01'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2502,
            'product_id'=>96,
            'category_option_id'=>142,
            'value'=>'2',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2503,
            'product_id'=>96,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2504,
            'product_id'=>96,
            'category_option_id'=>144,
            'value'=>'no',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2505,
            'product_id'=>96,
            'category_option_id'=>145,
            'value'=>'no',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2506,
            'product_id'=>96,
            'category_option_id'=>146,
            'value'=>'24.8 cm',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2507,
            'product_id'=>96,
            'category_option_id'=>147,
            'value'=>'14.9 cm',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2508,
            'product_id'=>96,
            'category_option_id'=>148,
            'value'=>'19.1 cm',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2509,
            'product_id'=>96,
            'category_option_id'=>149,
            'value'=>'3.2 kg',
            'created_at'=>'2018-12-11 00:10:33',
            'updated_at'=>'2018-12-11 00:10:33'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2510,
            'product_id'=>96,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:10:34',
            'updated_at'=>'2018-12-11 00:10:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2511,
            'product_id'=>96,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:10:34',
            'updated_at'=>'2018-12-11 00:10:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2512,
            'product_id'=>96,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 00:10:34',
            'updated_at'=>'2018-12-11 00:10:34'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2513,
            'product_id'=>97,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2514,
            'product_id'=>97,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2515,
            'product_id'=>97,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2516,
            'product_id'=>97,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2517,
            'product_id'=>97,
            'category_option_id'=>146,
            'value'=>'22.86 cm',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2518,
            'product_id'=>97,
            'category_option_id'=>147,
            'value'=>'8.89 cm',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2519,
            'product_id'=>97,
            'category_option_id'=>148,
            'value'=>'8.89 cm',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2520,
            'product_id'=>97,
            'category_option_id'=>149,
            'value'=>'975 g',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2521,
            'product_id'=>97,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2522,
            'product_id'=>97,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2523,
            'product_id'=>97,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 00:16:17',
            'updated_at'=>'2018-12-11 00:16:17'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2524,
            'product_id'=>98,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2525,
            'product_id'=>98,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2526,
            'product_id'=>98,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2527,
            'product_id'=>98,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2528,
            'product_id'=>98,
            'category_option_id'=>146,
            'value'=>'na',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2529,
            'product_id'=>98,
            'category_option_id'=>147,
            'value'=>'na',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2530,
            'product_id'=>98,
            'category_option_id'=>148,
            'value'=>'na',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2531,
            'product_id'=>98,
            'category_option_id'=>149,
            'value'=>'599 g',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2532,
            'product_id'=>98,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2533,
            'product_id'=>98,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2534,
            'product_id'=>98,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 00:20:11',
            'updated_at'=>'2018-12-11 00:20:11'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2535,
            'product_id'=>99,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-11 03:06:45',
            'updated_at'=>'2018-12-11 03:06:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2536,
            'product_id'=>99,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-11 03:06:45',
            'updated_at'=>'2018-12-11 03:06:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2537,
            'product_id'=>99,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:06:45',
            'updated_at'=>'2018-12-11 03:06:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2538,
            'product_id'=>99,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:06:45',
            'updated_at'=>'2018-12-11 03:06:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2539,
            'product_id'=>99,
            'category_option_id'=>146,
            'value'=>'na',
            'created_at'=>'2018-12-11 03:06:45',
            'updated_at'=>'2018-12-11 03:06:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2540,
            'product_id'=>99,
            'category_option_id'=>147,
            'value'=>'na',
            'created_at'=>'2018-12-11 03:06:45',
            'updated_at'=>'2018-12-11 03:06:45'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2541,
            'product_id'=>99,
            'category_option_id'=>148,
            'value'=>'na',
            'created_at'=>'2018-12-11 03:06:46',
            'updated_at'=>'2018-12-11 03:06:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2542,
            'product_id'=>99,
            'category_option_id'=>149,
            'value'=>'9.3 Kg',
            'created_at'=>'2018-12-11 03:06:46',
            'updated_at'=>'2018-12-11 03:06:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2543,
            'product_id'=>99,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:06:46',
            'updated_at'=>'2018-12-11 03:06:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2544,
            'product_id'=>99,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:06:46',
            'updated_at'=>'2018-12-11 03:06:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2545,
            'product_id'=>99,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 03:06:46',
            'updated_at'=>'2018-12-11 03:06:46'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2546,
            'product_id'=>100,
            'category_option_id'=>142,
            'value'=>'2',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2547,
            'product_id'=>100,
            'category_option_id'=>143,
            'value'=>'black',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2548,
            'product_id'=>100,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2549,
            'product_id'=>100,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2550,
            'product_id'=>100,
            'category_option_id'=>146,
            'value'=>'12.5 cm',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2551,
            'product_id'=>100,
            'category_option_id'=>147,
            'value'=>'14.5 cm',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2552,
            'product_id'=>100,
            'category_option_id'=>148,
            'value'=>'18 cm',
            'created_at'=>'2018-12-11 03:10:08',
            'updated_at'=>'2018-12-11 03:10:08'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2553,
            'product_id'=>100,
            'category_option_id'=>149,
            'value'=>'2.1 kg',
            'created_at'=>'2018-12-11 03:10:09',
            'updated_at'=>'2018-12-11 03:10:09'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2554,
            'product_id'=>100,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:10:09',
            'updated_at'=>'2018-12-11 03:10:09'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2555,
            'product_id'=>100,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:10:09',
            'updated_at'=>'2018-12-11 03:10:09'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2556,
            'product_id'=>100,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 03:10:09',
            'updated_at'=>'2018-12-11 03:10:09'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2557,
            'product_id'=>101,
            'category_option_id'=>142,
            'value'=>'1',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2558,
            'product_id'=>101,
            'category_option_id'=>143,
            'value'=>'Charcoal',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2559,
            'product_id'=>101,
            'category_option_id'=>144,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2560,
            'product_id'=>101,
            'category_option_id'=>145,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2561,
            'product_id'=>101,
            'category_option_id'=>146,
            'value'=>'14.8 cm',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2562,
            'product_id'=>101,
            'category_option_id'=>147,
            'value'=>'9.9 cm',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2563,
            'product_id'=>101,
            'category_option_id'=>148,
            'value'=>'9.9 cm',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2564,
            'product_id'=>101,
            'category_option_id'=>149,
            'value'=>'780 g',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2565,
            'product_id'=>101,
            'category_option_id'=>150,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2566,
            'product_id'=>101,
            'category_option_id'=>151,
            'value'=>'yes',
            'created_at'=>'2018-12-11 03:20:04',
            'updated_at'=>'2018-12-11 03:20:04'
            ] );
            
            
                        
            ProductOption::create( [
            'id'=>2567,
            'product_id'=>101,
            'category_option_id'=>152,
            'value'=>'1 Year(s)',
            'created_at'=>'2018-12-11 03:20:05',
            'updated_at'=>'2018-12-11 03:20:05'
            ] );
            
    }
}
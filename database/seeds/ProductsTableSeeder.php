<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create( [
        'id'=>1,
        'name'=>'Samsung Galaxy A6s',
        'category_id'=>3,
        'manufacturer_id'=>5,
        'price'=>'405.00',
        'sale_price'=>'405.00',
        'overview'=>'The Samsung Galaxy A6 and Galaxy A6+ are Android high to midrange smartphones produced by Samsung Electronics. It was announced on 2 May 2018 it was first released in Europe, Asia, and Latin America the same month, then released in South Korea, Africa, and China. The Galaxy A6+ is rebranded in South Korea as the Galaxy Jean & in China as the Galaxy A9 Star lite.',
        'more_information'=>'The Samsung Galaxy A6 and Galaxy A6+ are Android high to midrange smartphones produced by Samsung Electronics. It was announced on 2 May 2018 it was first released in Europe, Asia, and Latin America the same month, then released in South Korea, Africa, and China. The Galaxy A6+ is rebranded in South Korea as the Galaxy Jean & in China as the Galaxy A9 Star lite.',
        'thumbnail'=>'cell1.jpg',
        'created_at'=>'2018-12-08 22:49:40',
        'updated_at'=>'2018-12-13 03:49:21'
        ] );
        
        
                    
        Product::create( [
        'id'=>2,
        'name'=>'Google Pixel XL2',
        'category_id'=>3,
        'manufacturer_id'=>1,
        'price'=>'795.00',
        'sale_price'=>'795.00',
        'overview'=>'Google has come a long way since the days of Nexus smartphones. This is only the second iteration of the Google Pixel and it shows how much Google\'s software and hardware have evolved in the past year. Both Pixel smartphones got a refined, yet simpler appearance.',
        'more_information'=>'Google has come a long way since the days of Nexus smartphones. This is only the second iteration of the Google Pixel and it shows how much Google\'s software and hardware have evolved in the past year. Both Pixel smartphones got a refined, yet simpler appearance.',
        'thumbnail'=>'cell2.jpg',
        'created_at'=>'2018-12-08 23:55:55',
        'updated_at'=>'2018-12-08 23:55:55'
        ] );
        
        
                    
        Product::create( [
        'id'=>3,
        'name'=>'OnePlus 6T',
        'category_id'=>3,
        'manufacturer_id'=>2,
        'price'=>'825.00',
        'sale_price'=>'825.00',
        'overview'=>'The OnePlus 6T is powerful, yet reasonably priced Android phone. Starting at $550, it offers top-of-the-line specs, including a Snapdragon 845 and up to 8GB of RAM. It is also one of the few smartphones with a fingerprint scanner built right into the display. On the lists of skipped features are a headphone jack, water resistance, and wireless charging. The OnePlus 6T is the company\'s first to launch through a US carrier, namely T-Mobile.',
        'more_information'=>'The OnePlus 6T is powerful, yet reasonably priced Android phone. Starting at $550, it offers top-of-the-line specs, including a Snapdragon 845 and up to 8GB of RAM. It is also one of the few smartphones with a fingerprint scanner built right into the display. On the lists of skipped features are a headphone jack, water resistance, and wireless charging. The OnePlus 6T is the company\'s first to launch through a US carrier, namely T-Mobile.',
        'thumbnail'=>'cell3.jpg',
        'created_at'=>'2018-12-09 02:07:39',
        'updated_at'=>'2018-12-09 02:07:39'
        ] );
        
        
                    
        Product::create( [
        'id'=>4,
        'name'=>'Google Pixel 3 XL',
        'category_id'=>3,
        'manufacturer_id'=>1,
        'price'=>'1440.00',
        'sale_price'=>'1440.00',
        'overview'=>'he Pixel 3 XL is Google\'s latest flagship smartphone. It promises to bring everything we have come to expect with the Pixel brand, including a clean build of Android, fast performance, quality camera and some cool software tricks that only Google can come up with.',
        'more_information'=>'he Pixel 3 XL is Google\'s latest flagship smartphone. It promises to bring everything we have come to expect with the Pixel brand, including a clean build of Android, fast performance, quality camera and some cool software tricks that only Google can come up with.',
        'thumbnail'=>'cell4.jpg',
        'created_at'=>'2018-12-09 02:24:54',
        'updated_at'=>'2018-12-09 02:24:54'
        ] );
        
        
                    
        Product::create( [
        'id'=>5,
        'name'=>'Lenovo Z5',
        'category_id'=>3,
        'manufacturer_id'=>9,
        'price'=>'300.00',
        'sale_price'=>'300.00',
        'overview'=>'The Lenovo Z5 mobile features a 5.99" (15.21 cm) display with a screen resolution of 1080 x 2246 pixels and runs on Android v8.1 (Oreo) operating system. The device is powered by Octa core (1.8 GHz, Quad core, Kryo 260 + 1.6 GHz, Quad core, Kryo 260) processor paired with 6 GB of RAM. As far as the battery is concerned it has 3300 mAh. Over that, as far as the rear camera is concerned this mobile has a 16 MP + 8 MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Compass, Gyroscope. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Adreno 509 GPU.',
        'more_information'=>'The Lenovo Z5 mobile features a 5.99" (15.21 cm) display with a screen resolution of 1080 x 2246 pixels and runs on Android v8.1 (Oreo) operating system. The device is powered by Octa core (1.8 GHz, Quad core, Kryo 260 + 1.6 GHz, Quad core, Kryo 260) processor paired with 6 GB of RAM. As far as the battery is concerned it has 3300 mAh. Over that, as far as the rear camera is concerned this mobile has a 16 MP + 8 MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Compass, Gyroscope. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Adreno 509 GPU.',
        'thumbnail'=>'cell5.jpg',
        'created_at'=>'2018-12-09 02:45:32',
        'updated_at'=>'2018-12-09 02:45:32'
        ] );
        
        
                    
        Product::create( [
        'id'=>6,
        'name'=>'BlackBerry KEY2 LE',
        'category_id'=>3,
        'manufacturer_id'=>11,
        'price'=>'495.00',
        'sale_price'=>'495.00',
        'overview'=>'The Blackberry KEY2 LE (KEY2 Lite) mobile features a 4.5" (11.43 cm) display with a screen resolution of 1080 x 1620 pixels and runs on Android v8.1 (Oreo) operating system. The device is powered by Octa core (1.8 GHz, Quad core, Kryo 260 + 1.6 GHz, Quad core, Kryo 260) processor paired with 4 GB of RAM. As far as the battery is concerned it has 3000 mAh. Over that, as far as the rear camera is concerned this mobile has a 13 MP + 5 MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Compass, Gyroscope. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Adreno 509 GPU.',
        'more_information'=>'The Blackberry KEY2 LE (KEY2 Lite) mobile features a 4.5" (11.43 cm) display with a screen resolution of 1080 x 1620 pixels and runs on Android v8.1 (Oreo) operating system. The device is powered by Octa core (1.8 GHz, Quad core, Kryo 260 + 1.6 GHz, Quad core, Kryo 260) processor paired with 4 GB of RAM. As far as the battery is concerned it has 3000 mAh. Over that, as far as the rear camera is concerned this mobile has a 13 MP + 5 MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Compass, Gyroscope. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Adreno 509 GPU.',
        'thumbnail'=>'cell6.jpg',
        'created_at'=>'2018-12-09 02:54:07',
        'updated_at'=>'2018-12-09 02:54:07'
        ] );
        
        
                    
        Product::create( [
        'id'=>7,
        'name'=>'Huawei Mate 20 X',
        'category_id'=>3,
        'manufacturer_id'=>4,
        'price'=>'1200.00',
        'sale_price'=>'1200.00',
        'overview'=>'The Huawei Mate 20 X mobile features a 7.2" (18.29 cm) display with a screen resolution of 1080 x 2244 pixels and runs on Android v9.0 (Pie) operating system. The device is powered by Octa core (2.6 GHz, Dual core, Cortex A76 + 1.92 GHz, Dual core, Cortex A76 + 1.8 GHz, Quad core, Cortex A55) processor paired with 6 GB of RAM. As far as the battery is concerned it has 5000 mAh. Over that, as far as the rear camera is concerned this mobile has a 40MP + 20MP + 8MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Barometer, Compass, Gyroscope. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Mali-G76 MP10 GPU.',
        'more_information'=>'The Huawei Mate 20 X mobile features a 7.2" (18.29 cm) display with a screen resolution of 1080 x 2244 pixels and runs on Android v9.0 (Pie) operating system. The device is powered by Octa core (2.6 GHz, Dual core, Cortex A76 + 1.92 GHz, Dual core, Cortex A76 + 1.8 GHz, Quad core, Cortex A55) processor paired with 6 GB of RAM. As far as the battery is concerned it has 5000 mAh. Over that, as far as the rear camera is concerned this mobile has a 40MP + 20MP + 8MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Barometer, Compass, Gyroscope. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Mali-G76 MP10 GPU.',
        'thumbnail'=>'cell7.jpg',
        'created_at'=>'2018-12-09 03:01:25',
        'updated_at'=>'2018-12-09 03:01:25'
        ] );
        
        
                    
        Product::create( [
        'id'=>8,
        'name'=>'OPPO Find X',
        'category_id'=>3,
        'manufacturer_id'=>38,
        'price'=>'1155.00',
        'sale_price'=>'1155.00',
        'overview'=>'OPPO Find X launched on August 4, 2018, is an ultimate smartphone designed to scale your mobile user experience several notches higher. It?s a panoramic arc screen and integrated design offers an amazing viewing experience with an enhanced grip. The latest smartphone by Oppo exudes a lustrous gradient effect with cameras combined with a myriad of advanced features like 3D Face Recognition system, multiple sensors, better resolution and more.',
        'more_information'=>'OPPO Find X launched on August 4, 2018, is an ultimate smartphone designed to scale your mobile user experience several notches higher. It?s a panoramic arc screen and integrated design offers an amazing viewing experience with an enhanced grip. The latest smartphone by Oppo exudes a lustrous gradient effect with cameras combined with a myriad of advanced features like 3D Face Recognition system, multiple sensors, better resolution and more.',
        'thumbnail'=>'cell8.jpg',
        'created_at'=>'2018-12-09 03:11:12',
        'updated_at'=>'2018-12-09 03:11:12'
        ] );
        
        
                    
        Product::create( [
        'id'=>9,
        'name'=>'Huawei Honor Magic 2',
        'category_id'=>3,
        'manufacturer_id'=>4,
        'price'=>'825.00',
        'sale_price'=>'825.00',
        'overview'=>'Huawei\'s sub-brand Honor has recently released it\'s one of most anticipated smartphone Honor Magic 2. The smartphone features a unique slider mechanism that reveals the earpiece grille and front camera setup.',
        'thumbnail'=>'cell9.jpg',
        'created_at'=>'2018-12-09 04:26:25',
        'updated_at'=>'2018-12-09 04:26:25'
        ] );
        
        
                    
        Product::create( [
        'id'=>10,
        'name'=>'OnePlus 5T',
        'category_id'=>3,
        'manufacturer_id'=>2,
        'price'=>'750.00',
        'sale_price'=>'750.00',
        'overview'=>'OnePlus 5T is the latest flagship smartphone from the Chinese company OnePlus. The phone is expected to be launched in the month of November. The phone is an upgrade to the OnePlus 5 which was launched earlier this year in June. The new OnePlus 5T has carried all the major specification and features of its predecessor.',
        'more_information'=>'OnePlus 5T is the latest flagship smartphone from the Chinese company OnePlus. The phone is expected to be launched in the month of November. The phone is an upgrade to the OnePlus 5 which was launched earlier this year in June. The new OnePlus 5T has carried all the major specification and features of its predecessor.',
        'thumbnail'=>'cell10.jpg',
        'created_at'=>'2018-12-09 04:31:51',
        'updated_at'=>'2018-12-09 04:31:51'
        ] );
        
        
                    
        Product::create( [
        'id'=>11,
        'name'=>'Samsung Galaxy A9',
        'category_id'=>3,
        'manufacturer_id'=>5,
        'price'=>'900.00',
        'sale_price'=>'900.00',
        'overview'=>'The Samsung Galaxy A9 mobile features a 6.0" (15.24 cm) display with a screen resolution of Full HD (1080 x 1920 pixels) and runs on Android v5.1.1 (Lollipop) operating system. The device is powered by Octa core (1.8 GHz, Quad core, Cortex A72 + 1.4 GHz, Quad core, Cortex A53) processor paired with 3 GB of RAM. As far as the battery is concerned it has 4000 mAh. Over that, as far as the rear camera is concerned this mobile has a 13 MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Compass. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Adreno 510 GPU.',
        'more_information'=>'The Samsung Galaxy A9 mobile features a 6.0" (15.24 cm) display with a screen resolution of Full HD (1080 x 1920 pixels) and runs on Android v5.1.1 (Lollipop) operating system. The device is powered by Octa core (1.8 GHz, Quad core, Cortex A72 + 1.4 GHz, Quad core, Cortex A53) processor paired with 3 GB of RAM. As far as the battery is concerned it has 4000 mAh. Over that, as far as the rear camera is concerned this mobile has a 13 MP camera . Other sensors include Light sensor, Proximity sensor, Accelerometer, Compass. So, does it have a fingerprint sensor? Yes, it does. For graphical performance that can make games run smoothly, this phone has got a Adreno 510 GPU.',
        'thumbnail'=>'cell11.jpg',
        'created_at'=>'2018-12-09 04:37:27',
        'updated_at'=>'2018-12-09 04:37:27'
        ] );
        
        
                    
        Product::create( [
        'id'=>12,
        'name'=>'Motorola Moto Z3 Play',
        'category_id'=>3,
        'manufacturer_id'=>38,
        'price'=>'600.00',
        'sale_price'=>'600.00',
        'overview'=>'As the longest running modular Android phone series, the Moto Z-series offers a blend of flexibility and mid-tier performance that helps it stand out from the competition. The Z3 Play is their 2018 update to the popular lineup and comes with some interesting design changes and spec updates. But it also comes with a price increase -- a potential issue for people new to the Moto Mods ecosystem.',
        'more_information'=>'As the longest running modular Android phone series, the Moto Z-series offers a blend of flexibility and mid-tier performance that helps it stand out from the competition. The Z3 Play is their 2018 update to the popular lineup and comes with some interesting design changes and spec updates. But it also comes with a price increase -- a potential issue for people new to the Moto Mods ecosystem.',
        'thumbnail'=>'cell12.jpg',
        'created_at'=>'2018-12-09 04:48:07',
        'updated_at'=>'2018-12-09 04:48:07'
        ] );
        
        
                    
        Product::create( [
        'id'=>13,
        'name'=>'HTC U12 life',
        'category_id'=>3,
        'manufacturer_id'=>13,
        'price'=>'450.00',
        'sale_price'=>'450.00',
        'overview'=>'HTC U12 Life is the latest mobile in India which is expected to launch 26th September 2018 is the newest release by HTC. Being predecessor to the well-known HTC U11 Life, this smartphone will comprise of advanced specs and elegant design. It will function on Android v8.0 (Oreo) operating system, powered by Octa Core Snapdragon 660 chipset (1.8 GHz, Quad core, Kryo 260 + 1.6 GHz, Quad core, Kryo 260) processor along with 4 GB of RAM. The smartphone features a responsive fingerprint sensor which is bottom mounted.',
        'more_information'=>'HTC U12 Life is the latest mobile in India which is expected to launch 26th September 2018 is the newest release by HTC. Being predecessor to the well-known HTC U11 Life, this smartphone will comprise of advanced specs and elegant design. It will function on Android v8.0 (Oreo) operating system, powered by Octa Core Snapdragon 660 chipset (1.8 GHz, Quad core, Kryo 260 + 1.6 GHz, Quad core, Kryo 260) processor along with 4 GB of RAM. The smartphone features a responsive fingerprint sensor which is bottom mounted.',
        'thumbnail'=>'cell16.jpg',
        'created_at'=>'2018-12-09 04:54:06',
        'updated_at'=>'2018-12-09 04:54:06'
        ] );
        
        
                    
        Product::create( [
        'id'=>14,
        'name'=>'Xiaomi Pocophone F1',
        'category_id'=>3,
        'manufacturer_id'=>6,
        'price'=>'435.00',
        'sale_price'=>'435.00',
        'overview'=>'Being the first phone under Xiaomi sub-brand Poco, Pocophone F1 is all set to hit the market. If rumors are to be believed, it is expected to be launched on August 22, 2018. This latest smartphone in India falls in the premium segment of the smartphone and is packed with numerous high-end features to boost its user experience.',
        'more_information'=>'Being the first phone under Xiaomi sub-brand Poco, Pocophone F1 is all set to hit the market. If rumors are to be believed, it is expected to be launched on August 22, 2018. This latest smartphone in India falls in the premium segment of the smartphone and is packed with numerous high-end features to boost its user experience.',
        'thumbnail'=>'cell17.jpg',
        'created_at'=>'2018-12-09 05:00:11',
        'updated_at'=>'2018-12-09 05:00:11'
        ] );
        
        
                    
        Product::create( [
        'id'=>15,
        'name'=>'Apple iPhone XS',
        'category_id'=>3,
        'manufacturer_id'=>8,
        'price'=>'1725.00',
        'sale_price'=>'1725.00',
        'overview'=>'The latest iPhone XS by renowned brand Apple is all set to hit the market this September. A launch event on Wednesday 12th September is planned, where the brand will be introducing its brand new phone an iPhone XS along with the specifications it will come with. The smartphone will have an edge-to-edge screen with almost a bezel-less screen. iPhone XS Camera will be an improvised version with a TrueDepth camera system that supports Face ID.',
        'more_information'=>'The latest iPhone XS by renowned brand Apple is all set to hit the market this September. A launch event on Wednesday 12th September is planned, where the brand will be introducing its brand new phone an iPhone XS along with the specifications it will come with. The smartphone will have an edge-to-edge screen with almost a bezel-less screen. iPhone XS Camera will be an improvised version with a TrueDepth camera system that supports Face ID.',
        'thumbnail'=>'cell18.jpg',
        'created_at'=>'2018-12-09 05:07:28',
        'updated_at'=>'2018-12-09 05:07:28'
        ] );
        
        
                    
        Product::create( [
        'id'=>16,
        'name'=>'Apple iPhone XR',
        'category_id'=>3,
        'manufacturer_id'=>8,
        'price'=>'1275.00',
        'sale_price'=>'1275.00',
        'overview'=>'Apple iPhone XR pre-orders are expected to start from October 19, 2018 in India, is the newest mobile from the Apple brand. The affordable iPhone XR will be available on sale from 26th October 2018, it has an LCD-based Liquid retina display, faster Face ID and the most powerful & smartest chip ever in an iPhone. The advanced backlight design of the smartphone permits the screen to stretch into the corners, providing iPhone XR a stunning edge display.',
        'more_information'=>'Apple iPhone XR pre-orders are expected to start from October 19, 2018 in India, is the newest mobile from the Apple brand. The affordable iPhone XR will be available on sale from 26th October 2018, it has an LCD-based Liquid retina display, faster Face ID and the most powerful & smartest chip ever in an iPhone. The advanced backlight design of the smartphone permits the screen to stretch into the corners, providing iPhone XR a stunning edge display.',
        'thumbnail'=>'cell19.jpg',
        'created_at'=>'2018-12-09 05:13:52',
        'updated_at'=>'2018-12-09 05:13:52'
        ] );
        
        
                    
        Product::create( [
        'id'=>17,
        'name'=>'Apple iPhone X',
        'category_id'=>3,
        'manufacturer_id'=>8,
        'price'=>'1500.00',
        'sale_price'=>'1500.00',
        'overview'=>'iPhone X launched in November last year, still witnesses a huge craze amongst gadget lovers. The latest smartphone in India by Apple is an intelligently designed device packed with a plethora of advanced features. It is the smartest device by the brand that can be wirelessly charged and quickly responds to a single tap, voice, and even a glance. Apple presents its most modish iPhone X with outstanding camera quality,',
        'more_information'=>'iPhone X launched in November last year, still witnesses a huge craze amongst gadget lovers. The latest smartphone in India by Apple is an intelligently designed device packed with a plethora of advanced features. It is the smartest device by the brand that can be wirelessly charged and quickly responds to a single tap, voice, and even a glance. Apple presents its most modish iPhone X with outstanding camera quality,',
        'thumbnail'=>'cell20.jpg',
        'created_at'=>'2018-12-09 05:25:03',
        'updated_at'=>'2018-12-09 05:25:03'
        ] );
        
        
                    
        Product::create( [
        'id'=>18,
        'name'=>'Apple iPhone 8 Plus',
        'category_id'=>3,
        'manufacturer_id'=>8,
        'price'=>'1155.00',
        'sale_price'=>'1155.00',
        'overview'=>'The Apple iPhone 8 Plus mobile features a 5.5" (13.97 cm) display with a screen resolution of Full HD (1080 x 1920 pixels) and runs on iOS v11 operating system. The device is powered by Hexa Core (2.39 GHz, Dual core, Monsoon + 1.42 GHz, Quad core, Mistral) processor paired with 3 GB of RAM. As far as the battery is concerned it has 2675 mAh. Over that, as far as the rear camera is concerned this mobile has a 12 MP + 12 MP camera BSI Sensor supporting a resolution of 4608 x 2592 Pixels and the front snapper is powered by a BSI Sensor',
        'more_information'=>'The Apple iPhone 8 Plus mobile features a 5.5" (13.97 cm) display with a screen resolution of Full HD (1080 x 1920 pixels) and runs on iOS v11 operating system. The device is powered by Hexa Core (2.39 GHz, Dual core, Monsoon + 1.42 GHz, Quad core, Mistral) processor paired with 3 GB of RAM. As far as the battery is concerned it has 2675 mAh. Over that, as far as the rear camera is concerned this mobile has a 12 MP + 12 MP camera BSI Sensor supporting a resolution of 4608 x 2592 Pixels and the front snapper is powered by a BSI Sensor',
        'thumbnail'=>'cell21.jpg',
        'created_at'=>'2018-12-09 09:58:57',
        'updated_at'=>'2018-12-09 09:58:57'
        ] );
        
        
                    
        Product::create( [
        'id'=>19,
        'name'=>'XPS 13 Laptop',
        'category_id'=>2,
        'manufacturer_id'=>7,
        'price'=>'999.99',
        'sale_price'=>'999.99',
        'overview'=>'The smallest 13-inch laptop on the planet has the world\'s first virtually borderless InfinityEdge display — available with touch.',
        'more_information'=>'The smallest 13-inch laptop on the planet has the world\'s first virtually borderless InfinityEdge display — available with touch.',
        'thumbnail'=>'laptop1.jpg',
        'created_at'=>'2018-12-09 10:16:40',
        'updated_at'=>'2018-12-09 10:16:40'
        ] );
        
        
                    
        Product::create( [
        'id'=>20,
        'name'=>'MAC Airbook',
        'category_id'=>2,
        'manufacturer_id'=>8,
        'price'=>'1199.00',
        'sale_price'=>'1199.00',
        'overview'=>'The most loved Mac is about to make you fall in love all over again. Available in silver, space gray, and gold, the new thinner and lighter MacBook Air features a brilliant Retina display, Touch ID, the latest-generation keyboard, and a Force Touch trackpad. The iconic wedge is created from 100 percent recycled aluminum, making it the greenest Mac ever.1 And with all-day battery life, MacBook Air is your perfectly portable, do-it-all notebook.',
        'more_information'=>'The most loved Mac is about to make you fall in love all over again. Available in silver, space gray, and gold, the new thinner and lighter MacBook Air features a brilliant Retina display, Touch ID, the latest-generation keyboard, and a Force Touch trackpad. The iconic wedge is created from 100 percent recycled aluminum, making it the greenest Mac ever.1 And with all-day battery life, MacBook Air is your perfectly portable, do-it-all notebook.',
        'thumbnail'=>'laptop2.jpg',
        'created_at'=>'2018-12-09 10:32:59',
        'updated_at'=>'2018-12-09 10:32:59'
        ] );
        
        
                    
        Product::create( [
        'id'=>21,
        'name'=>'Apple MacBook Pro',
        'category_id'=>2,
        'manufacturer_id'=>8,
        'price'=>'1829.00',
        'sale_price'=>'1829.00',
        'overview'=>'MacBook Pro elevates the notebook to a whole new level of performance and portability. Wherever your ideas take you, you\'ll get there faster than ever with high‑performance processors and memory, advanced graphics, blazing‑fast storage, and more.',
        'more_information'=>'MacBook Pro elevates the notebook to a whole new level of performance and portability. Wherever your ideas take you, you\'ll get there faster than ever with high‑performance processors and memory, advanced graphics, blazing‑fast storage, and more.',
        'thumbnail'=>'laptop3.jpg',
        'created_at'=>'2018-12-09 10:52:05',
        'updated_at'=>'2018-12-09 10:52:05'
        ] );
        
        
                    
        Product::create( [
        'id'=>22,
        'name'=>'Asus rog',
        'category_id'=>2,
        'manufacturer_id'=>15,
        'price'=>'2499.99',
        'sale_price'=>'2499.99',
        'overview'=>'Unleash your gaming prowess on the competition with the ASUS ROG 15.6" laptop built to deliver the ferocious performance you demand. It\'s preloaded with Windows 10 and features a powerful 3.9GHz Intel Core i7-8750H processor with 16GB RAM and combination 1TB FireCuda SSHD and 256GB PCIe SSD hard drive for optimal speed, storage, and durability.',
        'more_information'=>'Unleash your gaming prowess on the competition with the ASUS ROG 15.6" laptop built to deliver the ferocious performance you demand. It\'s preloaded with Windows 10 and features a powerful 3.9GHz Intel Core i7-8750H processor with 16GB RAM and combination 1TB FireCuda SSHD and 256GB PCIe SSD hard drive for optimal speed, storage, and durability.',
        'thumbnail'=>'laptop4.jpg',
        'created_at'=>'2018-12-09 11:06:25',
        'updated_at'=>'2018-12-09 11:06:25'
        ] );
        
        
                    
        Product::create( [
        'id'=>23,
        'name'=>'Acer Aspire',
        'category_id'=>2,
        'manufacturer_id'=>10,
        'price'=>'899.99',
        'sale_price'=>'899.99',
        'overview'=>'This Acer Aspire 5 series 17.3" laptop has all the essentials you need. Equipped with a Core i5 processor, 12GB of RAM, and integrated Wi-Fi and Ethernet, this laptop lets you quickly browse the Web, manage a budget, or catch up on schoolwork. A compartment door lets you easily upgrade memory and storage components for added versatility.',
        'more_information'=>'1.6GHz Intel Core i5-8250U quad-core processor with 12GB of RAM delivers optimal performance for your everyday computing needs and multitasking\r\n1TB hard drive offers a compact warehouse for all your favourite movies, music, photos, and more in one place\r\n17.3-inch display with a resolution of 1600 x 900 offers a crisp, clear canvas for surfing the web and watching videos\r\nBluelightShield technology will keep your eyes safe from blue-light emission and reduce the strain to your eyes',
        'thumbnail'=>'laptop5.jpg',
        'created_at'=>'2018-12-09 11:23:18',
        'updated_at'=>'2018-12-09 11:23:18'
        ] );
        
        
                    
        Product::create( [
        'id'=>24,
        'name'=>'HP 14" Laptop',
        'category_id'=>2,
        'manufacturer_id'=>16,
        'price'=>'899.99',
        'sale_price'=>'899.99',
        'overview'=>'Bring reliable performance and maximum portability to your computing needs with the HP 14" laptop. It boasts an 8th generation Intel Core i5-8250U processor with 8GB DDR4 SDRAM for the speed and functionality you need. It\'s preinstalled with Windows 10 for maximum efficiency and includes 256GB SSD storage for quick, snappy responsiveness.',
        'more_information'=>'Powered by 1.6GHz 8th gen Intel Core i5-8250U processor with 8GB DDR4-2400 SDRAM delivers the quick performance you demand\r\n256 GB PCIe NVMe M.2 solid state drive provides reliable storage, snappy performance, and faster boot ups\r\nIntel UHD Graphics 620 graphics card gives you clear images and lag-free action for gaming\r\n14" diagonal FHD WLED-backlit display with 1920 x 1080 resolution features colour-popping IPS technology, Bright View crisp imagery, and a thin micro-edge for ultra-wide viewing angles\r\nBuilt-in 802.11b/g/n/ac Wi-Fi and Bluetooth 4.2 lets you connect to internet wherever there is a Wi-Fi connection',
        'thumbnail'=>'laptop6.jpg',
        'created_at'=>'2018-12-09 12:28:14',
        'updated_at'=>'2018-12-09 12:28:14'
        ] );
        
        
                    
        Product::create( [
        'id'=>25,
        'name'=>'HP ENVY',
        'category_id'=>2,
        'manufacturer_id'=>16,
        'price'=>'1499.99',
        'sale_price'=>'1499.99',
        'overview'=>'Enjoy robust multitasking performance with this 17.3” HP laptop, which features a 1.8GHz Intel Core i7 processor, 12GB of DDR4 RAM, and a massive 1TB hard drive for all your storage needs. It also features integrated Bluetooth and 4 USB ports so you can connect a wide range of additional peripherals.',
        'more_information'=>'1.8 GHz 8th Generation Intel Core i7-8550U quad-core processor and 12GB of DDR4-2400 power solid and reliable performance for all of your everyday computing needs\r\n1TB hard drive offers tons of storage space so you can keep all of your documents and media files just a quick click away\r\n17.3-inch Full HD touchscreen display with a native resolution of 1920 x 1080 presents images, videos, apps, software, and other content in crisp, crystal-clear detail\r\n802.11b/g/n/ac Wi-Fi gives you a fast wireless connection to your home or office Wi-Fi network, or to hotspots around town\r\n10/100/1000 GbE LAN Ethernet port gives you the option of connecting to your network with a secure and reliable wired connection for potentially faster download and upload speeds',
        'thumbnail'=>'laptop7.jpg',
        'created_at'=>'2018-12-09 12:41:17',
        'updated_at'=>'2018-12-09 12:41:17'
        ] );
        
        
                    
        Product::create( [
        'id'=>26,
        'name'=>'Dell 17.3',
        'category_id'=>2,
        'manufacturer_id'=>7,
        'price'=>'1299.99',
        'sale_price'=>'1299.99',
        'overview'=>'Maximize your productivity and downtime with this 17.3" Dell Inspiron laptop. It features a Full HD 1080p display for crystal-clear clarity. An Intel Core i7 processor, 16GB of RAM, and a 2TB hard drive deliver unparalleled performance.',
        'more_information'=>'1.8GHz Intel Core i7-8550U quad-core processor and 16GB of DDR4 RAM combine to deliver blazing speeds for multitasking ease\r\n2TB hard drive capacity offers lots of space for your music, photos, videos, and other media files\r\n17.3-inch Full HD display with a native resolution of 1920 x 1080 presents images, movies, games, and more in sharp detail\r\n16GB single-channel DDR4 2400MHz graphics card with optional discrete graphics lets you enjoy smooth game play and take advantage of powerful video and photo editing software\r\nAMD Radeon 530 graphics with 4GB GDDR5 graphics memory delivers life-like imagery and ultra-smooth onscreen motion for completely immersive gaming experiences\r\nBuilt-in 802.11ac Wi-Fi makes wirelessly connecting to mobile hotspots at home, work, or on the go quick and easy',
        'thumbnail'=>'laptop8.jpg',
        'created_at'=>'2018-12-09 12:59:47',
        'updated_at'=>'2018-12-09 12:59:47'
        ] );
        
        
                    
        Product::create( [
        'id'=>27,
        'name'=>'HP PB 17"',
        'category_id'=>2,
        'manufacturer_id'=>16,
        'price'=>'1634.99',
        'sale_price'=>'1634.99',
        'overview'=>'Bring reliable performance and maximize your computing needs with the 17" HP laptop for superior performance. Powered by an Intel Core i7 8550U processor. Laptop with 16 GB memory is designed to run efficiently and reliably. Packs 256GB SSD internal storage.',
        'more_information'=>'Intel Core i7 8550U processor and 16 GB RAM deliver solid and reliable performance for effortless everyday computing and more\r\n    256GB SSD smooth and powerful performance\r\n    Featuring a 17" Laptop,multimedia playing lapbook features a high resolution to let you view content in crystal clarity',
        'thumbnail'=>'laptop9.jpg',
        'created_at'=>'2018-12-09 13:04:25',
        'updated_at'=>'2018-12-09 13:04:25'
        ] );
        
        
                    
        Product::create( [
        'id'=>28,
        'name'=>'MSI GS65 8RE-060CA Stealth 15.6"',
        'category_id'=>2,
        'manufacturer_id'=>38,
        'price'=>'2249.00',
        'sale_price'=>'2249.00',
        'overview'=>'Latest 8th Generation Intel Core i7-8750H (2.2 - 4.1 Ghz w/ Turbo Boost)\r\nLatest GeForce GTX 1060 6GB GDDR5 with desktop level performance, VR Ready\r\n15.6" FHD IPS Level display (1920x1080, 144Hz Refresh Rate, 7ms response time panel, 72%NTSC) - 4.9mm bezel and built within in 14" design',
        'more_information'=>'16GB DDR IV (8GB*2) and 256GB NVMe SSD\r\nPer-Key RGB Keyboard; Cooler Boost Trinity (3 fans+5pipes); Killer LAN; BT 5.0; Thunderbolt 3; USB 3.1; mDP v1.2; High-Res Audio; 82whr battery and 8 hour life; Win 10 Pro',
        'thumbnail'=>'laptop10.jpg',
        'created_at'=>'2018-12-09 13:12:43',
        'updated_at'=>'2018-12-09 13:12:43'
        ] );
        
        
                    
        Product::create( [
        'id'=>29,
        'name'=>'Lenovo IdeaPad 320 15.6"',
        'category_id'=>2,
        'manufacturer_id'=>9,
        'price'=>'499.99',
        'sale_price'=>'499.99',
        'overview'=>'Streamline and simplify your life with the Lenovo IdeaPad 320 15.6" laptop. It boasts a powerful 2.7GHz quad-core processor with 8GB of RAM to handle daily tasks with ease. It comes equipped with Windows 10 Home which features Cortana, the personal assistant who can open apps and answer questions on command.',
        'more_information'=>'2.7GHz quad-core processor with 8GB of RAM offer the power and speed for everyday computing and web browsing\r\n1TB hard drive offers a compact warehouse for storing all your favourite movies, music, photos, and more in one convenient place\r\n15.6" LED-backlit display with 1366 x 768 native resolution provides clear HD visuals for an enjoyable viewing experience\r\nAMD Radeon R7 graphics transforms the computer into a digital media powerhouse for immersive entertainment\r\nBuilt-in 802.11ac Wi-Fi allows you to seamlessly connect to your wireless home network or any Wi-Fi hotspot',
        'thumbnail'=>'laptop11.jpg',
        'created_at'=>'2018-12-09 13:21:36',
        'updated_at'=>'2018-12-09 13:21:36'
        ] );
        
        
                    
        Product::create( [
        'id'=>30,
        'name'=>'Acer Aspire ES 17.3"',
        'category_id'=>2,
        'manufacturer_id'=>10,
        'price'=>'499.99',
        'sale_price'=>'499.99',
        'overview'=>'Stay efficient in everyday computing with the Acer Aspire ES1-732-P54M 17.3" laptop. Powered by an Intel Pentium quad-core processor and 4GB RAM, this laptop is best for everyday activities like surfing the web, word processing, and light multitasking. HDMI connectivity lets you connect to an external display, while the Precision Touchpad ensures smooth navigation.',
        'more_information'=>'1.1GHz Intel Pentium N4200 quad core processor and 4GB RAM power reliable performance for everyday computing tasks\r\n500GB hard drive provides ample storage space for your documents, photos, music, videos, movies, and other miscellaneous files\r\n17.3-inch LCD display with a resolution of 1600 x 900 gives you lots of screen real estate that presents content in bright and clear detail\r\n802.11b/g/n/ac Wi-Fi makes it easy to connect to wireless networks at home, at the office, or at a coffee shop when you\'re on the go\r\nIntegrated Ethernet port lets you connect directly to your home or office\'s internet network for faster and more reliable connection speeds\r\nConnectivity options include Bluetooth, a USB 3.0 port, and a pair of USB 2.0 ports so you can easily connect peripherals such as a printer or external hard drive',
        'thumbnail'=>'laptop12.jpg',
        'created_at'=>'2018-12-09 13:28:16',
        'updated_at'=>'2018-12-09 13:28:16'
        ] );
        
        
                    
        Product::create( [
        'id'=>31,
        'name'=>'Acer Predator Helios Gaming Laptop',
        'category_id'=>2,
        'manufacturer_id'=>10,
        'price'=>'1669.28',
        'sale_price'=>'1669.28',
        'overview'=>'Latest 7th Generation Intel Core i7-7700HQ Processor 2.8GHz with Turbo Boost Technology up to 3.8GHz | Windows 10 Home 64-bit\r\nLatest NVIDIA GeForce GTX 1060 with 6 GB of dedicated GDDR5 VRAM',
        'more_information'=>'15.6" Full HD (1920 x 1080) widescreen IPS display, Red Backlit Keyboard\r\n16GB DDR4 DRAM Memory & 256GB SSD | Extra empty expandable hard drive slot for 2.5" hard drives\r\nUp to 7-hours of battery life',
        'thumbnail'=>'laptop13.jpg',
        'created_at'=>'2018-12-09 13:36:58',
        'updated_at'=>'2018-12-09 13:36:58'
        ] );
        
        
                    
        Product::create( [
        'id'=>32,
        'name'=>'Apple MacBook Pro 13.3" Laptop',
        'category_id'=>2,
        'manufacturer_id'=>8,
        'price'=>'1729.99',
        'sale_price'=>'1729.99',
        'overview'=>'The new MacBook Pro is razor-thin, featherlight, and now even faster and more powerful than before. It has the brightest, most colourful Mac notebook display. And it features up to 10 hours of battery life.* It\'s a notebook built for the work you do every day. Ready to go anywhere a great idea takes you.',
        'more_information'=>'Brilliant Retina display\r\n    Seventh-generation dual-core Intel Core i5\r\n    Intel Iris Plus Graphics 640\r\n    Ultrafast SSD\r\n    Two Thunderbolt 3 (USB-C) ports\r\n    Up to 10 hours of battery life*\r\n    802.11ac Wi-Fi\r\n    Force Touch trackpad',
        'thumbnail'=>'laptop3.jpg',
        'created_at'=>'2018-12-09 13:49:12',
        'updated_at'=>'2018-12-09 13:49:12'
        ] );
        
        
                    
        Product::create( [
        'id'=>33,
        'name'=>'Google Pixelbook 12.3"',
        'category_id'=>2,
        'manufacturer_id'=>1,
        'price'=>'1299.99',
        'sale_price'=>'1299.99',
        'overview'=>'Say hello to Google Pixelbook, the high-performance Chromebook that features built-in Google Assistant, an HD touchscreen display, and everything you love about Chrome OS. An Intel Kaby Lake-Y i5 processor, 8GB of RAM, and 128GB eMMC storage drive combine to deliver powerful performance for work, play, and everything in between.',
        'more_information'=>'1.2GHz Intel 7th Gen i5 processor and 8GB of RAM delivers reliable performance for your everyday computing needs\r\n128GB eMMC storage drive lets you keep essential files close at hand for offline work, while Chrome OS\'s suite of cloud-based apps offers endless storage space for online work\r\n12.3-inch touchscreen display with a native resolution of 2400 x 1600 presents apps, webpages, and other content in sharp, vivid detail\r\n360-degree hinge on the display allows it to transform from a laptop into 3 other viewing modes: tablet, tent, and entertainment\r\n802.11b/g/n/ac Wi-Fi keeps you connected to the web at fast speeds anywhere there\'s a Wi-Fi hotspot',
        'thumbnail'=>'laptop15.jpg',
        'created_at'=>'2018-12-09 23:31:12',
        'updated_at'=>'2018-12-09 23:31:12'
        ] );
        
        
                    
        Product::create( [
        'id'=>34,
        'name'=>'ASUS ROG Zephyrus Gaming Laptop',
        'category_id'=>2,
        'manufacturer_id'=>15,
        'price'=>'2799.99',
        'sale_price'=>'2799.99',
        'overview'=>'Your competition won\'t stand a chance with you at the helm of the ASUS ROG Zephyrus M. This 15.6-inch, ultra-slim gaming laptop boasts a NVIDIA GeForce GTX 1070 GPU, plus a 2.2GHz Intel Core i7-8750H 6-core processor and 16GB of RAM. It also features an RGB gaming keyboard with AURA SYNC and 20-million keypress durability.',
        'more_information'=>'2.2GHz Intel Core i7-8750H 6-core processor and 16GB RAM combine to deliver premium speed and performance for big-league gaming experiences\r\n1TB FireCuda hard drive gives you tons of storage space to keep a massive digital arcade of games and save files on hand\r\n256GB PCIe NVMe solid state drive loads files, games, and other system processes fast, so you can keep your most-frequently played games a quick click away\r\n15.6-inch Full HD display with a native resolution of 1920 x 1080 presents images, videos, apps, software, and other content in crisp, crystal-clear detail\r\nNVIDIA GeForce GTX 1070 graphics card with 8GB of dedicated video memory delivers exceptional, super-smooth visuals and opens the door to a jaw-dropping virtual reality experience',
        'thumbnail'=>'laptop16.jpg',
        'created_at'=>'2018-12-09 23:38:25',
        'updated_at'=>'2018-12-09 23:38:25'
        ] );
        
        
                    
        Product::create( [
        'id'=>35,
        'name'=>'Microsoft Surface 13.5"',
        'category_id'=>2,
        'manufacturer_id'=>14,
        'price'=>'1049.99',
        'sale_price'=>'1049.99',
        'overview'=>'Go beyond traditional and power your productivity with the Microsoft Surface Laptop 2. Featuring an 8th generation Intel Core processor, this ultra-slim laptop is ready to perform when you are. A highly intuitive 13.5" PixelSense touchscreen and Windows 10 Home operating system make the Surface Laptop 2 a joy to work with.',
        'more_information'=>'8th generation Intel Core i5 processor and 8GB RAM provide powerful performance for multitasking, streaming HD videos, and other demanding tasks\r\n128GB solid state drive speeds up boot-up and load times so you spend less time waiting and more time doing\r\n13.5" PixelSense touchscreen display with 2256 x 1504 resolution showcases crystal-clear visuals\r\nWindows Ink helps boost your productivity by transforming drawings into shapes and editing Word documents in a way that feels natural to you',
        'thumbnail'=>'laptop17.jpg',
        'created_at'=>'2018-12-09 23:47:05',
        'updated_at'=>'2018-12-13 03:18:53'
        ] );
        
        
                    
        Product::create( [
        'id'=>36,
        'name'=>'Apple iPad 9.7" 128GB with Wi-Fi',
        'category_id'=>4,
        'manufacturer_id'=>8,
        'price'=>'549.99',
        'sale_price'=>'549.99',
        'overview'=>'iPad. Like a computer. Unlike any computer. Create, learn, work and play like never before. Featuring an immersive 9.7-inch Multi-Touch Retina display and powerful A10 Fusion chip, and now supporting Apple Pencil, there\'s nothing else quite like iPad.',
        'more_information'=>'Immersive 9.7-inch Multi-Touch Retina display\r\nA10 Fusion chip with the power and capability you\'d expect from a computer\r\nNow supports Apple Pencil\r\n8MP camera\r\nFaceTime HD camera\r\nTouch ID and Apple Pay\r\nWi-Fi',
        'thumbnail'=>'tablet1.jpg',
        'created_at'=>'2018-12-09 23:58:35',
        'updated_at'=>'2018-12-13 03:19:18'
        ] );
        
        
                    
        Product::create( [
        'id'=>37,
        'name'=>'Apple iPad Pro 11" 64GB with Wi-Fi',
        'category_id'=>4,
        'manufacturer_id'=>8,
        'price'=>'999.99',
        'sale_price'=>'999.99',
        'overview'=>'The new 11-inch iPad Pro features an advanced Liquid Retina display that goes edge to edge.1 Face ID. A12X Bionic—the smartest and most powerful chip Apple has ever made. And support for the new Apple Pencil and Smart Keyboard Folio.2 It\'s the biggest change to iPad since iPad.',
        'more_information'=>'11-inch edge-to-edge Liquid Retina display with ProMotion, True Tone and wide colour1\r\n    A12X Bionic chip with Neural Engine\r\n    Face ID for secure authentication and Apple Pay\r\n    12MP back camera, 7MP TrueDepth front camera\r\n    Four speaker audio with wider stereo sound\r\n    802.11ac Wi-Fi\r\n    Up to 10 hours of battery life3\r\n    USB-C connector for charging and accessories\r\n    iOS 12 with Group FaceTime, shared augmented reality experiences, Screen Time and more',
        'thumbnail'=>'tablet2.jpg',
        'created_at'=>'2018-12-10 00:05:49',
        'updated_at'=>'2018-12-13 03:22:10'
        ] );
        
        
                    
        Product::create( [
        'id'=>38,
        'name'=>'Apple iPad mini 4 128GB With Wi-Fi',
        'category_id'=>4,
        'manufacturer_id'=>8,
        'price'=>'529.99',
        'sale_price'=>'529.99',
        'overview'=>'The iPad mini 4 has a gorgeous 7.9-inch Retina display, yet it\'s only 6.1mm thin and weighs just 0.65 pounds, making it small enough to hold in one hand. It has a powerful A8 chip, advanced iSight and FaceTime HD cameras, WiFi connectivity, iCloud, the Touch ID fingerprint sensor, and up to 10 hours of battery life. It also includes great apps for productivity and creativity, and powerful new features in iOS 9 like multitasking.',
        'more_information'=>'7.9-inch Retina display with anti-reflective coating (diagonal)\r\n    A8 second-generation chip with 64-bit desktop-class architecture\r\n    Touch ID fingerprint sensor\r\n    8MP iSight camera with 1080p video\r\n    FaceTime HD camera\r\n    802.11ac WiFi with MIMO\r\n    Up to 10 hours of battery life\r\n    Two speaker audio',
        'thumbnail'=>'tablet3.jpg',
        'created_at'=>'2018-12-10 00:16:23',
        'updated_at'=>'2018-12-13 03:22:33'
        ] );
        
        
                    
        Product::create( [
        'id'=>39,
        'name'=>'Samsung Galaxy TabA 8" Tablet',
        'category_id'=>4,
        'manufacturer_id'=>5,
        'price'=>'199.99',
        'sale_price'=>'199.99',
        'overview'=>'Sleekly designed and full of convenient features, the Samsung Galaxy Tab A is perfect for the whole family. It offers 32GB of internal storage, a speedy 1.4GHz quad-core processor with 2GB RAM, and a vivid 8” touchscreen. Home Mode shows you what you need to know, while Kids Mode and Parental Control make supervising screen time a breeze.',
        'more_information'=>'8" WXGA TFT touchscreen with 1280 x 800 native resolution delivers sharp visuals when watching videos, playing games, and perusing apps\r\nAndroid operating system provides a smooth, intuitive user experience that\'s powerful and customizable\r\n32GB internal memory capacity offers space for you to take your favourite games, movies, videos, music, and apps with you on the go\r\nSupports microSD cards up to 256GB (sold separately) to give you additional storage space, turning your device into a mobile entertainment centre',
        'thumbnail'=>'tablet4.jpg',
        'created_at'=>'2018-12-10 00:25:16',
        'updated_at'=>'2018-12-13 03:23:24'
        ] );
        
        
                    
        Product::create( [
        'id'=>40,
        'name'=>'Samsung Galaxy Tab S2 9" Tablet',
        'category_id'=>4,
        'manufacturer_id'=>5,
        'price'=>'399.99',
        'sale_price'=>'399.99',
        'overview'=>'Entertainment, connectivity, productivity—you\'ll enjoy it all with this Samsung 9.7" Galaxy Tab S2. Sporting a super-slim and lightweight design, this tablet is your ideal on-the-go companion you\'ll never want to put down. Enjoy true-to-life visuals on the Super AMOLED display and get your work done efficiently with preloaded Microsoft Office.',
        'more_information'=>'The beautiful 9.7" QXGA Super AMOLED display with 2048 x 1536 resolution delivers crisp, lifelike images and bright, vivid colours for a more dynamic picture that\'ll make your movies and presentations pop\r\nThe screen is designed to automatically adjust brightness levels based on where you are or what you\'re watching so as to always give you the clearest picture\r\nAndroid 6.0 Marshmallow provides a smooth, intuitive user experience that\'s powerful and customizable\r\n32GB internal memory size, expandable to 128GB (microSD card sold separately), gives you plenty of space to store your music, photos, movies, and more\r\n64-bit 1.8GHz Quad + 1.4GHz Quad Exynos 5433 octa-core processor and 3GB RAM deliver speedy performance and responsive power for quick and effortless downloading, sharing, web-surfing, movie-watching and more',
        'thumbnail'=>'tablet5.jpg',
        'created_at'=>'2018-12-10 00:37:08',
        'updated_at'=>'2018-12-13 03:19:56'
        ] );
        
        
                    
        Product::create( [
        'id'=>41,
        'name'=>'Samsung Galaxy Tab S4 10.5" 256GB',
        'category_id'=>4,
        'manufacturer_id'=>5,
        'price'=>'929.99',
        'sale_price'=>'929.99',
        'overview'=>'Optimized for consumption and creation, the Samsung Galaxy Tab S4 10.5” tablet fuels productivity. Equipped with an octa-core processor, 3GB RAM, and Android Oreo, this tablet runs smoothly whether you\'re sketching with the S Pen or editing videos. The Tab S4\'s sAMOLED display shows off stunning visuals, while a 15-hour battery life keeps up with your demands.',
        'more_information'=>'10.5-inch sAMOLED display with 2560 x 1600 resolution and slim bezel showcases crystal-clear imagery to make movies, presentations\r\nOreo is Android\'s sweetest operating system yet, offering faster boot times, efficient power management, picture-in-picture capabilities, and much more\r\n256GB internal memory is expandable with a micro SD card up to 400GB (sold separately)\r\nQualcomm MSM 8998 octa-core processor and 3GB RAM propel smooth navigation\r\nAccess over a million apps on the Google Play Store\r\nAdvanced S Pen with expanded tools lets you write naturally, draw, edit videos, and translate text, all without interruption -- it never requires charging',
        'thumbnail'=>'tablet6.jpg',
        'created_at'=>'2018-12-10 00:49:05',
        'updated_at'=>'2018-12-10 00:49:05'
        ] );
        
        
                    
        Product::create( [
        'id'=>42,
        'name'=>'Samsung Galaxy Tab A 10.1" 16GB',
        'category_id'=>4,
        'manufacturer_id'=>5,
        'price'=>'369.99',
        'sale_price'=>'369.99',
        'overview'=>'Connect, engage, and be entertained all with a single device. The Samsung Galaxy Tab A 10.1" tablet features a WUXGA touchscreen display with 1920 x 1200 resolution for a stunning viewing experience. It has Built-in Wi-Fi and Bluetooth, and has an 8MP rear-facing camera that records video in Full HD and a front-facing 2MP, great for video chats.',
        'more_information'=>'10.1-inch WUXGA touchscreen display with a 1920 x 1200 resolution delivers high-quality imagery for an exceptional viewing experience when gaming, watching videos, browsing, and social networking\r\nAndroid 6.0 Marshmallow operating system provides an intuitive and customizable user experience\r\n16GB internal memory size, expandable with a microSD card (sold separately) up to 200GB, gives you plenty of space to store your music, photos, movies, and more\r\n1.6GHz Exynos 7870 octa-core processor with 2GB of RAM delivers powerful performance for quick and effortless navigation and responsive app operation\r\nIntegrated 802.11b/g/n/ac (2.4GHz/5GHz) Wi-Fi lets you connect to the web from your wireless home network or a WiFi hotspot',
        'thumbnail'=>'tablet7.jpg',
        'created_at'=>'2018-12-10 01:04:05',
        'updated_at'=>'2018-12-10 01:04:05'
        ] );
        
        
                    
        Product::create( [
        'id'=>43,
        'name'=>'Apple iPad Pro 12.9" 256GB',
        'category_id'=>4,
        'manufacturer_id'=>8,
        'price'=>'1449.99',
        'sale_price'=>'1449.99',
        'overview'=>'The new 12.9-inch iPad Pro features an advanced Liquid Retina display that goes edge to edge.1 Face ID. A12X Bionic—the smartest and most powerful chip Apple has ever made. And support for the new Apple Pencil and Smart Keyboard Folio.2 It\'s the biggest change to iPad since iPad.',
        'more_information'=>'12.9-inch edge-to-edge Liquid Retina display with ProMotion, True Tone and wide colour1\r\nA12X Bionic chip with Neural Engine\r\nFace ID for secure authentication and Apple Pay\r\n12MP back camera, 7MP TrueDepth front camera\r\nFour speaker audio with wider stereo sound\r\n802.11ac Wi-Fi\r\nUp to 10 hours of battery life3',
        'thumbnail'=>'tablet8.jpg',
        'created_at'=>'2018-12-10 01:12:48',
        'updated_at'=>'2018-12-10 01:12:48'
        ] );
        
        
                    
        Product::create( [
        'id'=>44,
        'name'=>'ASUS ZenPad 3S 9.7"',
        'category_id'=>4,
        'manufacturer_id'=>15,
        'price'=>'469.95',
        'sale_price'=>'469.95',
        'overview'=>'Stay a tap of the finger away from a world of entertainment no matter where you go with this ASUS ZenPad 3S tablet. It sports a vivid and crystal-clear 9.7" IPS touchscreen display with a resolution of 2040 x 1536, as well as integrated Wi-Fi and Bluetooth for breezy wireless connectivity. It also features a built-in fingerprint sensor for enhanced security.',
        'more_information'=>'9.7-inch IPS touchscreen display with a resolution of 2048 x 1536 presents apps, images, videos, and other content in sharp detail\r\nAndroid 6.0 Marshmallow provides a smooth, intuitive user experience that\'s powerful and customizable\r\n64GB internal memory size, expandable to 128GB (microSD card sold separately), gives you plenty of space to store your music, photos, movies, and more',
        'thumbnail'=>'tablet9.jpg',
        'created_at'=>'2018-12-10 02:12:58',
        'updated_at'=>'2018-12-10 02:12:58'
        ] );
        
        
                    
        Product::create( [
        'id'=>45,
        'name'=>'Microsoft Surface Pro 6 12.3"',
        'category_id'=>4,
        'manufacturer_id'=>14,
        'price'=>'1049.99',
        'sale_price'=>'1049.99',
        'overview'=>'Wherever you are when inspiration strikes, seize the moment with the Microsoft Surface Pro. Ideal for commuters and on-the-go professionals, this ultra-slim tablet packs an impressive amount of power with its advanced 8th generation Intel Core processor, and Windows 10 Home comes preloaded. Note: Keyboard, mouse, dial, and pen are sold separately.',
        'more_information'=>'12.3-inch PixelSense touchscreen display with a resolution of 2736 x 1824 is responsive, fluid, and easy on the eyes\r\n8th generation Intel Core i5 processor and 8GB of RAM combine to deliver robust and reliable performance that\'s ideal for multi-taskers and creative pros\r\n128GB solid state drive provides you with lots of storage space to keep important files and folders just a quick tap of the finger away',
        'thumbnail'=>'tablet10.jpg',
        'created_at'=>'2018-12-10 02:19:35',
        'updated_at'=>'2018-12-10 02:19:35'
        ] );
        
        
                    
        Product::create( [
        'id'=>46,
        'name'=>'Apple iPad 6th Generation',
        'category_id'=>4,
        'manufacturer_id'=>8,
        'price'=>'499.99',
        'sale_price'=>'499.99',
        'overview'=>'The item is fully functional in a 9/10 cosmetic condition. It ships in a non-retail box with iPad and aftermarket USB cable only. The wall adapter, earphones and SIM card are not included.',
        'more_information'=>'The item is fully functional in a 9/10 cosmetic condition. It ships in a non-retail box with iPad and aftermarket USB cable only. The wall adapter, earphones and SIM card are not included.',
        'thumbnail'=>'tablet11.jpg',
        'created_at'=>'2018-12-10 02:41:10',
        'updated_at'=>'2018-12-10 02:41:10'
        ] );
        
        
                    
        Product::create( [
        'id'=>47,
        'name'=>'Google Pixel Slate 12.3"',
        'category_id'=>4,
        'manufacturer_id'=>1,
        'price'=>'1299.99',
        'sale_price'=>'1299.99',
        'overview'=>'Thoughtfully engineered for modern living, Google Slate is primed to perform. Featuring a full-size backlit keyboard, this convertible tablet effortlessly adapts to the task at hand. Google Duo keeps you connected and collaborative, while Google Assistant is at your beck and call to answer questions and help you blaze through your to-do list like a boss.',
        'more_information'=>'12.3” Molecular Display boasts an outstanding 3000 x 2000 resolution and 6 million pixels to bring movies, videos, and more to life\r\nChrome OS starts up in seconds and features the apps you know and love including Google Drive, Gmail, YouTube, Netflix, Evernote, and many more\r\n128GB of storage space lets you keep essential files close at hand for offline work, while Chrome OS\'s suite of cloud-based apps offers endless storage space for online work\r\n8th generation Intel Core i5 processor with 8GB of RAM offer plenty of power for daily productivity and downtime',
        'thumbnail'=>'tablet12.jpg',
        'created_at'=>'2018-12-10 02:59:43',
        'updated_at'=>'2018-12-10 02:59:43'
        ] );
        
        
                    
        Product::create( [
        'id'=>48,
        'name'=>'Lenovo Tab4 10 Plus 10.1"',
        'category_id'=>4,
        'manufacturer_id'=>9,
        'price'=>'399.99',
        'sale_price'=>'399.99',
        'overview'=>'The Lenovo Tab 4 10 Plus is a portable powerhouse that\'s made to share. Boasting stunning FHD visuals, an intuitive Android Nougat operating system, and multi-user functionality, the Tab 4 Plus is ideal for browsing the web, building work presentations, helping your kids with homework, and keeping your little one entertained.',
        'more_information'=>'10.1-inch IPS Full HD multi-touch display with 1920 x 1200 resolution shows off clear detail for all your multimedia needs\r\nAndroid 7.1 Nougat operating system provides a smooth, intuitive user experience that\'s powerful and customizable\r\n16GB built-in storage capacity provides ample space for apps, music, and videos, and is upgradeable by adding a memory card up to 128GB\r\n2.0GHz Qualcomm Snapdragon 625 8-core processor drives buttery smooth performance and load-up times',
        'thumbnail'=>'tablet13.jpg',
        'created_at'=>'2018-12-10 03:13:49',
        'updated_at'=>'2018-12-10 03:13:49'
        ] );
        
        
                    
        Product::create( [
        'id'=>49,
        'name'=>'MICROSOFT SURFACE PRO 3',
        'category_id'=>4,
        'manufacturer_id'=>14,
        'price'=>'445.99',
        'sale_price'=>'445.99',
        'overview'=>'This Refurbished product was revived by one of the Leading Refurbished Computer Manufacturer in North America. As the Canadian largest Refurbished computer provider, we produce and ship out over 15,000 pcs business Class Computers each Month, all units are installed with Genius Windows Systems and made follow by Microsoft\'s high standard Requirements. They will be inspected, reconfigured, cleaned and re-packaged like new. The refurbished product allows customer to enjoy high-quality device at a fair price.',
        'more_information'=>'Microsoft Surface Pro 3 1631 Intel Core i5-4300U 1.9GHz, 8G RAM, 256G SSD, 12", Windows 10 Professional - 1 Year Warranty - Refurbished',
        'thumbnail'=>'tablet14.jpg',
        'created_at'=>'2018-12-10 03:39:29',
        'updated_at'=>'2018-12-10 03:39:29'
        ] );
        
        
                    
        Product::create( [
        'id'=>50,
        'name'=>'Google Pixel Slate 12.3"',
        'category_id'=>4,
        'manufacturer_id'=>1,
        'price'=>'1299.99',
        'sale_price'=>'1299.99',
        'overview'=>'Thoughtfully engineered for modern living, Google Slate is primed to perform. Featuring a full-size backlit keyboard, this convertible tablet effortlessly adapts to the task at hand. Google Duo keeps you connected and collaborative, while Google Assistant is at your beck and call to answer questions and help you blaze through your to-do list like a boss.',
        'more_information'=>'12.3” Molecular Display boasts an outstanding 3000 x 2000 resolution and 6 million pixels to bring movies, videos, and more to life\r\nChrome OS starts up in seconds and features the apps you know and love including Google Drive, Gmail, YouTube, Netflix, Evernote, and many more\r\n128GB of storage space lets you keep essential files close at hand for offline work, while Chrome OS\'s suite of cloud-based apps offers endless storage space for online work\r\n8th generation Intel Core i5 processor with 8GB of RAM offer plenty of power for daily productivity and downtime',
        'thumbnail'=>'tablet15.jpg',
        'created_at'=>'2018-12-10 03:46:50',
        'updated_at'=>'2018-12-10 03:46:50'
        ] );
        
        
                    
        Product::create( [
        'id'=>51,
        'name'=>'HP Spectre x360 15.6"',
        'category_id'=>4,
        'manufacturer_id'=>16,
        'price'=>'1999.99',
        'sale_price'=>'1999.99',
        'overview'=>'Experience unrivalled versatility with this HP Spectre x360 convertible laptop. Packed with an 8th gen Core i7 processor, 16GB RAM, and 512GB SSD this laptop powers through activities at work and play. A 360-degree hinge gives you the freedom to use the laptop in 4 modes, while the 4K UHD touchscreen display and stylus offer effortless navigation.',
        'more_information'=>'1.8GHz 8th generation Intel Core i7-8550U quad-core processor and 16GB DDR4 RAM deliver unquestionable power and performance for everyday activities and smooth multitasking\r\n512GB solid state drive provides ample storage and quick boot-up and load times when accessing software, movies, photos, music, documents, and more\r\n15.6-inch 4K IPS eDP micro-edge BrightView WLED-backlit touch-enabled display with 3840 x 2160 resolution delivers crystal-clear visuals and vibrant colours\r\nNVIDIA GeForce MX150 with 2GB of GDDR5 dedicated memory provides the graphics performance you need for a cinematic gaming experience, while extending battery life for longer enjoyment',
        'thumbnail'=>'tablet16.jpg',
        'created_at'=>'2018-12-10 05:30:46',
        'updated_at'=>'2018-12-10 05:30:46'
        ] );
        
        
                    
        Product::create( [
        'id'=>52,
        'name'=>'HUAWEI MateBook SigEdition 2',
        'category_id'=>4,
        'manufacturer_id'=>4,
        'price'=>'779.99',
        'sale_price'=>'779.99',
        'overview'=>'Huawei MateBook Signature Edition 2 in 1 PC, 4GB+128GB Intel Core m5 (Gray)',
        'more_information'=>'Huawei Matebook HZ-W19 Sigature Edition 2 in 1, 12 inch Tablet Laptop, Intel Core M5, 4GB DDR3+128GB SSD, Window 10_Space Gray color',
        'thumbnail'=>'tablet17.jpg',
        'created_at'=>'2018-12-10 05:54:03',
        'updated_at'=>'2018-12-13 03:26:25'
        ] );
        
        
                    
        Product::create( [
        'id'=>53,
        'name'=>'Samsung Galaxy Book 12"',
        'category_id'=>4,
        'manufacturer_id'=>5,
        'price'=>'1899.99',
        'sale_price'=>'1899.99',
        'overview'=>'The Samsung Galaxy Book 12" Windows 10 Pro tablet and keyboard is a portable productivity powerhouse unlike any other. It has a blazing-fast Intel Core i5 dual-core processor, backlit island-style keyboard, adaptive fast charging, 11 hours battery life, and Super AMOLED display. This is going to be your favourite gadget to get things done at home or on-the-go.',
        'more_information'=>'The Super AMOLED touchscreen display is 12" in size and offers a superior native resolution of 2160 x 1440 (FHD+)\r\nFeaturing a backlit island-style keyboard and S-Pen, this tablet has all the bells and whistles\r\n3.1GHz Intel Core i5 dual processor with 8GB of RAM delivers powerful performance for quick and effortless navigation and responsive app operation\r\nWith a built-in storage capacity of 256GB, built-in flash memory, and a built-in memory card reader, this tablet has a lot of storage and reading options',
        'thumbnail'=>'tablet18.jpg',
        'created_at'=>'2018-12-10 06:06:27',
        'updated_at'=>'2018-12-10 06:06:27'
        ] );
        
        
                    
        Product::create( [
        'id'=>54,
        'name'=>'Canon EOS 7D Mark II W-E1',
        'category_id'=>5,
        'manufacturer_id'=>18,
        'price'=>'1849.99',
        'sale_price'=>'1849.99',
        'overview'=>'Take your photographic ambitions to the next level with the Canon EOS 7D Mark II DSLR camera body with W-E1 Wi-Fi adapter. This 20.2MP camera features high-speed continuous shooting at 10fps, a fast and accurate autofocus, and built-in GPS. The Wi-Fi adapter allows remote shooting and wireless transferring of images to your compatible smart device.',
        'more_information'=>'A 20.2MP CMOS APS-C sensor with ISOs ranging from 100 to 16000 delivers clear, detailed images with minimal noise. Whether you\'re photographing a family wedding or your next faraway vacation, you\'ll get picture-perfect results suitable for blowing up, framing, and hanging on your wall.',
        'thumbnail'=>'camera1.jpg',
        'created_at'=>'2018-12-10 06:21:01',
        'updated_at'=>'2018-12-10 06:21:01'
        ] );
        
        
                    
        Product::create( [
        'id'=>55,
        'name'=>'Canon EOS M100 Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>18,
        'price'=>'429.99',
        'sale_price'=>'429.99',
        'overview'=>'Find your inspiration with the compact Canon EOS M100 interchangeable lens mirrorless camera. The 24.2MP sensor, DIGIC 7 processor, and Dual Pixel CMOS AF capture unforgettable moments in stunning detail. It has customizable controls, a tiltable LCD, and built-in Wi-Fi for easy image sharing. It includes an EF-M 15-45mm lens.',
        'more_information'=>'A 24.2MP APS-C-size CMOS sensor captures images with incredible depth and beauty that you can confidently share on the web or blow up into poster-sized prints\r\nPowered by the DIGIC 7 image processor to ensure photos and videos with fine details and dynamic, rich colours, even in in low light\r\nAn ISO range of 100 to 25600 lets you confidently shoot in low light with minimal noise and grain at high ISOs',
        'thumbnail'=>'camera2.jpg',
        'created_at'=>'2018-12-10 06:29:55',
        'updated_at'=>'2018-12-13 03:28:45'
        ] );
        
        
                    
        Product::create( [
        'id'=>56,
        'name'=>'Sony a7 III Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>19,
        'price'=>'2799.99',
        'sale_price'=>'2799.99',
        'overview'=>'Equipped with a 24.2MP full-frame image sensor, extreme AF coverage, and 4K video capability, the Sony a7 III mirrorless camera is a force to be reckoned with. Ideal for enthusiasts and pros alike, this feature-rich camera can tell a story with incredible detail, clarity, and colour accuracy. An FE 28-70mm lens is included for sharp everyday shooting.',
        'more_information'=>'Advanced 24.2MP back-illuminated 35mm full-frame image sensor with increased sensitivity, outstanding resolution, and 15 stops of dynamic range at low sensitivities for accurate colour reproduction\r\nUpdated BIONZ X image processing system for shooting full-res images at up to 10 fps with continuous, accurate AF/AE tracking for up to 177 standard JPEG images, 89 compressed RAW images, or 40 uncompressed RAW images',
        'thumbnail'=>'camera3.jpg',
        'created_at'=>'2018-12-10 06:36:09',
        'updated_at'=>'2018-12-10 06:36:09'
        ] );
        
        
                    
        Product::create( [
        'id'=>57,
        'name'=>'Sony a6500 Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>19,
        'price'=>'1449.99',
        'sale_price'=>'1449.99',
        'overview'=>'With incredible 11fps continuous shooting, and a convenient touchscreen LCD display with Touch AF, the Sony a6500 mirrorless camera makes it easier than ever to capture spectacular photos. It features 5-axis in-body image stabilization, and supports 4K video recording. It includes a 16-50mm Power Zoom lens with Optical SteadyShot image stabilization.',
        'more_information'=>'24.2MP APS-C Exmor CMOS sensor with advanced processing reproduces images with accurate colour and captures a high number of frames during continuous shooting',
        'thumbnail'=>'camera5.jpg',
        'created_at'=>'2018-12-10 06:47:19',
        'updated_at'=>'2018-12-10 06:47:19'
        ] );
        
        
                    
        Product::create( [
        'id'=>58,
        'name'=>'Canon EOS 5D Mark IV',
        'category_id'=>5,
        'manufacturer_id'=>18,
        'price'=>'3699.99',
        'sale_price'=>'3699.99',
        'overview'=>'The Canon EOS 5D Mark IV builds on the legacy of Canon\'s 5D series with a range of refinements in image quality, performance, and versatility, including the ability to record video in stunning 4K Ultra HD. The 30.4MP full-frame CMOS sensor and DIGIC 6+ processor combine to deliver incredible high-resolution photos suitable for publication or framing.',
        'more_information'=>'A 30.4MP full-frame CMOS sensor with ISO ranging from 100 to 32000 captures extremely detailed, high-resolution photos with professional-calibre quality and consistency. From first birthdays and family weddings to breaking news stories and major events, you get professional-grade results that will last a lifetime.',
        'thumbnail'=>'camera6.jpg',
        'created_at'=>'2018-12-10 06:50:45',
        'updated_at'=>'2018-12-10 06:50:45'
        ] );
        
        
                    
        Product::create( [
        'id'=>59,
        'name'=>'Nikon D5600',
        'category_id'=>5,
        'manufacturer_id'=>20,
        'price'=>'899.99',
        'sale_price'=>'899.99',
        'overview'=>'Create images that inspire with the Nikon D5600 DSLR camera. It features a 24.2MP DX CMOS sensor with an EXPEED 4 image processor for sharp, beautiful images. Built-in artistic filters let you show off your creative side. Nikon SnapBridge makes sharing images easy. Start photographing out of the box with the included AF-P DX NIKKOR 18-55mm VR lens.',
        'more_information'=>'The 24.2MP DX CMOS sensor was designed without an optical low-pass filter, enabling it to shoot in sharp detail with reduced moire and false colour\r\nThe EXPEED 4 image processor handles difficult lighting situations with ease, boasting a native ISO sensitivity of 100 to 25600 for crisp, detail images with less grain, especially in low light',
        'thumbnail'=>'camera7.jpg',
        'created_at'=>'2018-12-10 07:02:11',
        'updated_at'=>'2018-12-10 07:02:11'
        ] );
        
        
                    
        Product::create( [
        'id'=>60,
        'name'=>'Sony a5100 Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>19,
        'price'=>'529.99',
        'sale_price'=>'529.99',
        'overview'=>'How would you like to have the power and quality of a DSLR in a camera that\'s small enough to fit into your pocket? That\'s exactly what the Sony a5100 24.3MP interchangeable lens camera with 16-50mm lens gives you. It provides DSLR-grade image quality and shooting capabilities in a compact form factor that you can carry everywhere.',
        'more_information'=>'The 24.3MP Exmor R APS-C HD CMOS sensor brings out the beauty in even the simplest things. It delivers incredible detail and low noise, so you can capture the fine texture of a butterfly\'s wings as it rests gracefully on a nearby shrub.',
        'thumbnail'=>'camera8.jpg',
        'created_at'=>'2018-12-10 07:12:39',
        'updated_at'=>'2018-12-10 07:12:39'
        ] );
        
        
                    
        Product::create( [
        'id'=>61,
        'name'=>'FUJIFILM GFX 50S',
        'category_id'=>5,
        'manufacturer_id'=>22,
        'price'=>'8100.00',
        'sale_price'=>'8100.00',
        'overview'=>'New Firmware Ver.3.10 \r\n\r\nThe FUJIFILM GFX 50S medium format mirrorless digital camera combines an extraordinary sensor, processor and design with FUJINON lenses to give you the ultimate photography experience. The innovative GFX system uses a FUJIFILM exclusive 43.8 x 32.9mm, 51.4MP CMOS sensor with approximately 1.7x the area of a 35mm sensor. The GFX 50S combines the heritage of over 80 years of imaging and the innovative award-winning design of our X Series digital camera system in the relentless pursuit of image quality. The GFX 50S 51.4MP sensor shows its true potential when making large format prints, capturing majestic landscape photos or reproducing historical documents.',
        'more_information'=>'The camera is supported by GF lenses that encapsulate the design philosophy of FUJINON lenses, renowned by photographers around the world. Discover a camera that lets you capture the big picture in stunning detail.',
        'thumbnail'=>'camera9.jpg',
        'created_at'=>'2018-12-10 08:54:15',
        'updated_at'=>'2018-12-10 08:54:15'
        ] );
        
        
                    
        Product::create( [
        'id'=>62,
        'name'=>'Sony a6300 Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>19,
        'price'=>'949.99',
        'sale_price'=>'949.99',
        'overview'=>'Capture action like never before with the Sony a6300 mirrorless camera. Boasting lightning-fast autofocus (0.05 seconds), 425 phase-detection wide area AF points, and high-density AF tracking, the a6300 easily captures fast action. In-camera 4K video recording with Super 35mm format captures true-to-life footage with brilliant colour and detail.',
        'more_information'=>'Capturing exceptional images with amazing details, colours, and textures is easy thanks to the 24.2MP Exmor CMOS APS-C image sensor. It features an enhanced design, offering better light collection capabilities and increased readout speeds. And what does that all mean? It means you get stunning images with excellent colour reproduction, and a high number of frames during continuous shooting and smoother in-camera video recording.',
        'thumbnail'=>'camera10.jpg',
        'created_at'=>'2018-12-10 10:04:54',
        'updated_at'=>'2018-12-10 10:04:54'
        ] );
        
        
                    
        Product::create( [
        'id'=>64,
        'name'=>'NIKON D7500 DSLR Camera',
        'category_id'=>5,
        'manufacturer_id'=>20,
        'price'=>'1999.99',
        'sale_price'=>'1999.99',
        'overview'=>'Capture those fleeting and once-in-a-lifetime memories in picture-perfect detail with the Nikon D7500 DSLR camera. It delivers exceptional DX performance with a 20.9MP CMOS sensor and EXPEED 5 image processor for 4K UHD video and up to 8fps continuous shooting. It also includes an AF-S DX 18-140mm f/3.5-5.6 ED VR lens.',
        'more_information'=>'Capture exceptional images with amazing details, colours, and textures thanks to the 20.9MP DX-format CMOS image sensor. This sensor lets you take advantage of the smaller, lighter DX NIKKOR lenses, making this an ideal camera for travel.',
        'thumbnail'=>'camera11.jpg',
        'created_at'=>'2018-12-10 10:10:02',
        'updated_at'=>'2018-12-10 10:10:02'
        ] );
        
        
                    
        Product::create( [
        'id'=>65,
        'name'=>'Sony A6000 Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>19,
        'price'=>'629.99',
        'sale_price'=>'629.99',
        'overview'=>'Capture life at its finest with the Sony a6000 compact system camera with 16-50mm zoom lens. It snaps stunning 24.3MP images with realistic details, and records Full HD video. Built-in WiFi lets you control the camera through your compatible smartphone or tablet, while DSLR functionality lets you shoot like a pro without all the bulk.',
        'more_information'=>'The 24.3MP Exmor APS HD CMOS sensor brings out the beauty in even the simplest things. It delivers incredible detail and low noise, so you can capture not only the fine texture of a butterfly\'s wings as it rests gracefully on a nearby shrub but also the veined surface of the leaf it\'s sitting on.',
        'thumbnail'=>'camera12.jpg',
        'created_at'=>'2018-12-10 10:13:30',
        'updated_at'=>'2018-12-10 10:13:30'
        ] );
        
        
                    
        Product::create( [
        'id'=>66,
        'name'=>'New! Olympus OM-D E-M5 Mark II',
        'category_id'=>5,
        'manufacturer_id'=>21,
        'price'=>'1499.00',
        'sale_price'=>'1499.00',
        'overview'=>'PRODUCT HIGHLIGHTS 16MP Live MOS Sensor TruePic VII Image Processor 2360k-Dot Electronic Viewfinder 3.0" Tilting OLED Monitor Full HD 1080i Video 40MP High Res Shot Built-In WiFi 5-Axis VCM Image Stabilization 10 Frames Per Second Dust and Splashproof Construction BLN-1 Rechargeable Battery Olympus M. Zuiko Digital ED 12-40mm f/2.8 PRO Lens Micro Four Thirds Mount 24-80mm (35mm Equivalent) Filter Thread 62mmDESCRIPTION The Olympus OM-D E-M5 Mark II Mirrorless Micro Four Thirds Digital Camera combines a 16MP Live MOS sensor with a TruePic VII Image Processor for increased performance speed and reduced noise throughout the ISO range as compared to TruePic VI.',
        'more_information'=>'The Mark II also improves upon its predecessor by utilizing a 5-axis VCM Image Stabilization that provides up to 5 stops of compensation. Full HD 1080p video recording is now available on the Mark II with a higher bitrate of up to 77 Mb/s and various codecs. Built into the E-M5 Mark II is a unique feature that utilizes sensor shift to produce a 40MP image by combining 8 images in-camera. The camera\'s fully articulated 3" touchscreen LCD monitor makes taking self portraits easy, and the built-in WiFi lets you quickly share your images. When not using the LDC monitor for live view, the 2,360k-dot EVF provides a virtually lag-free view with a 1.48x magnification. Superior Image Quality The Olympus E-M5 Mark II uses a 16-megapixel High-Speed Live MOS Sensor offering improved performance and exceptional clarity and speed in all aspects of image capture. Olympus\' TruePic VII image-processing engine improves image quality in low-light environments. The camera\'s sensor allows for a maximum ISO of 25,600, and the dynamic range has been expanded for more faithful color reproduction. 5-Axis Image Stabilization The ultra-sensitive 5-axis VCM (voice coil motor) employs updated gyro sensors for a full 5 exposure steps of compensation. The E-M5 Mark II delivers sharp, clear still images in low light or steady HD video when captur',
        'thumbnail'=>'camera15.jpg',
        'created_at'=>'2018-12-10 10:35:16',
        'updated_at'=>'2018-12-10 10:35:16'
        ] );
        
        
                    
        Product::create( [
        'id'=>67,
        'name'=>'Olympus M2 Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>21,
        'price'=>'699.99',
        'sale_price'=>'699.99',
        'overview'=>'Capture every shot perfectly with the Olympus OM-D E-M10 Mark II. This 16.1MP camera with TruPic VII image processor is compact and includes a 14-42mm wide-angle zoom lens. It has 5-axis image stabilization that reduces camera shake and has built-in WiFi for easy sharing. It\'s equipped with in-camera creative features such as 4K Time Lapse and HDR.',
        'more_information'=>'16.1MP 4/3" Live MOS image sensor with TruePic VII image processor delivers stunning images and video\r\nContinuous shooting at 8.5fps and records Full HD video at variable frame rates (60p, 50p, 30p, 25p, and 24p) with in-camera creative effects that can be added while recording',
        'thumbnail'=>'camera14.jpg',
        'created_at'=>'2018-12-10 10:40:05',
        'updated_at'=>'2018-12-10 10:40:05'
        ] );
        
        
                    
        Product::create( [
        'id'=>68,
        'name'=>'Sony a7 II Mirrorless Camera',
        'category_id'=>5,
        'manufacturer_id'=>19,
        'price'=>'1399.99',
        'sale_price'=>'1399.99',
        'overview'=>'Capture images as they were meant to be seen with the Sony a7 II mirrorless camera. Its 24.3MP full-frame Exmor sensor and BIONZ X processor faithfully reproduce colours, textures, and details as seen by the naked eye. Built-in 5-axis image stabilization compensates for 5 different types of camera shake. Includes a FE 28-70mm f/3.5-5.6 OSS lens.',
        'more_information'=>'Equipped with a 35mm full-frame Exmor CMOS sensor, the a7 II captures stunning images with a wide dynamic range and true-to-life colour reproduction in 24.3MP resolution. Play back and admire the lifelike, vibrant colours of the wildflowers in your garden, output in brilliant 4K resolution on your compatible TV.',
        'thumbnail'=>'camera13.jpg',
        'created_at'=>'2018-12-10 10:45:21',
        'updated_at'=>'2018-12-10 10:45:21'
        ] );
        
        
                    
        Product::create( [
        'id'=>69,
        'name'=>'Pentax K-1',
        'category_id'=>5,
        'manufacturer_id'=>38,
        'price'=>'3040.00',
        'sale_price'=>'3040.00',
        'overview'=>'The Pentax K-1 is Ricoh\'s first full-frame DSLR. The camera\'s full-frame CMOS sensor features 36.4 effective megapixels and no AA filter and is stabilized using Ricoh\'s SRII Shake Reduction system. This five-axis shake reduction system is capable of reducing camera shake with a compensation range of up to five shutter steps. SRII also allows the PENTAX K-1 to incorporate other features such as Pixel Shift Resolution, which effectively eliminates moiré without the need for an anti-aliasing filter, increasing sharpness and overall image quality.',
        'more_information'=>'Also new is the SAFOX 12 autofocus mechanism with 33 autofocus points, and a Flexible tilt LCD monitor which can be adjusted to the desired angle horizontally and vertically without deviating from the lens\'s optical axis, making it easy to shoot at challenging angles. Other significant \'firsts\' are the K-1\'s Operation Assist Lights – compact white LEDs that facilitate camera operation in low-light environments, making it easy to change lenses, swap memory cards and adjust back-of-camera controls, and a Smart Function dial that allows photographers to easy select frequently-used functions without going into the LCD menu.',
        'thumbnail'=>'camera17.jpg',
        'created_at'=>'2018-12-10 10:52:37',
        'updated_at'=>'2018-12-10 10:52:37'
        ] );
        
        
                    
        Product::create( [
        'id'=>70,
        'name'=>'Sharp 43" 4K LED Roku Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>37,
        'price'=>'699.99',
        'sale_price'=>'699.99',
        'overview'=>'Enjoy incredible Ultra HD entertainment on a beautiful big screen with this Sharp 43" 4K LED Smart TV. Designed with Roku\'s streaming technology, this TV gives you access to over a thousand streaming channels, plus games, broadcast TV, and more. And it\'s all showcased in stunning 4K picture quality, making your screen\'s content look incredibly lifelike.',
        'more_information'=>'Big 43" display gives you plenty of screen real estate to enjoy blockbuster movies, action-packed video games, and your team scoring the winning goal in overtime',
        'thumbnail'=>'tv1.jpg',
        'created_at'=>'2018-12-10 11:19:27',
        'updated_at'=>'2018-12-10 11:19:27'
        ] );
        
        
                    
        Product::create( [
        'id'=>71,
        'name'=>'Samsung 65" 4K QLED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>5,
        'price'=>'3299.97',
        'sale_price'=>'3299.97',
        'overview'=>'Plunge into the detailed depths of your entertainment with the Samsung 65" 4K Ultra HD QLED Smart TV. The QLED display offers 4 times the resolution of 1080p and projects light in every direction for an extra-wide viewing area. 4K HDR Elite+ and Q Elite Black+ Infinite Array uncover remarkable depth and hidden details to make content better than you\'ve ever seen.',
        'more_information'=>'65-inch, 4K Ultra HD screen with a native resolution of 3840 x 2160 (4 times that of Full HD 1080p) delivers crystal-clear visuals that will pull you into all of your favourite shows and movies with remarkable realism',
        'thumbnail'=>'tv2.jpg',
        'created_at'=>'2018-12-10 11:24:34',
        'updated_at'=>'2018-12-10 11:24:34'
        ] );
        
        
                    
        Product::create( [
        'id'=>72,
        'name'=>'Samsung 43" 4K LED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>5,
        'price'=>'549.99',
        'sale_price'=>'549.99',
        'overview'=>'Your favourite movies, shows, sports, and video games will never be the same. This 43" Samsung 4K HDR Smart TV produces incredible colour, contrast, and clarity, thanks to a 4K Ultra HD resolution, High Dynamic Range technology, and PurColour enhancement technology. The Tizen-powered Smart TV hub gives you instant access to popular apps, such as Netflix and YouTube.',
        'more_information'=>'43-inch 4K Ultra HD display with a native resolution of 3840 x 2160 (4 times that of Full HD 1080p) delivers crystal-clear visuals that will pull you into all of your favourite shows and movies with remarkable realism',
        'thumbnail'=>'tv3.jpg',
        'created_at'=>'2018-12-10 12:00:58',
        'updated_at'=>'2018-12-10 12:00:58'
        ] );
        
        
                    
        Product::create( [
        'id'=>73,
        'name'=>'Samsung 55" 4K LED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>5,
        'price'=>'699.99',
        'sale_price'=>'699.99',
        'overview'=>'Your favourite movies, shows, sports, and video games will never be the same. This 55" Samsung 4K HDR Smart TV produces incredible colour, contrast, and clarity, thanks to a 4K Ultra HD resolution, High Dynamic Range technology, and PurColour enhancement technology. The Tizen-powered Smart TV hub gives you instant access to popular apps, such as Netflix and YouTube.',
        'more_information'=>'55-inch 4K Ultra HD display with a native resolution of 3840 x 2160 (4 times that of Full HD 1080p) delivers crystal-clear visuals that will pull you into all of your favourite shows and movies with remarkable realism',
        'thumbnail'=>'tv4.jpg',
        'created_at'=>'2018-12-10 12:10:13',
        'updated_at'=>'2018-12-10 12:10:13'
        ] );
        
        
                    
        Product::create( [
        'id'=>74,
        'name'=>'Toshiba 55" 4K LED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>36,
        'price'=>'699.99',
        'sale_price'=>'699.99',
        'overview'=>'Toshiba 4K UHD Smart TV is a new generation of television featuring the Fire TV experience built-in and including a Voice Remote with Alexa. With true-to-life 4K Ultra HD picture quality and access to all the movies and TV shows you love, Toshiba delivers a superior TV experience that gets smarter everyday. The Voice Remote with Alexa lets you do everything you\'d expect from a remote--plus, easily launch apps, search for titles, play music, switch inputs, control smart home devices, and more, using just your voice.',
        'more_information'=>'Toshiba 4K UHD Smart TV - Fire TV Edition delivers true-to-life 4K Ultra HD picture quality with over 8 million pixels for stunning clarity, deep contrast, and vivid colours.',
        'thumbnail'=>'tv5.jpg',
        'created_at'=>'2018-12-10 12:20:14',
        'updated_at'=>'2018-12-10 12:20:14'
        ] );
        
        
                    
        Product::create( [
        'id'=>75,
        'name'=>'New! LG 55" Series Smart Tv',
        'category_id'=>1,
        'manufacturer_id'=>34,
        'price'=>'3499.99',
        'sale_price'=>'3499.99',
        'overview'=>'Only OLED\'s revolutionary construction is so thin that your TV design can be as pure as LG OLED Floating Picture on Glass or stylish Picture on Metal. OLED55C8 Compliment any room with incredible technology. OLED pixels create an astonishingly accurate and wide colour range which is presented on the perfect black background only available from self emitting pixels.',
        'more_information'=>'LG OLED TVs with AI (Artificial Intelligence) ThinQ with voice control becomes the hub for your smart devices. | Dolby Vision HDR, as well as HDR10 and HLG support | α9 Intelligent Processor. LG\'s most powerful processor ever provides true-to-life images with incredibly rich colors, sharpness and depth to deliver the ultimate picture quality.',
        'thumbnail'=>'tv6.jpg',
        'created_at'=>'2018-12-10 21:27:13',
        'updated_at'=>'2018-12-13 03:32:01'
        ] );
        
        
                    
        Product::create( [
        'id'=>76,
        'name'=>'Sony Master Series 65" 4K',
        'category_id'=>1,
        'manufacturer_id'=>19,
        'price'=>'5999.99',
        'sale_price'=>'5999.99',
        'overview'=>'Create the ultimate home entertainment area with the Sony Master Series 65" 4K Ultra HD HDR Android OS Smart TV. It features an OLED display bursting with over 8 million self-illuminating pixels for exceptional black and colour. Built-in 3.2 channel speakers deliver exception sound that fully draws you in to your movies, TV shows, gaming, and more.',
        'more_information'=>'65" OLED screen features 4K Ultra HD with 3840 x 2160 native resolution for a crisp and lifelike picture that visually transports you into your favourite TV shows, movies, video games, and more',
        'thumbnail'=>'tv7.jpg',
        'created_at'=>'2018-12-10 21:36:16',
        'updated_at'=>'2018-12-10 21:36:16'
        ] );
        
        
                    
        Product::create( [
        'id'=>77,
        'name'=>'LG Signature W7 65" 4K',
        'category_id'=>1,
        'manufacturer_id'=>34,
        'price'=>'10999.97',
        'sale_price'=>'10999.97',
        'overview'=>'Open your eyes to a world of endless entertainment made spectacular with the 65" LG Signature W7 OLED TV. 4K resolution and OLED pixels deliver unprecedented contrast, colour, and clarity to scintillate your senses. Its stunningly thin Picture-on-Wall design offers a profile that practically disappears into the wall.',
        'more_information'=>'Stunning 4K UHD resolution is 4x that of Full HD, so you see amazingly vivid colours and picture details that\'ll blow you away',
        'thumbnail'=>'tv8.jpg',
        'created_at'=>'2018-12-10 21:43:22',
        'updated_at'=>'2018-12-10 21:43:22'
        ] );
        
        
                    
        Product::create( [
        'id'=>78,
        'name'=>'Sony 75" 4K',
        'category_id'=>1,
        'manufacturer_id'=>19,
        'price'=>'3499.99',
        'sale_price'=>'3499.99',
        'overview'=>'Bring your entertainment to life with the Sony X900F 4K HDR TV. It boasts a 4K Ultra HD resolution and a TRILUMINOS display, plus cutting-edge technology like the X1 Extreme Processor 4K HDR super bit mapping. This television offers blur-free motion, brilliant colours, and a dynamic range. Google Assistant and Android TV open up a world of possibilities.',
        'more_information'=>'75-inch 4K Ultra HD display with a stunning 3840 x 2160 resolution brings movies, games, TV shows, and sporting events to vivid life',
        'thumbnail'=>'tv9.jpg',
        'created_at'=>'2018-12-10 22:00:19',
        'updated_at'=>'2018-12-10 22:00:19'
        ] );
        
        
                    
        Product::create( [
        'id'=>79,
        'name'=>'Samsung 65" 4K LED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>5,
        'price'=>'999.99',
        'sale_price'=>'999.99',
        'overview'=>'Enjoy home entertainment like never before with the Samsung 65" 4K UHD LED Smart TV. HDR and PurColour technology bring out the smallest details and deliver an amazing array of colours for exceptional, lifelike clarity that immerses you in every scene. The Tizen operating system allows you to explore the expansive world of digital streaming.',
        'more_information'=>'65" screen features 4K Ultra HD with 3840 x 2160 native resolution (4 times the resolution of traditional 1080p HD) for a picture so crisp and lifelike, you\'ll always feel like you are part of the action',
        'thumbnail'=>'tv10.jpg',
        'created_at'=>'2018-12-10 22:14:05',
        'updated_at'=>'2018-12-10 22:14:05'
        ] );
        
        
                    
        Product::create( [
        'id'=>80,
        'name'=>'VIZIO SMARTCAST™ 65” ULTRA HD',
        'category_id'=>1,
        'manufacturer_id'=>35,
        'price'=>'1079.99',
        'sale_price'=>'1079.99',
        'overview'=>'VIntroducing the all-new VIZIO SmartCast P-Series Ultra HD HDR XLED Pro Display. Powered by XLED Pro, VIZIOs most powerful full-array local dimming backlight technology, the 2017 P-Series pushes the boundaries of high dynamic range, dramatically expanding the range of contrast with more detail, depth and color, than ever before*. With Dolby Vision and HDR10 content support, XHDR Pro features ultra-high brightness performance to dramatically expand the contrast range, revealing a world of new detail in the shadows and highlights. Ultra Color Spectrum offers over 1 billion colors to produce richer, more accurate color detail. Xtreme Black Engine Pro enables precise backlight control using 128 local dimming zones to adapt brightness to the on-screen image to achieve extremely deep black levels. And 4K Ultra HD creates breathtaking, lifelike detail.',
        'more_information'=>'XHDR Pro: Ultimate HDR performance with Dolby Vision and HDR10 content support. With enriched brightness, enjoy a dramatically expanded contrast range with more color, contrast, depth and detail than ever before*.',
        'thumbnail'=>'tv12.jpg',
        'created_at'=>'2018-12-10 22:19:57',
        'updated_at'=>'2018-12-10 22:19:57'
        ] );
        
        
                    
        Product::create( [
        'id'=>81,
        'name'=>'Samsung 49" 4K QLED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>5,
        'price'=>'1099.99',
        'sale_price'=>'1099.99',
        'overview'=>'Feast your eyes on the exceptional Samsung 40" Q6F QLED Smart TV. Boasting ground-breaking Quantum dot technology, this Smart TV delivers over a billion shades of colour, from rich, saturated hues to the brightest whites and the deepest blacks for the ultimate viewing experience.',
        'more_information'=>'Huge 49" screen with a 4K Ultra HD (3840 x 2160) delivers truly immersive entertainment\r\nHDR Elite offers improved contrast and luminosity for unbelievably stunning images\r\nHDR10+ adjusts brightness frame-by-frame for full colour range and exceptional detail in every shot\r\nMotion Rate 240 motion enhancement ensures that you follow the action without lags or ghosting, even during the most fast-paced scenes',
        'thumbnail'=>'tv13.jpg',
        'created_at'=>'2018-12-10 22:24:18',
        'updated_at'=>'2018-12-10 22:24:18'
        ] );
        
        
                    
        Product::create( [
        'id'=>82,
        'name'=>'Samsung The Frame 65" Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>5,
        'price'=>'2499.99',
        'sale_price'=>'2499.99',
        'overview'=>'A sensational fusion of technology and beauty, Samsung\'s The Frame exhibits stunning visuals at all times. This supremely elegant, wall-mountable smart TV is as slender as a picture frame. It puts incredible 4K HDR content at your fingertips, and when you switch off, Art Mode displays the finest works of renowned artists.',
        'more_information'=>'Interchangable magnetic frames in walnut, beige, and white, frame the television like a work of art (frames sold separately)\r\n    The Frame comes with the Samsung Collection -- a collection of preloaded masterpieces that that are displayed in vibrant colour; additional artwork is available in the Art Store\r\n    Upload your own photos to The Frame to prominently show off your favourite family pictures and memories',
        'thumbnail'=>'tv14.jpg',
        'created_at'=>'2018-12-10 22:29:06',
        'updated_at'=>'2018-12-13 03:51:51'
        ] );
        
        
                    
        Product::create( [
        'id'=>83,
        'name'=>'LG 65" 4K UHD HDR LED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>34,
        'price'=>'1199.99',
        'sale_price'=>'1199.99',
        'overview'=>'There are more than 8.2 million reasons why this LG 65" 4K UHD webOS 4.0 Smart TV belongs in your home. It boasts 4 times the resolution of Full HD to deliver breathtaking clarity and fine picture details. And it\'s not just a pretty face. LG\'s voice-controlled webOS 4.0 offers endless entertainment options, from Bluetooth streaming to web browsing.',
        'more_information'=>'4K Ultra HD resolution provides 8.2 million pixels that deliver fine picture details to amaze your senses\r\nHDR helps the TV produce billions of colours, so what you see on screen is what you\'re meant to see',
        'thumbnail'=>'tv15.jpg',
        'created_at'=>'2018-12-10 22:34:22',
        'updated_at'=>'2018-12-13 03:52:26'
        ] );
        
        
                    
        Product::create( [
        'id'=>84,
        'name'=>'LG 55" 4K OLED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>34,
        'price'=>'2299.99',
        'sale_price'=>'2299.99',
        'overview'=>'This LG UHD OLED 55" TV makes it crystal clear that the only way is 4K. Tell the voice-controlled webOS 4.0 what you want to watch, and 8.2 million individually lit pixels deliver breathtaking brightness and clarity to your movies and more. Advanced HDR helps create a truly cinematic experience, and Dolby Atmos technology brings big-screen surround sound.',
        'more_information'=>'4K Ultra HD resolution provides 8.2 million pixels that deliver fine picture details to amaze your senses',
        'thumbnail'=>'tv16.jpg',
        'created_at'=>'2018-12-10 22:41:28',
        'updated_at'=>'2018-12-10 22:41:28'
        ] );
        
        
                    
        Product::create( [
        'id'=>85,
        'name'=>'Sony 65" 4K OLED Smart TV',
        'category_id'=>1,
        'manufacturer_id'=>19,
        'price'=>'3299.97',
        'sale_price'=>'3299.97',
        'overview'=>'Absorb every detail with the Sony Bravia OLED Smart TV. It combines 4K Ultra HD resolution and 8 million self-illuminating pixels to deliver an exquisite symphony of detail, depth, contrast, and colour. With Acoustic Surface technology, audio comes from the entire screen of this TV to ensure you receive picture and sound in perfect harmony.',
        'more_information'=>'65-inch 4K Ultra HD display features a jaw-dropping 3840 x 2160 resolution that lets you savour every crisp detail when watching your favourite action flicks or sporting events',
        'thumbnail'=>'tv17.jpg',
        'created_at'=>'2018-12-10 22:47:16',
        'updated_at'=>'2018-12-10 22:47:16'
        ] );
        
        
                    
        Product::create( [
        'id'=>86,
        'name'=>'Ultimate Ears Wireless Speaker',
        'category_id'=>6,
        'manufacturer_id'=>24,
        'price'=>'99.99',
        'sale_price'=>'99.99',
        'overview'=>'Wherever you go, keep the party going with the UE BOOM 2 portable Bluetooth wireless speaker. It pumps out incredible sound with deep bass in all directions thanks to its 360-degree design. With a 100ft. range and 15-hour battery life, this speaker makes it easy to share your favourite tunes on the trail, at the beach, or just about anywhere.',
        'more_information'=>'NFC technology allows you to pair your NFC-compatible device with a simple tap. Still use an iPod or MP3 player? Use the 3.5mm audio output to connect music players without Bluetooth or NFC',
        'thumbnail'=>'sound1.jpg',
        'created_at'=>'2018-12-10 23:05:37',
        'updated_at'=>'2018-12-13 03:34:57'
        ] );
        
        
                    
        Product::create( [
        'id'=>87,
        'name'=>'JBL XTREME Splashproof Speaker',
        'category_id'=>6,
        'manufacturer_id'=>25,
        'price'=>'229.99',
        'sale_price'=>'229.99',
        'overview'=>'Dance to the beat indoors and out with the JBL Xtreme portable Bluetooth speaker. Wirelessly connect up to 3 smartphones or tablets to enjoy powerful stereo sound via 4 active transducers and dual external passive radiators. The splashproof design makes it perfect for outdoor use, while the reliable battery delivers up to 15 hours of playtime.',
        'more_information'=>'Bluetooth technology lets you wirelessly connect up to 3 smartphones, tablets, or other Bluetooth-enabled devices so you and your friends can take turns streaming classic rock hits, upbeat dance tunes, or the latest pop tracks.',
        'thumbnail'=>'sound2.jpg',
        'created_at'=>'2018-12-10 23:08:44',
        'updated_at'=>'2018-12-13 03:36:01'
        ] );
        
        
                    
        Product::create( [
        'id'=>88,
        'name'=>'Sonos Beam Sound Bar',
        'category_id'=>6,
        'manufacturer_id'=>27,
        'price'=>'499.99',
        'sale_price'=>'499.99',
        'overview'=>'Take home audio to the next level with the Sonos Beam smart sound bar. Equipped with 5 far-field microphones and Amazon Alexa voice control, this sound bar can hear your voice from anywhere in the room, even when your favourite song is blasting. Full-range woofers, a tweeter, and passive radiators deliver balanced sound to elevate your home entertainment experience.',
        'more_information'=>'Four elliptical full-range woofers and one tweeter harmonize low and high frequencies for a full spectrum of sound',
        'thumbnail'=>'sound3.jpg',
        'created_at'=>'2018-12-10 23:14:18',
        'updated_at'=>'2018-12-10 23:14:18'
        ] );
        
        
                    
        Product::create( [
        'id'=>89,
        'name'=>'Bose Solo 5 TV Sound System',
        'category_id'=>6,
        'manufacturer_id'=>38,
        'price'=>'239.99',
        'sale_price'=>'239.99',
        'overview'=>'Get more than your flat-screen TV\'s tiny speakers have to offer with the Bose Solo 5 TV sound system. This single-piece soundbar connects easily to your TV with one cable, and produces noticeably better sound, with a Dialogue mode that makes every word even easier to hear. Bluetooth connectivity lets you stream music wirelessly from a compatible device.',
        'more_information'=>'One-piece soundbar delivers dramatically better sound than your TV\r\n    Bluetooth connectivity is built in so you can stream music wirelessly\r\n    Dialogue mode makes every word even easier to hear\r\n    Setup is easy with only one connection to your TV\r\n    Place the unit almost anywhere: in front of the TV, on a shelf, or mounted on the wall\r\n    Includes universal remote for seamless control of many devices',
        'thumbnail'=>'sound4.jpg',
        'created_at'=>'2018-12-10 23:19:22',
        'updated_at'=>'2018-12-10 23:19:22'
        ] );
        
        
                    
        Product::create( [
        'id'=>90,
        'name'=>'Marshall Kilburn Wireless Speaker',
        'category_id'=>6,
        'manufacturer_id'=>28,
        'price'=>'399.99',
        'sale_price'=>'399.99',
        'overview'=>'Conveniently compact and unmistakably Marshall, the Kilburn portable Bluetooth wireless speaker produces remarkable sound and features an amp-inspired design complete with the iconic logo. It has a guitar-inspired leather strap and analog knobs to control the volume, bass, and treble. Lightweight at just 3kg, it offers up to 20 hours of battery life.',
        'more_information'=>'Bluetooth connectivity lets you stream audio wirelessly from your smartphone, tablet, PC, or other Bluetooth-enabled audio device\r\n    Guitar-amp inspired vintage-style design features the iconic Marshall logo\r\n    A toggle switch and analog knobs for on/off, volume, and tone control\r\n    Guitar-inspired leather strap for stylish portability\r\n    Built-in rechargeable battery offers up to 20 hours of playing time\r\n    4" woofer and two 3/4" dome tweeters produce well-balanced audio with midrange clarity and articulate highs\r\n    3.5mm stereo input lets you play music from a wide range of audio devices\r\n    Lightweight and portable; weighs just 3kg',
        'thumbnail'=>'sound5.jpg',
        'created_at'=>'2018-12-10 23:27:06',
        'updated_at'=>'2018-12-10 23:27:06'
        ] );
        
        
                    
        Product::create( [
        'id'=>91,
        'name'=>'Bose SoundLink Revolv',
        'category_id'=>6,
        'manufacturer_id'=>38,
        'price'=>'219.99',
        'sale_price'=>'219.99',
        'overview'=>'Deep. Loud. And immersive, too. The SoundLink Revolve Bluetooth speaker delivers true 360° sound for consistent, uniform coverage. Place it in the centre of the room to give everyone the same experience. Or set it near a wall so sound radiates and reflects. Durable and water-resistant, it features a rechargeable battery that plays up to 12 hours.',
        'more_information'=>'Engineered to deliver deep, loud, immersive sound from a small speaker\r\nTrue 360-degree sound for consistent, uniform coverage\r\nSeamless aluminum body is durable and water-resistant (IPX4)\r\nEnjoy up to 12 hours of play time from a rechargeable, lithium-ion battery\r\nWireless Bluetooth pairing with voice prompts',
        'thumbnail'=>'sound6.jpg',
        'created_at'=>'2018-12-10 23:39:39',
        'updated_at'=>'2018-12-10 23:39:39'
        ] );
        
        
                    
        Product::create( [
        'id'=>92,
        'name'=>'Samsung 160-Watt Wireless Speaker',
        'category_id'=>6,
        'manufacturer_id'=>5,
        'price'=>'199.99',
        'sale_price'=>'199.99',
        'overview'=>'Transform your compatible Samsung sound bar into a surround sound system with the Samsung SWA-8500S wireless rear speaker kit. It comes with 2 rear speakers and a wireless receiver that add 160W of power to your system for an immersive surround sound experience. Sound bar sold separately.',
        'more_information'=>'Kit includes a wireless receiver and 2 rear speakers\r\nIncluded wall mounts make it easy to integrate into a home theatre setup in which the TV is also wall mounted\r\nCompatible with the following Samsung sound bars (not included): HW-M360, HW-M450, HW-M550, HW-M4500\r\nElevates your home theatre experience with 160W of extra power and surround sound functionality',
        'thumbnail'=>'sound7.jpg',
        'created_at'=>'2018-12-10 23:43:34',
        'updated_at'=>'2018-12-10 23:43:34'
        ] );
        
        
                    
        Product::create( [
        'id'=>93,
        'name'=>'Apple HomePod',
        'category_id'=>6,
        'manufacturer_id'=>8,
        'price'=>'449.99',
        'sale_price'=>'449.99',
        'overview'=>'HomePod is a powerful speaker that sounds amazing and adapts to wherever it\'s playing. It\'s the ultimate music authority, bringing together Apple Music and Siri to learn your taste in music. It\'s also an intelligent home assistant, capable of handling everyday tasks — and controlling your smart home. HomePod takes the listening experience to a whole new level. And that\'s just the beginning.',
        'more_information'=>'Breakthrough speaker with amazing sound\r\nSpatial awareness that senses its location\r\nBuilt to bring out the best in Apple Music\r\nLearns what you like based on what you play\r\nIntelligent assistant helps with everyday tasks\r\nControls your smart home accessories',
        'thumbnail'=>'sound8.jpg',
        'created_at'=>'2018-12-10 23:50:53',
        'updated_at'=>'2018-12-10 23:50:53'
        ] );
        
        
                    
        Product::create( [
        'id'=>94,
        'name'=>'Hom Get Together Speaker',
        'category_id'=>6,
        'manufacturer_id'=>29,
        'price'=>'169.99',
        'sale_price'=>'169.99',
        'overview'=>'Wirelessly stream music with the Get Together Bluetooth Portable Audio System. The built-in rechargeable battery, Bluetooth, and convenient 3.5 millimeter input make it simple and easy to connect hundreds of devices and fire up your tunes whenever and wherever you need them. The exclusive REWIND fabric cover and natural bamboo front and back panels are beautiful and sustainable.',
        'more_information'=>'Quick pairing to any Bluetooth device, with the option to auto reconnect to the last Bluetooth device used\r\n    Exclusive REWIND fabric covering, and bamboo front baffle and rear trim\r\n    Controls: Power, Volume, Bluetooth pairing/disconnect. Bamboo faceplate\r\n    3.5mm stereo audio input for non-Bluetooth enabled audio devices\r\n    Powered by internal 8-hour lithium ion battery so you can bring your music on the go',
        'thumbnail'=>'sound9.jpg',
        'created_at'=>'2018-12-10 23:56:42',
        'updated_at'=>'2018-12-10 23:56:42'
        ] );
        
        
                    
        Product::create( [
        'id'=>95,
        'name'=>'Harman Onyx S5 Speaker',
        'category_id'=>6,
        'manufacturer_id'=>30,
        'price'=>'579.99',
        'sale_price'=>'579.99',
        'overview'=>'Signature style and premium sound come together in the Harman Kardon Onyx Studio 5 portable Bluetooth speaker. Its round silhouette commands attention in any space, while an aluminum handle makes it a cinch to take the speaker anywhere. Enjoy full-bodied sound for up to 8 hours on end, and connect up to two devices at once to take turns playing tunes.',
        'more_information'=>'Bluetooth 4.2 enables wireless streaming for room-filling sound\r\nSpeakerphone lets you take hands-free calls\r\nRechargeable battery provides up to 8 hours of playtime, so you can keep the party going all night long\r\n50W total system power and 50Hz – 20kHz frequency response for crisp sound\r\nConnect up to 2 smart devices at the same time and take turns playing DJ\r\nWirelessly connect 2 Harman Kardon Onyx Studio 5 speakers for an elevated audio experience (additional speaker sold separately)',
        'thumbnail'=>'sound10.jpg',
        'created_at'=>'2018-12-11 00:01:01',
        'updated_at'=>'2018-12-11 00:01:01'
        ] );
        
        
                    
        Product::create( [
        'id'=>96,
        'name'=>'Klipsch Bookshelf Speaker',
        'category_id'=>6,
        'manufacturer_id'=>26,
        'price'=>'379.95',
        'sale_price'=>'379.95',
        'overview'=>'The Klipsch Reference R-14M bookshelf speaker can fill small to medium rooms with powerful, lifelike performances. Its highly efficient design produces more output using less energy, and can play louder with less distortion. It features a 1" aluminum linear travel suspension, horn-loaded tweeter and a 4" copper-spun, high-output IMG woofer.',
        'more_information'=>'Highly efficient design produces more output using less energy\r\nAccurate, non-fatiguing sound provides hours of listening pleasure\r\nPlays louder with less distortion\r\nPerforms beautifully as a left, centre, right, or surround speaker\r\n1" aluminum linear travel suspension horn-loaded tweeter\r\n4" copper-spun high-output IMG woofer',
        'thumbnail'=>'sound11.jpg',
        'created_at'=>'2018-12-11 00:10:33',
        'updated_at'=>'2018-12-11 00:10:33'
        ] );
        
        
                    
        Product::create( [
        'id'=>97,
        'name'=>'JBL Pulse 3 Wireless Speaker',
        'category_id'=>6,
        'manufacturer_id'=>25,
        'price'=>'269.99',
        'sale_price'=>'269.99',
        'overview'=>'Increase the beats with the JBL Pulse 3 portable Bluetooth speaker. It pumps out 360-degree sound with an accompanying LED light show that you can customize via the JBL Connect app. Pair with up to 2 smartphones and connect up to 100 JBL Connect+ enabled speakers. Waterproof for versatile use, it offers up to 12 hours of playtime.',
        'more_information'=>'Bluetooth technology lets you wirelessly connect up to 2 smartphones, tablets, or other Bluetooth-enabled devices so you and your friends can take turns streaming classic rock hits, upbeat dance tunes, or the latest pop tracks.',
        'thumbnail'=>'sound12.jpg',
        'created_at'=>'2018-12-11 00:16:16',
        'updated_at'=>'2018-12-11 00:16:16'
        ] );
        
        
                    
        Product::create( [
        'id'=>98,
        'name'=>'Bang & Olufsen Beoplay Speaker',
        'category_id'=>6,
        'manufacturer_id'=>31,
        'price'=>'249.00',
        'sale_price'=>'249.00',
        'overview'=>'With A1 portable Bluetooth Speaker, music lovers can now hold decades of Bang & Olufsen sound technology and experience in the palm of their hand. This ultra-portable wireless speaker delivers crisp ambient sound in a compact 600 g (1.3 lbs) package. Grab it and go wherever life takes you; it charges in approximately 2.5 hours and provides up to 24 hours of battery life per charge. Created by award-winning designer Cecilie Manz, this mini audio powerhouse looks as good as it sounds: a smooth dust- and splash-resistant aluminum dome, double-moulded polymer base and rugged leather strap balance cool sophistication with warm accents for a unique tactile experience. The convenient Connect button allows you to pick up where you left off, activating your last played music at launch. Users can also take advantage of intuitive ToneTouch technology via the Beoplay app to personalize their listening experience, set up wireless stereo pairing and update products with the latest software. Not just a music speaker, the A1 also lets you make calls on the go with a multi-directional microphone that increases voice recognition for everyone on the call. Lightweight but powerful, the sleek A1 delivers signature Bang & Olufsen sound quality when and where you want it.¬',
        'more_information'=>'With a Beoplay A1 portable Bluetooth speaker, music lovers can now hold decades of Bang & Olufsen sound technology and experience in the palm of their hand. This ultra-portable wireless speaker delivers crisp ambient sound in a compact 1.3 lb package. Grab it and go wherever life takes you; it charges in approximately 2.5 hours and provides up to 24 hours of battery life per charge. Created by award-winning designer Cecilie Manz, this mini audio powerhouse looks as good as it sounds: a smooth dust- and splash-resistant aluminum dome, double-molded polymer base and rugged leather strap balance cool sophistication with warm accents for a unique tactile experience. Inspired by Nordic nature, the chic black coloring isn\'t harsh, but rich and deep with the coolness of slate, making it an elegant companion for your fall and winter escapes. The convenient connect button allows you to pick up where you left off, activating your last played music at launch. Users can also take advantage of intuitive ToneTouch technology via the Beoplay app to personalize their listening experience, set up wireless stereo pairing and update products with the latest software. Not just a music speaker, the A1 also lets you make calls on the go with a multi-directional microphone that increases voice recognition for everyone on the call. Lightweight but powerful, the sleek A1 delivers signature Bang & Olufsen sound quality when and where you want it.',
        'thumbnail'=>'sound13.jpg',
        'created_at'=>'2018-12-11 00:20:11',
        'updated_at'=>'2018-12-13 03:37:31'
        ] );
        
        
                    
        Product::create( [
        'id'=>99,
        'name'=>'Bowers & Wilkins Zeppelin Speaker',
        'category_id'=>6,
        'manufacturer_id'=>32,
        'price'=>'799.99',
        'sale_price'=>'799.99',
        'overview'=>'The first Zeppelin changed the game for speaker dock sound quality and design. Zeppelin Air raised the bar again with the addition of Airplay. Now we\'ve taken our biggest leap forward yet. Using scientific advances and the latest analytical techniques, we\'ve been able to improve almost every element of the system, introducing technologies never seen before in any speaker in its class, including Spotify Connect, Bluetooth aptX and AirPlay. The result isn\'t just a new Zeppelin. It\'s a whole new standard for sound quality in wireless home audio. As with any wireless network product, performance will be dependent on the home network equipment, how that equipment is configured, and the wireless environment your devices are operating in. The bandwidth requirements of transmitting high-quality audio mixed with other data (downloading, streaming etc.) may cause instabilities, which are out of Bowers & Wilkins control. Furthermore, temporary drops in audio may be caused by the positioning of your network equipment and devices, or interference from a device that operates on the same wireless frequency. In the first instance, try closing down all open applications/programs on the device you are streaming from (iPhone, iPod touch, iPad, Mac or PC), and turn it completely off and then on again. Ensure that both your Wireless Music System and streaming device have the latest versions of firmware/software installed. The firmware for your Wireless Music System can be checked and updated using the Bowers & Wilkins Control app. It may be necessary to restart your equipment to resolve this issue. If safe to do so turn your network router completely off and then on again (IMPORTANT: Switching your network router off may cause interruptions to download\'s or digitally recorded television). Consider the positioning of the Wireless Music System, network router and the devices you are wanting to stream from: The distances between the Wireless Music System, network router and the audio streaming devices (iPhone, iPod touch, iPad, Mac/PC running iTunes), should all be within recommended operating ranges. Generally speaking, the closer they are positioned together the better, minimizing the distance the wireless signal needs to travel. If possible, try to limit the number of walls, ceilings and other obstacles between each item. It is often advisable to place the network router in a central location within the home, to ensure that the wireless signal produced by the router is evenly distributed throughout your environment. Positioning a wireless network router in out of the way places (such as in a cupboard or under furniture) may reduce its range. Try moving the router out into the open to see if this helps. Wireless Music Systems operate on the same radio frequency (2.4GHz) used by some microwave ovens and cordless telephones, and if used simultaneously this could lead to interruptions in audio playback. The wireless signal can also be interrupted if you have a wireless alarm system installed. If plausible, placing the network router and Wireless Music System away from such causes of wireless interference can improve performance. If you have Bluetooth enabled on the device you are streaming from, try turning this off to see if this helps.',
        'more_information'=>'Zeppelin Wireless is the same iconic design of the original Zeppelin, but features dramatically improved acoustics and functionality, making it the best one-piece audio system available\r\nAll-new drive units featuring the latest audiophile technologies: 2 double dome tweeters, two midrange drivers with FST technology and a 6.5-Inch subwoofer for deep bass to be played at an impressive volume.\r\nNo matter what devise you use with Zeppelin Wireless, the combination of AirPlay, Bluetooth aptX and Spotify Connect means pristine sound is only a couple of button presses away.',
        'thumbnail'=>'sound14.jpg',
        'created_at'=>'2018-12-11 03:06:45',
        'updated_at'=>'2018-12-11 03:06:45'
        ] );
        
        
                    
        Product::create( [
        'id'=>100,
        'name'=>'Sony SA-Z9R Bookshelf Speaker',
        'category_id'=>6,
        'manufacturer_id'=>19,
        'price'=>'399.99',
        'sale_price'=>'399.99',
        'overview'=>'Made for use with the HT-Z9F sound bar, the Sony SA-Z9R 100-Watt bookshelf speaker takes your audio experience to the next level. These wall-mountable speakers add you to experience cinematic surround sound in the comfort of your own living room. A direct wireless connection with the Sony HT-Z9F soundbar makes setup quick and easy.',
        'more_information'=>'Dedicated wireless rear speakers for use with the HT-Z9F sound bar (sold separately) add a new dimension to your listening experience\r\nDirect wireless connection with the HT-Z9F sound bar for quick and easy setup\r\nWall mount capable to free up space on your shelves',
        'thumbnail'=>'sound15.jpg',
        'created_at'=>'2018-12-11 03:10:08',
        'updated_at'=>'2018-12-11 03:10:08'
        ] );
        
        
                    
        Product::create( [
        'id'=>101,
        'name'=>'Amazon Echo with Alexa',
        'category_id'=>6,
        'manufacturer_id'=>33,
        'price'=>'159.99',
        'sale_price'=>'159.99',
        'overview'=>'Your smart home never sounded so good. Say hello to the redesigned Amazon Echo Plus. Equipped with premium speakers powered by Dolby 360-degree audio, the Plus pumps out rich room-filling sound that\'s perfect for audiophiles. It also features a built-in Zigbee smart home hub and temperature sensor, so you can easily connect and control a huge array of smart home devices using only your voice.',
        'more_information'=>'Room-filling sound powered by Dolby: Equipped with premium speakers, the Echo Plus can play immersive Dolby 360-degree audio with crisp vocals and dynamic bass response. You can also use your voice to adjust the equalizer settings.\r\nYour personal assistant: Echo Plus connects to Alexa -- a cloud-based voice service -- that lets you play music, make calls, set alarms, ask questions, check your calendar, manage your shopping list, control smart devices, and more using only your voice.\r\nAt your beck and call: When you want to use Echo Plus, simply say "Alexa," and specially engineered microphones hear and recognize you from across the room, from any direction -- even while music\'s playing.',
        'thumbnail'=>'sound16.jpg',
        'created_at'=>'2018-12-11 03:20:03',
        'updated_at'=>'2018-12-11 03:20:03'
        ] );
        
        
                    
        Product::create( [
        'id'=>102,
        'name'=>'Sonos PLAY:1 Wireless Speaker',
        'category_id'=>6,
        'manufacturer_id'=>27,
        'price'=>'199.99',
        'sale_price'=>'199.99',
        'overview'=>'The Sonos PLAY:1 is a compact wireless speaker that delivers deep, crystal-clear hi-fi sound. Its sleek, versatile design easily fits anywhere in your home that needs sound. It features 2 Class-D amplifiers and 2 custom-built drivers for pitch-perfect music. Use the Sonos app to play your favourite music services or internet radio.',
        'more_information'=>'The Sonos PLAY:1 features 2 Class-D amplifiers and 2 custom-built drivers, which allow it to pump out crystal-clear hi-fi sound at any volume\r\nThe all-digital sound architecture reproduces sound precisely, so you hear what the artist intended\r\nThe Truepla',
        'thumbnail'=>'sound17.jpg',
        'created_at'=>'2018-12-11 03:25:27',
        'updated_at'=>'2018-12-11 03:25:27'
        ] );
        
        
    }
}

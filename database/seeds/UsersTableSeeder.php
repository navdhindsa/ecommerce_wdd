<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        	[
        		'first_name' => 'Binory',
        		'last_name' => 'Admin',
        		'email' => 'admin@binory.com',
                'phone' => '',
                'password' => bcrypt('Bin0ry'),
                'is_admin' => true,
        	]);
        DB::table('users')->insert(
        	[
        		'first_name' => 'Binory',
        		'last_name' => 'User',
        		'email' => 'user@binory.com',
                'phone' => '',
                'password' => bcrypt('Bin0ry'),
        	]);
    }
}

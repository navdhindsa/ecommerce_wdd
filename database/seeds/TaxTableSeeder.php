<?php

use Illuminate\Database\Seeder;

class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Alberta',
                'short_code' => 'AB',
                'rate' => 5
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'British Columbia',
                'short_code' => 'BC',
                'rate' => 12
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Manitoba',
                'short_code' => 'MB',
                'rate' => 13
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'New Brunswick',
                'short_code' => 'NB',
                'rate' => 15
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Newfoundland and Labrador',
                'short_code' => 'NL',
                'rate' => 15
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Nova Scotia',
                'short_code' => 'NS',
                'rate' => 5
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Northwest Territories',
                'short_code' => 'NT',
                'rate' => 15
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Nunavut',
                'short_code' => 'NU',
                'rate' => 5
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Ontario',
                'short_code' => 'ON',
                'rate' => 13
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Prince Edward Island',
                'short_code' => 'PE',
                'rate' => 15
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Quebec',
                'short_code' => 'QC',
                'rate' => 14.975
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Saskatchewan',
                'short_code' => 'SK',
                'rate' => 11
            ]);
        DB::table('province_taxes')->insert(
            [
                'province_name' => 'Yukon',
                'short_code' => 'YK',
                'rate' => 5
            ]);
    }
}

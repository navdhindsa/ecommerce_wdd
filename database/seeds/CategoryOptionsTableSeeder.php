<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoryOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		//category options for TV
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Screen Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Display Technology', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
	        'name'=>'HDR', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Motion Enhancement Tech', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Panel Shape', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Audio Enhancement', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Speaker Configuration', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Speaker output power', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'HDMI Inputs', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'USB Media Port', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Bluetooth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Wifi', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Wired', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'TV OS', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Cabinet Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Wallmount', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
	        'name'=>'Width', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Height',
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Warranty', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'1',
		    'name'=>'Model No', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);

		//category options for Laptop

		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Screen Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Touchscreen Display', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Processor Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Processor Cores', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Speed', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Processor cache', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'SSD Capacity', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Ram Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Ram Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Graphics Card', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Card Reader', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'USB Ports', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'WiFi', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Bluetooth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Battery Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'No. of cells', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Battery Capacity', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Preloaded OS', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'OS Language', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Webcam', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Keyboard Language', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Backlit Keyboard', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Dimensions(in)', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Dimensions(cm)', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Warranty Labour', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'2',
		    'name'=>'Warranty Parts', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);

		//Seeder for Cell Phones

		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Bluetooth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Dual Sim Card', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Built-in Memory', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'OS', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Battery Talktime', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Battery Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Battery Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Supported Networks', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Wifi', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'NFC', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Mobile Pay', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Wireless Charging', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Cable Connection Types', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Headphone Jack', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Front Camera Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Rear Camera Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Front Video resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Rear Video Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Display Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Display Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Fingerprint Sensor', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Depth Sensing', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Water Resistent', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Dimensions (in)', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Dimensions (cm)', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Warranty Labour', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'3',
		    'name'=>'Warranty Parts', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		
		//Seeder for Tablets

		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Display type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Screen Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Screen Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Touchscreen', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Built in Storage', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Storage Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Front Camera Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Rear Camera Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Front Video Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Rear Video Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'WiFi', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Sim', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Processor Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'OS', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Built in Speaker', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Microphone', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Bluetooth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Battery Life', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'No of Cells', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Battery Capacity', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Height', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Width', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Depth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Warranty Labour', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'4',
		    'name'=>'Warranty Parts', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		



		//Seeder for Camera

		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'WiFi', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'NFC', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Bluetooth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Jpeg Format', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Raw Format', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Lens Frame Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Lens Range', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Model No.', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Filter Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Lens Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Body Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Battery Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Camera Model', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Sensor Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Sensor Size Format', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Sensor Crop Format', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Total Pixels', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Horizontal Percentage Coverage', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Vertical Percentage Coverage', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Effective magnification', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'LCD Size', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'LCD Resolution', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'AutoFocus Points', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Shutter Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Shutter Speed', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'card Slot', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Data Interface', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Video Interface', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Height', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Width', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Depth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Warranty labour', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'5',
		    'name'=>'Warranty Parts', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
				
		//seeder for speaker

		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'No. of Speakers', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Color', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Wireless', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Bluetooth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Height', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Width', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Depth', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Weight', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Warranty Labour', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Warranty Parts', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		DB::table('category_options')->insert([
			'category_id'=>'6',
		    'name'=>'Type', 
			'created_at' =>Carbon::now(), 
			'updated_at' =>Carbon::now() 
		]);
		


    }
}

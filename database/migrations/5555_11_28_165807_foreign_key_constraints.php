<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeyConstraints extends Migration
{
    //Foreign key constraints in case we need them.. like for making erd
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers');
            $table->foreign('category_id')->references('id')->on('categories');
        });

        Schema::table('product_options', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('category_option_id')->references('id')->on('category_options');
        });

        Schema::table('invoices', function (Blueprint $table) {
            

            $table->foreign('billing_address_id')->references('id')->on('billing_addresses');
            $table->foreign('shipping_address_id')->references('id')->on('shipping_addresses');
        });

        Schema::table('category_options', function (Blueprint $table) {
            
            
            $table->foreign('category_id')->references('id')->on('categories');
        });

        Schema::table('invoice_items', function (Blueprint $table) {
            

            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::table('carts', function (Blueprint $table) {
            
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::table('reviews', function (Blueprint $table) {
            

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
        });
        Schema::table('billing_addresses', function (Blueprint $table) {
           
            
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('shipping_addresses', function (Blueprint $table) {
            
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }
    */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('subtotal', 8, 2);
            $table->unsignedDecimal('tax_amount', 8, 2);
            $table->unsignedDecimal('shipping_amount', 8, 2);
            $table->unsignedInteger('card_no');
            $table->unsignedDecimal('total', 8, 2);
            $table->string('order_status');
            $table->string('payment_status');
            $table->unsignedInteger('billing_address_id');
            $table->unsignedInteger('shipping_address_id');
            $table->unsignedInteger('shipping_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}

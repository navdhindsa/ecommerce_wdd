<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        
        'name'=>$faker->name,
        'category_id'=>'1',
        'manufacturer_id'=>'1',
        'price'=>'25.5',
        'sale_price'=>'25.5',
        'overview'=>$faker->paragraph,
        'more_information'=>$faker->paragraph,
        'thumbnail'=>'default.jpg'
    ];
});
